window.chart = {}
window.chart.colors = {
    solid: {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)',
        black: 'rgb(50, 50, 50)',
    },
    opaque: {
        red: 'rgba(255, 99, 132, 0.2)',
        orange: 'rgba(255, 159, 64, 0.2)',
        yellow: 'rgba(255, 205, 86, 0.2)',
        green: 'rgba(75, 192, 192, 0.2)',
        blue: 'rgba(54, 162, 235, 0.2)',
        purple: 'rgba(153, 102, 255, 0.2)',
        grey: 'rgba(201, 203, 207, 0.2)',
        black: 'rgba(50, 50, 50, 0.2)',
    }
};

window.chart.tooltips = function (tooltip) {
    // Tooltip Element
    var tooltipEl = document.getElementById('chartjs-tooltip');

    if (!tooltipEl) {
        tooltipEl = document.createElement('div');
        tooltipEl.id = 'chartjs-tooltip';
        tooltipEl.innerHTML = '<table></table>';
        this._chart.canvas.parentNode.appendChild(tooltipEl);
    }

    // Hide if no tooltip
    if (tooltip.opacity === 0) {
        tooltipEl.style.opacity = 0;
        return;
    }

    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform');
    if (tooltip.yAlign) {
        tooltipEl.classList.add(tooltip.yAlign);
    } else {
        tooltipEl.classList.add('no-transform');
    }

    function getBody(bodyItem) {
        return bodyItem.lines;
    }

    // Set Text
    if (tooltip.body) {
        var titleLines = tooltip.title || [];
        var bodyLines = tooltip.body.map(getBody);

        var innerHtml = '<thead>';

        titleLines.reverse().forEach(function(title) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
        });
        innerHtml += '</thead><tbody>';

        bodyLines.reverse().forEach(function(body, i) {
            var colors = tooltip.labelColors[(bodyLines.length-1) - i];
            var style = 'background:' + colors.backgroundColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px';
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            innerHtml += '<tr><td>' + span + body + '</td></tr>';
        });
        innerHtml += '</tbody>';

        var tableRoot = tooltipEl.querySelector('table');
        tableRoot.innerHTML = innerHtml;
    }

    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft;

    // Display, position, and set styles for font
    tooltipEl.style.opacity = 1;
    tooltipEl.style.left = positionX + tooltip.caretX + 'px';
    tooltipEl.style.top = positionY + tooltip.caretY + 'px';
    tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
    tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
    tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
    tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
};

$('.ajax-button').click(function() {
	var post = {};
	var callback = null;
	if ("remove" in $(this).data()) {
		post["remove"] = $(this).data("remove");
		post["name"] = $(this).data("name");
		callback = function(elem) {
			$('#removeid-'+elem.data("name")+'-'+elem.data("removeid")).remove();
		}
	} else if ("removeid" in $(this).data()) {
		post["removeid"] = $(this).data("removeid");
		post["name"] = $(this).data("name");
		callback = function(elem) {
			$('#removeid-'+elem.data("name")+'-'+elem.data("removeid")).remove();
		}
	} else {
		var input = $('#'+$(this).data("for"));
		console.log(input);
		var value = input.val();
		if (input[0].type == "checkbox") {
			value = input[0].checked;
		}
		console.log(value);
		post[input[0].name] = value;
	}
	var button = $(this);
	var form = $(this).length > 0 ? $($(this)[0].form) : $();
	$.ajax({
        url: form[0].action,
        type: "post",
        data: post,
        success: function (data, status, xhr) {
			if (callback != null) {
				callback(button);
				if (data.message) {
					$.notify(data.message, {
						type: data.type,
						placement: {
							from: "top",
							align: "center"
						},
					});
				}
			} else {
				location.reload();
			}
        },
        error: function (xhr, status, error) {
            if (xhr.responseJSON && xhr.responseJSON.message) {
                $.notify(xhr.responseJSON.message, {
                    type: xhr.responseJSON.type,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                });
            }
        },
    });
})

$('.ajax-input').change(function() {
	var post = {};
	post["change"] = true;
	post[this.name] = $(this).val();
	if (this.type == "checkbox") {
		post[this.name] = this.checked;
	}
	var form = $(this).length > 0 ? $($(this)[0].form) : $();
	$.ajax({
        url: form[0].action,
        type: "post",
        data: post,
        success: function (data, status, xhr) {
            if (data.message) {
                $.notify(data.message, {
                    type: data.type,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                });
            }
        },
        error: function (xhr, status, error) {
            if (xhr.responseJSON && xhr.responseJSON.message) {
                $.notify(xhr.responseJSON.message, {
                    type: xhr.responseJSON.type,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                });
            }
        },
    });
});

$('.ajax').on('submit', function(evt){
    evt.preventDefault();
    $.ajax({
        url: $(this).action,
        type: "post",
        data: $(this).serializeArray(),
        success: function (data, status, xhr) {
            if (data.message) {
                $.notify(data.message, {
                    type: data.type,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                });
            }
            /*if (scallback) {
                scallback(status, data);
            }*/
        },
        error: function (xhr, status, error) {
            if (xhr.responseJSON && xhr.responseJSON.message) {
                $.notify(xhr.responseJSON.message, {
                    type: xhr.responseJSON.type,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                });
            }
            /*if (ecallback) {
                ecallback(status, error);
            }*/
        },
    });
})

function ajax(e, action, post, scallback, ecallback) {
    if (e != null) { e.preventDefault(); }
    post['action'] = action;
    $.ajax({
        url: "/ajax.php",
        type: 'POST',
        data: post,
        success: function (data, status, xhr) {
            if (data.message) {
                $.notify(data.message, {
                    type: data.type,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                });
            }
            if (scallback) {
                scallback(status, data);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.responseJSON && xhr.responseJSON.message) {
                $.notify(xhr.responseJSON.message, {
                    type: xhr.responseJSON.type,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                });
            }
            if (ecallback) {
                ecallback(status, error);
            }
        },
    });
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
        inp.value = x[currentFocus].getElementsByTagName("input")[0].value;
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}