<?php

// Based on https://github.com/xPaw/PHP-Source-Query

namespace NetworkManager\RCon;

require_once NM_CLASS_ROOT . "rcon.php";
require_once "source/bootstrap.php";

abstract class SourceRCon extends RCon
{
    protected $socket;

    public function connect()
    {
        $this->socket = new \xPaw\SourceQuery\SourceQuery();
        try
        {
            $this->socket->Connect($this->serverIP, $this->serverPort, $this->timeout);
            $this->socket->SetRconPassword($this->serverSecret);
            
            return true;
        }
        catch( Exception $e )
        {
            return false;
        }
    }

    public function disconnect()
    {
        $this->socket->Disconnect();
    }

    public function getStatus()
    {
        return $this->socket->Ping();
    }

    public function send($command)
    {
        try
        {
            return $this->socket->Rcon($command);
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function receive()
    {
        // Source rcon doesn't actively send data
        return false;
    }
}