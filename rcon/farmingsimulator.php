<?php

namespace NetworkManager\RCon;

require_once NM_CLASS_ROOT . "rcon.php";

// http://147.135.93.152:4242/feed/dedicated-server-stats.xml?code=4571b5421387131aca4e5bb400572c26

// quality: 60 75 90
// size: 256, 512, 1024, 2048
// http://147.135.93.152:4242/feed/dedicated-server-stats-map.jpg?code=4571b5421387131aca4e5bb400572c26&quality=90&size=1024

class FarmingSimulator extends RCon
{
    private $stats;
    private $time = 0;

    public function connect()
    {
        $this->collectStats();

        return true;
    }
    public function disconnect()
    {
        
    }

    public function getStatus()
    {
        return false;
    }

    private function collectStats()
    {
        if (!isset($this->stats))
        {
            $this->stats = FarmingSimulator::getServerStatsSimpleXML("http://".$this->serverIP.":".$this->serverPort."/feed/dedicated-server-stats.xml?code=".$this->serverSecret);
        }

        return $this;
    }

    public function getPlayers()
    {
        $this->collectStats();

        $players = array();
        foreach ($this->stats->Slots->Player as $player) {
            if ($player["isUsed"] == "true") {
                $players[] = array($player->__toString(), $player->__toString());
            }
        }
        return $players;
    }

    /**
	 * Copyright (c) 2008-2013 GIANTS Software GmbH, Confidential, All Rights Reserved.
	 * Copyright (c) 2003-2013 Christian Ammann and Stefan Geiger, Confidential, All Rights Reserved.
	 */

	private static function loadFileHTTPSocket($domain, $port, $path, $timeout) {
        $fp = fsockopen($domain, $port, $errno, $errstr, $timeout);

		if ($fp) {
			// Make request
			$out = "GET ". $path . " HTTP/1.0\r\n";
			$out .= "Host: ". $domain . "\r\n";
			$out .= "Connection: Close\r\n\r\n";
			fwrite($fp, $out);

			// Get response
			$resp = "";
			while (!feof($fp)) {
				$resp .= fgets($fp, 256);
			}
            fclose($fp);
			
			// Check status is 200
			if(preg_match("/HTTP\/1\.\d\s(\d+)/", $resp, $matches) && $matches[1] == 200) {   
				// Load xml as object
				$parts = explode("\r\n\r\n", $resp);
				$temp = "";
				for ($i=1;$i<count($parts);$i++) {
					$temp .= $parts[$i];
				}
				return $temp;
			}
		}
		return false;
	}

	private static function getServerStatsSimpleXML($url) {
		$urlParts = parse_url($url);

		$cacheFile = "farmingsimulator.cache";
		$cacheTimeout = 60*2;
		
		if(file_exists($cacheFile) && filemtime($cacheFile) > (time() - ($cacheTimeout) + rand(0, 10))) {
			$xmlStr = file_get_contents($cacheFile);
		} else {
			$xmlStr = FarmingSimulator::loadFileHTTPSocket($urlParts["host"], $urlParts["port"], $urlParts["path"] . "?" . $urlParts["query"], 4);
			
            if ($xmlStr)
            {
				$fp = fopen($cacheFile, "w");
				fwrite($fp, $xmlStr);
				fclose($fp);			
			}
        }

		return simplexml_load_string($xmlStr);
	}

	private static function getVehicleClass($category, $type) {
		if($category == 'vehicle') {
			return 'vehicle';
		} else if($category == 'harvester') {
			return 'harvester';
		} else if($category == 'tool') {
			return 'tool';
		} else if($category == 'trailer') {
			return 'trailer';
		} else if($category == 'tractors') {
			return 'vehicle';
		} else if($category == 'trucks') {
			return 'vehicle';
		} else if($category == 'wheelLoaders') {
			if($type == 'dynamicMountAttacherImplement') {
				return 'tool';
			} else if($type == 'shovel_animated') {
				return 'tool';
			} else if($type == 'shovel_dynamicMountAttacher') {
				return 'tool';
			} else {
				return 'vehicle';
			}
		} else if($category == 'teleLoaders') {
			if($type == 'dynamicMountAttacherImplement') {
				return 'tool';
			} else if($type == 'baleGrab') {
				return 'tool';
			} else if($type == 'shovel_dynamicMountAttacher') {
				return 'tool';
			} else if($type == 'shovel_animated') {
				return 'tool';
			} else {
				return 'vehicle';
			}
		} else if($category == 'skidSteers') {
			if($type == 'dynamicMountAttacherImplement') {
				return 'tool';
			} else if($type == 'shovel') {
				return 'tool';
			} else if($type == 'shovel_dynamicMountAttacher') {
				return 'tool';
			} else if($type == 'stumpCutter') {
				return 'tool';
			} else if($type == 'treeSaw') {
				return 'tool';
			} else {
				return 'vehicle';
			}
		} else if($category == 'cars') {
			return 'vehicle';
		} else if($category == 'harvesters') {
			return 'harvester';
		} else if($category == 'forageHarvesters') {
			if($type == 'attachableCombine') {
				return 'tool';
			} else {
				return 'harvester';
			}
		} else if($category == 'potatoHarvesting') {
			if($type == 'defoliator_animated') {
				return 'tool';
			} else {
				return 'harvester';
			}
		} else if($category == 'beetHarvesting') {
			if($type == 'defoliater_cutter_animated') {
				return 'tool';
			} else {
				return 'harvester';
			}
		} else if($category == 'frontLoaders') {
			if($type == 'wheelLoader') {
				return 'vehicle';
			} else {
				return 'tool';
			}
		} else if($category == 'forageHarvesterCutters') {
			return 'tool';
		} else if($category == 'cutters') {
			return 'tool';
		} else if($category == 'plows') {
			return 'tool';
		} else if($category == 'cultivators') {
			return 'tool';
		} else if($category == 'sowingMachines') {
			return 'tool';
		} else if($category == 'sprayers') {
			if($type == 'selfPropelledSprayer') {
				return 'vehicle';
			} else {
				return 'tool';
			}
		} else if($category == 'fertilizerSpreaders') {
			return 'tool';
		} else if($category == 'weeders') {
			return 'tool';
		} else if($category == 'mowers') {
			return 'tool';
		} else if($category == 'tedders') {
			return 'tool';
		} else if($category == 'windrowers') {
			return 'tool';
		} else if($category == 'baling') {
			if($type == 'transportTrailer') {
				return 'trailer';
			} else if($type == 'baleLoader') {
				return 'trailer';
			} else if($type == 'baler') {
				return 'trailer';
			} else {
				return 'tool';
			}
		} else if($category == 'chainsaws') {
			return 'tool';
		} else if($category == 'wood') {
			if($type == 'transportTrailer') {
				return 'trailer';
			} else if($type == 'forwarderTrailer_steerable') {
				return 'trailer';
			} else if($type == 'woodCrusherTrailer') {
				return 'trailer';
			} else if($type == 'combine_animated') {
				return 'vehicle';
			} else if($type == 'forwarder') {
				return 'vehicle';
			} else if($type == 'woodHarvester') {
				return 'vehicle';
			} else {
				return 'tool';
			}
		} else if($category == 'animals') {
			if($type == 'selfPropelledMixerWagon') {
				return 'vehicle';
			} else {
				return 'trailer';
			}
		} else if($category == 'leveler') {
			return 'tool';
		} else if($category == 'misc') {
			if($type == 'fuelTrailer') {
				return 'trailer';
			} else {
				return 'tool';
			}
		} else if($category == 'dollys') {
			return 'trailer';
		} else if($category == 'weights') {
			return 'tool';
		} else if($category == 'pallets') {
			return 'tool';
		} else if($category == 'belts') {
			return 'tool';
		} else if($category == 'placeables') {
			return 'tool';
		} else if($category == 'tippers') {
			return 'trailer';
		} else if($category == 'augerWagons') {
			return 'trailer';
		} else if($category == 'slurryTanks') {
			if($type == 'manureBarrelCultivator') {
				return 'tool';
			} else {
				return 'trailer';
			}
		} else if($category == 'manureSpreaders') {
			return 'trailer';
		} else if($category == 'loaderWagons') {
			return 'trailer';
		} else if($category == 'lowloaders') {
			return 'trailer';
		} else if($category == 'cutterTrailers') {
			return 'trailer';
		} else {
			return 'tool';
		}
	}
}