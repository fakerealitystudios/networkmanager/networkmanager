<?php

namespace NetworkManager\RCon;

include "sourcercon.php";

class Rust extends SourceRCon
{
    public function getLinkType()
    {
        // Rust is only available on the steam platform, and uses steamids
        return "steamid64";
    }

    public function getPlayers()
    {
        //var_dump($this->send("playerlist"));
        return false;
    }

    public function kickPlayer($player, $reason = "NetworkManager has deemed you unworthy")
    {
        return false;
    }
    public function banPlayer($player, $reason = "NetworkManager has deemed you unworthy")
    {
        return false;
    }
}