<?php

$config = array();

/**
 * Database configuration
 *
 * Please see the Medoo page for database options
 * https://medoo.in/api/new
 */

$config['database'] = array();
// If you enable this, then you can view the queries from the bottom left in the footer. Helpful for debugging.
$config['database']['logging'] = {database[logging]|false};

$config['database']['database_type'] = '{database[database_type]}';
$config['database']['prefix'] = '{database[prefix]}';
$config['database']['server'] = '{database[server]}';
$config['database']['port'] = '{database[port]}';
$config['database']['database_name'] = '{database[database_name]}';
$config['database']['username'] = '{database[username]}';
$config['database']['password'] = '{database[password]}';

// Should we be sending stats to https://api.networkmanager.center
$config['nm_api'] = array();
$config['nm_api']['statistics'] = true;

$config['registration'] = array();
$config['registration']['enabled'] = true;

$config['cdn_url'] = "{cdn_url|/}";