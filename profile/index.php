<?php

$SESSION_BLOCKING = true;
require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

// Just for ease...
$member = $NM->getAuthedMember();

$auth->collect2fa();

if (!isset($auth->twofa['2step'])) {
    if (isset($_POST['2step'])) {
        $googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();
        if ($googleAuthenticator->authenticate($_SESSION['2step'], $_POST['2step'])) {
            $NM->db->insert("member2step", array(
                "uid" => $member->getId(),
                "secret" => $_SESSION['2step'],
            ));
            $_alerts[] = array(
                "text" => "Authenticator app enabled",
            );
            $auth->twofa['2step'] = $_SESSION['2step'];
            unset($_SESSION['2step']);
        } else {
            $_alerts[] = array(
                "type" => "danger",
                "text" => "Failed to verify authenticator app, verify your app and time and try the new secret!",
            );
        }
    } else {
        $secretFactory = new \Dolondro\GoogleAuthenticator\SecretFactory();
        $secret = $secretFactory->create(str_replace(":", "_", NM_URL_HOST), $member->getDisplayName());
        $_SESSION['2step'] = $secret->getSecretKey();

        $qrImageGenerator = new \Dolondro\GoogleAuthenticator\QrImageGenerator\EndroidQrImageGenerator();
    }
}

// Now we can close the session and continue on our day
session_write_close();

if (isset($_POST['type'])) {
    $db = \NetworkManager\Database::getInstance();
    $row = $db->get("memberPending", array("id", "token"), array(
        "uid" => $_POST['nmid'],
        "type" => $_POST['type'],
        "linkToken" => $_POST['token'],
        "time[>]" => (time() - 300),
    ));
    if ($row) {
        $db->delete("memberPending", array(
            "id" => $row['id'],
        ));
        $uid = \NetworkManager\MemberManager::getNMID($_POST['type'], $row['token']);
        if ($uid) {
            // Do more work, check how old this account and that one. If one is less then a month old, we can allow a merge to the eldest account.
            // Ask the user to confirm this task
            if ($auth->merge($uid)) {
                $_alerts[] = array(
                    'text' => "Your accounts have now been merged!",
                );
            } else {
                $_alerts[] = array(
                    'type' => 'danger',
                    'text' => "Failed to perform the merge, ask an admin to merge them.",
                );
            }
        } else {
            $_alerts[] = array(
                'text' => "Account has been linked.",
            );
            // Is not already linked, create link.
            \NetworkManager\MemberManager::addToken($member->getId(), $_POST['type'], $row['token']);
        }
    } else {
        echo "Bad token";
    }
}

if (isset($_GET['validate'])) {
    if ($member->emailValidated()) {
        $_alerts[] = array(
            'type' => 'warning',
            'text' => "Your email has already been validated!",
        );
    } else if ($member->validateEmail($_GET['validate'])) {
        // popup saying successful
        $_alerts[] = array(
            'text' => "Thank you for validating your email!",
        );
    } else {
        $_alerts[] = array(
            'type' => 'danger',
            'text' => "Failed to validate your email, is that the proper token?",
        );
    }
}

$group = isset($_GET['group']) ? $_GET['group'] : "personal";

if (isset($_POST['group'])) {
    if ($group == "personal") {
        // Only set username if it wasn't set before...
        if ($member->getUsername() == null && $_POST['username'] != "") {
            if ($member->setUsername($_POST['username'])) {
                $_alerts[] = array(
                    'text' => "Assigned your new username",
                );
            } else {
                $_alerts[] = array(
                    'text' => "Invalid username or username already in use!",
                    'type' => 'danger',
                );
            }
        }
        if ($_POST['displayName'] != $member->getDisplayName()) {
            if ($member->setDisplayName($_POST['displayName'])) {
                $_alerts[] = array(
                    'text' => "Your new display name has been set!",
                );
            } else {
                $_alerts[] = array(
                    'text' => "Invalid display name!",
                    'type' => 'danger',
                );
            }
        }
        if ($member->getEmail(true) != $_POST['email']) {
            if ($member->setEmail($_POST['email'])) {
                $_alerts[] = array(
                    'text' => "We have sent you a validation email!",
                );
            } else {
                $_alerts[] = array(
                    'text' => "Invalid email format!",
                    'type' => 'danger',
                );
            }
		}
		
		if ($member->isSetup()) {
			$_alerts = array();
			$_alerts[] = array(
				'text' => "Your account is now setup!"
			);
		}

        if (isset($_FILES['avatar']) && $_FILES['avatar']['tmp_name'] != "") {
            $image = new Bulletproof\Image($_FILES['avatar']);
            $image->setSize(0, 100000);
            $image->setMime(array('jpeg', 'jpg', 'gif', 'png'));
            $image->setDimension(512, 512, 256, 256);
            $image->setLocation(NM_ROOT . 'uploads/avatars', 755);
            if ($image->upload()) {
                $url = NM_URL . 'uploads/avatars/' . $image->getName() . '.' . $image->getMime();
                $member->setAvatar($url);
                $NM->db->insert("avatars", array("avatar" => $image->getFullPath()));
                $_alerts[] = array(
                    'text' => "Uploaded your new avatar!",
                );
            } else {
                // popup saying error
                $_alerts[] = array(
                    'text' => $image->getError(),
                    'type' => 'danger',
                );
            }
        }
    } elseif ($_POST['group'] == "security") {
        $auth->changePassword($_POST['password'], $_POST['newpassword']);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Profile - NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<!-- END CSS for this page -->
	</head>

	<body class="adminbody">

		<div id="main">

			<?php include NM_ROOT . "includes/header.php";?>
			<?php include NM_ROOT . "includes/sidebar.php";?>

			<div class="content-page">

				<!-- Start content -->
				<div class="content">

					<div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="breadcrumb-holder">
                                    <h1 class="main-title float-left">Profile</h1>
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item">Home</li>
                                        <li class="breadcrumb-item active">Profile</li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

						<div class="row">
							<div class="col-lg-3 col-md-12 col-sm-3 sidebar-section hidden-xs">
								<div class="container nav-sidebar">
									<div class="nav flex-column">
										<a class="nav-link <?php echo ($group == "personal") ? "active" : ""; ?>" href="?group=personal"><i class="fa-solid fa-user" aria-hidden="true"></i> Personal Info</a>
										<a class="nav-link <?php echo ($group == "security") ? "active" : ""; ?>" href="?group=security"><i class="fa-solid fa-key" aria-hidden="true"></i> Password &amp; Security</a>
										<a class="nav-link <?php echo ($group == "devices") ? "active" : ""; ?>" href="?group=devices"><i class="fa-solid fa-server" aria-hidden="true"></i> Devices</a>
										<a class="nav-link <?php echo ($group == "links") ? "active" : ""; ?>" href="?group=links"><i class="fa-solid fa-share-all" aria-hidden="true"></i> Connected Accounts</a>
									</div>
								</div>
							</div>
							<div class="col-6">
								<div class="container">
									<?php if ($group == "personal") {?>
                                    <form class="row" action="<?php echo NM_URL; ?>profile/?group=<?php echo $group; ?>" method="post" enctype="multipart/form-data">
										<input type="hidden" name="group" value="<?php echo $group; ?>">
										<div class="col-12"><h2>Personal Details</h2></div>
										<div class="col-12">
											<div class="form-group">
												<label for="username">Username<span class="required">*</span></label>
												<p class="description" style="margin-bottom: 5px;">Usernames can only be set once, make sure it's the name you want. Must be only letters, numbers, and underscore.</p>
												<input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $member->getUsername(); ?>" pattern="^\w{3,32}$" <?php echo ($member->getUsername() != "") ? 'disabled="disabled"' : ""; ?> required="required">
											</div>
											<div class="form-group">
												<label for="displayName">Display Name<span class="required">*</span></label>
												<input type="text" class="form-control" name="displayName" placeholder="Display Name" value="<?php echo $member->getDisplayName(); ?>" pattern="^.+$" required="required">
											</div>
											<div class="form-group">
												<label for="email">Email<span class="required">*</span></label>
												<?php if (!$member->emailValidated() && $member->getEmail(true) != "") {?><p class="description" style="color:#e50000;">Email is awaiting validation</p><?php }?>
												<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $member->getEmail(true); ?>">
											</div>
											<div class="form-group" style="padding:15px;background-color: #fff;background-clip: padding-box;border: 1px solid #ced4da;border-radius: .25rem;">
												<label for="avatar">Avatar</label>
												<br>
												<img style="height:128px;"src="<?php echo $member->getAvatar(); ?>">
												<input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>
												<input class="float-right" type="file" accept="image/*" class="form-control" name="avatar" placeholder="Avatar">
											</div>
										</div>
										<div class="col-12"><input type="submit" class="btn btn-primary float-right" vale="Submit"></div>
                                    </form>
									<?php } elseif ($group == "security") {?>
                                    <form class="row" action="<?php echo NM_URL; ?>profile/?group=<?php echo $group; ?>" method="post">
										<input type="hidden" name="group" value="<?php echo $group; ?>">
										<div class="col-12">
											<h2>Change your password</h2>
											<p class="description">Make sure to be safe, don't store your password on public computers, and don't reuse passwords for multiple applications.</p>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label for="password">Current Password<span class="required">*</span></label>
												<input type="password" class="form-control" name="password" placeholder="Password">
											</div>
											<div class="form-group">
												<label for="newpassword">New Password<span class="required">*</span></label>
												<input type="password" class="form-control" name="newpassword" placeholder="Password" required="required" autocomplete="off">
											</div>
											<div class="form-group">
												<label for="verifypassword">Verify Password<span class="required">*</span></label>
												<input type="password" class="form-control" name="verifypassword" placeholder="Password" required="required" autocomplete="off">
											</div>
										</div>
										<div class="col-6">
											<h4>Password Requirements</h4>
											<p class="description">Password must have...</p>
										</div>
										<div class="col-12"><button type="submit" class="btn btn-primary float-right">Save Changes</button></div>
									</form>
									<hr/>
									<div>
										<div class="col-12">
											<h2>Two-Step Authentication</h2>
											<p class="description">Create an additional means of securing your account</p>
										</div>
										<div class="row" style="margin-top:50px;">
											<div class="col-5">
												<h4>Authenticator App</h4>
												<p class="description">An additional means of security using apps such as <a href="https://support.google.com/accounts/answer/1066447?co=GENIE.Platform%3DAndroid&hl=en">Google Authenticator</a> and <a href="https://authy.com/">Authy</a>.</p>
												<?php if (!isset($auth->twofa['2step'])) {?>
												<button class="btn btn-primary" data-toggle="modal" data-target="#authenticatorappModel">Enable</button>
												<?php } else {?>
												<button class="btn btn-danger" data-toggle="modal" data-target="#authenticatorappModel">Disable</button>
												<?php }?>
											</div>
										</div>
									</div>
									<?php } elseif ($group == "links") {?>
									<div class="col-12"><h2>Linked Accounts</h2></div>
									<div class="col-12">
										<div class="row container text-center">
                                            <?php // TODO: Let's fix the entire authlinks/linkedaccounts system
                                            foreach ($auth->getAuthLinks() as $method) {
                                                ?>
                                                <div class="col-4 container pt-3 pb-3">
                                                    <div class="border rounded pt-3 pb-3 mx-auto">
                                                        <h5><?php echo $method->getTitle(); ?></h5>
                                                        <div class="clearfix"></div>
														<?php if (isset($member->getLinkedAccounts()[$method->getType()])) {?>
                                                        <a href="#" class="btn btn-link-custom disabled rounded">Linked</a>
                                                        <?php } else {?>
                                                        <a href="<?php echo $method->generateUrl(NM_URL . 'login.php'); ?>" class="btn btn-link-custom rounded">Link</a>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
											<div class="col-4 container pt-3 pb-3">
												<div class="border rounded pt-3 pb-3 mx-auto">
													<h5>Minecraft</h5>
													<div class="clearfix"></div>
													<?php if (isset($member->getLinkedAccounts()['minecraft'])) {?>
													<a href="#" class="btn btn-link-custom disabled rounded">Linked</a>
													<?php } else {?>
                                                    <form action="<?php echo NM_URL; ?>profile/?group=<?php echo $group; ?>" method="post">
                                                        <input type="hidden" name="group" value="<?php echo $group; ?>">
                                                        <input type="hidden" name="type" value="minecraft">
                                                        <input type="hidden" name="nmid" value="<?php echo $member->getId(); ?>">
                                                        <div class="col-12 mt-1 mb-2"><input type="text" class="form-control" style="font-size: .75rem;" name="token" placeholder="Token" value="<?php echo ((isset($_GET["type"]) && $_GET["type"] == "minecraft") ? $_GET["token"] : ""); ?>" required="required"></div>
                                                        <div class="col-12"><button type="submit" class="btn btn-link-custom rounded">Link</button></div>
                                                    </form>
													<?php }?>
												</div>
											</div>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
							<div class="col-3"></div>
						</div>

					</div>
					<!-- END container-fluid -->

				</div>
				<!-- END content -->

			</div>
			<!-- END content-page -->

			<?php include NM_ROOT . "includes/footer.php";?>

			<!-- Authenticator App -->
			<?php if (!isset($auth->twofa['2step'])) {?>
			<div class="modal fade" id="authenticatorappModel" tabindex="-1" role="dialog" aria-labelledby="authenticatorappModelLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content" style="max-width: 475;">
						<div class="modal-header" style="height:175px;background: no-repeat url(https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2018/08/28/105420789-1535480940323google-titan-key.720x405.jpg?v=1535480976) -100px -180px">
							<button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<h5 class="modal-title" id="authenticatorappModelLabel">Add Authenticator App</h5>
							<div class="row">
								<div class="col-sm-12 col-md-6">
									<img src="<?php echo $qrImageGenerator->generateUri($secret); ?>">
								</div>
								<div class="col-sm-12 col-md-6">
									<p class="description">Secret Key</p>
									<a id="2stepsecret" href="#" onclick="replacetoken()">Reveal</a>
								</div>
							</div>
							<form class="input-group" role="form" method="post" action="<?php echo NM_URL; ?>profile/?group=security">
								<input type="text" name="2step" class="form-control" style="margin:0px;" placeholder="Code">
								<div class="input-group-append">
									<button class="btn btn-outline-secondary" type="submit">Submit</button>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<?php } else {?>
				<div class="modal fade" id="authenticatorappModel" tabindex="-1" role="dialog" aria-labelledby="authenticatorappModelLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content" style="max-width: 475;">
						<div class="modal-header" style="height:175px;background: no-repeat url(https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2018/08/28/105420789-1535480940323google-titan-key.720x405.jpg?v=1535480976) -100px -180px">
							<button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<h5 class="modal-title" id="authenticatorappModelLabel">Remove Authenticator App</h5>
							<p>Are you sure you want to remove your authenticator app?</p>
							<form class="input-group" role="form" method="post" action="<?php echo NM_URL; ?>profile/?group=security">
								<button class="btn btn-danger" name="2step" value="disable" type="submit">Yes</button>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<?php }?>

		</div>
		<!-- END main -->

		<?php include NM_ROOT . "includes/script.php";?>

		<script>
			function replacetoken() {
				var hidden = document.getElementById("2stepsecret");
				var token = document.createElement("p");
				hidden.replaceWith(token);
				token.innerHTML = "<?php echo $_SESSION['2step']; ?>";
			}
		</script>

		<!-- BEGIN Java Script for this page -->
		<!-- END Java Script for this page -->

	</body>
</html>
