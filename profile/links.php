<?php

$SESSION_BLOCKING = true;
require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

// Just for ease...
$member = $NM->getAuthedMember();
$links = array();
foreach($member->getLinkedAccounts() as $tokenset) {
    $links[$tokenset['type']] = isset($links[$tokenset['type']]) ? $links[$tokenset['type']] : array();
    $links[$tokenset['type']][] = $tokenset['token'];
}

$data = array();
$data['title'] = "Links | ".$member->getDisplayName()." | NetworkManager";
$data['page_name'] = "Links";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => $member->getDisplayName()
    ),
    array(
		'name' => "Links"
	),
);

$data['body'] = "";
$data['body'] .= '<div class="row">';
$data['body'] .= '<div class="col-lg-3 col-md-12 col-sm-12 sidebar-section hidden-xs">
    <div class="container nav-sidebar">
        <div class="nav flex-column">
            <a class="nav-link" href="?group=personal"><i class="fa-solid fa-user" aria-hidden="true"></i> Personal Info</a>
            <a class="nav-link" href="?group=security"><i class="fa-solid fa-key" aria-hidden="true"></i> Password &amp; Security</a>
            <a class="nav-link" href="?group=devices"><i class="fa-solid fa-server" aria-hidden="true"></i> Devices</a>
            <a class="nav-link active" href="?group=links"><i class="fa-solid fa-share" aria-hidden="true"></i> Connected Accounts</a>
        </div>
    </div>
</div>';
$data['body'] .= '<div class="col-6">';
$data['body'] .= '<div class="container">';
$data['body'] .= '<div class="col-12"><h2>Linked Accounts</h2></div>';


$data['body'] .= '<div class="col-12">';
$data['body'] .= '<div class="row container text-center">';
foreach ($auth->getAuthLinks() as $method) {
$data['body'] .= '<div class="col-4 container pt-3 pb-3"><div class="border rounded pt-3 pb-3 mx-auto">';

$data['body'] .= '<h5>'.$method->getTitle().'</h5>';
$data['body'] .= '<div class="clearfix"></div>';
if (isset($links[$method->getType()])) {
    foreach($links[$method->getType()] as $token) {
        $data['body'] .= '<a href="'.$method->getProfile($token).'" class="">'.$token.'</a>';
    }
    $data['body'] .= '<div class="clearfix pt-2"></div>';
}
$data['body'] .= '<a href="'.$method->generateUrl(NM_URL . 'login.php').'" class="btn btn-link-custom rounded">Link</a>';

$data['body'] .= '</div></div>';
}
$data['body'] .= '</div>';
$data['body'] .= '</div>';


$data['body'] .= '</div>';
$data['body'] .= '</div>';
$data['body'] .= '</div>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);