<?php
require "includes/networkmanager.php";
require "includes/classes/logs.php";
$auth = \NetworkManager\Authentication::getInstance();
/*if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}
*/

// All the current params
if (isset($_GET["sid"])) {
    $server = $NM->getServer($_GET["sid"]);
    if (!$server->isValid()) {

    } else {
        $sid = $server->getId();
    }
}
if (isset($_GET["ssid"])) {
    $ssid = $_GET["ssid"];
}
if (isset($_GET["usid"])) {
    $usid = $_GET["usid"];
}
if (isset($_GET["page"])) {
    $page = $_GET["page"];
} else {
    $page = 1;
}
$limit = isset($_GET['limit']) ? $_GET['limit'] : 25;

$url_params = $_GET;
unset($url_params['page']); // changes enough to be annoying to add?
if (count($url_params) == 0) { // need something, we assume we always have atleast one param down the line for the url param
    $url_params['limit'] = $limit;
}
$url = NM_URL . 'bans.php?' . http_build_query($url_params);

// All the current params
$url_params = $_GET;
unset($url_params['page']); // changes enough to be annoying to add?
if (count($url_params) == 0) { // need something, we assume we always have atleast one param down the line for the url param
    $url_params['limit'] = $limit;
}
$url = NM_URL . 'bans.php?' . http_build_query($url_params);

// Begin logs lookup
$condition = array();
if (isset($server)) {
    $condition["realm[~]"] = $server->getId().".%";
}
if (isset($ssid)) {
    $condition['ssid'] = $ssid;
}


$data = array();

$data['title'] = "Bans | NetworkManager";
$data['page_name'] = "Bans";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Bans"
	),
);

$data['url'] = $url;

$data['bans_count'] = $NM->db->count("memberBans", $condition);
$pages = ceil($data['bans_count'] / $limit);
if ($page > $pages) {
    $page = $pages;
}
$condition["LIMIT"] = $limit;
if ($page > 1) {
    $condition["LIMIT"] = [$limit * ($page - 1), $limit];
}

$condition["ORDER"] = array("id" => "DESC");
$rows = $NM->db->select("memberBans", "*", $condition);
$data['bans'] = array();
foreach($rows as $row) {
    $user = $NM->getMember($row["uid"]);
    $admin = $NM->getMember($row["adminuid"]);

    $row['user'] = $user;
    $row['admin'] = $admin;
    $row['date'] = date('Y-m-d H:i:s', $row['banTime']);
    $row['expires'] = ($row["length"] == "0") ? "Never" : \NetworkManager\Common::timeTillHumanReadable($row["unbanTime"]);
    $row['length'] = ($row["length"] == "0") ? "Permanent" : \NetworkManager\Common::timeTillHumanReadable($row["banTime"], $row["unbanTime"], true);

    $data['bans'][] = $row;
}

$data['url_params'] = $url_params;
$data['page'] = $page;
$data['page_next'] = $page + 1;
$data['page_prev'] = $page - 1;
if($pages <= 1) {
    $data['pages'] = array(1);
} elseif($pages == 2) {
    $data['pages'] = array(1,2);
} elseif($pages == 3) {
    $data['pages'] = array(1,2,3);
} elseif($pages == 4) {
    $data['pages'] = array(1,2,3,4);
} elseif($pages == 5) {
    $data['pages'] = array(1,2,3,4,5);
} elseif ($page > ($pages - 3)) {
    $data['pages'] = array(1,-1,$pages-4,$pages-3,$pages-2,$pages-1,$pages);
} elseif($page < 4) {
    $data['pages'] = array(1,2,3,4,5,-1,$pages);
} else {
    $data['pages'] = array(1,-1,$page-2,$page-1,$page,$page+1,$page+2,-1,$pages);
}

$data['body'] = $NM->template->build(NM_TEMPLATE_ROOT . 'bans.html', $data);

$data['style'] = '';
$data['javascript'] = '';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);