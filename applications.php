<?php
require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$data = array();
$data['title'] = "Applications | NetworkManager";
$data['page_name'] = "Applications";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Applications"
	),
);

$data['body'] = "";
if ($NM->getAuthedMember()->hasPermission("nm.applications.create")) {
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'apply.html', $data);
}
if ($NM->getAuthedMember()->hasPermission("nm.applications.view")) {
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'applications.html', $data);
}

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);