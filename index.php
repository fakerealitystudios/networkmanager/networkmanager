<?php

require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$data = array();
$data['title'] = "Dashboard | NetworkManager";
$data['page_name'] = "Dashboard";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Dashboard"
	),
);

$data['body'] = "";

$data['name'] = "Users Online";
$data['player_count'] = $NM->db->count("memberSessions", array("endTime" => "-1"));
$data['player_count'] = is_numeric($data['player_count']) ? $data['player_count'] : 0;
$data['member_count'] = $NM->db->count("members");
$data['member_count'] = is_numeric($data['member_count']) ? $data['member_count'] : 0;
//$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'dashboard_users.html', $subdata);

$data['modules'] = array();
$downloads = \NetworkManager\API::get("/core/downloads");
if (isset($downloads['data'])) {
	$downloads = $downloads['data'];
	foreach($downloads as $v) {
		$data['modules'][] = $v;
	}
}

$data['servers'] = array();
foreach ($NM->getServers() as $server) {
	if ($server->getGame() == "web") {continue;}
	$players = $NM->db->count("memberSessions", array("sid" => $server->getId(), "endTime" => "-1"));
	$row = $NM->db->get("serverSessions", "id", array("sid" => $server->getId(), "endTime" => -1, "LIMIT" => 1));
	$data['servers'][] = array(
		"sid" => $server->getId(),
		"name" => $server->getName(),
		"game" => $server->getGame(),
		"online" => $row,
		"player_count" => $players,
	);
}
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'dashboard.html', $data);

$data['javascript'] = '<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>

<script>
	$(document).ready(function() {
		// counter-up
		$(\'.counter\').counterUp({
			delay: 10,
			time: 600
		});
	} );
</script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);