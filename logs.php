<?php
require "includes/networkmanager.php";
require NM_CLASS_ROOT . "logs.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.logs")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

//require NM_CLASS_ROOT . "server.php";
if (isset($_GET["sid"])) {
    $server = \NetworkManager\ServerManager::getInstance()->get($_GET["sid"]);
    if (!$server->isValid()) {

    }
}
if (isset($_GET["ssid"])) {
    $ssid = $_GET["ssid"];
}
if (isset($_GET["usid"])) {
    $usid = $_GET["usid"];
}
if (isset($_GET["page"])) {
    $page = $_GET["page"];
} else {
    $page = 1;
}
$limit = isset($_GET['limit']) ? $_GET['limit'] : 25;

if (isset($_GET['log_event']) && $_GET['log_event'] == "any") {
    unset($_GET['log_event']);
}

if (isset($_GET['log_search']) && $_GET['log_search'] == "") {
    unset($_GET['log_search']);
}

$logEvents = $NM->db->distinct()->select("logs", "event");
sort($logEvents);
if (!isset($_GET['log_event'])) {
    $_GET['log_event'] = array();
    foreach ($logEvents as $row) {
        if (strpos($row, "http") === false && strpos($row, "runstring") === false) {
            $_GET['log_event'][] = $row;
        }

    }
}

// Begin logs lookup
$condition = array();
if (isset($server) && $server->IsValid()) {
    $condition['sid'] = $server->getId();
}
if (isset($ssid)) {
    $condition['ssid'] = $ssid;
}
if (isset($_GET['log_event']) && is_array($_GET['log_event']) && count($_GET['log_event']) > 0) {
    $condition['event'] = $_GET['log_event'];
}

// If using both log search and uid, need to perform an OR
if (isset($_GET['log_search'])) {
    $condition['ld.value[~]'] = $_GET['log_search'];
}

if (isset($usid)) {
    $condition["ld.type"] = "USID";
    $condition["ld.value"] = $usid;
}

$lcondition = $condition;
$pages = ceil(\NetworkManager\Logs::fullCount($lcondition) / $limit);
if ($page > $pages) {
    $page = $pages;
}
$condition["ORDER"] = array("id" => "DESC");
$condition["LIMIT"] = $limit;
if ($page > 1) {
    $condition["LIMIT"] = [$limit * ($page - 1), $limit];
}

$data = array();

$data['title'] = "Logs | NetworkManager";
$data['page_name'] = "Logs";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Logs"
	),
);

$logs = new \NetworkManager\Logs($condition);
$data['logs'] = array();
foreach($logs->getRows() as $row) {
    $data['logs'][] = array(
        "id" => $row['id'],
        "event" => $row['type'] . "." . $row["event"] . (isset($row['subevent']) ? "." . $row['subevent'] : ""),
        "log" => $logs->getReadableLog($row),
        "time" => '<span class="js-time">'.$row['time'].'</span>',
        "sid" => $row["sid"],
        "server" => $NM->getServer($row["sid"])->getName(),
        "details" => $row['details'],
    );
}
$data['logEvents'] = $logEvents;

require NM_INCL_ROOT . "pagination.php";

$data['body'] = $NM->template->build(NM_TEMPLATE_ROOT . 'logs.html', $data);

$data['style'] = '<link rel="stylesheet" type="text/css" href="'.NM_CDN.'assets/css/bootstrap-multiselect.min.css"/>';
$data['javascript'] = '<script src="'.NM_CDN.'assets/js/bootstrap-multiselect.min.js"></script>
<script>
$(function () {
    $(\'[data-toggle="tooltip"]\').tooltip();
    $(\'[data-toggle="multiselect"]\').multiselect();

    $(".js-time").each(function(){
        $(this).html(new Date(parseInt($(this).html())*1000).toLocaleString());
    });
});

function show_hide_row(row)
{
    $("#"+row).toggle();
};
</script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);