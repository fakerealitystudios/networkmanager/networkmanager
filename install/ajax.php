<?php

define('NM_INSTALL', 1);

require_once "../includes/networkmanager.php";

if (file_exists("lock")) {
    echo "lock file exists! Remove it from install folder to continue.";
    exit();
}

header('Content-Type: application/json');

$return = array();

if (!isset($_POST['action'])) {
    header("HTTP/1.0 400 Bad Request");
    $return["type"] = "danger";
    $return["message"] = "Invalid action provided!";
    $return["errorcode"] = 1001;
    exit();
} elseif ($_POST['action'] == "database") {
    require_once NM_CLASS_ROOT . 'medoo.php';

    try {
        $db = new \Medoo\Medoo([
            "database_type" => $_POST['database']['database_type'],
            "server" => $_POST['database']['server'],
            "port" => $_POST['database']['port'],
            "database_name" => $_POST['database']['database_name'],
            "username" => $_POST['database']['username'],
            "password" => $_POST['database']['password'],
        ]);

        $res = $db->query("SELECT 1");
        if ($res && $res->fetch()) {
            $return["valid"] = true;
            $return["message"] = "Database connected!";
        } else {
            header("HTTP/1.0 400 Bad Request");
            $return["type"] = "danger";
            $return["message"] = "Invalid database connection!";
            $return["errorcode"] = 1002;
        }
    } catch (PDOException $e) {
        //nope
        header("HTTP/1.0 400 Bad Request");
        $return["type"] = "danger";
        $return["message"] = "Invalid database connection!";
        $return["errorcode"] = 1002;
    }
}

echo json_encode($return);
