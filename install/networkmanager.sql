CREATE TABLE IF NOT EXISTS `apiKeys` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `secret` char(80) NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `secret` (`secret`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `badges` (
  `badge` char(255) NOT NULL,
  `stat` char(255) NOT NULL,
  `value` int(32) NOT NULL,
  UNIQUE KEY `badge` (`badge`),
  KEY `stat` (`stat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `geolocations` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `country` char(2) NOT NULL,
  `province` char(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang` char(3) NOT NULL,
  `event` char(255) NOT NULL,
  `args` int(11) DEFAULT NULL,
  `format` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lang` (`lang`),
  KEY `event` (`event`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `logDetails` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lid` int(32) UNSIGNED NOT NULL,
  `type` enum('NMID','USID','ENTITY','NPC','WEAPON','WORLD','TABLE','STRING','INTEGER','FLOAT','TOKENID') NOT NULL,
  `argid` int(8) UNSIGNED NOT NULL,
  `vid` char(32) DEFAULT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(32) UNSIGNED NOT NULL,
  `ssid` int(32) UNSIGNED DEFAULT NULL,
  `type` enum('SERVER','PLAYER') NOT NULL,
  `event` char(32) NOT NULL,
  `subevent` char(32) DEFAULT NULL,
  `time` bigint(15) NOT NULL,
  `args` int(8) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sid` (`sid`),
  KEY `ssid` (`ssid`),
  KEY `event` (`type`),
  KEY `type` (`event`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `member2step` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `secret` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberAliases` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberAlts` (
  `uid` int(32) UNSIGNED NOT NULL,
  `otheruid` int(32) UNSIGNED NOT NULL,
  `reason` text NOT NULL,
  KEY `uid` (`uid`),
  KEY `otheruid` (`otheruid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberBadges` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `badge` char(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `badge` (`badge`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberBans` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `usid` int(32) UNSIGNED DEFAULT NULL,
  `adminuid` int(32) UNSIGNED DEFAULT NULL,
  `adminusid` int(32) UNSIGNED DEFAULT NULL,
  `banTime` bigint(15) NOT NULL,
  `unbanTime` bigint(15) NOT NULL,
  `length` bigint(15) NOT NULL,
  `reason` text NOT NULL,
  `realm` char(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `adminuid` (`adminuid`),
  KEY `usid` (`usid`),
  KEY `adminusid` (`adminusid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberData` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `vid` varchar(500) NOT NULL,
  `value` text NOT NULL,
  `realm` char(255) NOT NULL DEFAULT '*',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_vid` (`uid`,`vid`),
  KEY `realm` (`realm`),
  KEY `uid` (`uid`),
  KEY `vid` (`vid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberLinks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `type` char(100) NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_token` (`type`,`token`),
  KEY `type` (`type`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberNotifications` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `type` enum('INFO','WARNING','ERROR','') NOT NULL,
  `link` text,
  `header` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `time` bigint(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberPending` (
  `id` varchar(36) NOT NULL,
  `type` char(100) NOT NULL,
  `token` text NOT NULL,
  `time` bigint(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberPermissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `value` text NOT NULL,
  `realm` char(255) NOT NULL DEFAULT '*',
  PRIMARY KEY (`id`),
  KEY `realm` (`realm`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberPunishments` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `usid` int(32) UNSIGNED DEFAULT NULL,
  `adminuid` int(32) UNSIGNED DEFAULT NULL,
  `adminusid` int(32) UNSIGNED DEFAULT NULL,
  `punishment` char(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `time` bigint(15) NOT NULL,
  `realm` char(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `usid` (`usid`),
  KEY `adminuid` (`adminuid`),
  KEY `adminusid` (`adminusid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberRanks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `rank` char(30) NOT NULL,
  `realm` char(255) NOT NULL DEFAULT '*',
  PRIMARY KEY (`id`),
  KEY `realm` (`realm`),
  KEY `uid` (`uid`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `centerId` CHAR(36) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `displayName` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ip` varchar(45) NOT NULL DEFAULT '',
  `created` bigint(15) NOT NULL,
  `validated` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `newid` int(32) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `disabled` (`disabled`),
  KEY `validated` (`validated`) USING BTREE,
  KEY `newid` (`newid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberSessions` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `ip` char(40) DEFAULT NULL,
  `sid` int(32) UNSIGNED NOT NULL,
  `ssid` int(10) UNSIGNED DEFAULT NULL,
  `startTime` bigint(15) NOT NULL,
  `endTime` bigint(15) NOT NULL,
  `invalidated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `sid` (`sid`),
  KEY `ssid` (`ssid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberStatistics` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usid` int(32) UNSIGNED NOT NULL,
  `provider` char(30) DEFAULT NULL,
  `stat` char(30) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`stat`),
  KEY `usid` (`usid`),
  KEY `provider` (`provider`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberTokens` (
  `id` varchar(80) NOT NULL,
  `uid` int(32) UNSIGNED NOT NULL,
  `ip` varchar(39) NOT NULL,
  `token` varchar(64) NOT NULL,
  `counter` int(11) NOT NULL DEFAULT '0',
  `expires` bigint(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `memberValidation` (
  `uid` int(32) UNSIGNED NOT NULL,
  `token` varchar(80) NOT NULL,
  PRIMARY KEY (`token`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `oauthAuthorizations` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `uid` int(32) UNSIGNED DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(4000) DEFAULT NULL,
  `id_token` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`),
  KEY `uid` (`uid`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `oauthClients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `oauthTokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `uid` int(32) UNSIGNED DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`access_token`),
  KEY `uid` (`uid`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `permissions` (
  `perm` char(255) NOT NULL,
  `game` char(255) NOT NULL,
  UNIQUE KEY `game_permissions` (`game`,`perm`),
  KEY `game` (`game`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `quickActions` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `realm` char(255) NOT NULL,
  `type` char(32) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cmd` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `rankPermissions` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rank` char(30) NOT NULL,
  `value` text NOT NULL,
  `realm` char(255) NOT NULL DEFAULT '*',
  PRIMARY KEY (`id`),
  KEY `realm` (`realm`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ranks` (
  `rank` char(30) NOT NULL,
  `inherit` char(30) DEFAULT NULL,
  `displayName` varchar(255) NOT NULL,
  `subgrouping` int(32) UNSIGNED NOT NULL,
  `data` text NOT NULL,
  `realm` char(255) NOT NULL DEFAULT '*',
  UNIQUE KEY `rank` (`rank`),
  KEY `inherit` (`inherit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `serverActions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(10) UNSIGNED NOT NULL,
  `payload` text NOT NULL,
  `performed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sid` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `servers` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `game` varchar(255) NOT NULL,
  `gamemode` varchar(255) DEFAULT NULL,
  `secret` varchar(100) NOT NULL,
  `communication` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `serverSessions` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(32) UNSIGNED NOT NULL,
  `startTime` bigint(15) NOT NULL,
  `endTime` bigint(15) NOT NULL,
  `invalidated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sid` (`sid`),
  KEY `invalidated` (`invalidated`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `serverStatistics` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(32) UNSIGNED NOT NULL,
  `ssid` int(32) UNSIGNED DEFAULT NULL,
  `provider` char(30) DEFAULT NULL,
  `stat` char(30) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`stat`),
  KEY `provider` (`provider`),
  KEY `serverStatistics_ibfk_1` (`sid`),
  KEY `ssid` (`ssid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL,
  `value` text NOT NULL,
  `realm` char(255) NOT NULL DEFAULT '*',
  PRIMARY KEY (`id`),
  KEY `realm` (`realm`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ticketMessages` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tid` int(32) UNSIGNED NOT NULL,
  `uid` int(32) UNSIGNED NOT NULL,
  `time` bigint(15) NOT NULL,
  `msg` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rid` (`tid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(32) UNSIGNED NOT NULL,
  `usid` int(32) UNSIGNED DEFAULT NULL,
  `otheruid` int(32) UNSIGNED DEFAULT NULL,
  `otherusid` int(32) UNSIGNED DEFAULT NULL,
  `sid` int(32) UNSIGNED NOT NULL,
  `ssid` int(32) UNSIGNED DEFAULT NULL,
  `time` bigint(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `usid` (`usid`),
  KEY `otherusid` (`otherusid`),
  KEY `otheruid` (`otheruid`),
  KEY `sid` (`sid`),
  KEY `ssid` (`ssid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `geolocations`
  ADD CONSTRAINT `geolocations_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `logDetails`
  ADD CONSTRAINT `logDetails_ibfk_1` FOREIGN KEY (`lid`) REFERENCES `logs` (`id`);

ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `servers` (`id`),
  ADD CONSTRAINT `logs_ibfk_2` FOREIGN KEY (`ssid`) REFERENCES `serverSessions` (`id`);

ALTER TABLE `member2step`
  ADD CONSTRAINT `member2step_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `memberAliases`
  ADD CONSTRAINT `memberAliases_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `memberAlts`
  ADD CONSTRAINT `memberAlts_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberAlts_ibfk_2` FOREIGN KEY (`otheruid`) REFERENCES `members` (`id`);

ALTER TABLE `memberBadges`
  ADD CONSTRAINT `memberBadges_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberBadges_ibfk_2` FOREIGN KEY (`badge`) REFERENCES `badges` (`badge`);

ALTER TABLE `memberBans`
  ADD CONSTRAINT `memberBans_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberBans_ibfk_2` FOREIGN KEY (`usid`) REFERENCES `memberSessions` (`id`),
  ADD CONSTRAINT `memberBans_ibfk_3` FOREIGN KEY (`adminuid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberBans_ibfk_4` FOREIGN KEY (`adminusid`) REFERENCES `memberSessions` (`id`);

ALTER TABLE `memberData`
  ADD CONSTRAINT `memberData_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `memberLinks`
  ADD CONSTRAINT `memberLinks_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `memberNotifications`
  ADD CONSTRAINT `memberNotifications_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `memberPermissions`
  ADD CONSTRAINT `memberPermissions_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `memberPunishments`
  ADD CONSTRAINT `memberPunishments_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberPunishments_ibfk_2` FOREIGN KEY (`usid`) REFERENCES `memberSessions` (`id`),
  ADD CONSTRAINT `memberPunishments_ibfk_3` FOREIGN KEY (`adminuid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberPunishments_ibfk_4` FOREIGN KEY (`adminusid`) REFERENCES `memberSessions` (`id`);

ALTER TABLE `memberRanks`
  ADD CONSTRAINT `memberRanks_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberRanks_ibfk_2` FOREIGN KEY (`rank`) REFERENCES `ranks` (`rank`);

ALTER TABLE `members`
  ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`newid`) REFERENCES `members` (`id`);

ALTER TABLE `memberSessions`
  ADD CONSTRAINT `memberSessions_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `memberSessions_ibfk_2` FOREIGN KEY (`sid`) REFERENCES `servers` (`id`),
  ADD CONSTRAINT `memberSessions_ibfk_3` FOREIGN KEY (`ssid`) REFERENCES `serverSessions` (`id`);

ALTER TABLE `memberStatistics`
  ADD CONSTRAINT `memberStatistics_ibfk_1` FOREIGN KEY (`usid`) REFERENCES `memberSessions` (`id`);

ALTER TABLE `memberTokens`
  ADD CONSTRAINT `memberTokens_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `memberValidation`
  ADD CONSTRAINT `memberValidation_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `oauthAuthorizations`
  ADD CONSTRAINT `oauthAuthorizations_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `oauthAuthorizations_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `oauthClients` (`client_id`);

ALTER TABLE `oauthTokens`
  ADD CONSTRAINT `oauthTokens_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `oauthTokens_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `oauthClients` (`client_id`);

ALTER TABLE `rankPermissions`
  ADD CONSTRAINT `rankPermissions_ibfk_1` FOREIGN KEY (`rank`) REFERENCES `ranks` (`rank`);

ALTER TABLE `ranks`
  ADD CONSTRAINT `ranks_ibfk_1` FOREIGN KEY (`inherit`) REFERENCES `ranks` (`rank`);

ALTER TABLE `serverActions`
  ADD CONSTRAINT `serverActions_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `servers` (`id`);

ALTER TABLE `serverSessions`
  ADD CONSTRAINT `serverSessions_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `servers` (`id`);

ALTER TABLE `serverStatistics`
  ADD CONSTRAINT `serverStatistics_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `servers` (`id`),
  ADD CONSTRAINT `serverStatistics_ibfk_2` FOREIGN KEY (`ssid`) REFERENCES `serverSessions` (`id`);

ALTER TABLE `ticketMessages`
  ADD CONSTRAINT `ticketMessages_ibfk_1` FOREIGN KEY (`tid`) REFERENCES `tickets` (`id`),
  ADD CONSTRAINT `ticketMessages_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `members` (`id`);

ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`usid`) REFERENCES `memberSessions` (`id`),
  ADD CONSTRAINT `tickets_ibfk_3` FOREIGN KEY (`otheruid`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `tickets_ibfk_4` FOREIGN KEY (`otherusid`) REFERENCES `memberSessions` (`id`),
  ADD CONSTRAINT `tickets_ibfk_5` FOREIGN KEY (`sid`) REFERENCES `servers` (`id`),
  ADD CONSTRAINT `tickets_ibfk_6` FOREIGN KEY (`ssid`) REFERENCES `serverSessions` (`id`);