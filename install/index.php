<?php

define('NM_INSTALL', 1);

require_once "../includes/networkmanager.php";

if (file_exists("lock")) {
    echo "lock file exists! Remove it from install folder to continue.";
    exit();
}

$configdata = "";

if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);

    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

if (PHP_VERSION_ID > 80003) {
    // Our version is greater then 8.0.3, which is what we need at least
}

$required_exts = array();
$required_exts["calendar"] = false;
$required_exts["curl"] = false;
$required_exts["date"] = false;
$required_exts["gd"] = false;
$required_exts["json"] = false;
$required_exts["mbstring"] = false;
$required_exts["openssl"] = false;
$required_exts["pdo_mysql"] = false;
$required_exts["zip"] = false;

foreach (get_loaded_extensions() as $i => $ext) {
    if (isset($required_exts[$ext])) {
        $required_exts[$ext] = true;
    }
}

$writable = array();

$writable['config'] = false;
$file = @fopen(NM_ROOT . 'config.php', 'w');
if ($file != false) {
	if (@chmod(NM_ROOT . 'config.php', 0664) != false) {
		$writable['config'] = true;
	}
	@fclose($file);
	@unlink(NM_ROOT . 'config.php');
}

$writable['uploads'] = false;
if (file_exists(NM_ROOT . "uploads") && is_writable(NM_ROOT . "uploads")) {
    $writable['uploads'] = true;
} elseif (mkdir(NM_ROOT . "uploads", 0744)) {
    $writable['uploads'] = true;
}

$writable['avatars'] = false;
if (file_exists(NM_ROOT . "uploads/avatars") && is_writable(NM_ROOT . "uploads/avatars")) {
    $writable['avatars'] = true;
} elseif (mkdir(NM_ROOT . "uploads/avatars", 0744)) {
    $writable['avatars'] = true;
}

$writable['attachments'] = false;
if (file_exists(NM_ROOT . "uploads/attachments") && is_writable(NM_ROOT . "uploads/attachments")) {
    $writable['attachments'] = true;
} elseif (mkdir(NM_ROOT . "uploads/attachments", 0744)) {
    $writable['attachments'] = true;
}

$requirements = true;
foreach ($required_exts as $row) {
    if ($row == false) {
        $requirements = false;
        break;
    }
}

foreach ($writable as $row) {
    if ($row == false) {
        $requirements = false;
        break;
    }
}

$directory = "./languages";
$scanned_directory = array_diff(scandir($directory), array('..', '.'));
$languages = array();
foreach ($scanned_directory as $lang_file) {
	$languages[] = pathinfo($lang_file, PATHINFO_FILENAME);
}

function install_getenv($var, $default="") {
	$res = getenv($var);
	if ($res === false)
		return $default;
	return $res;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>NetworkManager Installation</title>
	<meta name="description" content="NetworkManager">
	<meta name="author" content="NetworkManager">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo NM_URL_PATH; ?>favicon.ico">

	<?php include NM_ROOT . "includes/style.php";?>

	<!-- BEGIN CSS for this page -->
	<!-- END CSS for this page -->
</head>

<body>
	<div class="headerbar">
		<!-- LOGO -->
		<div class="headerbar-left">
			<a href="/" class="logo"><img alt="Logo" src="<?php echo NM_CDN; ?>assets/img/logo.png" /> <span>NetworkManager</span></a>
		</div>

		<nav class="navbar-custom">
			<ul class="list-inline float-right mb-0">
				<li class="list-inline-item dropdown notif">
					<a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
						<i class="fa-solid fa-circle-question"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-arrow-success dropdown-lg">
						<!-- item-->
						<div class="dropdown-item noti-title">
							<h5><small>Help and Support</small></h5>
						</div>
						<!-- All-->
						<a title="Contact Us" target="_blank" href="https://www.networkmanager.io/contact/" class="dropdown-item notify-item notify-all"><i class="fa-solid fa-link"></i> Contact Us</a>
					</div>
				</li>
			</ul>
		</nav>
	</div>
	<!-- End Navigation -->

	<div class="container bg-white pt-3 pb-5" style="margin-top: 60px;">
		<form id="installForm" action="finialize.php" method="post">
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-12 sidebar-section hidden-xs" style="margin-top: 50px;">
					<div class="container nav-sidebar">
						<div class="nav flex-column">
							<a class="nav-link active" id="link-welcome" data-toggle="tab" href="#welcome"><i class="fa-solid fa-hashtag" aria-hidden="true"></i> Welcome</a>
							<a class="nav-link <?php echo ($requirements) ? "valid" : "invalid"; ?>" id="link-requirements" data-toggle="tab" href="#requirements"><i class="fa-solid fa-microchip" aria-hidden="true"></i> Requirements</a>
							<a class="nav-link invalid" id="link-database" data-toggle="tab" href="#database"><i class="fa-solid fa-share" aria-hidden="true"></i> Database</a>
							<a class="nav-link" id="link-ranks" data-toggle="tab" href="#ranks"><i class="fa-solid fa-users" aria-hidden="true"></i> Ranks</a>
							<a class="nav-link invalid" id="link-user" data-toggle="tab" href="#user"><i class="fa-solid fa-user" aria-hidden="true"></i> User</a>
							<a class="nav-link invalid" id="link-settings" data-toggle="tab" href="#settings"><i class="fa-solid fa-gears" aria-hidden="true"></i> Settings</a>
							<a class="nav-link invalid" id="link-finish" data-toggle="tab" href="#finish"><i class="fa-solid fa-gear" aria-hidden="true"></i> Finish</a>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 tab-content">
					<div class="tab-pane active container" id="welcome">
						<h1>welcome</h1>
						<p>Please scan your first item</p>
						<p>
							Thanks for using NetworkManager! This is the installation helper to prep the web configuration, database, ranks, and main user.
							This installation will perform the following tasks:
						</p>
						<ul>
							<li>Confirm web requirements</li>
							<li>Perform database connection</li>
							<li>Setup initial ranks</li>
							<li>Setup initial user</li>
							<li>Finialize installation</li>
						</ul>
						
						<div class="checkbox checkbox-primary">
							<input id="checkbox_nmcenter" name="nmcenter" type="checkbox" name="nmcenter"<?php if (getenv('DB_HOST') == false) { ?> checked<?php } ?>>
							<label for="checkbox_nmcenter"> Get NetworkManager Center Instance ID</label>
						</div>
					</div>
					<div class="tab-pane container" id="requirements">
						<h1>requirements</h1>
						<hr>
						<table class="table table-bordered table-response-xl table-hover display">
							<thead>
								<tr>
									<th colspan="3">Requirements</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>PHP Version</td>
									<td><?php echo phpversion(); ?></td>
									<td><?php echo (version_compare(PHP_VERSION, '5.4.0', ">=")) ? '<i class="fa-solid fa-check text-success" aria-hidden="true"></i>' : '<i class="fa-solid fa-xmark text-danger" aria-hidden="true"></i>'; ?></td>
								</tr>
								<?php
foreach ($required_exts as $k => $v) {
    ?>
								<tr>
									<td><?php echo $k; ?></td>
									<td><?php echo phpversion($k); ?></td>
									<td><?php echo $v ? '<i class="fa-solid fa-check text-success" aria-hidden="true"></i>' : '<i class="fa-solid fa-xmark text-danger" aria-hidden="true"></i>'; ?></td>
								</tr>
									<?php
}
?>
								<tr>
									<td>Configuration Writable</td>
									<td></td>
									<td><?php echo $writable['config'] ? '<i class="fa-solid fa-xmark text-success" aria-hidden="true"></i>' : '<i class="fa-solid fa-xmark text-danger" aria-hidden="true"></i>'; ?></td>
								</tr>
								<tr>
									<td>Avatars Directory Writable</td>
									<td></td>
									<td><?php echo $writable['avatars'] ? '<i class="fa-solid fa-check text-success" aria-hidden="true"></i>' : '<i class="fa-solid fa-xmark text-danger" aria-hidden="true"></i>'; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="tab-pane container" id="database">
						<h1>Database</h1>
						<p class="description text-danger">Not Connected</p>
						<div class="form-group">
							<label for="database[database_type]">type<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Type of database server.</p>
							<select class="form-control" name="database[database_type]">
								<option value="mysql">MySQL</option>
							</select>
						</div>
						<div class="form-group">
							<label for="database[server]">server<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Address for the database server.</p>
							<input type="text" class="form-control" name="database[server]" placeholder="server" required value="<?php echo install_getenv('DB_HOST'); ?>">
						</div>
						<div class="form-group">
							<label for="database[port]">port<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Port for the database server.</p>
							<input type="number" class="form-control" name="database[port]" placeholder="Port" value="3306" required value="<?php echo install_getenv('DB_PORT'); ?>">
						</div>
						<div class="form-group">
							<label for="database[database_name]">database<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Database name for the database server.</p>
							<input type="text" class="form-control" name="database[database_name]" placeholder="database" required value="<?php echo install_getenv('DB_NAME'); ?>">
						</div>
						<div class="form-group">
							<label for="database[username]">Username<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Username for the database server.</p>
							<input type="text" class="form-control" name="database[username]" placeholder="username" required value="<?php echo install_getenv('DB_USER'); ?>">
						</div>
						<div class="form-group">
							<label for="database[password]">Password<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Password for the database server.</p>
							<input type="text" class="form-control" name="database[password]" placeholder="password" required value="<?php echo install_getenv('DB_PASS'); ?>">
						</div>

						<button class="btn btn-primary" onclick="testconnection(event);">Test Connection</button>
					</div>
					<div class="tab-pane container" id="ranks">
						<h1>ranks</h1>
						<p>Create your default ranks now, we provide our basic structure that you can work with. After you've performed the intial setup you can always further edit your ranks and their hierarchy.</p>
						<p>To learn more about a specific rank element, just hover over the table element.</p>
						<!-- No Access -->
						<!-- User -->
						<!-- Moderator -->
						<!-- Administrator -->
						<!-- Owner -->
						<table id="Ranks" class="table table-striped table-bordered table-responsive-xl table-hover display mb-0">
							<thead>
								<tr>
									<th>Rank</th>
									<th>Display Name</th>
									<th>Inherits</th>
									<th>Realm</th>
									<th>Subgrouping</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<div id="rank-inputs">
							<!-- Input-per rank -->
						</div>
					</div>
					<div class="tab-pane container" id="user">
						<h1>user</h1>
						<div class="form-group">
							<label for="user[username]">Username<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Username for your administrative user.</p>
							<input type="text" class="form-control" name="user[username]" placeholder="Username" oninput="testuser(1)" pattern="^\w{3,32}$" required value="<?php echo install_getenv('USER_USER'); ?>">
						</div>
						<div class="form-group">
							<label for="user[password]">Password<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Password for your administrative user.</p>
							<input type="password" class="form-control" name="user[password]" placeholder="Password" oninput="testuser(2)" pattern="^.{6,32}$" required value="<?php echo install_getenv('USER_PASS'); ?>">
						</div>
						<div class="form-group">
							<label for="user[verifypassword]">Password Confirmation<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Confirm your password.</p>
							<input type="password" class="form-control" name="user[verifypassword]" placeholder="Password" oninput="testuser(3)" required value="<?php echo install_getenv('USER_PASS'); ?>">
						</div>
						<div class="form-group">
							<label for="user[displayName]">Display Name<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Display Name for your administrative user.</p>
							<input type="text" class="form-control" name="user[displayName]" placeholder="Display Name" oninput="testuser(4)" pattern="^.+$" required value="<?php echo install_getenv('USER_NAME'); ?>">
						</div>
					</div>
					<div class="tab-pane container" id="settings">
						<h1>settings</h1>
						<div class="form-group">
							<label for="language">Language<span class="required">*</span></label>
							<p class="description" style="margin-bottom: 5px;">Language for your panel.</p>
							<select class="form-control" name="settings[language]" required>
								<?php foreach($languages as $lang) {
									?><option value="<?php echo $lang; ?>"><?php echo $lang; ?></option><?php
								} ?>
							</select>
						</div>
						<div>
							<h5>Email Settings</h5>
							<div class="form-group">
								<label for="settings[email][method]">Email Method<span class="required">*</span></label>
								<p class="description" style="margin-bottom: 5px;">Method to use to serve emails.</p>
								<select class="form-control" id="email" name="settings[email][method]" required>
									<option value="php">PHP</option>
									<option value="smtp">SMTP</option>
									<option value="custom">Custom -- Not yet prepared</option>
								</select>
							</div>
							<hr>
							<div class="form-group">
								<label for="settings[email][from]">Outgoing email address<span class="required">*</span></label>
								<input type="text" class="form-control" name="settings[email][from]" placeholder="Email" required value="<?php echo install_getenv('EMAIL_FROM'); ?>">
							</div>
							<div id="smtp">
								<div class="form-group">
									<label for="settings[email][smtp][host]">SMTP Host<span class="required">*</span></label>
									<input type="text" class="form-control" name="settings[email][smtp][host]" placeholder="Host">
								</div>
								<div class="form-group">
									<label for="settings[email][smtp][protocol]">SMTP Protocol<span class="required">*</span></label>
									<select class="form-control" name="settings[email][smtp][protocol]">
										<option value="plain">Plaintext</option>
										<option value="tls">TLS</option>
										<option value="ssl">SSL</option>
									</select>
								</div>
								<div class="form-group">
									<label for="settings[email][smtp][port]">SMTP Port<span class="required">*</span></label>
									<input type="number" class="form-control" name="settings[email][smtp][port]" placeholder="Port" value="25">
								</div>
								<div class="form-group">
									<label for="settings[email][smtp][username]">SMTP Username<span class="required">*</span></label>
									<input type="text" class="form-control" name="settings[email][smtp][username]" placeholder="Username">
								</div>
								<div class="form-group">
									<label for="settings[email][smtp][password]">SMTP Password<span class="required">*</span></label>
									<input type="password" class="form-control" name="settings[email][smtp][password]" placeholder="Password">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane container" id="finish">
						<h1>finish</h1>
						<button type="submit" class="btn btn-success" onclick="testfinal(event)">Finialize</button>
					</div>
					<p class="mt-3 description"><span class="required">*</span> means the element must be filled</p>
				</div>
			</div>
		</form>
	</div>

	<footer class="footer" style="left:0;">
		<div class="container-fluid">
			<span class="text-right">
			Copyright <a target="_blank" href="https://www.networkmanager.io/">NetworkManager</a>
			</span>
			<span class="float-right">
			Powered by <a target="_blank" href="https://www.pikeadmin.com"><b>Pike Admin</b></a>
			</span>
		</div>
	</footer>

	<?php include NM_ROOT . "includes/script.php";?>

	<!-- BEGIN Java Script for this page -->
	<script>
		$(document).ready(function (){
			$("#smtp").hide();
            $("#email").change(function() {
                // foo is the id of the other select box 
                if ($(this).val() == "smtp") {
                    $("#smtp").show();
					$('#smtp').find('input').prop('required',true);
					console.log($('#smtp').find('input'));
                }else{
					$("#smtp").hide();
					$('#smtp').find('input').prop('required',false);
                } 
            });
        });

		var requirements = <?php echo $requirements ? "true" : "false"; ?>;
		var database = false;
		var user = false;

		function testconnection(e) {
			if (e != null) { e.preventDefault(); }
			var post = $("#installForm").serializeArray();
			post.push({name: "action", value: "database"});
			iajax(post, function() {
				var link = document.getElementById("link-database");
				link.classList.remove("invalid");
				link.classList.remove("valid");
				link.classList.add("valid");
				database = true;
			}, function() {
				var link = document.getElementById("link-database");
				link.classList.remove("invalid");
				link.classList.remove("valid");
				link.classList.add("invalid");
				database = false;
			});
		}

		var userp1 = false;
		var userp2 = false;
		var userp3 = false;
		var userp4 = false;

		function testuser(part) {
			if (part == 1) {
				var input = document.getElementById("installForm").elements["user[username]"];
				var patt = new RegExp(input.pattern);
				if (patt.test(input.value)) {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-valid");
					userp1 = true;
				} else {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-invalid");
					userp1 = false;
				}
			} else if (part == 2) {
				var input = document.getElementById("installForm").elements["user[password]"];
				var patt = new RegExp(input.pattern);
				if (patt.test(input.value)) {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-valid");
					userp2 = true;
				} else {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-invalid");
					userp2 = false;
				}
			} else if (part == 3) {
				var pwd = document.getElementById("installForm").elements["user[password]"];
				var input = document.getElementById("installForm").elements["user[verifypassword]"];
				if (pwd.value == input.value) {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-valid");
					userp3 = true;
				} else {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-invalid");
					userp3 = false;
				}
			} else if (part == 4) {
				var input = document.getElementById("installForm").elements["user[displayName]"];
				var patt = new RegExp(input.pattern);
				if (patt.test(input.value)) {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-valid");
					userp4 = true;
				} else {
					input.classList.remove("is-invalid");
					input.classList.remove("is-valid");
					input.classList.add("is-invalid");
					userp4 = false;
				}
			}

			if (userp1 && userp2 && userp3 && userp4) {
				var link = document.getElementById("link-user");
				link.classList.remove("invalid");
				link.classList.remove("valid");
				link.classList.add("valid");
				user = true;
			} else {
				var link = document.getElementById("link-user");
				link.classList.remove("invalid");
				link.classList.remove("valid");
				link.classList.add("invalid");
				user = false;
			}
		}

		function testfinal(e) {
			if (!requirements || !database || !user) {
				if (!requirements) {
					$.notify({message: 'Didn\'t meet the PHP Requirements!'}, { type: 'danger', allow_dismiss: true });
				}
				if (!database) {
					$.notify({message: 'Couldn\t connect to the database!'}, { type: 'danger', allow_dismiss: true });
				}
				if (!user) {
					$.notify({message: 'Must setup your main user!'}, { type: 'danger', allow_dismiss: true });
				}
				e.preventDefault();
			}
		}

		function iajax(post, scallback, ecallback) {
			$.ajax({
				url: "ajax.php",
				type: 'POST',
				data: post,
				success: function(data, status, xhr) {
					console.log(data);
					if (data.message) {
						$.notify(data.message, {
							type: data.type,
							placement: {
								from: "top",
								align: "center"
							},
						});
					}
					if (scallback) {
						scallback(status, data);
					}
				},
				error: function(xhr, status, error) {
					console.log(xhr);
					if (xhr.responseJSON && xhr.responseJSON.message) {
						$.notify(xhr.responseJSON.message, {
							type: xhr.responseJSON.type,
							placement: {
								from: "top",
								align: "center"
							},
						});
					}
					if (ecallback) {
						ecallback(status, error);
					}
				},
			});
		}

		function removeRank(rank) {
			var elem = document.getElementById("row-" + rank);
			elem.parentNode.removeChild(elem);

			var elem = document.getElementById("rin-rank-" + rank);
			elem.parentNode.removeChild(elem);
			var elem = document.getElementById("rin-name-" + rank);
			elem.parentNode.removeChild(elem);
			var elem = document.getElementById("rin-inherit-" + rank);
			elem.parentNode.removeChild(elem);
			var elem = document.getElementById("rin-subgrouping-" + rank);
			elem.parentNode.removeChild(elem);
			var elem = document.getElementById("rin-realm-" + rank);
			elem.parentNode.removeChild(elem);
		}

		function addRank(rank, name, inherit, realm, subgrouping, required) {
			var table = document.getElementById("Ranks").getElementsByTagName('tbody')[0];
			var tr = table.insertRow();
			tr.id = "row-" + rank;
			tr.insertCell(0).innerHTML = rank;
			tr.insertCell(1).innerHTML = name;
			tr.insertCell(2).innerHTML = inherit;
			tr.insertCell(3).innerHTML = realm;
			tr.insertCell(4).innerHTML = subgrouping;
			var td = tr.insertCell(5);
			td.innerHTML = "";
			if (required === false) {
				td.innerHTML = "<button class='btn btn-danger rounded-0' onclick='removeRank(\"" + rank + "\")'><span aria-hidden='true'>×</span></button>";
			}
			td.classList.add('p-0');
			td.classList.add('border-0');

			var inputs = document.getElementById("rank-inputs");
			inputs.innerHTML += '<input type="hidden" id="rin-rank-' + rank + '" name="ranks[' + rank + '][rank]" value="' + rank + '">';
			inputs.innerHTML += '<input type="hidden" id="rin-name-' + rank + '" name="ranks[' + rank + '][displayName]" value="' + name + '">';
			inputs.innerHTML += '<input type="hidden" id="rin-inherit-' + rank + '" name="ranks[' + rank + '][inherits]" value="' + inherit + '">';
			inputs.innerHTML += '<input type="hidden" id="rin-subgrouping-' + rank + '" name="ranks[' + rank + '][subgrouping]" value="' + subgrouping + '">';
			inputs.innerHTML += '<input type="hidden" id="rin-realm-' + rank + '" name="ranks[' + rank + '][realm]" value="' + realm + '">';
		}

		(function() {
			'use strict';
			window.addEventListener('load', function() {
				addRank("noaccess", "No Access", "", "*", "0", false);
				addRank("user", "User", "", "*", "0", true);
				addRank("moderator", "Moderator", "user", "*", "0", false);
				addRank("admin", "Administrator", "moderator", "*", "0", false);
				addRank("owner", "Owner", "admin", "*", "0", false);
			}, false);
			<?php if (getenv('DB_HOST')) { ?>
			testconnection();
			testuser(1);
			testuser(2);
			testuser(3);
			testuser(4);
			<?php } ?>
		})();
	</script>
	<!-- END Java Script for this page -->
</body>
</html>