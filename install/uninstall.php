<?php

define('NM_INSTALL', 1);

require_once "../includes/networkmanager.php";

// Can't have the lock file for uninstall. This is to help prevent accidents for installed instances.
if (file_exists("lock")) {
    echo "lock file exists! Remove it from install folder to continue.";
    exit();
}

if (file_exists(NM_ROOT . "config.php")) {
    require_once NM_ROOT . "config.php";
    require_once NM_CLASS_ROOT . "database.php";
    $db = \NetworkManager\Database::getInstance();
    //$db->query("DROP TABLE avatars,badges,bans,geolocations,languages,logDetails,logs,member2step,memberAliases,memberAlts,memberBadges,memberData,memberLinks,memberNotifications,memberPending,memberPermissions,memberRanks,members,memberSessions,memberStatistics,memberTokens,memberValidation,oauthAuthorizations,oauthClients,oauthTokens,permissions,quickActions,rankPermissions,ranks,serverActions,servers,serverSessions,settings,ticketMessages,tickets;");
    var_dump($db->error());

    @unlink(NM_ROOT . 'config.php');
}
echo "Cleaned up system";
