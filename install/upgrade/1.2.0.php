<?php

if (!defined("NM_ROOT") || defined("NM_INSTALL")) {
    exit();
}

return $NM->db->action(function($db) {
    $db->query("UPDATE `settings` SET `name`='instance.id` WHERE `name`='instance_id';");
    $db->query("UPDATE `settings` SET `name`='instance.apikey` WHERE `name`='instance_apikey';");
    $db->query("UPDATE `settings` SET `name`='steam.apikey` WHERE `name`='steam_apikey';");
    $db->query("UPDATE `settings` SET `name`='discord.clientid` WHERE `name`='discord_clientid';");
    $db->query("UPDATE `settings` SET `name`='discord.secret` WHERE `name`='discord_secret';");
    $db->query("UPDATE `settings` SET `name`='twitch.clientid` WHERE `name`='twitch_clientid';");
    $db->query("UPDATE `settings` SET `name`='twitch.secret` WHERE `name`='twitch_secret';");
    $db->query("ALTER TABLE memberAlts CHANGE reason triggers text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
    $db->query("ALTER TABLE memberAlts ADD likelihood FLOAT NOT NULL;");
    $db->query("ALTER TABLE tickets CHANGE closed status SMALLINT UNSIGNED DEFAULT 0 NOT NULL;");
    $db->query("ALTER TABLE tickets MODIFY COLUMN status SMALLINT UNSIGNED DEFAULT 0 NOT NULL;");
    $db->query("ALTER TABLE geolocations DROP INDEX continent;");
    $db->query("ALTER TABLE geolocations DROP COLUMN continent;");
});