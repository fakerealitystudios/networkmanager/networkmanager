<?php

if (!defined("NM_ROOT") || defined("NM_INSTALL")) {
    exit();
}

return $NM->db->action(function($db) {
    $db->query("ALTER TABLE `members` ADD `setup` TINYINT(1) NOT NULL DEFAULT '0' AFTER `validated`;");
    $db->update("members", array("setup" => 1), array(
        "AND" => array(
            "username[!]" => "",
            "displayName[!]" => ""
        )
    ));
});