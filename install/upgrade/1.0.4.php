<?php

if (!defined("NM_ROOT") || defined("NM_INSTALL")) {
    exit();
}

return $NM->db->action(function($db) {
    // Fuck that shit. We'll just see if things have been set...
    $db->query("ALTER TABLE `members` DROP COLUMN `setup`;");

    $db->query("ALTER TABLE `logs` ADD `args` INTEGER(8) NULL DEFAULT NULL AFTER `time`;");
    $db->query("ALTER TABLE `memberSessions` ADD `ip` CHAR(40) NULL DEFAULT NULL AFTER `uid`;");
    $db->query("ALTER TABLE `memberData` DROP INDEX `vid`, ADD INDEX `vid` (`vid`) USING BTREE;");
    $db->query("ALTER TABLE `settings` DROP INDEX `name`, ADD INDEX `name` (`name`) USING BTREE;");
    $db->query("ALTER TABLE `bans` ADD `adminusid` INT(32) UNSIGNED NULL DEFAULT NULL AFTER `adminuid`, ADD INDEX (`adminusid`);");
    $db->query("ALTER TABLE `bans` ADD FOREIGN KEY (`adminusid`) REFERENCES `memberSessions`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    $db->query("ALTER TABLE `permissions` ADD `description` TEXT NULL DEFAULT NULL AFTER `game`;");
});