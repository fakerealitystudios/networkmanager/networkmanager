<?php

require "../../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (DB_VERSION >= NM_VERSION) {
    // You already have the current version installed/configured!
    echo "Database version is equal or above the currently installed web version.";
    exit();
}

$data = array();
$data["versions"] = array();
$data["upgrades"] = array();
$upgrades = array();
if ($handle = opendir("./")) {
    while(false !== ($file = readdir($handle))) {
        if (preg_match("/(\d+)\.(\d+)\.(\d+)/", $file, $matches) == false) {
            continue;
        }
        $version = ($matches[1] * 10000 + $matches[2] * 100 + $matches[3]);
        if ($version > DB_VERSION && $version <= NM_VERSION) { // Get all upgrades above our installed version
            $data["upgrades"][$version] = /*NM_ROOT."install/upgrade/".*/$file;
            array_splice($matches, 0, 1);
            $data["versions"][$version] = implode(".", $matches);
        }
        unset($version);
    }
    closedir($handle);
}
ksort($data["upgrades"]);
ksort($data["versions"]);
$data["curr_version"] = $nm_version;
$data["curr_version_full"] = NM_VERSION;
$failed = false;
foreach($data["upgrades"] as $k => $upgrade) {
    $val = include($upgrade);
    if ($val == false) {
        $failed = true;
        // Stop all further upgrades. We need to try this one again
        \Sentry\withScope(function (\Sentry\State\Scope $scope): void {
            $scope->setData('current_version', $nm_version);
            $scope->setData('upgrade_version', $data['curr_version']);
            $scope->setData('failed_version', $k);
            $scope->setLevel(\Sentry\Severity::error());
            // will be tagged with my-tag="my value"
            \Sentry\captureMessage("Failed upgrade");
        });
        break;
    } else {
        $data["curr_version"] = $data["versions"][$k];
        $data["curr_version_full"] = $k;
    }
}

$config_template = file_get_contents(NM_ROOT . "config.default.php");
$config_template = $NM->template->evaluate($config_template, $config);
$file = @fopen(NM_ROOT . 'config.php', 'w');
fwrite($file, $config_template);
fclose($file);

// Send api the new current version for stat purposes, generate a new instance id if none exist
if (\NetworkManager\API::getInstanceId() == null) {
    \NetworkManager\API::getInstanceId($NM->db);
} elseif ($data["curr_version_full"] != NM_VERSION) { // if an upgrade was performed...
    if ($config['nm_api']['statistics']) {
        \NetworkManager\API::get("/instance/upgrade/".$data['curr_version']);
    }
}

$NM->db->update("settings", array("value" => $data["curr_version"]), array("name" => "version"));

$data['body'] = "";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'upgrade.html', $data);

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'basic.html', $data);