<?php

if (!defined("NM_ROOT") || defined("NM_INSTALL")) {
    exit();
}

return $NM->db->action(function($db) {
    $db->query("ALTER TABLE `members` DROP `ip`;");
    $db->query("ALTER TABLE `memberAlts` DROP `reason`;");
    $db->query("ALTER TABLE `memberAlts` ADD `triggers` JSON NOT NULL AFTER `otheruid`;");
    $db->query("ALTER TABLE `memberAlts` ADD `likelihood` DECIMAL(3,2) NOT NULL DEFAULT '0.00' AFTER `triggers`;");
    $db->query("ALTER TABLE `memberData` ADD UNIQUE `uid_vid` (`uid`, `vid`);");
    $db->query("ALTER TABLE `memberLinks` CHANGE `token` `token` VARCHAR(255) NOT NULL;"); // token must have size for unique key
    $db->query("ALTER TABLE `memberLinks` ADD UNIQUE `type_token` (`type`, `token`);"); // shared unique key
    $db->query("TRUNCATE `memberPending`;");
    $db->query("ALTER TABLE `memberPending` CHANGE `id` `id` VARCHAR(36) NOT NULL;"); // change id from int to varchar(for uuid)
    $db->query("ALTER TABLE `memberPending` DROP `uid`, DROP `linkToken`;"); // change id from int to varchar(for uuid)
    $db->query("DROP TABLE `memberU2F`;");
    $db->query("ALTER TABLE `permissions` DROP `description`;");
    $db->query("ALTER TABLE `permissions` ADD UNIQUE `game_permission` (`game`, `perm`);");
    $db->query("ALTER TABLE `serverSessions` ADD INDEX (`invalidated`);");
    $db->query("ALTER TABLE `tickets` CHANGE `closed` `status` ENUM('PENDING','OPEN','CLOSED') NOT NULL DEFAULT 'OPEN';");
});