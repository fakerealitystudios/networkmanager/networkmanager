<?php

if (!defined("NM_ROOT") || defined("NM_INSTALL")) {
    exit();
}

return $NM->db->action(function($db) {
    $db->query("ALTER TABLE `members` ADD `centerId` CHAR(36) NULL DEFAULT NULL AFTER `id`;");
});