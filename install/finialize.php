<?php

define('NM_INSTALL', 1);

require_once "../includes/networkmanager.php";

if (file_exists("lock")) {
    echo "lock file exists! Remove it from install folder to continue.";
    exit();
}

if (!isset($_POST['database'])) {
    header("Location: " . NM_URL . "install");
    exit();
}

require_once NM_CLASS_ROOT . "template.php";

$config = file_get_contents(NM_ROOT . "config.default.php");
$config = \NetworkManager\Template::evaluateVariables($config, $_POST);
$file = @fopen(NM_ROOT . 'config.php', 'w');
fwrite($file, $config);
fclose($file);

require_once NM_ROOT . "config.php";
require_once NM_CLASS_ROOT . "database.php";
$db = \NetworkManager\Database::getInstance();

unset($version);
$version = null;
try {
    $version = $db->get("settings", "value", array("name" => "version"));
} catch (\PDOException $e) {}

if ($version != null) {
    // Well, apparently they already have a NetworkManager application! Let's see if we can hook in...
    $version = explode('.', $version);
    if ($version[0] != NM_VERSION_MAJOR) {
        // If the major version is different, we need to either upgrade the database(if we're higher)
        // Or the person needs to get a different version of the application to match their current system
        echo "The installed NetworkManager version is newer then this application's! Get a newer version of the application at <a href='https://www.networkmanager.io'>NetworkManager</a>";
        exit();
    }

    // Do this last, since this will mean that a NetworkManager instance now fully exists for this web panel
    $file = @fopen('lock', 'w');
    fwrite($file, time());
    fclose($file);

    header("Location: ".NM_URL);
    exit();
} else {
    // No version exists, so no application has been installed yet. Let's setup the database then!

    // Create tables
    $sql = file_get_contents("networkmanager.sql");
    $sqlArray = explode(";", $sql);
    foreach ($sqlArray as $stmt) {
        if (strlen($stmt) > 3 && substr(ltrim($stmt), 0, 2) != '/*') {
            // TODO: Figure out why I can't perform the CREATE TABLE then insert the rows...
            $db->query($stmt);

            if ($db->error()[0] != "00000") {
                exit();
            }
        }
    }

    $settings = array();
    $settings['version'] = $nm_version;
    $settings['language'] = "en";
    $settings['site'] = NM_URL;
    $settings['install_date'] = time();
    if (isset($_POST['nmcenter']) && $_POST['nmcenter'] == 'on') {
        $settings['instance.id'] = \NetworkManager\API::getInstanceId($db);
        if ($settings['instance.id'] == NULL)
        {
            unset($settings['instance.id']);
        }
    }
    $new_settings = array();
    foreach($settings as $name => $value) {
        $new_settings[] = array(
            'name' => $name,
            'value' => $value
        );
    }
    $db->insert("settings", $new_settings);

    // Used by the following classes for communication
    require_once NM_CLASS_ROOT . "servermanager.php";

    // Create user
    require_once NM_CLASS_ROOT . "authentication.php";
    $auth = \NetworkManager\Authentication::getInstance();
    $db->insert("members", array(
        "username" => "Console",
        "displayName" => "Console",
        "ip" => "127.0.0.1",
        "created" => time(),
        "validated" => 1,
        "disabled" => 1
    ));
    $nmid = $auth->register($_POST['user']['username'], $_POST['user']['password'], "", $_POST['user']['displayName']);
    if (!$nmid) {
        echo "failed registration";
        exit();
    }
    require_once NM_CLASS_ROOT . "memberfull.php";
    $member = new \NetworkManager\MemberFull($nmid);
    $member->givePermission("*", "*");

    // Create ranks
    require_once NM_CLASS_ROOT . "rankmanager.php";
    $rankManager = \NetworkManager\RankManager::getInstance();
    foreach ($_POST['ranks'] as $rank) {
        $rankManager->createRank($rank['displayName'], $rank['rank'], $rank['inherits'], $rank['realm'], $rank['subgrouping']);
    }

    // Create server(default web server)
    $serverManager = \NetworkManager\ServerManager::getInstance();
    $serverManager->create("web", "web", NM_URL);

    // Do this last, since this will mean that a NetworkManager instance now fully exists for this web panel
    $file = @fopen('lock', 'w');
    fwrite($file, time());
    fclose($file);

    header("Location: " . NM_URL);
    exit();
}
