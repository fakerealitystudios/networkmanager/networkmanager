<?php

// include our OAuth2 Server object
require_once 'srv.php';

if (!$oauth->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
    $oauth->getResponse()->send();
    die;
}

$token = $oauth->getAccessTokenData(OAuth2\Request::createFromGlobals());

$member = $NM->getMember($token['user_id'], true);

$scopes = explode(' ', $token['scope']);

$return = array();
$return['success'] = true;
if (in_array("identity", $scopes)) {
    $return['nmid'] = $member->getId();
    $return['displayName'] = $member->getDisplayName();
    $return['avatar'] = $member->getAvatar();
    $return['rank'] = $member->getRank();
    $ranks = array();
    if ($member->isValid()) {
        $ranks = $member->getRanks();
    }
    if (count($ranks) == 0) {
        $ranks[] = array(
            "rank" => "user",
            "realm" => "*",
        );
    }
    $return['ranks'] = $ranks;
    $return['permissions'] = $member->perms;
    $return['linked'] = $member->getLinkedAccounts();
}
if (in_array("email", $scopes)) {
    $return['email'] = $member->getEmail();
}

echo json_encode($return);
