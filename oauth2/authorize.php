<?php

// include our OAuth2 Server object
require_once 'srv.php';

if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

// validate the authorize request
if (!$oauth->validateAuthorizeRequest($request, $response)) {
    $response->send();
    die;
}

$client_id = $request->query('client_id', $request->request('client_id'));

$details = $storage->getClientDetails($client_id);

$scopeDescs = array(
    "identity" => "<li>Access to your NetworkManager ID, display name, and avatar</li>",
    "email" => "<li>Access to your email</li>",
);

// display an authorization form
if (!isset($_POST['authorized'])) {
    $scope = $oauth->getAuthorizeController()->getScope();
    if (!$storage->scopeExists($scope)) {
        exit();
    }
    $scopes = explode(' ', $scope);
    ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Authorize access to your account | NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

        <?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<link href="<?php echo NM_CDN; ?>assets/css/login.min.css" rel="stylesheet">
		<link href="<?php echo NM_CDN; ?>assets/css/bootstrap-checkbox.min.css" rel="stylesheet">
		<!-- END CSS for this page -->
	</head>

	<body>

		<div class="headerbar">
			<!-- LOGO -->
			<div class="headerbar-left">
				<a href="/" class="logo"><img alt="Logo" src="<?php echo NM_CDN; ?>assets/img/logo.png" /> <span>NetworkManager</span></a>
			</div>

			<nav class="navbar-custom">
				<ul class="list-inline float-right mb-0">
					<li class="list-inline-item dropdown notif">
						<a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<i class="fa-solid fa-circle-question"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-arrow-success dropdown-lg">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5><small>Help and Support</small></h5>
							</div>
							<!-- All-->
							<a title="Contact Us" target="_blank" href="https://www.networkmanager.io/contact/" class="dropdown-item notify-item notify-all"><i class="fa-solid fa-link"></i> Contact Us</a>
						</div>
					</li>
				</ul>
			</nav>
		</div>
		<!-- End Navigation -->

		<div class="container h-100" style="margin-top:75px;">

			<div class="row h-100 justify-content-center align-items-center">

				<div class="card">
					<div class="card-header">
						<h6>Authorize</h6>
						<h4 class="text-right" style="color:#848484;"><?php echo $details['title']; ?></h4>
					</div>

					<div class="card-body" style="min-width:450px;">

						<p class="description">This will allow <span style="color:#848484;font-weight:725;"><?php echo $details['title']; ?></span> to...</p>
						<ul class="justify-content-center align-items-center">
							<?php
foreach ($scopes as $scope) {
        echo $scopeDescs[$scope];
    }
    ?>
						</ul>
						<hr>

						<form method="post">
							<input type="submit" class="btn btn-info" value="Cancel" name="authorized" />
							<input type="submit" class="btn btn-primary float-right" value="Authorize" name="authorized" />
						</form>

						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>

		<footer class="footer">
			<div class="container">
				<span class="text-right">
				Copyright <a target="_blank" href="https://www.networkmanager.io/">NetworkManager</a>
				</span>
				<span class="float-right">
				Powered by <a target="_blank" href="https://www.pikeadmin.com"><b>Pike Admin</b></a>
				</span>
			</div>
		</footer>

		<?php include NM_ROOT . "includes/script.php";?>

		<!-- BEGIN Java Script for this page -->
		<!-- END Java Script for this page -->

	</body>
</html>
	<?php
exit();
}

// print the authorization code if the user has authorized your client
$is_authorized = ($_POST['authorized'] === 'Authorize');
$oauth->handleAuthorizeRequest($request, $response, $is_authorized, $auth->getUserId());
if ($is_authorized) {
    // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
    //$code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
    //exit("SUCCESS! Authorization Code: $code");
}
$response->send();