<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once '../includes/networkmanager.php';
require_once NM_CLASS_ROOT . 'oauth.php';
OAuth2\Autoloader::register();

$storage = new NetworkManager\OAuth();

$oauth = new OAuth2\Server($storage, array(
    //'enforce_state' => false, // Only used for testing via Google Playground. WIll probably make a dynamic setting later.
    'require_exact_redirect_uri' => false,
));
$oauth->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));
$oauth->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
