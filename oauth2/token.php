<?php

// include our OAuth2 Server object
require_once 'srv.php';

// Handle a request for an OAuth2.0 Access Token and send the response to the client
$oauth->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
