FROM php:8-apache

LABEL maintainer="montana.tuska@gmail.com"

ENV DOCKER_BUILD=true
RUN apt-get update
RUN apt-get install -y \
        libzip-dev \
        zip \ 
        libpng-dev
RUN docker-php-ext-configure zip
RUN docker-php-ext-install pdo pdo_mysql zip gd calendar

COPY . /var/www/html
RUN chown www-data:www-data /var/www/html
RUN chown www-data:www-data /var/www/html/install