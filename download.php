<?php

require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$module = isset($_GET['module']) ? $_GET['module'] : '';

$downloads = \NetworkManager\API::get("/core/downloads");
if ($downloads['state'] != "success") {
    exit();
}
$downloads = $downloads['data'];
if (!isset($downloads[$module])) {
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.download.".$module)) {
    // Not permitted to download!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

$url = "https://cdn.networkmanager.app/modules/".$module.".zip";
if ($module == 'web') {
    $url = "https://gitlab.com/fakerealitystudios/networkmanager-web/-/archive/master/networkmanager-web-master.zip";
}
$fh = tmpfile();
$file = stream_get_meta_data($fh)['uri'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
$raw_file_data = curl_exec($ch);
curl_close($ch);
fwrite($fh, $raw_file_data);

$oldFile = $downloads[$module]['config'];
$newFile = $downloads[$module]['config'];
if (is_array($downloads[$module]['config'])) {
    $oldFile = $oldFile[0];
    $newFile = $newFile[1];
}

if ($oldFile != '') {
    $zip = new ZipArchive;
    if (($err = $zip->open($file)) === TRUE) {
        //Read contents into memory
        $oldContents = $zip->getFromName($oldFile);
        //Modify contents:
        $newContents = $NM->template->evaluateVariables($oldContents, $config);
        //Delete the old...
        $zip->deleteName($oldFile);
        //Write the new...
        $zip->addFromString($newFile, $newContents);
        //And write back to the filesystem.
        $zip->close();
    }
}

header("Content-type: application/zip;\n");
header("Content-Transfer-Encoding: Binary");
header("Content-length: ".filesize($file).";\n");
header("Content-disposition: attachment; filename=\"".basename($module).".zip\"");

readfile($file);
fclose($fh);
exit();