<?php

$SESSION_READONLY = true;

set_time_limit(0);
ini_set('auto_detect_line_endings', 1);
ini_set('mysql.connect_timeout', '3600');
ini_set('max_execution_time', '0');

require "includes/networkmanager.php";
require NM_CLASS_ROOT . "logs.php";

if (!$auth->isLoggedIn()) {
    exit();
}

class Event
{
    protected $id;
    protected $type;
    protected $data;
    protected $retry;
    protected $comment;
    /**
     * Event constructor.
     * @param array $event [id=>id,type=>type,data=>data,retry=>retry,comment=>comment]
     */
    public function __construct(array $event)
    {
        $this->id = isset($event['id']) ? $event['id'] : null;
        $this->type = isset($event['type']) ? $event['type'] : null;
        $this->data = isset($event['data']) ? $event['data'] : null;
        $this->retry = isset($event['retry']) ? $event['retry'] : null;
        $this->comment = isset($event['comment']) ? $event['comment'] : null;
    }
    public function __toString()
    {
        $event = [];
        strlen($this->comment) > 0 and $event[] = sprintf(': %s', $this->comment); //:comments
        strlen($this->id) > 0 and $event[] = sprintf('id: %s', $this->id);
        strlen($this->retry) > 0 and $event[] = sprintf('retry: %s', $this->retry); //millisecond
        strlen($this->type) > 0 and $event[] = sprintf('event: %s', $this->type);
        strlen($this->data) > 0 and $event[] = sprintf('data: %s', $this->data);
        return implode("\n", $event) . "\n\n";
    }
}

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
header('Connection: keep-alive');
header('X-Accel-Buffering: no');

$lastId = -1;

if (isset($_SERVER["HTTP_LAST_EVENT_ID"]) && !empty($_SERVER["HTTP_LAST_EVENT_ID"]) && is_numeric($_SERVER["HTTP_LAST_EVENT_ID"])) {
    $lastId = intval($_SERVER["HTTP_LAST_EVENT_ID"]);
}

echo "retry: 2000\n\n"; // Reconnect every 2 seconds
flush();

function sendMsg($event)
{
    echo new Event($event);
    flush();
}

$checkInterval = 1;

if (isset($_GET['sid'])) {
    $server = $NM->getServer($_GET['sid']);
}

if (isset($_GET['page'])) {
    if ($_GET['page'] == "logs") {
        if (!$NM->getAuthedMember()->hasPermission("nm.logs.view")) {
            // Not permitted to view!
            exit();
        }
        if (isset($server)) {
            while (connection_aborted() === 0) {
                // Send log datas
                $condition = array();
                $condition['sid'] = $server->getId();
                if ($lastId != -1) {
                    $condition['lid[>]'] = $lastId; // get newer ids
                } elseif (!isset($_GET['limit']) || $_GET['limit'] != "false") {
                    $condition["LIMIT"] = isset($_GET['limit']) ? $_GET['limit'] : 25;
                }
                $condition["ORDER"] = array("id" => "DESC");
                $logs = new \NetworkManager\Logs($condition);
                $rows = array();
                foreach ($logs->getRows() as $k => $row) {
                    $log = $logs->getReadableLog($row);
                    if ($log == "") {
                        continue;
                    }
                    $rows[] = array(
                        "time" => $row['time'],
                        "log" => $log
                    );
                    if ($row['id'] > $lastId) {
                        $lastId = $row['id'];
                    }
                }
                $rows = array_values($rows);
                $rows = array_reverse($rows);
                if (count($rows) > 0) {
                    sendMsg([
                        'id' => $lastId,
                        'data' => json_encode($rows),
                    ]);
                } else {
                    sendMsg([
                        'comment' => "No content",
                    ]);
                }
                sleep($checkInterval);
            }
        } elseif (isset($_GET["uid"])) {
            while (connection_aborted() === 0) {
                $condition = array();
                $condition['ld.type'] = "NMID";
                $condition['ld.value'] = $_GET["uid"];
                if ($lastId != -1) {
                    $condition['lid[>]'] = $lastId; // get newer ids
                } elseif (!isset($_GET['limit']) || $_GET['limit'] != "false") {
                    $condition["LIMIT"] = isset($_GET['limit']) ? $_GET['limit'] : 25;
                }
                $condition["ORDER"] = array("id" => "DESC");
                $logs = new \NetworkManager\Logs($condition);
                $rows = array();
                foreach ($logs->getRows() as $k => $row) {
                    $log = $logs->getReadableLog($row);
                    if ($log == "") {
                        continue;
                    }
                    $rows[] = array(
                        "time" => $row['time'],
                        "log" => $log
                    );
                    if ($row['id'] > $lastId) {
                        $lastId = $row['id'];
                    }
                }
                $rows = array_values($rows);
                $rows = array_reverse($rows);
                if (count($rows) > 0) {
                    sendMsg([
                        'id' => $lastId,
                        'data' => json_encode($rows),
                    ]);
                } else {
                    sendMsg([
                        'comment' => "No content",
                    ]);
                }
                sleep($checkInterval);
            }
        }
    } elseif ($_GET['page'] == "notifications") {
        while (connection_aborted() === 0) {
            sendMsg([
                'comment' => "Notification",
            ]);
            sleep($checkInterval);
        }
    }
}

sendMsg([
    'comment' => "No content",
]);
