<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$data = array();
$data['title'] = "Geolocations | Statistics | NetworkManager";
$data['page_name'] = "Geolocations";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Statistics"
    ),
    array(
		'name' => "Geolocations"
	),
);

$data['body'] = "";
$data['body'] .= '<div class="card"><div class="card-body"><div id="world-map-markers" class="jvector-map"></div></div></div>';
//$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'dashboard.html', $data);

$data['style'] = '';
$data['style'] .= '<link href="/assets/css/jvectormap-2.0.3.css" rel="stylesheet">';
$data['style'] .= '<style>
.jvector-map {
    width: 100%;
    height: 600px;
}
</style>';

$data['javascript'] = '';
$data['javascript'] .= '<script src="/assets/js/jquery-jvectormap-2.0.3.min.js"></script>';
$data['javascript'] .= '<script src="/assets/js/jquery-jvectormap-world-mill.js"></script>';
$data['javascript'] .= '<script>
$(function () {
    var regionColors = {
        "US": "#3e9d01"
    };

    var map = $("#world-map-markers").vectorMap({
        map: "world_mill",
        normalizeFunction: "polynomial",
        hoverOpacity: 0.7,
        hoverColor: false,
        backgroundColor: "transparent",
        regionStyle: {
            initial: {
                fill: "rgba(210, 214, 222, 1)",
                "fill-opacity": 1,
                stroke: "none",
                "stroke-width": 0,
                "stroke-opacity": 1
            },
            hover: {
                "fill-opacity": 0.7,
                cursor: "pointer"
            },
            selected: {
                fill: "yellow"
            },
            selectedHover: {}
        },
        series: {
            regions: [{
                values: regionColors,
                attribute: "fill"
            }]
        }
    });
});
</script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);