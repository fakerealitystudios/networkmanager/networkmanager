<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$data = array();
$data['title'] = "Sessions | Statistics | NetworkManager";
$data['page_name'] = "Sessions";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Statistics"
    ),
    array(
		'name' => "Sessions"
	),
);

$data['body'] = "";
//$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'dashboard.html', $data);

$data['javascript'] = '';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);