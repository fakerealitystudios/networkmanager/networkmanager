<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$data = array();
$data['title'] = "Activity | Statistics | NetworkManager";
$data['page_name'] = "Activity";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Statistics"
    ),
    array(
		'name' => "Activity"
	),
);

$data['body'] = "";
//$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'dashboard.html', $data);

$box = array();
$box['title'] = "";
$box['list'] = array();
$box['list'][] = "Eventually I'll add some to this. I mean, I've got plenty of data to use...";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/box.html', $box);

$weeks = 8;
$weekInSeconds = 86400 * 7; // 86400 seconds in a day
$timespan = $weeks * $weekInSeconds;
$interval = 1;

$template = array();
$labels = array();
for ($i = $weeks; $i >= 0; $i = $i - $interval) {
    $labels[] = date('F j', time() - ($weekInSeconds * $i));
    $template[] = array();
}
$interval *= $timespan;

{
	$chart = array();
	$chart['id'] = 'activity';
	$chart['name'] = "Playerbase Development";
    $chart['labels'] = $labels;
	$chart['options'] = array(
		"spanGaps" => false,
		"scales" => array(
			"yAxes" => array(array(
				"stacked" => true,
				"ticks" => array(
					"beginAtZero" => true
				)
			))
        ),
        "tooltips" => array(
            "enabled" => false,
            "mode" => "\"index\"",
            "position" => "\"nearest\"",
            "custom" => "window.chart.tooltips"
        )
    );
    $chart['options'] = $NM->template->javascriptArray($chart['options']);
	$chart['datasets'] = array();

    $stat = array();
    foreach($template as $h => $_) {
        $stat[$h] = rand(0,5);
        /*foreach($newbs as $row) {
            
        }*/
    }
	$chart['datasets'][] = array(
		"label" => "\" Inactive Players\"",
		"data" => $stat,
		"backgroundColor" => "window.chart.colors.opaque.black",
		"borderColor" => "window.chart.colors.solid.black",
		"borderWidth" => 1,
    );
    $stat = array();
    foreach($template as $h => $_) {
        $stat[$h] = rand(0,5);
        /*foreach($newbs as $row) {
            
        }*/
    }
	$chart['datasets'][] = array(
		"label" => "\" Irregular Players\"",
		"data" => $stat,
		"backgroundColor" => "window.chart.colors.opaque.orange",
		"borderColor" => "window.chart.colors.solid.orange",
		"borderWidth" => 1,
    );
    $stat = array();
    foreach($template as $h => $_) {
        $stat[$h] = rand(0,5);
        /*foreach($newbs as $row) {
            
        }*/
    }
	$chart['datasets'][] = array(
		"label" => "\" Regular Players\"",
		"data" => $stat,
		"backgroundColor" => "window.chart.colors.opaque.yellow",
		"borderColor" => "window.chart.colors.solid.yellow",
		"borderWidth" => 1,
    );
    $stat = array();
    foreach($template as $h => $_) {
        $stat[$h] = rand(0,5);
        /*foreach($newbs as $row) {
            
        }*/
    }
	$chart['datasets'][] = array(
		"label" => "\" Active Players\"",
		"data" => $stat,
		"backgroundColor" => "window.chart.colors.opaque.green",
		"borderColor" => "window.chart.colors.solid.green",
		"borderWidth" => 1,
	);

	$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/charts/line.html', $chart);
}

$data['javascript'] = '';
$data['javascript'] .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);