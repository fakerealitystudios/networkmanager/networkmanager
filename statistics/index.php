<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$data = array();
$data['title'] = "Overview | Statistics | NetworkManager";
$data['page_name'] = "Overview";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Statistics"
    ),
    array(
		'name' => "Overview"
	),
);

$data['body'] = "";
$data['body'] .= '<div class="row">';

$days = 7; // days
$chartTimeframe = $days * 24; // into hours
$time = time() - ($chartTimeframe * 3600);
$interval = 2;

$template = array();
$labels = array();
date_default_timezone_set("America/New_York");
for ($i = $chartTimeframe; $i >= 0; $i = $i - $interval) {
    $labels[] = "new Date(\"".date('Y-m-d H:00:00', time()-($i*(3600)))."\").toLocaleString()";
    $template[] = array();
}
$interval *= 3600;

$players = array();
$newbs = $NM->db->select("members", array("count" => \Medoo\Medoo::raw("COUNT(id)"), "created", "hour" => \Medoo\Medoo::raw("HOUR(FROM_UNIXTIME(created))")), array("created[>]" => $time, "ORDER" => array("id" => "DESC"), "GROUP" => "hour"));
$unique = $NM->db->distinct()->select("memberSessions", "uid", array("ORDER" => array("id" => "DESC"), "OR" => array("endTime[>]" => $time, "endTime" => -1)));
$sessions = $NM->db->select("memberSessions", array("uid", "startTime", "endTime"), array("ORDER" => array("id" => "DESC"), "OR" => array("endTime[>]" => $time, "endTime" => -1)));
$curr = 0;
{
    foreach($template as $h => $_) {
        $uids = array();
        foreach($sessions as $row) {
            $sh = (time() - $row['startTime']);
            $sh -= ($sh % $interval);
            $sh /= $interval;

            $eh = 0;
            if ($row['endTime'] != -1) {
                $eh = (time() - $row['endTime']);
                $eh -= ($eh % $interval);
                $eh /= $interval;
            } else {
                $curr++;
            }

            if ($sh >= $h && $eh <= $h) {
                $uids[$row['uid']] = 1;
            }
        }
        $players[$h] = count($uids);
    }
}

{
    $stat = array();
    $stat['color'] = "bg-primary";
    $stat['icon'] = "fa-solid fa-user";
    $stat['title'] = "PLAYERS ONLINE";
    $stat['content'] = $curr;
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/info.html', $stat);

    $stat = array();
    $stat['color'] = "bg-warning";
    $stat['icon'] = "fa-solid fa-user";
    $stat['title'] = "NEW PLAYERS / ".$days." days";
    $stat['content'] = count($newbs);
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/info.html', $stat);

    $stat = array();
    $stat['color'] = "bg-info";
    $stat['icon'] = "fa-solid fa-user";
    $stat['title'] = "UNIQUE PLAYERS / ".$days." days";
    $stat['content'] = count($unique);
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/info.html', $stat);

    $stat = array();
    $stat['color'] = "bg-danger";
    $stat['icon'] = "fa-solid fa-user";
    $stat['title'] = "TOTAL SESSIONS / ".$days." days";
    $stat['content'] = count($sessions);
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/info.html', $stat);
}

$data['body'] .= '</div>';

{
    $chart = array();
    $chart['id'] = 'activity';
    $chart['name'] = "Online Activity Outlook / ".$days." days";
    $chart['labels'] = $labels;
    $chart['options'] = array(
        "elements" => array(
            "line" => array(
                "tension" => 0.4
            )
        ),
		"scales" => array(
			"x" => array(
                "title" => array(
                    "unit" => "\"hour\"",
                    "display" => true,
                    "text" => "\"Date\""
                )
            ),
			"y" => array(
				"beginAtZero" => true
			)
		)
    );
    $chart['options'] = $NM->template->javascriptArray($chart['options']);
    $chart['datasets'] = array();

    $stat = $players;
    $stat = array_reverse($stat);
    $chart['datasets'][] = array(
        "label" => "\"# of Players\"",
        "data" => json_encode($stat),
        "backgroundColor" => "window.chart.colors.opaque.blue",
        "borderColor" => "window.chart.colors.solid.blue",
        "borderWidth" => 1,
        "fill" => "\"start\""
    );

    $stat = array();
    foreach($template as $h => $_) {
        $stat[$h] = 0;
        foreach($newbs as $row) {
            $ch = (time() - $row['created']);
            $ch -= ($ch % $interval);
            $ch /= $interval;

            if ($ch == $h) {
                $stat[$h] += 1;
            }
        }
    }
    $stat = array_reverse($stat);
    $chart['datasets'][] = array(
        "label" => "\"# of New Players\"",
        "data" => json_encode($stat),
        "backgroundColor" => "window.chart.colors.opaque.orange",
        "borderColor" => "window.chart.colors.solid.orange",
        "borderWidth" => 1,
        "fill" => "\"start\""
    );

    $stat = array();
    foreach($template as $h => $_) {
        $stat[$h] = 0;
        foreach($sessions as $row) {
            $sh = (time() - $row['startTime']);
            $sh -= ($sh % $interval);
            $sh /= $interval;

            $eh = 0;
            if ($row['endTime'] != -1) {
                $eh = (time() - $row['endTime']);
                $eh -= ($eh % $interval);
                $eh /= $interval;
            }

            if ($sh >= $h && $eh <= $h) {
                $stat[$h] += 1;
            }
        }
    }
    $stat = array_reverse($stat);
    $chart['datasets'][] = array(
        "label" => "\"# of Sessions\"",
        "data" => json_encode($stat),
        "backgroundColor" => "window.chart.colors.opaque.red",
        "borderColor" => "window.chart.colors.solid.red",
        "borderWidth" => 1,
        "fill" => "\"start\""
    );

    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/charts/line.html', $chart);
}
$data['body'] .= '<div class="mt-5 mb-3 row">';
{
    $box = array();
    $box['title'] = "NETWORK LIFETIME INFORMATION";
    $box['list'] = array();
    $days = max(unixtojd(time()) - unixtojd($NM->settings['install_date']), 1);
    $count = $NM->db->count("members");
    $box['list'][] = '<i class="fa-solid fa-users"></i> Total Players <span class="pull-right"><b>'.$count.'</b></span>';
    $unique = $NM->db->select("memberSessions", array("players" => \Medoo\Medoo::raw("COUNT(DISTINCT <uid>)"),"day" => \Medoo\Medoo::raw("((<startTime> - (<startTime> % 86400)) / 86400)")), array("GROUP" => "day"));
    $unique_avg = 0;
    $highest = 0;
    $date = 0;
    foreach($unique as $row) {
        $unique_avg += $row['players'];
        if ($row['players'] > $highest) {
            $highest = $row['players'];
            $date = $row['day'];
        }
    }
    $date *= 86400;
    $box['list'][] = '<i class="text-primary fa-solid fa-user"></i> Unique Players / Day <small>(AVG.)</small><span class="pull-right"><b>'.round($unique_avg/$days).'</b></span>';
    $box['list'][] = '<i class="text-success fa-solid fa-user-plus"></i> New Players / Day <small>(AVG.)</small><span class="pull-right"><b>'.round($count/$days).'</b></span>';
    $sessions = $NM->db->count("memberSessions");
    $box['list'][] = '<i class="text-primary fa-solid fa-gamepad-modern"></i> Sessions / Day <small>(AVG.)</small><span class="pull-right"><b>'.round($sessions/$days).'</b></span>';
    if ($date != 0) {
        $box['list'][] = '';
        $box['list'][] = '<i class="text-success fa-solid fa-chart-line"></i> Last Peak: '.date('F j, Y', $date).'<span class="pull-right"><b>'.$highest.'</b> Players</span>';
        $box['list'][] = '<i class="text-primary fa-solid fa-chart-line"></i> All Time Peak: '.date('F j, Y', $date).'<span class="pull-right"><b>'.$highest.'</b> Players</span>';
    }
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/box.html', $box);

    $box = array();
    $box['title'] = "WEEK INFORMATION";
    $box['list'] = array();
    $last_day = time()-(7*24*3600);
    $days = max(unixtojd(time()) - unixtojd($last_day), 1);
    $unique = $NM->db->select("memberSessions", array("players" => \Medoo\Medoo::raw("COUNT(DISTINCT <uid>)"),"day" => \Medoo\Medoo::raw("((<startTime> - (<startTime> % 86400)) / 86400)")), array("startTime[>=]" => $last_day, "GROUP" => "day"));
    $unique_avg = 0;
    $highest = 0;
    $date = 0;
    foreach($unique as $row) {
        $unique_avg += $row['players'];
        if ($row['players'] > $highest) {
            $highest = $row['players'];
            $date = $row['day'];
        }
    }
    $date *= 86400;
    $box['list'][] = '<i class="text-primary fa-solid fa-user"></i> Unique Players / Day <small>(AVG.)</small><span class="pull-right"><b>'.round($unique_avg/$days).'</b></span>';
    $box['list'][] = '<i class="text-success fa-solid fa-user-plus"></i> New Players / Day <small>(AVG.)</small><span class="pull-right"><b>'.round($count/$days).'</b></span>';
    $sessions = $NM->db->count("memberSessions");
    $box['list'][] = '<i class="text-primary fa-solid fa-gamepad-modern"></i> Sessions / Day <small>(AVG.)</small><span class="pull-right"><b>'.round($sessions/$days).'</b></span>';
    if ($date != 0) {
        $box['list'][] = '';
        $box['list'][] = '<i class="text-success fa-solid fa-chart-line"></i> Last Peak: '.date('F j, Y', $date).'<span class="pull-right"><b>'.$highest.'</b> Players</span>';
        $box['list'][] = '<i class="text-primary fa-solid fa-chart-line"></i> All Time Peak: '.date('F j, Y', $date).'<span class="pull-right"><b>'.$highest.'</b> Players</span>';
    }
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/box.html', $box);

    $box = array();
    $box['title'] = "USER INFORMATION";
    $box['list'] = array();
    $total = $NM->db->get("memberSessions", array("playtime" => \Medoo\Medoo::raw("SUM(<endTime> - <startTime>)")))["playtime"];
    $box['list'][] = '<i class="text-info far fa-fw fa-clock"></i> Playtime <small>(TOTAL)</small><span class="pull-right"> '.\NetworkManager\Common::timeTillHumanReadable($total, 0).' </span>';
    $avg = $NM->db->get("memberSessions", array("playtime" => \Medoo\Medoo::raw("AVG(<endTime> - <startTime>)")))["playtime"];
    $box['list'][] = '<i class="text-info far fa-fw fa-clock"></i> Playtime <small>(AVG.)</small><span class="pull-right"> '.\NetworkManager\Common::timeTillHumanReadable($avg, 0).' </span>';
    $count = $NM->db->count("memberSessions");
    $box['list'][] = '<i class="text-info far fa-fw fa-calendar-check"></i> Sessions<span class="pull-right">'.$count.'</span>';
    $box['list'][] = '';
    $count = $NM->db->count("logs", array("type" => "PLAYER", "event" => "kill"));
    $box['list'][] = '<i class="fa-solid fa-crosshairs"></i> Player Kills<span class="pull-right">'.$count.'</span>';
    $count = $NM->db->count("logs", array("type" => "PLAYER", "event" => "death"));
    $box['list'][] = '<i class="fa-solid fa-skull"></i> Player Deaths<span class="pull-right">'.$count.'</span>';
    $count = 0;
    $box['list'][] = '<i class="text-danger fa-solid fa-xmark"></i> Punishments<span class="pull-right">'.$count.'</span>';
    $count = $NM->db->count("memberBans");
    $box['list'][] = '<i class="text-danger fa-solid fa-ban"></i> Bans<span class="pull-right">'.$count.'</span>';
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/box.html', $box);

    $box = array();
    $box['title'] = "TOTALLY";
    $box['list'] = array();
    $box['list'][] = "Didn't just copy this all from PLAN";
    $data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'statistics/box.html', $box);
}
$data['body'] .= '</div>';
//$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'dashboard.html', $data);

$data['style'] = '<link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.8.1/css/v4-shims.css" rel="stylesheet">
<style>
.small, small {
    font-size: 8px;
}
</style>';

$data['javascript'] = '';
$data['javascript'] .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"></script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);