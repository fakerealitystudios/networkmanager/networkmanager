<?php
require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.stats")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}
require "../includes/classes/statistics.php";

$stats = null;
if (isset($_GET['sid'])) {
    $stats = new \NetworkManager\Statistics\Server($_GET['sid']);
} elseif (isset($_GET['uid'])) {
    $stats = new \NetworkManager\Statistics\PlayerByNetwork($_GET['uid']);
} else {
    $stats = new \NetworkManager\Statistics\Network();
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Statistics - NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.min.css"/>
		<!-- END CSS for this page -->
	</head>

	<body class="adminbody">

		<div id="main">

			<?php include NM_ROOT . "includes/header.php";?>
			<?php include NM_ROOT . "includes/sidebar.php";?>

			<div class="content-page">

				<!-- Start content -->
				<div class="content">

					<div class="container-fluid">

						<div class="row">
							<div class="col-xl-12">
								<div class="breadcrumb-holder">
									<h1 class="main-title float-left">Statistics</h1>
									<ol class="breadcrumb float-right">
										<li class="breadcrumb-item">Home</li>
										<li class="breadcrumb-item active">Statistics</li>
									</ol>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<!-- end row -->

						<div class="row">

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
							<div class="card mb-3">
								<div class="card-header bg-dark text-white">
									<h3><i class="fa-solid fa-chart-line"></i> Player Activity</h3>
								</div>

								<div class="card-body">
									<canvas id="lineChart"></canvas>
								</div>
							</div><!-- end card-->
						</div>
						<?php if (!isset($_GET['uid'])) {?>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
							<div class="card mb-3">
								<div class="card-header bg-dark text-white">
									<h3><i class="fa-solid fa-chart-bar"></i> Geolocations</h3>
								</div>

								<div class="card-body">
									<canvas id="doughnutChart"></canvas>
								</div>
								<div class="card-footer small text-muted">Geolocation data provided by <a href="http://geoip.nekudo.com">Nekudo</a></div>
							</div><!-- end card-->
						</div>
						<?php } /*if (!isset($_GET['uid']))*/?>
					</div>
					<!-- end row -->
					<?php if (!isset($_GET['sid'])) {?>
						<div class="row">

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
								<div class="card mb-3">
									<div class="card-header bg-dark text-white">
										<h3><i class="fa-solid fa-chart-line"></i> Top Server by Sessions</h3>
									</div>

									<div class="card-body p-0">

										<table id="Top" class="table table-striped table-bordered table-responsive-xl table-hover display mb-0">
											<thead>
												<tr>
													<th style="width: 50px;">Ranking</th>
													<th>Server</th>
													<th>Sessions</th>
												</tr>
											</thead>
											<tbody>

											<?php
$i = 0;
    foreach ($stats->topServersBySessions as $sid => $sessions) {
        if ($NM->getServer($sid) == null) {continue;}
        $i++;
        ?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><a href="<?php echo NM_URL; ?>server.php?id=<?php echo $sid; ?>"><?php echo $NM->getServer($sid)->getName(); ?></a></td>
														<td><?php echo $sessions; ?></td>
													</tr>
													<?php
}
    ?>
											</tbody>
										</table>

									</div>
								</div><!-- end card-->
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
								<div class="card mb-3">
									<div class="card-header bg-dark text-white">
										<h3><i class="fa-solid fa-chart-line"></i> Top Server by Play Time</h3>
									</div>

									<div class="card-body p-0">

										<table id="Top" class="table table-striped table-bordered table-responsive-xl table-hover display mb-0">
											<thead>
												<tr>
													<th style="width: 50px;">Ranking</th>
													<th>Server</th>
													<th>Play Time</th>
												</tr>
											</thead>
											<tbody>

											<?php
$i = 0;
    foreach ($stats->topServersByPlaytime as $sid => $time) {
        if ($NM->getServer($sid) == null) {continue;}
        $i++;
        ?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><a href="<?php echo NM_URL; ?>server.php?id=<?php echo $sid; ?>"><?php echo $NM->getServer($sid)->getName(); ?></a></td>
														<td><?php echo $time; ?></td>
													</tr>
													<?php
}
    ?>
											</tbody>
										</table>

									</div>
								</div><!-- end card-->
							</div>

						</div>
						<?php } /*if (!isset($_GET['sid']))*/?>
						<!-- end row -->

						<?php if (!isset($_GET['uid'])) {?>
						<div class="row">

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
								<div class="card mb-3">
									<div class="card-header bg-dark text-white">
										<h3><i class="fa-solid fa-chart-line"></i> Top Players by Sessions</h3>
									</div>

									<div class="card-body p-0">

										<table id="Top" class="table table-striped table-bordered table-responsive-xl table-hover display mb-0">
											<thead>
												<tr>
													<th style="width: 50px;">Ranking</th>
													<th>Player</th>
													<th>Sessions</th>
												</tr>
											</thead>
											<tbody>

											<?php
$i = 0;
    foreach ($stats->topPlayersBySessions as $uid => $sessions) {
        $i++;
        ?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><a href="<?php echo NM_URL; ?>player.php?id=<?php echo $uid; ?>"><?php echo $NM->getMember($uid)->getDisplayName(); ?></a></td>
														<td><?php echo $sessions; ?></td>
													</tr>
													<?php
}
    ?>
											</tbody>
										</table>

									</div>
								</div><!-- end card-->
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
								<div class="card mb-3">
									<div class="card-header bg-dark text-white">
										<h3><i class="fa-solid fa-chart-line"></i> Top Players by Play Time</h3>
									</div>

									<div class="card-body p-0">

										<table id="Top" class="table table-striped table-bordered table-responsive-xl table-hover display mb-0">
											<thead>
												<tr>
													<th style="width: 50px;">Ranking</th>
													<th>Player</th>
													<th>Play Time</th>
												</tr>
											</thead>
											<tbody>

											<?php
$i = 0;
    foreach ($stats->topPlayersByPlaytime as $uid => $time) {
        $i++;
        ?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><a href="<?php echo NM_URL; ?>player.php?id=<?php echo $uid; ?>"><?php echo $NM->getMember($uid)->getDisplayName(); ?></a></td>
														<td><?php echo $time; ?></td>
													</tr>
													<?php
}
    ?>
											</tbody>
										</table>

									</div>
								</div><!-- end card-->
							</div>

						</div>
						<?php } /*if (!isset($_GET['uid']))*/?>
						<!-- end row -->

					</div>
					<!-- END container-fluid -->

				</div>
				<!-- END content -->

			</div>
			<!-- END content-page -->

			<?php include NM_ROOT . "includes/footer.php";?>

		</div>
		<!-- END main -->

		<?php include NM_ROOT . "includes/script.php";?>

		<!-- BEGIN Java Script for this page -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap4.min.js"></script>

			<script>
			var ctx1 = document.getElementById("lineChart").getContext('2d');
			var lineChart = new Chart(ctx1, {
				type: 'bar',
				data: {
					labels: ["7 days ago", "6 days ago", "5 days ago", "4 days ago", "3 days ago", "2 days ago", "1 day ago", "Today"],
					datasets: [{
						label: 'Player Sessions',
						backgroundColor: '#3EB9DC',
						data: [
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight -7 days"), strtotime("midnight -6 days")])); ?>,
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight -6 days"), strtotime("midnight -5 days")])); ?>,
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight -5 days"), strtotime("midnight -4 days")])); ?>,
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight -4 days"), strtotime("midnight -3 days")])); ?>,
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight -3 days"), strtotime("midnight -2 days")])); ?>,
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight -2 days"), strtotime("midnight -1 days")])); ?>,
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight -1 days"), strtotime("midnight")])); ?>,
							<?php echo $NM->db->count("memberSessions", array("endTime[<>]" => [strtotime("midnight"), time()])); ?>]
					}, {
						label: 'Unique Players',
						backgroundColor: '#32CD32',
						data: [
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight -7 days"), strtotime("midnight -6 days")])); ?>,
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight -6 days"), strtotime("midnight -5 days")])); ?>,
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight -5 days"), strtotime("midnight -4 days")])); ?>,
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight -4 days"), strtotime("midnight -3 days")])); ?>,
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight -3 days"), strtotime("midnight -2 days")])); ?>,
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight -2 days"), strtotime("midnight -1 days")])); ?>,
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight -1 days"), strtotime("midnight")])); ?>,
							<?php echo $NM->db->count("members", array("created[<>]" => [strtotime("midnight"), time()])); ?>]
					}]
				},
				options: {
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});

			var ctx3 = document.getElementById("doughnutChart").getContext('2d');
			var colorArray = [
				'#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
				'#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
				'#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
				'#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
				'#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
				'#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
				'#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
				'#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
				'#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
				'#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF',
				'#00FFFF', '#F0FFFF', '#F5F5DC', '#0000FF', '#00008B',
				'#9932CC', '#8B008B', '#8B0000', '#FF00FF', '#E9967A'
			];

			var doughnutChart = new Chart(ctx3, {
				type: 'doughnut',
				data: {
						datasets: [{
							data: [
								<?php
$rows = $NM->db->query("SELECT country, COUNT(1) FROM geolocations GROUP BY country ORDER BY country")->fetchAll();
$max = count($rows);
$i = 0;
foreach ($rows as $row) {
    $i++;
    if ($i == $max) {echo $row["COUNT(1)"];
        break;}
    echo $row["COUNT(1)"] . ",";
}
?>
							],
							backgroundColor: colorArray,
							label: 'Dataset 1'
						}],
						labels: [
							<?php
$i = 0;
foreach ($rows as $row) {
    $i++;
    if ($i == $max) {echo "\"" . $row["country"] . "\"";
        break;}
    echo "\"" . $row["country"] . "\",";
}
?>
						]
					},
					options: {
						responsive: true
					}

			});
			</script>
		<!-- END Java Script for this page -->

	</body>
</html>
