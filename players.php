<?php
require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.players")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if (isset($_GET["page"])) {
    $page = $_GET["page"];
} else {
    $page = 1;
}
$limit = isset($_GET['limit']) ? $_GET['limit'] : 25;

// All the current params
$url_params = $_GET;
unset($url_params['page']); // changes enough to be annoying to add?
if (count($url_params) == 0) { // need something, we assume we always have atleast one param down the line for the url param
    $url_params['limit'] = $limit;
}
$url = NM_URL . 'players.php?' . http_build_query($url_params);

$data = array();

$data['title'] = "Players | NetworkManager";
$data['page_name'] = "Players";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Players"
	),
);

$condition = array();
$condition["newid"] = null;

if (isset($_GET['player_search'])) {
    $condition['displayName[~]'] = $_GET['player_search'];
}
$data['player_count'] = $NM->db->count("members", $condition);
$pages = ceil($data['player_count'] / $limit);

$condition["LIMIT"] = $limit;
if ($page > 1) {
    $condition["LIMIT"] = [$limit * ($page - 1), $limit];
}

$data['players'] = $NM->db->select("members", "*", $condition);

$data['player_search'] = isset($_GET['player_search']) ? $_GET['player_search'] : '';
require NM_INCL_ROOT . "pagination.php";

$data['body'] = $NM->template->build(NM_TEMPLATE_ROOT . 'players.html', $data);

$data['style'] = '';
$data['javascript'] = '';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);