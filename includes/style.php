<!-- Bootstrap CSS -->
<link href="<?php echo NM_CDN; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo NM_CDN; ?>assets/css/bootstrap-checkbox.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo NM_CDN; ?>assets/css/summernote-bs4.min.css" rel="stylesheet" type="text/css" />

<!-- Font Awesome CSS -->
<link href="<?php echo NM_CDN; ?>assets/font-awesome/css/all.min.css" rel="stylesheet" type="text/css" />

<!-- Custom CSS -->
<link href="<?php echo NM_CDN; ?>assets/css/style.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo NM_CDN; ?>assets/css/networkmanager.min.css" rel="stylesheet" type="text/css" />