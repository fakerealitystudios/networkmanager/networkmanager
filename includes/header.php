<!-- top bar navigation -->
<div class="headerbar">

    <!-- LOGO -->
    <div class="headerbar-left">
        <a href="/" class="logo"><img alt="Logo" src="<?php echo NM_CDN; ?>assets/img/logo.png" /> <span>NetworkManager</span></a>
    </div>

    <nav class="navbar-custom">

		<ul class="list-inline float-right mb-0">

			<!--<li class="list-inline-item dropdown notif">
				<a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
					<i class="fa-solid fa-circle-question"></i>
				</a>
				<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-arrow-success dropdown-lg">
					<div class="dropdown-item noti-title">
						<h5><small>Help and Support</small></h5>
					</div>
					<a title="Contact Us" target="_blank" href="<?php echo NM_URL; ?>contact.php" class="dropdown-item notify-item notify-all">
						<i class="fa-solid fa-link"></i> Contact Us
					</a>

				</div>
			</li>-->

			<?php if (!$auth->isLoggedIn()) {?>
                <a href="<?php echo NM_URL; ?>login.php">Login</a>
            <?php } else {
    $rows = $NM->db->select("memberNotifications", "*", array("uid" => $NM->getAuthedMember()->getId(), "ORDER" => ["id" => "DESC"]));
    $color = "success";
    if (count($rows) > 10) {
        $color = "danger";
    } elseif (count($rows) > 3) {
        $color = "warning";
    }
    ?>
			<li class="list-inline-item dropdown notif">
				<a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
					<i class="fa-solid fa-bell"></i><span class="notif-bullet badge badge-pill badge-<?php echo $color; ?>"><?php echo count($rows); ?></span>
				</a>
				<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg">
					<!-- item-->
					<div class="dropdown-item noti-title">
						<h5><small>Notifications</small></h5>
					</div>

					<?php
foreach ($rows as $row) {
        ?>
						<!-- item-->
						<a href="<?php echo isset($row['link']) ? NM_URL . $row['link'] : "#"; ?>" class="dropdown-item notify-item">
							<div class="notify-icon bg-faded">
								<img src="<?php echo $NM->getAuthedMember()->getAvatar(); ?>" alt="img" class="rounded-circle img-fluid">
							</div>
							<p class="notify-details">
								<b><?php echo $row['header']; ?></b>
								<span><?php echo $row['info']; ?></span>
								<small class="text-muted"><?php echo \NetworkManager\Common::timeElapsedString($row['time']); ?></small>
							</p>
						</a>
						<?php
}
    unset($rows);
    unset($row);
    ?>

					<a href="<?php echo NM_URL; ?>profile/notifications.php" class="dropdown-item notify-item notify-all">View All Notifications</a>
				</div>
			</li>

			<li class="list-inline-item dropdown notif">
				<a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
					<img src="<?php echo $NM->getAuthedMember()->getAvatar(); ?>" alt="Profile image" class="avatar-rounded">
				</a>
				<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
					<!-- item-->
					<div class="dropdown-item noti-title">
						<h5 class="text-overflow"><small><?php echo $NM->getAuthedMember()->getDisplayName(); ?></small> </h5>
					</div>

					<!-- item-->
					<a href="<?php echo NM_URL; ?>profile/" class="dropdown-item notify-item">
						<i class="fa-solid fa-user"></i> <span>Profile</span>
					</a>

					<!-- item-->
					<a href="<?php echo NM_URL; ?>logout.php" class="dropdown-item notify-item">
						<i class="fa-solid fa-power-off"></i> <span>Logout</span>
					</a>
				</div>
			</li>
			<?php }?>

		</ul>

		<ul class="list-inline menu-left mb-0">
			<li class="float-left">
				<button class="button-menu-mobile open-left">
					<i class="fa-solid fa-bars"></i>
				</button>
			</li>
		</ul>

    </nav>

</div>
<!-- End Navigation -->
