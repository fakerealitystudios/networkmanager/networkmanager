<?php
$lgames = array();
foreach ($NM->getServers() as $lserver) {
    if (!isset($lgames[$lserver->getGame()])) {
        $lgames[$lserver->getGame()] = array();
    }
    $lgames[$lserver->getGame()][] = $lserver;
}

$sidebar = array();
$sidebar[] = array(
    "Dashboard",
    "fa-solid fa-bars",
    NM_URL."index.php"
);
$sidebar[] = array(
    "Servers",
    "fa-solid fa-server",
    array()
);
$i = 1;
$ii = 0;
foreach($lgames as $lgame => $lservers) {
    $sidebar[1][2][$ii] = array(
        $lgame,
        array()
    );
    if (count($lservers) > 1) {
        $iii = 0;
        foreach ($lservers as $lserver) {
            $sidebar[$i][2][$ii][1][$iii] = array(
                $lserver->getName(),
                array()
            );
            $sidebar[$i][2][$ii][1][$iii][1][] = array(
                "Server Info",
                NM_URL."server.php?id=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][$iii][1][] = array(
                "Settings",
                NM_URL."settings/server.php?sid=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][$iii][1][] = array(
                "Statistics",
                NM_URL."statistics/server.php?sid=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][$iii][1][] = array(
                "Bans",
                NM_URL."bans.php?sid=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][$iii][1][] = array(
                "Logs",
                NM_URL."logs.php?sid=".$lserver->getId()
            );
            $iii++;
        }
    } else {
        $lserver = $lservers[0];
        if ($lgame == "web") {
            $sidebar[$i][2][$ii][1][] = array(
                "Logs",
                NM_URL."logs.php?sid=".$lserver->getId()
            );
        } elseif ($lgame == "processor") {
            $sidebar[$i][2][$ii][1][] = array(
                "Settings",
                NM_URL."settings/server.php?sid=".$lserver->getId()
            );
        } elseif ($lgame == "discord") {
            $sidebar[$i][2][$ii][1][] = array(
                "Settings",
                NM_URL."settings/server.php?sid=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][] = array(
                "Logs",
                NM_URL."logs.php?sid=".$lserver->getId()
            );
        } else {
            $sidebar[$i][2][$ii][1][] = array(
                "Server Info",
                NM_URL."server.php?id=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][] = array(
                "Settings",
                NM_URL."settings/server.php?sid=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][] = array(
                "Statistics",
                NM_URL."statistics/server.php?sid=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][] = array(
                "Bans",
                NM_URL."bans.php?sid=".$lserver->getId()
            );
            $sidebar[$i][2][$ii][1][] = array(
                "Logs",
                NM_URL."logs.php?sid=".$lserver->getId()
            );
        }
    }
    $ii++;
}
$sidebar[] = array(
    "Logs",
    "fa-solid fa-list",
    NM_URL."logs.php"
);
$i = count($sidebar);
$sidebar[$i] = array(
    "Statistics",
    "fa-solid fa-chart-pie",
    array(
        array(
            "Overview",
            "fa-solid fa-info",
            NM_URL."statistics/index.php"
        ),
        array(
            "Activity",
            "fa-solid fa-chart-line",
            NM_URL."statistics/activity.php"
        ),
        array(
            "Sessions",
            "fa-solid fa-signal",
            NM_URL."statistics/sessions.php"
        ),
        array(
            "Performance",
            "fa-solid fa-chart-pie",
            NM_URL."statistics/performance.php"
        ),
        array(
            "Geolocations",
            "fa-solid fa-globe",
            NM_URL."statistics/geolocations.php"
        ),
    )
);
$sidebar[] = array(
    "Players",
    "fa-solid fa-users",
    NM_URL."players.php"
);
$sidebar[] = array(
    "Bans",
    "fa-solid fa-ban",
    NM_URL."bans.php"
);
$sidebar[] = array(
    "Tickets",
    "fa-solid fa-exclamation",
    NM_URL."tickets.php"
);
$sidebar[] = array(
    "Applications",
    "fa-solid fa-user-secret",
    NM_URL."applications.php"
);
$sidebar[] = array(
    "Permissions",
    "fa-solid fa-lock",
    NM_URL."settings/permissions.php"
);
$i = count($sidebar);
$sidebar[$i] = array(
    "Settings",
    "fa-solid fa-folder-gear",
    array()
);
$sidebar[$i][2][] = array(
    "Web Panel",
    "fa-solid fa-wrench",
    array()
);
$sidebar[$i][2][0][2][] = array(
    "General",
    "fa-solid fa-gear",
    NM_URL."settings/index.php"
);
$sidebar[$i][2][0][2][] = array(
    "Email",
    "fa-solid fa-envelope",
    NM_URL."settings/email.php"
);
$sidebar[$i][2][0][2][] = array(
    "Authentication",
    "fa-solid fa-lock",
    NM_URL."settings/authentication.php"
);
$sidebar[$i][2][0][2][] = array(
    "Quick Actions",
    "fa-solid fa-sliders",
    NM_URL."settings/quickactions.php"
);
$sidebar[$i][2][0][2][] = array(
    "API Keys",
    "fa-solid fa-key",
    NM_URL."settings/api.php"
);
$sidebar[$i][2][0][2][] = array(
    "OAuth2 Keys",
    "fa-solid fa-certificate",
    NM_URL."settings/oauth.php"
);
$sidebar[$i][2][] = array(
    "Badges",
    "fa-solid fa-badge",
    NM_URL."settings/badges.php"
);
$sidebar[$i][2][] = array(
    "Ranks",
    "fa-solid fa-users",
    NM_URL."settings/ranks.php"
);
$sidebar[$i][2][] = array(
    "Languages",
    "fa-solid fa-language",
    NM_URL."settings/languages.php"
);
if (!function_exists('buildSidebar')) {
    function buildSidebar($arr) {
        $text = "";
        foreach($arr as $part) {
            $i = 1;
            $class = "";
            if (count($part) == 3) {
                $i = 2;
                $class = $part[1];
            }
            if (is_array($part[$i])) {
                $text .= '<li class="submenu">';
                $text .= '<a>';
                if ($class != "") {
                    $text .= '<i class="'.$class.'"></i>';
                }
                $text .= '<span>'.$part[0].'</span><span class="menu-arrow"></span>';
                $text .= '</a>';
                $text .= '<ul>';
                $text .= buildSidebar($part[$i]);
                $text .= '</ul>';
            } else {
                $text .= '<li>';
                $text .= '<a ';
                if ($part[$i] == NM_URL_FULL) {
                    $text .= 'class="active" ';
                }
                $text .= 'href="'.$part[$i].'">';
                if ($class != "") {
                    $text .= '<i class="'.$class.'"></i>';
                }
                $text .= '<span>'.$part[0].'</span>';
                $text .= '</a>';
            }
            $text .= '</li>';
        }
        return $text;
    }
}
?>
<!-- Left Sidebar -->
<div class="left main-sidebar">

    <div class="sidebar-inner leftscroll">

        <div id="sidebar-menu">

            <ul>
                <?php echo buildSidebar($sidebar); ?>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>

    </div>

</div>
<!-- End Sidebar -->
<?php

unset($i);
unset($ii);
unset($iii);
unset($sidebar);
unset($lgames);
unset($lgame);
unset($lservers);
unset($lserver);

?>