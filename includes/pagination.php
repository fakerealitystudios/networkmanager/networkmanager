<?php

// We require certain variables to setup pagination
if (!isset($data, $limit, $page, $pages))
    return;

// All the current params
$url_params = $_GET;
unset($url_params['page']); // changes enough to be annoying to add?
if (count($url_params) == 0) { // need something, we assume we always have atleast one param down the line for the url param
    $url_params['limit'] = $limit;
}

$data['url_params'] = $url_params;
$data['page_url'] = NM_URL_DIR . '?' . http_build_query($url_params) . "&";
$data['page'] = $page;
$data['page_next'] = $page + 1;
$data['page_prev'] = $page - 1;
if($pages <= 1) {
    $data['pages'] = array(1);
} elseif($pages == 2) {
    $data['pages'] = array(1,2);
} elseif($pages == 3) {
    $data['pages'] = array(1,2,3);
} elseif($pages == 4) {
    $data['pages'] = array(1,2,3,4);
} elseif($pages == 5) {
    $data['pages'] = array(1,2,3,4,5);
} elseif ($page > ($pages - 3)) {
    $data['pages'] = array(1,"-1",$pages-4,$pages-3,$pages-2,$pages-1,$pages);
} elseif($page < 4) {
    $data['pages'] = array(1,2,3,4,5,"-1",$pages);
} else {
    $data['pages'] = array(1,"-1",$page-2,$page-1,$page,$page+1,$page+2,"-1",$pages);
}