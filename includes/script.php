<script src="<?php echo NM_CDN; ?>assets/js/modernizr.min.js"></script>
<script src="<?php echo NM_CDN; ?>assets/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js"></script>
<script src="<?php echo NM_CDN; ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo NM_CDN; ?>assets/js/summernote-bs4.min.js"></script>

<script src="<?php echo NM_CDN; ?>assets/js/detect.js"></script>
<script src="<?php echo NM_CDN; ?>assets/js/fastclick.js"></script>
<script src="<?php echo NM_CDN; ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo NM_CDN; ?>assets/js/jquery.sortable.js"></script>

<!-- App js -->
<script src="<?php echo NM_CDN; ?>assets/js/pikeadmin.js"></script>
<script src="<?php echo NM_CDN; ?>assets/js/networkmanager.js"></script>

<script>
	function prettyDate2(time){
		var date = new Date(0);
		date.setUTCSeconds(time);
		return  date.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit', second:'2-digit'});
	}
</script>

<?php if (count($_alerts) > 0) {?>
<script>
	<?php foreach ($_alerts as $alert) {
    if (!isset($alert['type'])) {
        $alert['type'] = 'success';
    }
    ?>
		var notify = $.notify({message: '<?php echo $alert['text']; ?>'}, { type: '<?php echo $alert['type']; ?>', allow_dismiss: true });
	<?php }?>
</script>
<?php }?>

<?php /*if ($auth->isLoggedIn()) {*/?>
<?php if (false) {?>
<!-- BEGIN Notifications -->
<script>
	$(function() {
		if (!!window.EventSource) {
			var notification_source = new EventSource("<?php echo NM_URL_PATH; ?>stream.php?page=notifications");
		}

		notification_source.addEventListener('message', function(e) {
			console.log("SSE Notifications: " + e.data);
			var div = document.getElementById('notifications');
			var notifications = JSON.parse(e.data);
			notifications.forEach(function (notification) {
				//div.innerHTML += "<div><span>"+prettyDate2(log.time)+"</span> - <span>"+log.log+"</span></div>";
			});
		}, false);

		notification_source.addEventListener('open', function(e) {
			// Connection was opened.
			console.log("SSE Notifications: Opened");
		}, false);

		notification_source.addEventListener('error', function(e) {
			if (e.readyState == EventSource.CLOSED) {
				console.log("SSE Notifications: Closed");
			}
		}, false);
	});
</script>
<!-- END Notifications -->
<?php }?>