<?php

error_reporting(E_ALL);
ini_set('display_errors', 0);

$nm_version = "1.2.0";
$version = explode('.', $nm_version);
define('NM_VERSION_DELIMITED', $nm_version);
define('NM_VERSION_MAJOR', $version[0]);
define('NM_VERSION', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
unset($version);

define('NM_API_SITE', "https://api.networkmanager.center");

define('NM_ROOT', dirname(dirname(__FILE__)) . "/");
define('NM_INCL_ROOT', NM_ROOT . 'includes/');
define('NM_CLASS_ROOT', NM_ROOT . 'includes/classes/');
define('NM_LIB_ROOT', NM_ROOT . 'includes/classes/libraries/');
$schema = (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) ? $_SERVER['HTTP_X_FORWARDED_PROTO'] : (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on")) ? "https" : "http";
$path = substr($_SERVER['SCRIPT_FILENAME'], strlen(NM_ROOT) - 1);
$path = substr($path, 0, strlen($path) - strlen($_SERVER['SCRIPT_NAME']));
if (substr($path, -1) != "/") {
    $path .= "/";
}

$environment = "production";
if (file_exists(NM_ROOT."development")) {
    ini_set('display_errors', 1);
    $environment = "development";
}

if ($environment == "production") {
	ini_set('log_errors', 1);
}

define('NM_URL_HOST', $_SERVER['HTTP_HOST']); // This is the domain with name only. No schema or path.
define('NM_URL_DIR', str_replace('\\', '/', substr($_SERVER['SCRIPT_FILENAME'], strlen(NM_ROOT)))); // This is the current path within the panel. Used to know what file we are in
define('NM_URL_PATH', $path); // This is the path to the base of the panel. Used for loading assets. i.e. "/" or "/networkmanager"

define('NM_URL', $schema . "://" . $_SERVER['HTTP_HOST'] . $path); // This is the panel's base location.
$query = "";
if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != "") {
    $query = "?".$_SERVER['QUERY_STRING'];
}
define('NM_URL_FULL', NM_URL.NM_URL_DIR.$query); // This is the current full URL path(minus any query parameters)

unset($dir);
unset($path);

require_once NM_INCL_ROOT . 'composer/vendor/autoload.php';

\Sentry\init([
    'dsn' => 'https://5aba20b0243940ac9c5c6f0888fe2f28@o1207037.ingest.sentry.io/6340768',
    'release' => $nm_version,
    'environment' => $environment
]);

require_once NM_CLASS_ROOT . "api.php";

// Used for current page popup info
// http://bootstrap-notify.remabledesigns.com/
$_alerts = array();

$errors = array();

// We don't need the following stuff if we're just installing
if (defined("NM_INSTALL")) {
    // Set the CDN url to local when performing install
    define('NM_CDN', NM_URL_PATH);
    return;
}

if (substr($_SERVER['SCRIPT_NAME'], -18) != '/install/index.php' && !file_exists(NM_ROOT . "install/lock")) {
    header('Location: ' . NM_URL . 'install/');
    exit();
}

require_once NM_ROOT . "config.php";

if (isset($config['cdn_url']))
    define('NM_CDN', $config['cdn_url']);
else
    define('NM_CDN', NM_URL_PATH);

require_once NM_CLASS_ROOT . "mail.php";
require_once NM_CLASS_ROOT . "database.php";
require_once NM_CLASS_ROOT . "rankmanager.php";
require_once NM_CLASS_ROOT . "membermanager.php";
require_once NM_CLASS_ROOT . "servermanager.php";
require_once NM_CLASS_ROOT . "authentication.php";
require_once NM_CLASS_ROOT . "template.php";
require_once NM_LIB_ROOT . "mobile_detect.php";

// Load session
require_once NM_INCL_ROOT . "session.php";

$detect = new Mobile_Detect;

class NetworkManager
{
    private static $instance;

    protected $realms;

    public $db;
    public $rankManager;
    public $memberManager;
    public $serverManager;
    public $auth;
    public $template;

    public $perms;
    public $settings;
    public $config;
    public $member;

    private $loaded;

    private function __construct()
    {
        $this->loaded = microtime(true);

        $this->db = \NetworkManager\Database::getInstance();
        $this->rankManager = \NetworkManager\RankManager::getInstance();
        $this->memberManager = \NetworkManager\MemberManager::getInstance();
        $this->serverManager = \NetworkManager\ServerManager::getInstance();
        $this->auth = \NetworkManager\Authentication::getInstance();
    }

    public static function getRuntime()
    {
        return microtime(true) - NetworkManager::getInstance()->loaded;
    }

    // Grabs the singleton
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getAuthedMember()
    {
        if (isset($this->member)) {
            return $this->member;
        } elseif ($this->auth->isLoggedIn()) {
            $this->member = $this->getMember($this->auth->getUserId(), true);
            return $this->member;
        }
        return new \NetworkManager\MemberNull();
    }

    public function getMember($uid, $fullMember = false)
    {
        if ($fullMember) {
            return $this->memberManager->getFull($uid);
        } else {
            return $this->memberManager->get($uid);
        }
    }

    public function getRankLevel($inherit) {
        $level = 0;

        foreach($this->getRanks() as $rank) {
            if ($inherit == $rank->getRank()) {
                $level += 1;
                if ($rank->getParent() != NULL) {
                    $level += $this->getRankLevel($rank->getParent());
                }
                break;
            }
        }
        return $level;
    }

    private $leveledRanks = null;

    public function getRanksLeveled()
    {
        if (isset($this->leveledRanks)) {
            return $this->leveledRanks;
        }

        $rankrows = $this->getRanks();
        usort($rankrows, function($a, $b) {
            $vala = $this->getRankLevel($a->getParent());
            $valb = $this->getRankLevel($b->getParent());
            if ($vala == $valb)
                return 0;
            return ($vala < $valb) ? -1 : 1;
        });

        $this->leveledRanks = $rankrows;

        return $rankrows;
    }

    public function getRanks()
    {
        return $this->rankManager->getRanks();
    }

    public function getRank($rank)
    {
        return $this->rankManager->get($rank);
    }

    public function getGames()
    {
        $games = array();
        foreach ($this->serverManager->getGames() as $game => $gamemodes) {
            $games[] = $game;
        }
        return $games;
    }

    public function getServers()
    {
        return $this->serverManager->getAll();
    }

    public function getServer($sid)
    {
        return $this->serverManager->get($sid);
    }

    public function log($logType, $log)
    {
        /*$this->db->insert("logs", array(
    "sid" => 1,
    "type" => $logType,
    "log" => $log,
    ));*/
    }

    public function getUrlPath()
    {
        
    }

    public function getRealms()
    {
        if (isset($this->realms)) {
            return $this->realms;
        }
        // Key is the realm, value is the written type for the user
        $realms = array();
        $realms["*"] = "*";
        foreach ($this->serverManager->getGames() as $game => $gamerealms) {
            $realms["*." . $game] = "*." . $game;
            foreach ($gamerealms as $gamerealm) {
                $realms["*." . $game . "." . $gamerealm] = "*." . $game . "." . $gamerealm;
            }
        }
        foreach ($this->serverManager->getAll() as $server) {
            $realms[$server->getId() . "." . $server->getGame()] = $server->getName();
            foreach ($server->getGameRealms() as $gamerealm) {
                $realms[$server->getId() . "." . $server->getGame() . "." . $gamerealm] = $server->getName() . " - " . $gamerealm;
            }
        }
        $this->realms = $realms;
        return $this->realms;
    }

    public function getReadableRealm($realm)
    {
        $this->getRealms();
        if (isset($this->realms[$realm])) {
            return $this->realms[$realm];
        }
        return $realm;
    }

    public function isMobile()
    {
        global $detect;
        return $detect->isMobile();
    }
}

$GLOBALS['NM'] = NetworkManager::getInstance();

if (!isset($NM)) {
    http_response_code(500);
    exit();
}
$auth = $NM->auth; // Create global variable(ease of access to the authentication class)

if (!isset($_SESSION['permissions'])) {
    foreach($NM->db->select("permissions", array("perm", "game")) as $v) {
        if (!isset($_SESSION['permissions'][$v['game']]))
            $_SESSION['permissions'][$v['game']] = array();
        $_SESSION['permissions'][$v['game']][] = $v['perm'];
    }
    $NM->perms = &$_SESSION['permissions'];
} else {
    $NM->perms = &$_SESSION['permissions'];
}

$NM->settings = array();
$rows = $NM->db->select("settings", array("name", "value"), array("realm" => "*"));
foreach ($rows as $row) {
    $NM->settings[$row["name"]] = $row["value"];
}

$rows = $NM->db->select("settings", array("name", "value"), array("realm" => "*.web")); // web realm overrides * realm
foreach ($rows as $row) {
    $NM->settings[$row["name"]] = $row["value"];
}

$db_version = $NM->settings['version'];
$db_version = explode('.', $db_version);
define('DB_VERSION', ($db_version[0] * 10000 + $db_version[1] * 100 + $db_version[2]));
$db_version = implode(".", $db_version);

define('NM_TEMPLATE', isset($NM->settings['template']) ? $NM->settings['template'] : 'default');
define('NM_TEMPLATE_ROOT', NM_ROOT.'templates/'.NM_TEMPLATE.'/');

$NM->template = \NetworkManager\Template::getInstance();

$rows = null;

if (DB_VERSION > 10002 && $NM->getAuthedMember()->isValid() && !$NM->getAuthedMember()->isSetup()) {
    if (substr(NM_URL_DIR, 0, 10) == "logout.php") {
        
    } elseif (substr(NM_URL_DIR, 0, 7) != "profile") {
        header('Location: ' . NM_URL . 'profile/');
        exit();
    } else {
        $_alerts[] = array(
            'type' => 'danger',
            'text' => 'You must set your display name and username to continue!',
        );
    }
}

if (\NetworkManager\API::getInstanceId() == null) {
    $errors[] = "You currently have no instance id for the NetworkManager API! Click <a href='#'>here</a> to attempt to create one.<br>
    Missing an instance id disables some features and is required for NetworkManager Center.";
} else {
    \Sentry\configureScope(function (\Sentry\State\Scope $scope) use ($NM): void {
        $scope->setExtra('instance-id', \NetworkManager\API::getInstanceId());
    });
}

// The items following this are for the human website
if (defined("NM_API")) {
    return;
}

if ($NM->getAuthedMember() != null) {
    \Sentry\configureScope(function (\Sentry\State\Scope $scope) use ($NM): void {
        $scope->setUser([
            'id' => $NM->getAuthedMember()->getId()
        ]);
    });
}

$site_version = \NetworkManager\API::get("/core/version/networkmanager");
if (isset($site_version['data']['version']))
    $site_version = $site_version['data']['version'];
else
    $site_version = $nm_version;
$site_version = explode('.', $site_version);
define('SITE_VERSION', ($site_version[0] * 10000 + $site_version[1] * 100 + $site_version[2]));
$site_version = implode(".", $site_version);
if (NM_VERSION < SITE_VERSION) {
    $errors[] = "A new version of the NetworkManager web panel is available to download! Current installed version is ".$nm_version."<br>
    Please install the new version, then perform an upgrade.";
} else if (NM_VERSION > SITE_VERSION) {
    $errors[] = "You are using a beta build of Network Manager. Be aware some unforseen errors may occur.";
}

if (DB_VERSION < NM_VERSION) {
    $errors[] = "You have a newer version of the web panel installed then your database! Please perform an <a href='".NM_URL."/install/upgrade/'>upgrade</a><br>
    Not performing an upgrade could result in unknown, and unsupported, behaviors!";
} else if (DB_VERSION > NM_VERSION) {
    $errors[] = "You have a newer version of the database installed then your web panel! Please download the latest version.<br>
    Not performing an upgrade could result in unknown, and unsupported, behaviors!";
}
