<?php

namespace NetworkManager\LinkMethods;

// Used for things like openid
abstract class Base
{
    // Returns a boolean
    abstract public function isEnabled();

    abstract public function isLogin();

    abstract public function getTitle();

    // Unique name for storing in memberLinks
    abstract public function getType();
    // Should return the URL for users to goto to login(with a return parameter)
    abstract public function generateUrl($returnTo);
    // Returns a string token or false
    abstract public function getToken();

    // Returns a URL
    abstract public function getProfile($token);
    // Returns HTML
    abstract public function getButton();
    // Returns HTML for 16x16 icon
    abstract public function getIcon();
}
