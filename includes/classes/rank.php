<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/database.php";
require_once NM_ROOT . "includes/classes/permissions.php";
require_once NM_ROOT . "includes/classes/realm.php";

class Rank extends RealmObject
{
    use Permissions;

    private $db = null;

    // rank data
    private $rank = "";
    private $displayName = "";
    private $inherit = "";
    private $subgrouping = 0;
    private $data = array();
    private $realm = "";

    public function __construct($rank, $displayName, $inherit, $subgrouping, $data, $realm)
    {
        $this->db = Database::getInstance();

        $this->rank = $rank;
        $this->displayName = $displayName;
        $this->inherit = $inherit;
        $this->subgrouping = $subgrouping;
        $this->data = $data;
        $this->realm = $realm;
    }

    public function isValid()
    {
        return $this->rank != "";
    }

    public function getRank()
    {
        if (!$this->isValid()) {
            return null;
        }
        return $this->rank;
    }

    public function getRankRealm()
    {
        if (!$this->isValid()) {
            return null;
        }
        return $this->realm;
    }

    public function getDisplayName()
    {
        if (!$this->isValid()) {
            return null;
        }
        return $this->displayName;
    }

    public function getParent()
    {
        if (!$this->isValid()) {
            return null;
        }
        return $this->inherit;
    }

    public function getInherit()
    {
        if (!$this->isValid()) {
            return null;
        }
        return $this->inherit;
    }

    public function getSubGrouping()
    {
        if (!$this->isValid()) {
            return null;
        }
        return $this->subgrouping;
    }

    public function canTarget($group2)
    {
        return RankManager::getInstance()->canTarget($this, $group2);
    }
}
