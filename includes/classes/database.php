<?php

namespace NetworkManager;

require_once NM_CLASS_ROOT . "medoo_nm.php";

class Database
{
    private static $instance;
    private $connection;

    private function __construct()
    {
        global $config;
        $config['database']['port'] = intval($config['database']['port']);
        try {
            $this->connection = new NMMedoo($config['database']);
        } catch (\PDOException $e) {
            echo "Database initialization failed!<br>";
            echo $e->getMessage();
            http_response_code(500);
            //var_dump($e);
            exit();
        }
    }

    public function __destruct()
    {
        //$this->connection->close();
    }

    // Grabs the singleton
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance->connection;
    }
}
