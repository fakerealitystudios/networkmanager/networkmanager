<?php

namespace NetworkManager;

require_once NM_CLASS_ROOT . "database.php";

class OAuth implements
\OAuth2\Storage\AuthorizationCodeInterface,
\OAuth2\Storage\AccessTokenInterface,
\OAuth2\Storage\ClientCredentialsInterface,
\OAuth2\Storage\UserCredentialsInterface,
\OAuth2\Storage\RefreshTokenInterface,
\OAuth2\Storage\ScopeInterface,
\OAuth2\OpenID\Storage\UserClaimsInterface,
\OAuth2\OpenID\Storage\AuthorizationCodeInterface
{
    private $db;

    private $validScopes = array(
        'identity' => false,
        'email' => false,
    );

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function getUser($nmid)
    {
        $row = $this->db->get("members", "*", array(
            "id" => $nmid,
        ));

        $row['nmid'] = $row['id'];
        unset($row['id']);

        return $row;
    }

    // AuthorizationCodeInterface
    public function getAuthorizationCode($code)
    {
        $row = $this->db->get("oauthAuthorizations", "*", array("authorization_code" => $code));

        if ($row) {
            $row['expires'] = strtotime($row['expires']);
            $row['user_id'] = $row['uid'];
            unset($row['uid']);
        }

        return $row;
    }

    public function setAuthorizationCode($code, $client_id, $user_id, $redirect_uri, $expires, $scope = null, $id_token = null)
    {
        // convert expires to datestring
        $expires = date('Y-m-d H:i:s', $expires);

        // if it exists, update it.
        if ($this->getAuthorizationCode($code)) {
            $stmt = $this->db->update("oauthAuthorizations", array(
                "client_id" => $client_id,
                "uid" => $user_id,
                "redirect_uri" => $redirect_uri,
                "expires" => $expires,
                "scope" => $scope,
                "id_token" => $id_token,
            ), array(
                "authorization_code" => $code,
            ));
        } else {
            $stmt = $this->db->insert("oauthAuthorizations", array(
                "authorization_code" => $code,
                "client_id" => $client_id,
                "uid" => $user_id,
                "redirect_uri" => $redirect_uri,
                "expires" => $expires,
                "scope" => $scope,
                "id_token" => $id_token,
            ));
        }

        return $stmt->rowCount() > 0;
    }

    public function expireAuthorizationCode($code)
    {
        $stmt = $this->db->delete("oauthAuthorizations", array(
            "authorization_code" => $code,
        ));

        return $stmt->rowCount() > 0;
    }

    // AccessTokenInterface
    public function getAccessToken($oauth_token)
    {
        $row = $this->db->get("oauthTokens", "*", array(
            "access_token" => $oauth_token,
        ));

        if ($row) {
            $row['expires'] = strtotime($row['expires']);
            $row['user_id'] = $row['uid'];
            unset($row['uid']);
        }

        return $row;
    }

    public function setAccessToken($oauth_token, $client_id, $user_id, $expires, $scope = null)
    {
        // convert expires to datestring
        $expires = date('Y-m-d H:i:s', $expires);

        // if it exists, update it.
        if ($this->getAccessToken($oauth_token)) {
            $stmt = $this->db->update("oauthTokens", array(
                "client_id" => $client_id,
                "uid" => $user_id,
                "redirect_uri" => $redirect_uri,
                "scope" => $scope,
            ), array(
                "access_token" => $oauth_token,
            ));
        } else {
            $stmt = $this->db->insert("oauthTokens", array(
                "access_token" => $oauth_token,
                "client_id" => $client_id,
                "uid" => $user_id,
                "expires" => $expires,
                "scope" => $scope,
            ));
        }

        return $stmt->rowCount() > 0;
    }

    public function unsetAccessToken($oauth_token)
    {
        $stmt = $this->db->delete("oauthTokens", array(
            "access_token" => $oauth_token,
        ));

        return $stmt->rowCount() > 0;
    }

    // ClientInterface (from ClientCredentialsInterface)
    public function getClientDetails($client_id)
    {
        $row = $this->db->get("oauthClients", "*", array(
            "client_id" => $client_id,
        ));

        return $row;
    }

    public function getClientScope($client_id)
    {
        if (!$clientDetails = $this->getClientDetails($client_id)) {
            return false;
        }

        if (isset($clientDetails['scope'])) {
            return $clientDetails['scope'];
        }

        return null;
    }

    public function checkRestrictedGrantType($client_id, $grant_type)
    {
        $details = $this->getClientDetails($client_id);
        if (isset($details['grant_types'])) {
            $grant_types = explode(' ', $details['grant_types']);

            return in_array($grant_type, (array) $grant_types);
        }

        // if grant_types are not defined, then none are restricted
        return true;
    }

    // ClientCredentialsInterface
    public function checkClientCredentials($client_id, $client_secret = null)
    {
        $secret = $this->db->get("oauthClients", "client_secret", array(
            "client_id" => $client_id,
        ));

        return $secret && $secret == $client_secret;
    }

    public function isPublicClient($client_id)
    {
        $secret = $this->db->get("oauthClients", "client_secret", array(
            "client_id" => $client_id,
        ));

        return $secret && empty($secret);
    }

    // UserCredentialsInterface
    public function checkUserCredentials($username, $password)
    {
        // Not accepting this rn
        return false;
    }

    public function getUserDetails($username)
    {
        return $this->getUser($username);
    }

    // RefreshTokenInterface
    public function getRefreshToken($refresh_token)
    {
        return false;
    }

    public function setRefreshToken($refresh_token, $client_id, $user_id, $expires, $scope = null)
    {
        return false;
    }

    public function unsetRefreshToken($refresh_token)
    {
        return false;
    }

    // ScopeInterface
    public function scopeExists($scope)
    {
        $scopes = explode(' ', $scope);
        foreach ($scopes as $lscope) {
            if (!isset($this->validScopes[$lscope])) {
                return false;
            }
        }
        return true;
    }

    public function getDefaultScope($client_id = null)
    {
        $scopes = array();
        foreach ($this->validScopes as $scope => $def) {
            if ($def) {
                $scopes[] = $scope;
            }

        }
        return $scopes;
    }

    // UserClaimsInterface
    public function getUserClaims($user_id, $scope)
    {
        if (!$userDetails = $this->getUser($user_id)) {
            return false;
        }

        $claims = explode(' ', trim($claims));
        $userClaims = array();

        // for each requested claim, if the user has the claim, set it in the response
        $validClaims = explode(' ', self::VALID_CLAIMS);
        foreach ($validClaims as $validClaim) {
            if (in_array($validClaim, $claims)) {
                if ($validClaim == 'address') {
                    // address is an object with subfields
                    $userClaims['address'] = $this->getUserClaim($validClaim, $userDetails['address'] ?: $userDetails);
                } else {
                    $userClaims = array_merge($userClaims, $this->getUserClaim($validClaim, $userDetails));
                }
            }
        }

        return $userClaims;
    }
}
