<?php

namespace NetworkManager;

require_once NM_CLASS_ROOT . "comms/base.php";
require_once NM_CLASS_ROOT . "comms/collection.php";
require_once NM_CLASS_ROOT . "database.php";
require_once NM_CLASS_ROOT . "server.php";

class ServerManager
{
    private static $instance;
    private $db;

    private $hasLoaded = false;
    private $serversMap = array();
    private $games = array();

    private function __construct()
    {
        $this->db = Database::getInstance();
    }

    // Grabs the singleton
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function load()
    {
        if ($this->hasLoaded) {
            return;
        }
        $this->hasLoaded = true;

        $rows = $this->db->select("servers", "*");
        $this->games = array();
        foreach ($rows as $row) {
            $this->serversMap[$row["id"]] = new Server($row);
        }

        foreach ($rows as $row) {
            if ($row['game'] != "web" && $row['game'] != "discord") {
                if (!isset($this->games[$row['game']])) {
                    $this->games[$row['game']] = array();
                }
                foreach ($this->get($row['id'])->getGameRealms() as $gamerealm) {
                    if (!in_array($gamerealm, $this->games[$row['game']])) {
                        $this->games[$row['game']][] = $gamerealm;
                    }
                }
            }
        }
    }

    public function getGames()
    {
        $this->load();
        return $this->games;
    }

    public function getByGame($game)
    {
        $this->load();
        $servers = array();
        foreach ($this->serversMap as $server) {
            if ($server->getGame() == $game) {
                $servers[] = $server;
            }
        }
        return $servers;
    }

    public function getIds()
    {
        $this->load();
        $servers = array();
        foreach ($this->serversMap as $server) {
            $servers[] = $server->getId();
        }
        return $servers;
    }

    public function getAll()
    {
        $this->load();
        //var_dump($this->serversMap);
        return $this->serversMap;
    }

    public function get($sid)
    {
        $this->load();
        if (isset($this->serversMap[$sid])) {
            return $this->serversMap[$sid];
        }
        //$this->serversMap[$sid] = new Server($sid);
        //return $this->serversMap[$sid];
        return NULL;
    }

    public function create($name, $game, $address = "")
    {
        $arr = array("name" => $name, "game" => $game, "address" => $address, "secret" => Server::generateSecret());
        $this->db->insert("servers", $arr);

        $arr['communication'] = 0;

        if ($this->db->id() > 0) {
            $arr['id'] = $this->db->id();
            $this->serversMap[$this->db->id()] = new Server($arr);
            return $this->serversMap[$this->db->id()];
        }
        return NULL;
    }

    public static function getRestCommunicationSockets()
    {
        $sockets = new Communication\Collection();
        foreach (ServerManager::getInstance()->getAll() as $server) {
            if ($server->getCommunication() > 2) {
                $sockets->addSocket($server->getCommunicationSocket());
            }
        }
        return $sockets;
    }

    public static function getRealmCommunicationSockets($realm)
    {
        $sockets = new Communication\Collection();
        foreach (ServerManager::getInstance()->getAll() as $server) {
            if ($server->getRealm() == $realm) { // TODO: Actually compare the realms...
                $sockets->addSocket($server->getCommunicationSocket());
            }
        }
        return $sockets;
    }

    public static function getSelectiveCommunicationSockets($sids)
    {
        $sockets = new Communication\Collection();
        foreach (ServerManager::getInstance()->getAll() as $server) {
            if (in_array($server->getId(), $sids)) {
                $sockets->addSocket($server->getCommunicationSocket());
            }
        }
        return $sockets;
    }

    public static function getAllCommunicationSockets()
    {
        $sockets = new Communication\Collection();
        foreach (ServerManager::getInstance()->getAll() as $server) {
            $sockets->addSocket($server->getCommunicationSocket());
        }
        return $sockets;
    }
}
