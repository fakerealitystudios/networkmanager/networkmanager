<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/database.php";

class Language
{
    protected $db;

    private static $instance;
    protected $languages;

    private function __construct()
    {
        $this->db = Database::getInstance();

        $rows = $this->db->select("languages", ["lang", "event", "args", "format"]);
        foreach ($rows as $row) {
            if (!isset($this->languages[$row['lang']])) {
                $this->languages[$row['lang']] = array();
            }
            $this->languages[$row['lang']][] = $row;
        }
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        //self::$instance->setupDatabase();
        return self::$instance;
    }

    public function getLanguage($lang, $event, $args)
    {
        $args = isset($args) ? $args : 0;
        $format = "";
        if (!isset($this->languages[$lang])) {return $format;}
        foreach ($this->languages[$lang] as $lang) {
            if ($lang['event'] == $event && $lang['args'] == $args) {
                $format = $lang['format'];
                break;
            } else if ($lang['event'] == $event) {
                $format = $lang['format'];
            }
        }
        return $format;
    }
}
