<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/database.php";
require_once NM_ROOT . "includes/classes/language.php";
// UPDATE `logs`SET `logs`.`args` = (SELECT COUNT(`logDetails`.`id`) FROM `logDetails` WHERE `logDetails`.`lid` = `logs`.`id`)
class Logs
{
    public $rows = array();
    public $details = null;

    public function __construct($condition)
    {
        $this->rows = array();
        if (!isset($condition)) {
            $condition = array();
        }
        
        $condition['GROUP'] = array("logs.id", "ld.lid");
        $lids = Database::getInstance()->select("logs", array(
            "[>]logDetails (ld)" => array(
                "logs.id" => "lid",
            ),
        ), "logs.id", $condition);

        if (count($lids) == 0)
            return;

        $logs = Database::getInstance()->select("logs", array(
            "id",
            "type",
            "event",
            "subevent",
            "sid",
            "ssid",
            "time",
            "args",
        ), array("id" => $lids));
        $details = Database::getInstance()->select("logDetails", array(
            "lid",
            "argid",
            "type",
            "vid",
            "value"
        ), array("lid" => $lids));
        foreach($details as $detail) {
            if (!isset($this->details[$detail['lid']])) {
                $this->details[$detail['lid']] = array();
            }
            if (!isset($this->details[$detail['lid']][$detail['argid']])) {
                $this->details[$detail['lid']][$detail['argid']] = array();
            }
            $detail['safevalue'] = nl2br(htmlentities($detail['value']));
            $this->details[$detail['lid']][$detail['argid']][] = $detail;
        }

        foreach($logs as $log) {
            if (!isset($this->details[$log['id']])) {
                $this->details[$log['id']] = array();
            }
            $args = 0;
            foreach($this->details[$log['id']] as $detail) {
                $args += count($detail);
            }
            if ($log['args'] != null && $log['args'] != $args) {
                // Don't put into the rows, missing data, wait for the db to catch up.
                continue;
            }
            $log['details'] = $this->details[$log['id']];
            $this->rows[] = $log;
        }
        $this->rows = array_reverse($this->rows);
    }

    public static function fullCount($condition)
    {
        $condition['GROUP'] = array("logs.id", "ld.lid");
        //$count = Database::getInstance()->query("SELECT COUNT(1) FROM (SELECT COUNT(1) FROM <logs> RIGHT JOIN <logDetails> AS `ld` ON <logs.id> = <ld.lid> WHERE <logs.event> IN ('CompileString performed', 'attack', 'chat', 'command', 'concommand', 'connect', 'damage', 'death', 'disconnect', 'kill', 'print', 'respawn', 'socket', 'start', 'stop', 'test') GROUP BY `logs`.`id`) AS Dervied");
        $query = Database::getInstance()->sql()->count("logs", array(
            "[<]logDetails (ld)" => array(
                "logs.id" => "lid",
            ),
        ), "logs.id", $condition);
        $count = Database::getInstance()->query("SELECT COUNT(1) as `count` FROM (".$query.") AS Dervied")->fetchAll()[0]['count'];
        return $count;
    }

    public function getRows()
    {
        return $this->rows;
    }

    public function getReadableLog($row)
    {
        $event = $row['type'] . "." . $row["event"] . (isset($row['subevent']) ? "." . $row['subevent'] : "");
        $preg = preg_replace_callback("/\{(\d+)\}/", function ($matches) use ($row) {
            $argid = $matches[1];
            //$details[$row['lid']][$argid]
            return \NetworkManager\Logs::GetReadableArgument($this->details[$row['id']][$argid]);
        }, Language::getInstance()->getLanguage("en", $event, count($this->details[$row['id']])));
        return utf8_encode($preg);
    }

    public static function getReadableArgument($args)
    {
        $argsFormatted = array();
        if (isset($args)) {
            foreach ($args as $arg) {
                $argsFormatted[isset($arg['vid']) ? $arg['vid'] : $arg['type']] = $arg;
            }
        }

        if (isset($argsFormatted['NMID']) && isset($argsFormatted['name'])) { // We know it's a player, most likely with a username
            $text = '<a href="' . NM_URL . 'player.php?id=' . $argsFormatted['NMID']['value'] . '">' . $argsFormatted['name']['value'] . '</a>';
            if (isset($argsFormatted['USID']) && $argsFormatted['USID']['value'] != '-1') {
                $text .= '(<a href="' . NM_URL . 'logs.php?usid=' . $argsFormatted['USID']['value'] . '">' . $argsFormatted['USID']['value'] . '</a>)';
            }
            return $text;
        } else if (isset($argsFormatted['WEAPON'])) {
            return $argsFormatted['WEAPON']['value'];
        } else if (isset($argsFormatted['ENTITY'])) {
            return $argsFormatted['ENTITY']['value'];
        } else if (isset($argsFormatted['INTEGER'])) {
            return $argsFormatted['INTEGER']['value'];
        } else if (isset($argsFormatted['FLOAT'])) {
            return $argsFormatted['FLOAT']['value'];
        } else if (isset($argsFormatted['STRING'])) {
            return $argsFormatted['STRING']['value'];
        }

        return "";
    }
}
