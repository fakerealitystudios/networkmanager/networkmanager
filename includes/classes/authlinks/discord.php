<?php

namespace NetworkManager\LinkMethods;

// https://discordapp.com/developers/applications/
class Discord extends Base
{
    public function isEnabled()
    {
        global $NM;
        return isset($NM->settings['discord.clientid'], $NM->settings['discord.secret']) && $NM->settings['discord.clientid'] != "" && $NM->settings['discord.secret'] != "";
    }

    public function isLogin()
    {
        return true;
    }

    public function getTitle()
    {
        return "Discord";
    }

    public function getType()
    {
        return "discord";
    }

    public function generateUrl($returnTo)
    {
        global $NM;

        return "https://discordapp.com/api/oauth2/authorize?client_id=" . $NM->settings['discord.clientid'] . "&redirect_uri=" . urlencode($returnTo) . "&response_type=code&scope=identify%20email";
    }

    public function getToken()
    {
        global $NM;

        $redirect_uri = NM_URL . 'login.php';
        $token_request = "https://discordapp.com/api/oauth2/token";

        if (isset($_GET["error"])) {
            // Error during initial authorization
            return false;
        } elseif (isset($_GET["code"])) {
            $token = curl_init();
            $fields = array(
                "grant_type" => "authorization_code",
                "client_id" => $NM->settings['discord.clientid'],
                "client_secret" => $NM->settings['discord.secret'],
                "redirect_uri" => $redirect_uri,
                "code" => $_GET["code"],
            );

            curl_setopt_array($token, array(
                CURLOPT_URL => $token_request,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $fields,
            ));
            curl_setopt($token, CURLOPT_RETURNTRANSFER, true);

            $resp = json_decode(curl_exec($token));
            curl_close($token);

            if (isset($resp->access_token)) {
                $access_token = $resp->access_token;

                $info_request = "https://discordapp.com/api/users/@me";

                $info = curl_init();
                curl_setopt_array($info, array(
                    CURLOPT_URL => $info_request,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer {$access_token}",
                    ),
                    CURLOPT_RETURNTRANSFER => true,
                ));

                $user = json_decode(curl_exec($info));
                curl_close($info);

                return $user->id;
            } else {
                //echo json_encode(array("message" => "Authentication Error"));
            }
            return false;
        }
        return null;
    }

    public function getProfile($token)
    {
        return "https://discordapp.com/users/".$token;
    }

    public function getButton()
    {
        global $NM;
        return $NM->template->build(NM_TEMPLATE_ROOT . '/buttons/discord.html');
    }

    public function getIcon()
    {
        return '<i class="fa-brands fa-discord align-middle" style="font-size:16px;" aria-hidden="true"></i>';
    }
}
