<?php

namespace NetworkManager\LinkMethods;

class Minecraft extends Base
{
    public function isEnabled()
    {
        return true;
    }

    public function isLogin()
    {
        return false;
    }

    public function getTitle()
    {
        return "Minecraft";
    }

    public function getType()
    {
        return "minecraft";
    }

    public function generateUrl($returnTo)
    {
        return "";
    }

    public function getToken()
    {
        return null;
    }

    public function getProfile($token)
    {
        return "https://mcuuid.net/?q=".$token;
    }

    public function getButton()
    {
        global $NM;
        return $NM->template->build(NM_TEMPLATE_ROOT . '/buttons/minecraft.html');
    }

    public function getIcon()
    {
        return '<i class="fa-solid fa-block-brick align-middle" style="font-size:16px;" aria-hidden="true"></i>';
    }
}
