<?php

namespace NetworkManager\LinkMethods;

require __DIR__ . "/../libraries/SteamAuth.class.php";

class Steam extends Base
{
    public function isEnabled()
    {
        return true;
    }

    public function isLogin()
    {
        return true;
    }

    public function getTitle()
    {
        return "Steam";
    }

    public function getType()
    {
        return "steamid64";
    }

    public function generateUrl($returnTo)
    {
        $steamauth = new \SteamAuth();
        $steamauth->OpenID->returnUrl = $returnTo;
        return $steamauth->GetLoginURL();
    }

    public function getToken()
    {
        $steamauth = new \SteamAuth();
        return $steamauth->Init();
    }

    public function getProfile($token)
    {
        return "http://steamcommunity.com/profiles/".$token;
    }

    public function getButton()
    {
        global $NM;
        return $NM->template->build(NM_TEMPLATE_ROOT . '/buttons/steam.html');
    }

    public function getIcon()
    {
        return '<i class="fa-brands fa-steam align-middle" style="font-size:16px;" aria-hidden="true"></i>';
    }
}
