<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/common.php";

trait Data
{
    public $data = array();
    public static $data_delimiter = "|";

    protected function insertData($vid, $value, $realm)
    {
        $data = array();
        $data["vid"] = $vid;
        $data["value"] = $value;
        $data["realm"] = $realm;
        if (substr(get_class($this), 0, 21) == "NetworkManager\Member") {
            $data['uid'] = $this->getId();
            $table = "memberData";
        }
        $this->db->insert($table, $data);
    }

    protected function updateData($vid, $value, $realm)
    {
        $where = array();
        $where["vid[~]"] = $vid;
        $where["realm"] = $realm;
        if (substr(get_class($this), 0, 21) == "NetworkManager\Member") {
            $where['uid'] = $this->getId();
            $table = "memberData";
        }
        $data = array();
        $data['value'] = $value;
        $this->db->update($table, $data, $where);
    }

    protected function deleteData($vids)
    {
        $where = array();
        $where["vid[~]"] = $vids;
        if (substr(get_class($this), 0, 21) == "NetworkManager\Member") {
            $where['uid'] = $this->getId();
            $table = "memberData";
        }
        $this->db->delete($table, $where);
    }

    public function loadData($vid, $value, $fullRealm)
    {
        $this->data[] = array("vid" => $vid, "value" => $value, "realm" => $fullRealm);
        $vid = explode(self::$data_delimiter, $vid);
        $realm = &$this->getRealm($fullRealm)["__data"];
        $realm = array_merge_recursive($realm, self::decodeData($vid, $value));
        //$realm
        //$data[$realm]->addData($this->decodeData($vid, $row["value"]));
    }

    public static function encodeData($tbl, $realm, $key = "", $index = "")
    {
        $insert = array();
        $update = array();
        $delete = array();
        if (!is_array($tbl)) {
            if (isset($realm[$key]))
                $update[$index] = $tbl;
            else
                $insert[$index] = $tbl;
        } else {
            if (count($tbl) == 0) {
                $delete[] = $index;
            }
            foreach ($tbl as $k => $v) {
                if ($index == "")
                    $vid = $k;
                else
                    $vid = $index . self::$data_delimiter . $k;
                if (is_array($v)) {
                    if (isset($realm[$k]))
                        $realm = &$realm[$k];
                    else
                        $realm = null;
                    $data = self::encodeData($v, $realm, $k, $vid);
                    $insert = array_merge($insert, $data[0]);
                    $update = array_merge($update, $data[1]);
                    $delete = array_merge($delete, $data[2]);
                } elseif ($v === "_nil") { // Difference table null string
                    $delete[] = $vid;
                } else {
                    if (isset($realm[$k]))
                        $update[$vid] = $v;
                    else
                        $insert[$vid] = $v;
                }
            }
        }
        return array($insert, $update, $delete);
    }

    public static function decodeData($vid, $value)
    {
        $tbl = array();
        $id = $vid[0];
        array_splice($vid, 0, 1);
        if (count($vid) > 0) {
            $tbl[$id] = self::decodeData($vid, $value);
        } else {
            $tbl[$id] = $value;
        }
        return $tbl;
    }

    public function setData($fullRealm, $value, ...$indexes)
    {
        $realm = &$this->getRealm($fullRealm)["__data"];
        $data = array();
        $ldata = &$data;
        $i = 0;
        $count = count($indexes);
        foreach($indexes as $index) {
            if (!isset($ldata[$index])) {
                $ldata[$index] = array();
            }
            $i++;
            if ($i == $count) {
                $ldata[$index] = $value;
            } else {
                $ldata = &$ldata[$index];
            }
        }
        $diffTable = Common::GetDifference($data, $realm);
        if (count($diffTable) == 0) {
            return false;
        }
        $result = $this->encodeData($data, $realm);
        $realm = array_merge_recursive($realm, $data);
        $insert = $result[0];
        $update = $result[1];
        $delete = $result[2];
        if (count($insert) > 0) {
            foreach ($insert as $k => $v) {
                $this->insertData($k, $v, $fullRealm);
            }
        }
        if (count($update) > 0) {
            foreach ($update as $k => $v) {
                $this->updateData($k, $v, $fullRealm);
            }
        }
        if (count($delete) > 0) {
            $this->deleteData($delete);
        }
        return true;
    }

    public function getData($fullRealm, ...$indexes)
    {
        $realm = &$this->getRealm($fullRealm)["__data"];
        if (count($indexes) == 0)
            return $realm;
        $ldata = &$realm;
        foreach ($indexes as $index) {
            if (!isset($ldata[$index]))
                return null;
            $ldata = &$ldata[$index];
        }
        return $ldata;
    }
}
