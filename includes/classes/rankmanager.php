<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/database.php";
require_once NM_ROOT . "includes/classes/rank.php";

class RankManager
{
    public static $DefaultRank = "user";
    private static $instance;
    private $db;

    private $hasLoaded = false;
    private $ranksMap = array();
    private $children = array();
    private $parents = array();

    private function __construct($realm = "*")
    {
        $this->db = Database::getInstance();
    }

    // Grabs the singleton
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add($rank) {
        if (!$rank->isValid()) return;
        $this->ranksMap[$rank->getRank()] = $rank;
    }

    public function gatherRanks()
    {
        if ($this->hasLoaded) {
            return;
        }
        $this->hasLoaded = true;

        $res = $this->db->select("ranks", [
            "rank",
            "displayName",
            "inherit",
            "subgrouping",
            "data",
            "realm",
        ]);
        if ($res) {
            foreach ($res as $row) {
                $this->ranksMap[$row["rank"]] = new Rank($row["rank"], $row["displayName"], $row["inherit"], $row["subgrouping"], $row["data"], $row["realm"]);
            }
        } else {
            $this->createRank("User", "user", ""); // Always have the basic user group
        }

        $res = $this->db->select("rankPermissions", [
            "rank",
            "value",
            "realm",
        ]);
        $perms = array();
        if ($res) {
            foreach ($res as $row) {
                if (isset($this->ranksMap[$row["rank"]])) {
                    $this->ranksMap[$row["rank"]]->loadPermission($row["value"], $row["realm"]);
                }
            }
        }
    }

    public function get($rank)
    {
        $this->gatherRanks();
        if (!isset($this->ranksMap[$rank])) {
            return NULL;
        }

        return $this->ranksMap[$rank];
    }

    public function getRanks()
    {
        $this->gatherRanks();
        return $this->ranksMap;
    }

    public function orderRanks()
    {
        $this->gatherRanks();
    }

    public function getParentRanks($gid)
    {
        $this->gatherRanks();
        /*$child = $this->getGroup($gid);
        $parents = array();
        foreach($this->ranks as $group) {
        if ($group->getRank() > $child->getRank()) {
        $parents[$group->getId()] = $group;
        }
        }*/
        return $parents;
    }

    public function getChildRanks($rank)
    {
        $this->gatherRanks();
        $group = $this->get($rank);
        $children = array();
        foreach ($this->ranksMap as $child) {
            if ($child->getRank() == $group->getParent()) {
                $children = $this->getChildRanks($child->getRank());
                array_push($children, $child->getRank());
            }
        }
        return $children;
    }

    public function canTarget($rank, $other)
    {
        $group1 = $this->get($rank);
        $group2 = $this->get($other);
        if (!$group1->isValid()) {
            return null;
        }
        if (!$group2->isValid()) {
            return null;
        }
        if ($group1->getSubGrouping() != $group2->getSubGrouping()) { // We can't compare these!
            return null;
        }
        return $group1->hasPermission("nm.target." . $group2->getRank());
    }

    public function createRank($displayName, $rank, $inherit = null, $fullRealm = "*", $subgrouping = 0)
    {
        if ($inherit == "") {
            $inherit = null;
        }
        $this->db->insert("ranks", [
            "displayName" => $displayName,
            "rank" => $rank,
            "inherit" => $inherit,
            "subgrouping" => $subgrouping,
            "data" => json_encode(array()),
            "realm" => $fullRealm,
        ]);
        
        $this->ranksMap[$rank] = new Rank($rank, $displayName, $inherit, $subgrouping, array(), $fullRealm);

        return $this->ranksMap[$rank];
    }

    public function deleteRank($rank)
    {
        if ($gid == RankManager::$DefaultRank) {
            return false;
        }

        $res = $this->db->delete("ranks", [
            "rank" => $rank,
        ]);

        return $res->rowCount() > 0;
    }
}
