<?php

namespace NetworkManager;

class API
{
    private static $timedout = false;
    private static $connectionError = array(
        "state" => "error",
        "error" => "Failed to connect to external API"
    );
    private static $noInstanceId = array(
        "state" => "error",
        "error" => "No Instance ID"
    );
    private static $instance_id;
    private static $instance_apikey;

    public static function is_timedout() {
        if (!self::$timedout && isset($_SESSION['timedout_api']))
            self::$timedout = true;
        
        return self::$timedout;
    }

    public static function get($action) {
        if (self::is_timedout())
        {
            return self::$connectionError;
        }
        if (API::getInstanceId() == null && $action != "/core/version/networkmanager")
        {
            return self::$noInstanceId;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NM_API_SITE.$action);
        // Provide instance id whenever available(provides access to additional api endpoints)
        $header = array();
        if (self::getInstanceId() != null) {
            $header[] = 'X-Instance: '.self::getInstanceId();
        }
        if (self::getAPIKey() != null) {
            $header[] = 'Authorization: '.self::getAPIKey();
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        $result = curl_exec($ch);
        if($result === false)
        {
            self::$timedout = true;
            $_SESSION['timedout_api'] = true;
            return self::$connectionError;
        }
        curl_close($ch);
        $json = \json_decode($result, true);
        if ($json == null) {
            return array("message" => "Unknown error occured");
        }
        return $json;
    }

    public static function post($action, $data = array()) {
        if (self::is_timedout())
        {
            return self::$connectionError;
        }
        if (API::getInstanceId() == null && $action != "/instance/".NM_VERSION_DELIMITED)
        {
            return self::$noInstanceId;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NM_API_SITE.$action);
        // Provide instance id whenever available(provides access to additional api endpoints)
        $header = array();
        if (self::getInstanceId() != null) {
            $header[] = 'X-Instance: '.self::getInstanceId();
        }
        if (self::getAPIKey() != null) {
            $header[] = 'Authorization: '.self::getAPIKey();
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        $result = curl_exec($ch);
        if($result === false)
        {
            self::$timedout = true;
            $_SESSION['timedout_api'] = true;
            return self::$connectionError;
        }
        curl_close($ch);
        $json = \json_decode($result, true);
        if ($json == null) {
            return array("message" => "Unknown error occured");
        }
        return $json;
    }

    public static function put($action, $data = array()) {
        if (self::is_timedout())
        {
            return self::$connectionError;
        }
        if (API::getInstanceId() == null)
        {
            return self::$noInstanceId;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NM_API_SITE.$action);
        // Provide instance id whenever available(provides access to additional api endpoints)
        $header = array();
        if (self::getInstanceId() != null) {
            $header[] = 'X-Instance: '.self::getInstanceId();
        }
        if (self::getAPIKey() != null) {
            $header[] = 'Authorization: '.self::getAPIKey();
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        $result = curl_exec($ch);
        if($result === false)
        {
            self::$timedout = true;
            $_SESSION['timedout_api'] = true;
            return self::$connectionError;
        }
        curl_close($ch);
        $json = \json_decode($result, true);
        if ($json == null) {
            return array("message" => "Unknown error occured");
        }
        return $json;
    }

    public static function getInstanceId($db = null) {
        if (isset(self::$instance_id)) {
            return self::$instance_id;
        }
        global $NM;
        if ($NM && isset($NM->settings["instance.id"])) {
            self::$instance_id = $NM->settings["instance.id"];
            return self::$instance_id;
        }
        if ($db) {
            $id = self::post("/instance/".NM_VERSION_DELIMITED);
            if (isset($id['data']['id'])) {
                $id = $id['data']['id'];
                $db->insert("settings", array(
                    array(
                        "name" => "instance.id",
                        "value" => $id
                    )
                ));
                self::$instance_id = $id;
                return $id;
            }
        }
        return null;
    }

    public static function getAPIKey() {
        if (isset(self::$instance_apikey)) {
            return self::$instance_apikey;
        }
        if (self::getInstanceId() != null) {
            global $NM, $nm_version;
            if ($NM && isset($NM->settings["instance.apikey"])) {
                self::$instance_apikey = $NM->settings["instance.apikey"];
                return self::$instance_apikey;
            }
        }
        return null;
    }
}
