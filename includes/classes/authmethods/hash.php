<?php

namespace NetworkManager\AuthMethods;

// Uses PHP's password hashing and verification
class HashAuthentication extends Base
{
    public function login($username, $password)
    {
        return array('username' => $username, 'password' => $password);
    }

    public function register($username, $password, $email)
    {
        return array('username' => $username, 'password' => $this->hashpassword($password), 'email' => $email);
    }

    public function comparepassword($password, $db_password)
    {
        return password_verify($password, $db_password);
    }

    public function hashpassword($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function remembertoken()
    {
        $token = bin2hex(random_bytes(32));
        return array('client' => $token, 'server' => hash('sha256', $token));
    }

    public function comparetoken($token, $db_token)
    {
        return hash_equals($db_token, hash('sha256', $token));
    }
}
