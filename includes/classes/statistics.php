<?php

namespace NetworkManager\Statistics;

require_once NM_ROOT . "includes/classes/database.php";
require_once NM_ROOT . "includes/classes/servermanager.php";

class Statistics
{
    public static function getTopKills($uid = null, $sid = null, $limit = 10, $grouping = 1)
    {
        $db = \NetworkManager\Database::getInstance();

        $query = "SELECT logDetails.value as uid, logs.sid as sid, COUNT(1) as kills FROM logDetails logDetails, logs logs WHERE";
        if ($uid != null) {
            $query .= " logDetails.value = '" . $uid . "' AND";
        }
        if ($sid != null && $sid != -1) {
            $query .= " logs.sid = '" . $sid . "' AND";
        }

        $query .= " logs.type = 'player' AND logs.event = 'kill' AND logDetails.lid = logs.id AND logDetails.type = 'nmid' AND logDetails.argid = '0' GROUP BY ";
        if ($grouping == 1) {
            $query .= "logDetails.value";
        } elseif ($grouping == 2) {
            $query .= "logs.sid";
        }
        $query .= " ORDER BY `kills`";
        $query .= " DESC LIMIT " . $limit;

        $rows = $db->query($query)->fetchAll();
        return $rows;
    }

    public static function getTopDeaths($uid = null, $sid = null, $limit = 10, $grouping = 1)
    {
        $db = \NetworkManager\Database::getInstance();

        $query = "SELECT logDetails.value as uid,logs.sid as sid, COUNT(1) as deaths FROM logDetails logDetails, logs logs WHERE";
        if ($uid != null) {
            $query .= " logDetails.value = '" . $uid . "' AND";
        }
        if ($sid != null && $sid != -1) {
            $query .= " logs.sid = '" . $sid . "' AND";
        }

        $query .= " logs.type = 'player' AND logs.event = 'death' AND logDetails.lid = logs.id AND logDetails.type = 'nmid' AND logDetails.argid = '0' GROUP BY ";
        if ($grouping == 1) {
            $query .= "logDetails.value";
        } elseif ($grouping == 2) {
            $query .= "logs.sid";
        }
        $query .= " ORDER BY `deaths`";
        $query .= " DESC LIMIT " . $limit;

        $rows = $db->query($query)->fetchAll();
        return $rows;
    }

    public static function getTopPlaytime($uid = null, $sid = null, $limit = 10, $grouping = 1)
    {
        $db = \NetworkManager\Database::getInstance();

        $query = "SELECT uid, sid, SUM(endTime) - SUM(startTime) as length FROM memberSessions WHERE";
        if ($uid != null) {
            $query .= " `uid` = '" . $uid . "' AND";
        }
        if ($sid != null && $sid != -1) {
            $query .= " `sid` = '" . $sid . "' AND";
        }

        $query .= " `endTime` != '-1' GROUP BY ";
        if ($grouping == 1) {
            $query .= "`uid`";
        } elseif ($grouping == 2) {
            $query .= "`sid`";
        }
        $query .= " ORDER BY `length`";
        $query .= " DESC LIMIT " . $limit;

        $rows = $db->query($query)->fetchAll();
        return $rows;
    }

    public static function getTopSessions($uid = null, $sid = null, $limit = 10, $grouping = 1)
    {
        $db = \NetworkManager\Database::getInstance();

        $query = "SELECT uid, sid, COUNT(1) as total FROM memberSessions WHERE";
        if ($uid != null) {
            $query .= " `uid` = '" . $uid . "' AND";
        }
        if ($sid != null && $sid != -1) {
            $query .= " `sid` = '" . $sid . "' AND";
        }

        $query .= " `endTime` != '-1' GROUP BY ";
        if ($grouping == 1) {
            $query .= "`uid`";
        } elseif ($grouping == 2) {
            $query .= "`sid`";
        }
        $query .= " ORDER BY `total`";
        $query .= " DESC LIMIT " . $limit;

        $rows = $db->query($query)->fetchAll();
        return $rows;
    }

    public static function getRetentionRate($sid = null)
    {
        //$start = microtime(true);
        $db = \NetworkManager\Database::getInstance();

        $members = $db->select("members", array(
            "id",
            "day" => \Medoo\Medoo::raw("CAST(FROM_UNIXTIME(created) AS date)"),
        ));
        $sessions = $db->select("memberSessions", array(
            "uid",
            "day" => \Medoo\Medoo::raw("CAST(FROM_UNIXTIME(startTime) AS date)"),
        ));

        //var_dump(microtime(true) - $start);

        $member_sessions = array();
        $session_count = count($sessions);
        $member_count = count($members);

        foreach ($members as $member) {
            $data = array();
            $data[] = 1;
            $date = new \DateTime($member['day']);
            for($i = 0; $i < 10; $i++) {
                $date->modify('+1 days');
                $amount = 0;
                foreach ($sessions as $session) {
                    if($amount == ($session_count-1)) {
                        $data[] = 0;
                        break;
                    }
                    if($session['uid'] == $member['id']) {
                        $sessiondate = new \DateTime($session['day']);
    
                        if($sessiondate->getTimestamp() == $date->getTimestamp()) {
                            $data[] = 1;
                            break;
                        }
                    }
                    $amount++;
                }
            }

            $member_sessions[] = $data;
        }

        //var_dump(microtime(true) - $start);

        $result = array();
        for($i = 0; $i < 11; $i++) {
            $amount = 0;
            foreach ($member_sessions as $session) {
                $amount+=$session[$i];
            }
            $result[] = $amount;
        }

        $retention = array();
        for($i = 0; $i < 11; $i++) {
            $retention[] = floatval(($result[$i]/$member_count)*100);
        }

        //var_dump(microtime(true) - $start);

        return $retention;
    }
}

// Statistics for the entire network, fills the main statistics page
class Network
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $uniquePlayers = 0; // amount
    public $totalSessions = 0; // amount

    public $topServersByPlaytime = array(); // Server Ids : time
    public $topServersBySessions = array(); // Server Ids : amount

    public $geolocations = array(); // User Ids : Country
    public $topPlayersByKills = array(); // User Ids, amount
    public $topPlayersByDeaths = array(); // User Ids, amount
    public $topPlayersBySessions = array(); // User Ids, amount
    public $topPlayersByPlaytime = array(); // User Ids, time

    public function __construct()
    {
        $db = \NetworkManager\Database::getInstance();

        /*$this->deaths = $db->count("logDetails", array("vid" => "type", "value" => "death", "main" => 1));
        $this->kills = $db->count("logDetails", array("vid" => "type", "value" => "kill", "main" => 1));*/
        $this->deaths = $db->count("logs", array("type" => "death"));
        $this->kills = $db->count("logs", array("type" => "death"));
        $this->uniquePlayers = $db->count("members");
        $this->totalSessions = $db->count("memberSessions");

        // Top Servers
        foreach (Statistics::getTopSessions(null, -1, 10, 2) as $row) {
            $this->topServersBySessions[$row["sid"]] = $row["total"];
        }
        foreach (Statistics::getTopPlaytime(null, -1, 10, 2) as $row) {
            $this->topServersByPlaytime[$row["sid"]] = \NetworkManager\Common::timeTillHumanReadable(time() + $row["length"]);
        }

        // Top Players
        foreach (Statistics::getTopKills() as $row) {
            $this->topPlayersByKills[$row["uid"]] = $row["kills"];
        }
        foreach (Statistics::getTopDeaths() as $row) {
            $this->topPlayersByDeaths[$row["uid"]] = $row["deaths"];
        }

        foreach (Statistics::getTopSessions() as $row) {
            $this->topPlayersBySessions[$row["uid"]] = $row["total"];
        }
        foreach (Statistics::getTopPlaytime() as $row) {
            $this->topPlayersByPlaytime[$row["uid"]] = \NetworkManager\Common::timeTillHumanReadable(time() + $row["length"]);
        }
    }
}

// Statistics for a specific game
class Game
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $uniquePlayers = 0; // amount
    public $totalSessions = 0; // amount

    public $topServersByPlaytime = array(); // Server Ids : time
    public $topServersBySessions = array(); // Server Ids : amount

    public $geolocations = array(); // User Ids : Country
    public $topPlayersByKills = array(); // User Ids, amount
    public $topPlayersByDeaths = array(); // User Ids, amount
    public $topPlayersBySessions = array(); // User Ids, amount
    public $topPlayersByPlaytime = array(); // User Ids, time

    public function __construct($servers)
    {
        $db = \NetworkManager\Database::getInstance();

    }
}

// Statistics for a specific server
class Server
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $uniquePlayers = 0; // amount
    public $totalSessions = 0; // amount

    public $geolocations = array(); // User Ids : Country
    public $topPlayersByKills = array(); // User Ids, amount
    public $topPlayersByDeaths = array(); // User Ids, amount
    public $topPlayersBySessions = array(); // User Ids, amount
    public $topPlayersByPlaytime = array(); // User Ids, time

    public function __construct($sid)
    {
        $db = \NetworkManager\Database::getInstance();

        $this->deaths = $db->count("logDetails", array("vid" => "type", "value" => "death"));
        $this->kills = $db->count("logDetails", array("vid" => "type", "value" => "kill"));
        $this->uniquePlayers = $db->count("memberSessions", array("sid" => $sid, "GROUP" => "sid"));
        $this->totalSessions = $db->count("memberSessions", array("sid" => $sid));

        // Top Players
        /*$rows = $db->query("SELECT uid, COUNT(1) as kills FROM logDetails WHERE `type` = 'kill' AND `recipient` = '0' GROUP BY `uid` ORDER BY `kills` DESC LIMIT 10")->fetchAll();
        foreach($rows as $row) {
        $this->topPlayersByKills[$row["uid"]] = $row["kills"];
        }
        $rows = $db->query("SELECT uid, COUNT(1) as deaths FROM logDetails WHERE `type` = 'death' AND `recipient` = '0' GROUP BY `uid` ORDER BY `deaths` DESC LIMIT 10")->fetchAll();
        foreach($rows as $row) {
        $this->topPlayersByDeaths[$row["uid"]] = $row["deaths"];
        }*/
        $rows = $db->query("SELECT uid, COUNT(1) as total FROM memberSessions WHERE `sid` = '" . $sid . "' GROUP BY `uid` ORDER BY `total` DESC LIMIT 10")->fetchAll();
        foreach ($rows as $row) {
            $this->topPlayersBySessions[$row["uid"]] = $row["total"];
        }
        $rows = $db->query("SELECT uid, SUM(endTime) - SUM(startTime) as length FROM memberSessions WHERE `sid` = '" . $sid . "' AND `endTime` != '-1' GROUP BY `uid` ORDER BY `length` DESC LIMIT 10")->fetchAll();
        foreach ($rows as $row) {
            $this->topPlayersByPlaytime[$row["uid"]] = \NetworkManager\Common::timeTillHumanReadable(time() + $row["length"]);
        }
    }
}

// Up next is rank statistics
// These are based on the player statistics, just collectively

// Statistics for the network
class RankByNetwork
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $uniquePlayers = 0; // amount
    public $totalSessions = 0; // amount
    public $mostPopularBySessions = 1; // Server Id
    public $mostPopularByPlaytime = 1; // Server Id
    public $leastPopularBySessions = 1; // Server Id
    public $leastPopularByPlaytime = 1; // Server Id
    public $mostPlaytime = 1; // User Id
    public $mostSessions = 1; // User Id

    public $geolocations = array(); // User Ids : Country
    public $topPlayersByKills = array(); // User Ids, amount
    public $topPlayersByDeaths = array(); // User Ids, amount
    public $topPlayersBySessions = array(); // User Ids, amount
    public $topPlayersByPlaytime = array(); // User Ids, time

    public function __construct($rank)
    {

    }
}

// Statistics for a specific game
class RankByGame
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $uniquePlayers = 0; // amount
    public $totalSessions = 0; // amount
    public $mostPopularBySessions = 1; // Server Id
    public $mostPopularByPlaytime = 1; // Server Id
    public $leastPopularBySessions = 1; // Server Id
    public $leastPopularByPlaytime = 1; // Server Id
    public $mostPlaytime = 1; // User Id
    public $mostSessions = 1; // User Id

    public $geolocations = array(); // User Ids : Country
    public $topPlayersByKills = array(); // User Ids, amount
    public $topPlayersByDeaths = array(); // User Ids, amount
    public $topPlayersBySessions = array(); // User Ids, amount
    public $topPlayersByPlaytime = array(); // User Ids, time

    public function __construct($rank, $game)
    {

    }
}

// Statistics for a specific server
class RankByServer
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $uniquePlayers = 0; // amount
    public $totalSessions = 0; // amount
    public $mostPlaytime = 1; // User Id
    public $mostSessions = 1; // User Id

    public $geolocations = array(); // User Ids : Country
    public $topPlayersByKills = array(); // User Ids, amount
    public $topPlayersByDeaths = array(); // User Ids, amount
    public $topPlayersBySessions = array(); // User Ids, amount
    public $topPlayersByPlaytime = array(); // User Ids, time

    public function __construct($rank, $server)
    {

    }
}

// Statistics for a specific rank
class Rank
{
    public $chatCount = 0; // Amount
    public $commandsUsed = 0; // Amount

    public function __construct($rank)
    {

    }
}

// Up next is player statistics
// These are mainly based on logs, via mysql searching

// Statistics for the network
class PlayerByNetwork
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $totalSessions = 0; // amount
    public $mostPlaytime = 1; // Server Id
    public $mostSessions = 1; // Server Id

    public $topServersByPlaytime = array(); // Server Ids : time
    public $topServersBySessions = array(); // Server Ids : amount

    public $topPlayersByKills = array(); // User Ids
    public $topPlayersByDeaths = array(); // User Ids

    public function __construct($uid)
    {
        $db = \NetworkManager\Database::getInstance();

        // Top Servers
        $rows = $db->query("SELECT sid, SUM(endTime) - SUM(startTime) as length FROM memberSessions WHERE `uid` = '" . $uid . "' AND `endTime` != '-1' GROUP BY `sid` ORDER BY `length` DESC")->fetchAll();
        foreach ($rows as $row) {
            $this->topServersByPlaytime[$row["sid"]] = \NetworkManager\Common::timeTillHumanReadable(time() + $row["length"]);
        }
        $rows = $db->query("SELECT sid, COUNT(1) as total FROM memberSessions WHERE `uid` = '" . $uid . "' GROUP BY `sid` ORDER BY `total` DESC")->fetchAll();
        foreach ($rows as $row) {
            $this->topServersBySessions[$row["sid"]] = $row["total"];
        }
    }
}

// Statistics for a specific game
class PlayerByGame
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $totalSessions = 0; // amount
    public $mostPlaytime = 1; // Server Id
    public $mostSessions = 1; // Server Id

    public $topPlayersByKills = array(); // User Ids
    public $topPlayersByDeaths = array(); // User Ids

    public function __construct($player, $servers)
    {

    }
}

// Statistics for a specific server
class PlayerByServer
{
    public $deaths = 0; // amount
    public $kills = 0; // amount
    public $totalSessions = 0; // amount

    public $topPlayersByKills = array(); // User Ids
    public $topPlayersByDeaths = array(); // User Ids

    public function __construct($player, $server)
    {

    }
}

// Statistics for a specific player
class Player
{
    public $chatCount = 0; // Amount
    public $commandsUsed = 0; // Amount

    public function __construct($player)
    {

    }
}
