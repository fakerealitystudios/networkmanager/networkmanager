<?php

namespace NetworkManager;

class Common
{
    public static function uuidv4($data = null) {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
    
        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    
        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public static function getRealIpAddr()
    {
        return Common::getIpAddr();
    }

    public static function getIpAddr()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            return $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            return $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        };
        return 'UNKNOWN';
    }

    public static function timeElapsedString($ptime)
    {
        $etime = time() - $ptime;
        if ($etime < 1) {
            return '0 seconds';
        }
        $a = array(12 * 30 * 24 * 60 * 60 => 'year', 30 * 24 * 60 * 60 => 'month', 24 * 60 * 60 => 'day', 60 * 60 => 'hour', 60 => 'minute', 1 => 'second');
        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
            };
        };
    }

    public static function timeTillHumanReadable($time1, $time2 = null, $useInvert = false)
    {
        if (is_null($time1)) {
            return null;
        }

        $now = new \DateTime;
        if (!is_null($time2)) {
            $now->setTimeStamp(intval($time2));
        }
        $then = new \DateTime;
        $then->setTimestamp(intval($time1));
        $interval = $now->diff($then);

        if ($interval->invert && !$useInvert) {
            return 'Now';
        }

        $text = '';

        if ($interval->y) {
            $text .= $interval->format(' %y year' . (($interval->y != 1) ? 's' : ''));
        } elseif ($interval->m) {
            $text .= $interval->format(' %m month' . (($interval->m != 1) ? 's' : ''));
        } elseif ($interval->d > 7) {
            $weeks = round($interval->d / 7);
            $text .= " {$weeks} week" . (($weeks != 1) ? 's' : '');
        }
        /*if ($interval->d == 1) {
        return 'Tomorrow';
        } else*/if ($interval->d) {
            $text .= $interval->format(' %d day' . (($interval->d != 1) ? 's' : ''));
        } elseif ($interval->h) {
            $text .= $interval->format(' %h hour' . (($interval->h != 1) ? 's' : ''));
        } elseif ($interval->i) {
            $text .= $interval->format(' %i minute' . (($interval->i != 1) ? 's' : ''));
        }

        if ($text != '')
            return trim($text) ;
        return 'Less than a minute';
    }

    public static function GetDifference($table1, $table2)
    {
        $diff = array();
        foreach ($table1 as $k => $v) {
            if (!isset($table2[$k])) {
                $diff[$k] = $table1[$k];
                continue;
            } elseif (is_callable($table2[$k])) { // disregard functions(sanity check)
                continue;
            } elseif (is_array($table1[$k]) && is_array($table2[$k])) {
                $diff2 = Common::GetDifference($table1[$k], $table2[$k]);
                if (count($diff2) != 0) {
                    $diff[$k] = $diff2;
                }
                continue;
            } elseif ($table1[$k] == $table2[$k]) {
                continue;
            }
            $diff[$k] = $table1[$k];
        }
        foreach ($table2 as $k => $v) {
            if (!isset($table1[$k])) {
                $diff[$k] = "_nil";
            } elseif (is_callable($table1[$k])) { // disregard functions(sanity check)
                continue;
            } elseif (is_array($table1[$k]) && is_array($table2[$k])) {
                $diff2 = Common::GetDifference($table1[$k], $table2[$k]);
                if (count($diff2) != 0) {
                    $diff[$k] = $diff2;
                }
                continue;
            }
        }
        return $diff;
    }

    public static function emailHandler($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function GenerateRandomString($length = 10, $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $charLength = strlen($chars);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $chars[rand(0, $charLength - 1)];
        }
        return $randomString;
    }
}

abstract class BasicEnum
{
    private static $constCacheArray = null;

    private function __construct()
    {
        /*
    Preventing instance :)
     */
    }

    private static function getConstants()
    {
        if (self::$constCacheArray == null) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function isValidName($name, $strict = false)
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict = true);
    }
}
