<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/memberfull.php";

class MemberNull extends MemberFull
{
    // Overrides the constructor for nothingness
    public function __construct()
    {
        
    }

    public function hasPermission($fullPerm, $fullRealm = "web")
    {
        return false;
    }
}
