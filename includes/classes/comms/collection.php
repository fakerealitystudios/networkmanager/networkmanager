<?php

namespace NetworkManager\Communication;

require_once NM_CLASS_ROOT . 'comms/base.php';

// A null implementation
class Collection extends Base
{
    private $sockets = array();

    public function __construct(...$sockets)
    {
        $this->sockets = $sockets;
    }

    public function addSocket($socket)
    {
        $this->sockets[] = $socket;
    }

    // obviously, we can't return neatly whether each action all passed/failed unless we want to form arrays with server ids, just pass true for now

    public function command($cmd)
    {
        foreach ($this->sockets as $socket) {
            $socket->command($cmd);
        }
        return true;
    }

    public function merge($nmid, $merged_nmid)
    {
        foreach ($this->sockets as $socket) {
            $socket->merge($nmid, $merged_nmid);
        }
        return true;
    }
    public function ban($nmid, $banTime, $length, $reason)
    {
        foreach ($this->sockets as $socket) {
            if (!$socket->performBanInCollection()) {continue;}
            $socket->ban($nmid, $banTime, $length, $reason);
        }
        return true;
    }
    public function kick($nmid, $reason)
    {
        foreach ($this->sockets as $socket) {
            if (!$socket->performKickInCollection()) {continue;}
            $socket->kick($nmid, $reason);
        }
        return true;
    }
    public function giveRank($nmid, $rank, $realm, $id)
    {
        foreach ($this->sockets as $socket) {
            $socket->giveRank($nmid, $rank, $realm, $id);
        }
        return true;
    }
    public function takeRank($nmid, $rank, $realm, $id)
    {
        foreach ($this->sockets as $socket) {
            $socket->takeRank($nmid, $rank, $realm, $id);
        }
        return true;
    }
    public function givePermission($nmid, $perm, $realm)
    {
        foreach ($this->sockets as $socket) {
            $socket->givePermission($nmid, $perm, $realm);
        }
        return true;
    }
    public function takePermission($nmid, $perm, $realm)
    {
        foreach ($this->sockets as $socket) {
            $socket->takePermission($nmid, $perm, $realm);
        }
        return true;
    }

    public function createRank($rank, $realm)
    {
        foreach ($this->sockets as $socket) {
            $socket->createRank($rank, $realm);
        }
        return true;
    }
    public function destroyRank($rank, $realm)
    {
        foreach ($this->sockets as $socket) {
            $socket->destroyRank($rank, $realm);
        }
        return true;
    }
    public function giveRankPermissions($rank, $perm, $realm)
    {
        foreach ($this->sockets as $socket) {
            $socket->giveRankPermissions($rank, $perm, $realm);
        }
        return true;
    }
    public function takeRankPermissions($rank, $perm, $realm)
    {
        foreach ($this->sockets as $socket) {
            $socket->takeRankPermissions($rank, $perm, $realm);
        }
        return true;
    }
}
