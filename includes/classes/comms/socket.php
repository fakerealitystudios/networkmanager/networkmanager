<?php

namespace NetworkManager\Communication;

require_once NM_CLASS_ROOT . 'comms/base.php';

class Socket extends Base
{
    protected function sendPayload($payload)
    {
        $payload['skey'] = $this->srv->getSecret();

        $json = json_encode($payload);

        $ip = explode(":", $this->srv->getAddress());
        $port = $ip[1] - 2000;
        $ip = $ip[0];
        $socket = @fsockopen($ip, $port, $errno, $errstr, 3);
        if ($socket) {
            fwrite($socket, $json . "\1");
            fclose($socket);
            return true;
        }
        return false;
    }

    public function performKickInCollection()
    {
        return true;
    }

    public function performBanInCollection()
    {
        return true;
    }

    public function command($cmd)
    {
        $payload = array(
            "action" => "command",
            "cmd" => $cmd,
        );
        return $this->sendPayload($payload);
    }

    public function merge($nmid, $merged_nmid)
    {
        $payload = array(
            "action" => "merge",
            "nmid" => $nmid,
            "merged_nmid" => $merged_nmid,
        );
        return $this->sendPayload($payload);
    }
    public function ban($nmid, $banTime, $length, $reason)
    {
        $payload = array(
            "action" => "ban",
            "nmid" => $nmid,
            "banTime" => $banTime,
            "length" => $length,
            "reason" => $reason,
        );
        return $this->sendPayload($payload);
    }
    public function kick($nmid, $reason)
    {
        $payload = array(
            "action" => "kick",
            "nmid" => $nmid,
            "reason" => $reason,
        );
        return $this->sendPayload($payload);
    }
    public function giveRank($nmid, $rank, $realm, $id)
    {
        $payload = array(
            "action" => "giveRank",
            "nmid" => $nmid,
            "rank" => $rank,
            "realm" => $realm,
            "id" => $id,
        );
        return $this->sendPayload($payload);
    }
    public function takeRank($nmid, $rank, $realm, $id)
    {
        $payload = array(
            "action" => "takeRank",
            "nmid" => $nmid,
            "rank" => $rank,
            "realm" => $realm,
            "id" => $id,
        );
        return $this->sendPayload($payload);
    }
    public function givePermission($nmid, $perm, $realm)
    {
        $payload = array(
            "action" => "givePermission",
            "nmid" => $nmid,
            "permission" => $perm,
            "realm" => $realm,
        );
        return $this->sendPayload($payload);
    }
    public function takePermission($nmid, $perm, $realm)
    {
        $payload = array(
            "action" => "takePermission",
            "nmid" => $nmid,
            "permission" => $perm,
            "realm" => $realm,
        );
        return $this->sendPayload($payload);
    }

    public function createRank($rank, $realm)
    {
        $payload = array(
            "action" => "createRank",
            "rank" => $rank,
            "realm" => $realm,
        );
        return $this->sendPayload($payload);
    }
    public function destroyRank($rank, $realm)
    {
        $payload = array(
            "action" => "destroyRank",
            "rank" => $rank,
            "realm" => $realm,
        );
        return $this->sendPayload($payload);
    }
    public function giveRankPermissions($rank, $perm, $realm)
    {
        $payload = array(
            "action" => "giveRankPermissions",
            "rank" => $rank,
            "permission" => $perm,
            "realm" => $realm,
        );
        return $this->sendPayload($payload);
    }
    public function takeRankPermissions($rank, $perm, $realm)
    {
        $payload = array(
            "action" => "takeRankPermissions",
            "rank" => $rank,
            "permission" => $perm,
            "realm" => $realm,
        );
        return $this->sendPayload($payload);
    }
}
