<?php

namespace NetworkManager\Communication;

abstract class Base
{
    protected $srv; // Server

    public function __construct($srv)
    {
        $this->srv = $srv;
    }

    public function performKickInCollection()
    {
        return false;
    }

    public function performBanInCollection()
    {
        return false;
    }

    // For each function, retrun false on fails with no error, string for error message, and true for succeed.

    // Server Actions
    abstract public function command($cmd);

    // Member Actions
    abstract public function merge($nmid, $merged_nmid);
    abstract public function ban($nmid, $banTime, $length, $reason);
    abstract public function kick($nmid, $reason);
    abstract public function giveRank($nmid, $rank, $realm, $id);
    abstract public function takeRank($nmid, $rank, $realm, $id);
    abstract public function givePermission($nmid, $perm, $realm);
    abstract public function takePermission($nmid, $perm, $realm);

    // Rank Actions
    abstract public function createRank($rank, $realm);
    abstract public function destroyRank($rank, $realm);
    abstract public function giveRankPermissions($rank, $perm, $realm);
    abstract public function takeRankPermissions($rank, $perm, $realm);
}
