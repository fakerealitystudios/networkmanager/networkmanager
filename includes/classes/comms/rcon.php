<?php

namespace NetworkManager\Communication;

require_once NM_CLASS_ROOT . 'comms/base.php';

class RCon extends Base
{
    private $rcon;

    public function __construct($srv)
    {
        $this->srv = $srv;
        $this->rcon = \NetworkManager\RCon\RCon::getClass($srv->getGame());
        $this->rcon = new $this->rcon($this->srv->getIpAddress(), $this->srv->getPort(), $this->srv->get);
    }

    public function performKickInCollection()
    {
        return true;
    }

    public function performBanInCollection()
    {
        return true;
    }

    public function command($cmd)
    {
        return false;
    }

    public function merge($nmid, $merged_nmid)
    {
        return false;
    }
    public function ban($nmid, $banTime, $length, $reason)
    {
        return false;
    }
    public function kick($nmid, $reason)
    {
        return false;
    }
    public function giveRank($nmid, $rank, $realm, $id)
    {
        return false;
    }
    public function takeRank($nmid, $rank, $realm, $id)
    {
        return false;
    }
    public function givePermission($nmid, $perm, $realm)
    {
        return false;
    }
    public function takePermission($nmid, $perm, $realm)
    {
        return false;
    }

    public function createRank($rank, $realm)
    {
        return false;
    }
    public function destroyRank($rank, $realm)
    {
        return false;
    }
    public function giveRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
    public function takeRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
}
