<?php

namespace NetworkManager\Communication;

require_once NM_CLASS_ROOT . 'comms/base.php';
require_once NM_CLASS_ROOT . 'medoo.php';

class Invision extends Base
{
    private $apikey = "3b8b5baafa47030cf543c041bb6766de";
    private $db;
    private $db_info = array(
        'logging' => false,
        'database_type' => 'mysql',

        'prefix' => '',
        'server' => 'forum.refugeecentral.com',
        'port' => 3306,
        'database_name' => 'aberrationweb_forum',
        'username' => 'aberrationweb',
        'password' => 'TG3HMxKiRe',
    );

    public function __construct($srv)
    {
        parent::__construct($srv);

        try {
            $this->db = new \Medoo\Medoo($this->db_info);
        } catch (\PDOException $e) {

        }
    }

    protected function sendPayload($endpoint, $post)
    {
        $ch = curl_init($this->srv->getAddress() . $endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->apikey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            return false;
        }

        curl_close($ch);

        return $response;
    }

    private function getInvisionMemberId($nmid)
    {
        // Due to limitations by the API(because fuck me I guess) we'll have to access the database
        $id = $this->db->get("forumcore_login_links", "token_member", array("token_identifier" => $nmid));
        if ($id and $id > 0) {
            return $id;
        }
        return -1;
    }

    public function command($cmd)
    {
        return "Invision doesn't allow commands, sorry!";
    }

    // Members
    public function merge($nmid, $merged_nmid)
    {
        return false;
    }
    public function ban($nmid, $banTime, $length, $reason)
    {
        return false;
    }
    public function kick($nmid, $reason)
    {
        return false;
    }
    public function giveRank($nmid, $rank, $realm, $id)
    {
        /*$post = array(
            //"name" => "Madkiller Max"
        );
        return $this->sendPayload("/core/members/" . $this->getInvisionMemberId($nmid), $post);*/
        return false;
    }
    public function takeRank($nmid, $rank, $realm, $id)
    {
        return false;
    }
    public function givePermission($nmid, $perm, $realm)
    {
        return false;
    }
    public function takePermission($nmid, $perm, $realm)
    {
        return false;
    }

    // Ranks
    public function createRank($rank, $realm)
    {
        return false;
    }
    public function destroyRank($rank, $realm)
    {
        return false;
    }
    public function giveRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
    public function takeRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
}
