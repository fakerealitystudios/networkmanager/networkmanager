<?php

namespace NetworkManager\Communication;

require_once NM_CLASS_ROOT . 'comms/base.php';

class Webhook extends Base
{
    protected function sendPayload($endpoint, $post)
    {
        $post['panel'] = NM_URL;

        $ch = curl_init($this->srv->getAddress() . $endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            return false;
        }

        curl_close($ch);

        return $response;
    }

    public function command($cmd)
    {
        $post = array(
            "cmd" => $cmd,
        );
        return $this->sendPayload("command", $post);
    }

    public function merge($nmid, $merged_nmid)
    {
        $post = array(
            "nmid" => $nmid,
            "merged_nmid" => $merged_nmid,
        );
        return $this->sendPayload("merge", $post);
    }
    public function ban($nmid, $banTime, $length, $reason)
    {
        $post = array(
            "nmid" => $nmid,
            "banTime" => $banTime,
            "length" => $length,
            "reason" => $reason,
        );
        return $this->sendPayload("ban", $post);
    }
    public function kick($nmid, $reason)
    {
        $post = array(
            "nmid" => $nmid,
            "reason" => $reason,
        );
        return $this->sendPayload("kick", $post);
    }
    public function giveRank($nmid, $rank, $realm, $id)
    {
        $post = array(
            "nmid" => $nmid,
            "rank" => $rank,
            "realm" => $realm,
            "id" => $id,
        );
        return $this->sendPayload("giveRank", $post);
    }
    public function takeRank($nmid, $rank, $realm, $id)
    {
        $post = array(
            "nmid" => $nmid,
            "rank" => $rank,
            "realm" => $realm,
            "id" => $id,
        );
        return $this->sendPayload("takeRank", $post);
    }
    public function givePermission($nmid, $perm, $realm)
    {
        $post = array(
            "nmid" => $nmid,
            "permission" => $perm,
            "realm" => $realm,
        );
        return $this->sendPayload("givePermission", $post);
    }
    public function takePermission($nmid, $perm, $realm)
    {
        $post = array(
            "nmid" => $nmid,
            "permission" => $perm,
            "realm" => $realm,
        );
        return $this->sendPayload("takePermission", $post);
    }

    // Ranks
    public function createRank($rank, $realm)
    {
        return false;
    }
    public function destroyRank($rank, $realm)
    {
        return false;
    }
    public function giveRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
    public function takeRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
}
