<?php

namespace NetworkManager\Communication;

require_once NM_CLASS_ROOT . 'comms/base.php';

// A null implementation
class None extends Base
{
    // Server
    public function command($cmd)
    {
        return false;
    }

    // Members
    public function merge($nmid, $merged_nmid)
    {
        return false;
    }
    public function ban($nmid, $banTime, $length, $reason)
    {
        return false;
    }
    public function kick($nmid, $reason)
    {
        return false;
    }
    public function giveRank($nmid, $rank, $realm, $id)
    {
        return false;
    }
    public function takeRank($nmid, $rank, $realm, $id)
    {
        return false;
    }
    public function givePermission($nmid, $perm, $realm)
    {
        return false;
    }
    public function takePermission($nmid, $perm, $realm)
    {
        return false;
    }

    // Ranks
    public function createRank($rank, $realm)
    {
        return false;
    }
    public function destroyRank($rank, $realm)
    {
        return false;
    }
    public function giveRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
    public function takeRankPermissions($rank, $perm, $realm)
    {
        return false;
    }
}
