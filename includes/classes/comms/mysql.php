<?php

namespace NetworkManager\Communication;

require_once NM_CLASS_ROOT . 'comms/socket.php';
require_once NM_CLASS_ROOT . 'database.php';

// We perform the same as Socket, just send it elsewhere...
class MySQL extends Socket
{
    protected function sendPayload($payload)
    {
        $json = json_encode($payload);

        \NetworkManager\Database::getInstance()->insert("serverActions", array("sid" => $this->srv->getId(), "payload" => $json));
        return true;
    }
}
