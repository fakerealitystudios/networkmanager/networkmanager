<?php

namespace NetworkManager;

require_once NM_CLASS_ROOT . "database.php";
require_once NM_CLASS_ROOT . "rankmanager.php";
require_once NM_CLASS_ROOT . "permissions.php";
require_once NM_CLASS_ROOT . "data.php";
require_once NM_CLASS_ROOT . "realm.php";

class MemberFull extends RealmObject
{
    use Data;
    use Permissions {
        givePermission as protected superGivePermission;
        takePermission as protected superTakePermission;
        hasPermission as protected superHasPermission;
    }

    protected $db;

    // member data
    protected $uid = null;
    protected $centerid = null;
    protected $displayName = "";
    protected $username = "";
    protected $email = "";
    protected $avatar = null;
    protected $validated = false;
    protected $ip = "";
    protected $rank = "user";
    protected $ranks = array();
    protected $created = 0;
    protected $disabled = 0;

    public function __sleep()
    {
        $arr = array_keys(get_object_vars($this));
        if (($key = array_search("db", $arr)) !== false) {
            unset($arr[$key]);
        }
        return $arr;
    }

    public function __wakeup() {
        $this->db = Database::getInstance();

        MemberManager::getInstance()->add($this);
    }

    public function __construct($uid)
    {
        $this->db = Database::getInstance();

        $res = $this->db->get('members', [
            "centerId",
            "username",
            "displayName",
            "email",
            "avatar",
            "created",
            "validated",
            "disabled",
        ], [
            "id" => $uid,
        ]);
        // If we didn't find user, he is invalid, quit
        if (!$res) {
            return;
        }
        $this->uid = $uid;
        $this->centerid = $res['centerId'];

        $this->displayName = $res["displayName"];
        $this->username = $res["username"];
        $this->email = $res["email"];
        $this->avatar = $res["avatar"];
        $this->created = $res["created"];
        $this->validated = $res["validated"];
        $this->disabled = $res["disabled"];

        $this->ip = $this->db->get('memberSessions', "ip", ['uid' => $uid, 'ORDER' => ['startTime' => 'DESC']]);
        $res = $this->db->select('memberRanks', ["id", "rank", "realm"], [
            'uid' => $uid,
        ]);
        if ($res) {
            foreach ($res as $row) {
                $this->ranks[] = $row;
            }
        }
        $this->rank = $this->getRealmRank("web", 0);

        $res = $this->db->select('memberData', [
            "vid",
            "value",
            "realm",
        ], [
            'uid' => $uid,
        ]);
        if ($res) {
            foreach ($res as $row) {
                $this->loadData($row["vid"], $row["value"], $row["realm"]);
            }
        }

        $res = $this->db->select("memberPermissions", ["value", "realm"], [
            "uid" => $uid,
        ]);
        if ($res) {
            foreach ($res as $row) {
                $this->loadPermission($row["value"], $row["realm"]);
            }
        }

        $this->createCenterId(); // Will create a center only if one is not already available
    }

    // Methods
    public function ban($length, $reason, $server, $admin = 1, $global = 0)
    {

    }

    // Setters
    public function setUsername($username)
    {
        $username = strtolower($username);
        if ($this->getUsername() == $username) {return true;}
        if (!$this->db->has("members", array("username" => $username))) {
            $this->db->update("members", array("username" => $username), array("id" => $this->getId()));
            $this->username = $username;
            return true;
        }
        return false;
    }

    public function setDisplayName($displayName)
    {
        if ($this->getDisplayName() == $displayName) {return true;}
        $this->db->update("members", array("displayName" => $displayName), array("id" => $this->getId()));
        $this->db->insert("memberAliases", array("uid" => $this->getId(), "alias" => $displayName));
        $this->displayName = $displayName;
        return true;
    }

    public function setAvatar($url)
    {
        $this->avatar = $url;
        $this->db->update("members", array("avatar" => $url), array("id" => $this->getId()));
    }

    public function setEmail($email)
    {
        if ($this->getEmail() == $email) {
            return true;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false; // invalid email format
        }
        $this->db->update("members", array("email" => $email, "validated" => 0), array("id" => $this->getId()));
        $this->email = $email;
        $this->validated = 0;
        $this->db->delete("memberValidation", array("uid" => $this->getId()));
        $this->db->insert("memberValidation", array("uid" => $this->getId(), "token" => \Medoo\Medoo::raw('UUID()')));
        $token = $this->db->get("memberValidation", "token", array("uid" => $this->getId()));
        
        $link = NM_URL . 'profile/?group=personal&validate=' . $token;

        $body = '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hello <strong>'.$this->getDisplayName().'</strong>,</p>';
        $body .= '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Thanks for registering your email with NetworkManager! Please just click or copy the link below to validate your email.</p>';
        $body .= '<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
            <tbody>
            <tr>
                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                    <tbody>
                    <tr>
                        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> <a href="'.$link.'" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; border-color: #3498db;">'.$link.'</a> </td>
                    </tr>
                    </tbody>
                </table>
                </td>
            </tr>
            </tbody>
        </table>';
        $body .= '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">End words</p>';
        
        return $this->email("Validate your Email", $body, true);
    }

    public function validateEmail($token)
    {
        $stmt = $this->db->delete("memberValidation", array("uid" => $this->getId(), "token" => $token));
        //var_dump($stmt->rowCount());
        if ($stmt->rowCount() > 0) {
            $this->db->update("members", array("validated" => 1), array("id" => $this->getId()));
            $this->validated = 1;
            return true;
        }
        return false;
    }

    public function notify($subject, $body)
    {
        $this->db->insert("memberNotifications", array(
            "uid" => $this->getId(),
            "type" => "WARNING",
            "link" => null,
            "header" => $subject,
            "info" => $body,
            "time" => time(),
        ));
    }

    public function createCenterId()
    {
        if ($this->getCenterId() != null)
            return $this->getCenterId();
        $tokens = $this->getLinkedAccounts();
        if (count($tokens) == 0) {
            // the fuck?
            \Sentry\captureMessage("Member has no token", \Sentry\Severity::warning());
            return false;
        }
        $data = array();
        $data['display_name'] = $this->getDisplayName();
        $data['ipaddress'] = count($this->getIpAddresses()) > 0 ? $this->getIpAddresses()[0] : null;
        $data['nmid'] = $this->getId();
        $data['tokens'] = array();
        if (count($tokens) >= 1) {
            foreach($tokens as $token) {
                $data['tokens'][] = array($token['type'], $token['token']);
            }
        }
        $data['tokens'] = json_encode($data['tokens']);
        $res = \NetworkManager\API::post("/member/", $data);
        if (isset($res['data']['id'])) {
            $this->centerid = $res['data']['id'];
            foreach($res['data']['tokens'] as $token) {
                MemberManager::addToken($this->getId(), $token['token_name'], $token['token']);
            }
        }
        $this->db->update("members", array(
            "centerid" => $this->centerid,
        ), array(
            "id" => $this->getId(),
        ));
        return $this->getCenterId();
    }

    // Getters
    public function isValid()
    {
        return $this->uid != null;
    }

    public function getId()
    {
        return $this->uid;
    }

    public function getCenterId()
    {
        return $this->centerid;
    }

    public function getDisplayName()
    {
        return $this->displayName;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getEmail($force = false)
    {
        if (!$force && !$this->emailValidated()) {
            return null; // Don't return non-validated emails
        }
        return $this->email;
    }

    public function emailValidated()
    {
        return $this->validated == 1;
    }

    public function getAvatar()
    {
        if ($this->avatar == null || $this->avatar == "") {
            return "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_full.jpg";
        }
        return $this->avatar;
    }

    public function getBadges()
    {
        if (!isset($this->badges)) {
            $this->badges = array();
            $this->badges = $this->db->select('memberBadges', "badge", [
                'uid' => $this->uid,
            ]);
        }
        return $this->badges;
    }

    public function getIpAddresses()
    {
        if (!isset($this->ips)) {
            $this->ips = $this->db->distinct()->select('memberSessions', "ip", [
                'uid' => $this->uid,
                'ip[!]' => null,
                'ORDER' => array('startTime' => 'DESC')
            ]);
            if (!$this->ips) {
                $this->ips = array();
            }
        }
        return $this->ips;
    }

    public function addLinkedAccount($type, $token)
    {
        $this->db->insert("memberLinks", array(
            "uid" => $this->uid,
            "type" => $type,
            "token" => $token,
        ));
        $this->linkedAccounts[$type] = $token;
    }

    public function getLinkedAccounts()
    {
        if (!isset($this->linkedAccounts)) {
            $this->linkedAccounts = array();
            $res = $this->db->select('memberLinks', array("type", "token"), [
                'uid' => $this->uid,
            ]);
            if ($res) {
                $this->linkedAccounts = $res;
            }
        }
        return $this->linkedAccounts;
    }

    public function getAliases()
    {
        if (!isset($this->aliases)) {
            $this->aliases = array();
            $this->aliases = $this->db->select('memberAliases', "alias", [
                'uid' => $this->uid,
            ]);
        }
        return $this->aliases;
    }

    public function getServers()
    {
        if (!isset($this->sids)) {
            $this->sids = array();
            $this->sids = $this->db->distinct()->select('memberSessions', "sid", [
                'uid' => $this->uid,
            ]);
        }
        return $this->sids;
    }

    public function getCurrentServers()
    {
        return array_keys($this->getSessionIds());
    }

    public function getSessionIds()
    {
        if (!isset($this->usids)) {
            $rows = $this->db->select('memberSessions', ["id", "sid"], [
                'uid' => $this->uid,
                'endTime' => -1,
            ]);
            $this->usids = array();
            foreach($rows as $row) {
                $this->usids[$row['sid']] = $row['id'];
            }
        }
        return $this->usids;
    }

    public function getSessionId($sid)
    {
        if (!isset($this->usids)) {
            $this->getSessionIds();
        }
        if (!isset($this->usids[$sid]))
            return null;
        return $this->usids[$sid];
    }

    public function isSetup()
    {
        return $this->getUsername() != "" && $this->getDisplayName() != "";
    }

    public function isDisabled()
    {
        return $this->disabled == 1;
    }

// Ranks
    public function getRanks()
    {
        if (count($this->ranks) == 0)
        {
            return array(array(
                "rank" => "user",
                "realm" => "*",
            ));
        }
        return $this->ranks;
    }

    public function getRealmRanks($fullRealm, $subgroup = -1)
    {
        $ranks = array();
        $rankrealm = array();
        $realmParts = Realm::getRealm($fullRealm)->getParts();
        if (($realmParts[0] == "*" || is_numeric($realmParts[0])) && count($realmParts) > 1) {
            $rankrealm = "*";
            foreach ($this->ranks as $row) {
                $rsubgroup = RankManager::getInstance()->get($row['rank'])->getSubGrouping();
                if ($subgroup != -1 && $subgroup != $rsubgroup)
                    continue;
                if ($row['realm'] == $fullRealm || ($row['realm'] == ("*" . Realm::$delimiter . $realmParts[1]) && $rankrealm[$rsubgroup] == "*") || ($row['realm'] == "*" && $rankrealm[$rsubgroup] == "*")) {
                    $ranks[] = $row['rank'];
                    $rankrealm[$rsubgroup] = $row['realm'];
                }
            }
        } else {
            $rankrealm = "*";
            foreach ($this->ranks as $row) {
                $rsubgroup = RankManager::getInstance()->get($row['rank'])->getSubGrouping();
                if ($subgroup != -1 && $subgroup != $rsubgroup)
                    continue;
                if ($row['realm'] == $fullRealm || ($row['realm'] == "*" && $rankrealm[$rsubgroup] == "*")) {
                    $ranks[] = $row['rank'];
                    $rankrealm[$rsubgroup] = $row['realm'];
                }
            }
        }
        return $ranks;
    }

    public function getRealmRank($fullRealm, $subgroup = -1)
    {
        $rank =  null;
        if ($subgroup == 0)
            $rank = "user";
        $realmParts = Realm::getRealm($fullRealm)->getParts();
        if (($realmParts[0] == "*" || is_numeric($realmParts[0])) && count($realmParts) > 1) {
            $rankrealm = "*";
            foreach ($this->ranks as $row) {
                if ($subgroup != -1 && $subgroup != RankManager::getInstance()->get($row['rank'])->getSubGrouping())
                    continue;
                if ($row['realm'] == $fullRealm || $row['realm'] == ("*" . Realm::$delimiter . $realmParts[1]) || ($row['realm'] == "*" && $rankrealm == "*")) {
                    $rank = $row['rank'];
                    $rankrealm = $row['realm'];
                }
            }
        } else {
            $rankrealm = "*";
            foreach ($this->ranks as $row) {
                if ($subgroup != -1 && $subgroup != RankManager::getInstance()->get($row['rank'])->getSubGrouping())
                    continue;
                if ($row['realm'] == $fullRealm || ($row['realm'] == "*" && $rankrealm == "*")) {
                    $rank = $row['rank'];
                    $rankrealm = $row['realm'];
                }
            }
        }
        return $rank;
    }

    public function getRank($sid = null)
    {
        if ($sid != null) { // Attempt to find rank for specific server
            $srv = \NetworkManager\ServerManager::getInstance()->get($sid);
            if ($srv->isValid()) {
                return $this->getRealmRank($srv->getRealm(), 0);
            }
        }
        return $this->rank;
    }

    public function addRank($rank, $fullRealm)
    {
        $this->giveRank($rank, $fullRealm);
    }
    public function giveRank($rank, $fullRealm)
    {
        $r = RankManager::getInstance()->get($rank);
        foreach ($this->getRanks() as $k => $v) {
            if ($v['realm'] == $fullRealm) {
                if ($v['rank'] == $rank) {
                    return false; // already have the same damn rank in the same damn place
                } else if (RankManager::getInstance()->get($v['rank'])->getSubGrouping() == $r->getSubGrouping()) {
                    $this->takeRank($v['rank'], $v['realm']);
                }
            }
        }
        $this->db->insert('memberRanks', array(
            "uid" => $this->uid,
            "rank" => $rank,
            "realm" => $fullRealm,
        ));
        $id = $this->db->id();
        $this->ranks[] = array("rank" => $rank, "realm" => $fullRealm);

        \NetworkManager\ServerManager::getSelectiveCommunicationSockets($this->getCurrentServers())->giveRank($this->uid, $rank, $fullRealm, $id);
        return $id;
    }

    public function removeRank($rank, $fullRealm)
    {
        $this->takeRank($rank, $fullRealm);
    }
    public function takeRank($rank, $fullRealm)
    {
        $rid = $this->db->get('memberRanks', "id", array(
            "AND" => [
                "uid" => $this->uid,
                "rank" => $rank,
                "realm" => $fullRealm,
            ],
        ));
        $this->db->delete("memberRanks", array(
			"id" => $rid
		));
        foreach ($this->ranks as $id => $row) {
            if ($row['rank'] == $rank && $row['realm'] == $fullRealm) {
                array_splice($this->ranks, $id, 1);
            }
        }

        \NetworkManager\ServerManager::getSelectiveCommunicationSockets($this->getCurrentServers())->takeRank($this->uid, $rank, $fullRealm, $rid);
    }

    public function canTarget($other)
    {
        if ($this == $other) { // You can (almost)always run commands on yourself
            return true;
        }
        $userSpecific = $this->hasPermission("nm.target." . $other->getId());
        if ($userSpecific != null) { // If we have a user specific rule, use that
            return $userSpecific;
        }
        // Now the ranks can determine if we can target them
        return RankManager::getInstance()->canTarget($this->getRank(), $other->getRank());
    }

    public function email($subject, $body, $force = false)
    {
        return Mail::getInstance()->sendMail($this->getEmail($force), $this->getDisplayName(), $subject, $body);
    }

// Permissions Interface
    public function hasPermission($fullPerm, $fullRealm = "web")
    {
        $result = $this->superHasPermission($fullPerm, $fullRealm);
        if ($result === true) {
            return true;
        } elseif ($result === false) {
            return false;
        } elseif (RankManager::getInstance()->get($this->getRealmRank($fullRealm)) != NULL && ($result = RankManager::getInstance()->get($this->getRealmRank($fullRealm))->hasPermission($fullPerm, $fullRealm)) === true) {
            return true;
        } elseif ($result === false) {
            return false;
        } else {
            //return RankManager::getInstance()->get($this->getRank())->hasPermission($fullPerm, $fullRealm);
        }
        return null;
    }

    public function givePermission($fullPerm, $fullRealm = "web") {
        if ($this->superGivePermission($fullPerm, $fullRealm)) {
            \NetworkManager\ServerManager::getSelectiveCommunicationSockets($this->getCurrentServers())->givePermission($this->uid, $fullPerm, $fullRealm);
        }
    }

    public function takePermission($fullPerm, $fullRealm = "web") {
        if ($this->superTakePermission($fullPerm, $fullRealm)) {
            \NetworkManager\ServerManager::getSelectiveCommunicationSockets($this->getCurrentServers())->takePermission($this->uid, $fullPerm, $fullRealm);
        }
    }

// Data Interface
    public function saveData()
    {

    }

    public function deleteData()
    {

    }
}
