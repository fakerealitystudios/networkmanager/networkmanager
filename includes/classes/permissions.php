<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/realm.php";

trait Permissions
{
    public static $perm_delimiter = ".";
    public $perms = array();

    public function loadPermission($fullPerm, $fullRealm)
    {
        $realm = &$this->getRealm($fullRealm)["__perms"];
        $realm[] = $fullPerm;
        $this->perms[] = array("perm" => $fullPerm, "realm" => $fullRealm); // full list of perms, for all realms, for this object
    }

    public function givePermission($fullPerm, $fullRealm = "web")
    {
        $found = false;
        $realm = &$this->getRealm($fullRealm)["__perms"];
        foreach ($realm as $id => $perm) {
            if ($fullPerm == $perm) {
                $found = true;
                break;
            }
        }
        if ($found) {
            return false;
        }

        $this->loadPermission($fullPerm, $fullRealm);

        $data = array();
        $data['value'] = $fullPerm;
        $data['realm'] = $fullRealm;
        $table = "";
        if (substr(get_class($this), 0, 21) == "NetworkManager\Member") {
            $data['uid'] = $this->getId();
            $table = "memberPermissions";
        } elseif (get_class($this) == "NetworkManager\Rank") {
            $data['rank'] = $this->getRank();
            $table = "rankPermissions";
        }

        if ($table != "") {
            $this->db->insert($table, $data);
        }
        return true;
    }

    public function takePermission($fullPerm, $fullRealm = "web")
    {
        $found = false;
        $realm = &$this->getRealm($fullRealm)["__perms"];
        foreach ($realm as $id => $perm) {
            if ($fullPerm == $perm) {
                unset($realm[$id]);
                $found = true;
            }
        }
        if (!$found) {
            return false;
        }
        foreach ($this->perms as $id => $row) {
            if ($row['perm'] == $fullPerm && $row['realm'] == $fullRealm) {
                unset($this->perms[$id]);
            }
        }

        $data = array();
        $data['value'] = $fullPerm;
        $data['realm'] = $fullRealm;
        $table = "";
        if (substr(get_class($this), 0, 21) == "NetworkManager\Member") {
            $data['uid'] = $this->getId();
            $table = "memberPermissions";
        } elseif (substr(get_class($this), 0, 19) == "NetworkManager\Rank") {
            $data['rank'] = $this->getRank();
            $table = "rankPermissions";
        }
        $this->db->delete($table, $data);
        return true;
    }

    public function hasPermission($fullPerm, $fullRealm = "web")
    {
        $realm = $this->getRealmInclusive($fullRealm)["__perms"];
        $splitPerm = explode(".", $fullPerm);

        // Don't save targetting perms
        if (substr($fullPerm, 0, 10) != "nm.target.") {
            global $NM;
            $game = (new Realm($fullRealm))->getFirst();
            if (!isset($NM->perms[$game]) || !in_array($fullPerm, $NM->perms[$game])) {
                if (!isset($NM->perms[$game]))
                    $NM->perms[$game] = array();
                $NM->perms[$game][] = $fullPerm;
                $NM->db->insert("permissions", array(
                    "perm" => $fullPerm,
                    "game" => (new Realm($fullRealm))->getFirst(),
                ));
            }
        }

        // Check if any perms are negated first, they take priority
        if (in_array("!" . $fullPerm, $realm)) {
            return false;
        } elseif (substr($fullPerm, -1, 1) == "%") {
            $perm = "!" . substr($fullPerm, 0, -1); // Remove the wildcard
            foreach ($realm as $realmPerm) {
                if (substr($realmPerm, 0, strlen($perm)) == $perm) {
                    return false;
                }
            }
        } else {
            $perm = "*";
            foreach ($splitPerm as $partPerm) {
                if (in_array("!" . $perm, $realm)) {
                    return false;
                } else {
                    $perm = substr($perm, 0, -1);
                    $perm = $perm . $partPerm . ".*";
                }
            }
        }

        // Now we check if we do have the perm
        if (in_array($fullPerm, $realm)) {
            return true;
        } elseif (substr($fullPerm, -1, 1) == "%") {
            $perm = substr($fullPerm, 0, -1); // Remove the wildcard
            foreach ($realm as $realmPerm) {
                if (substr($realmPerm, 0, strlen($perm)) == $perm) {
                    return true;
                }
            }
        } else {
            $perm = "*";
            foreach ($splitPerm as $partPerm) {
                if (in_array($perm, $realm)) {
                    return true;
                } else {
                    $perm = substr($perm, 0, -1);
                    $perm = $perm . $partPerm . ".*";
                }
            }
        }
        return null; // null so we know nothing was found
    }

    public function getPermissions($fullRealm = "web")
    {
        return $this->getRealmInclusive($fullRealm)["__perms"];
    }
}
