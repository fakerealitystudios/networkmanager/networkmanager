<?php

namespace NetworkManager;

class Server
{
    private $db;

    private $id = null;
    private $address;
    private $name;
    private $game;
    private $gamemode;
    private $secret;
    private $communication = 0;
    private $commsocket = null;

    private $nmids;
    private $cnmids;
    private $ssids;
    private $cssids;

    public function __construct($row)
    {
        $this->db = Database::getInstance();

        if (!$row) {
            return;
        }
        // If we didn't find user, he is invalid, quit
        $this->id = $row["id"];

        $this->address = $row["address"];
        $this->name = $row["name"];
        $this->game = $row["game"];
        if (isset($row["gamemode"])) {
            $this->gamemode = $row["gamemode"];
        }

        $this->secret = $row["secret"];
        $this->communication = $row["communication"];
        $this->makeCommunicationSocket();
    }

    public function isValid()
    {
        return $this->id != null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getAddress()
    {
        return $this->address;
    }

    public function getIpAddress()
    {
        // Just in case it's an ipv6 address
        $arr = explode(":", $this->address);
        array_pop($arr);
        return implode(":", $arr);
    }
    public function getPort()
    {
        return array_pop(explode(":", $this->address));
    }

    public function getGame()
    {
        return $this->game;
    }

    public function getGameRealms()
    {
        // Dependent on the game
        if ($this->getGame() == "minecraft") {
            //return $this->data['worlds'];
        }
        return [];
    }

    public function getRealm()
    {
        return $this->id . "." . $this->game;
    }

    public function getGamemode()
    {
        return $this->gamemode;
    }

    public function getSecret()
    {
        return $this->secret;
    }

    public function getSessions() {
        if (!isset($this->ssids)) {
            $this->ssids = $this->db->select("serverSessions", "id", ["sid" => $this->id]);
        }
        return $this->ssids;
    }

    // Get an array of all currently active sessions. (Some servers can have multiple sessions at once)
    public function getCurrentSessions() {
        if (!isset($this->cssids)) {
            $this->cssids = $this->db->select("serverSessions", "id", ["sid" => $this->id, "endTime" => -1]);
        }
        return $this->cssids;
    }

    public function getPlayers()
    {
        if (!isset($this->nmids)) {
            $this->nmids = $this->db->distinct()->select("memberSessions", "uid", ["sid" => $this->id]);
        }
        return $this->nmids;
    }

    public function getCurrentPlayers()
    {
        if (!isset($this->cnmids)) {
            $this->cnmids = $this->db->distinct()->select("memberSessions", "uid", ["sid" => $this->id, "endTime" => -1]);
        }
        return $this->cnmids;
    }

    public static function generateSecret()
    {
        return "~_~" . \NetworkManager\Common::GenerateRandomString(30, "abcdefghijklmnopqrstuvwxyz");
    }

    public function newSecret()
    {
        $this->secret = Server::generateSecret();
        $this->db->update("servers", array("secret" => $this->secret), array("id" => $this->getId()));
        return $this->secret;
    }

    public function getCommunication()
    {
        return $this->communication;
    }

    public function makeCommunicationSocket()
    {
        $this->commsocket = null;
        if ($this->communication == 1) {
            require_once NM_CLASS_ROOT . 'comms/mysql.php';
            $this->commsocket = new Communication\MySQL($this);
        } elseif ($this->communication == 2) {
            require_once NM_CLASS_ROOT . 'comms/socket.php';
            $this->commsocket = new Communication\Socket($this);
        } elseif ($this->communication == 3) {
            require_once NM_CLASS_ROOT . 'comms/rcon.php';
            $this->commsocket = new Communication\RCon($this);
        } elseif ($this->communication == 4) {
            require_once NM_CLASS_ROOT . 'comms/webhook.php';
            $this->commsocket = new Communication\Webhook($this);
        } elseif ($this->communication == 5 && file_exists(NM_CLASS_ROOT . 'comms/other/' . $this->game . '.php')) {
            require_once NM_CLASS_ROOT . 'comms/other/' . $this->game . '.php';
            $class = '\NetworkManager\Communication\\'.ucfirst($this->game);
            $this->commsocket = new $class($this);
        } else {
            require_once NM_CLASS_ROOT . 'comms/none.php';
            $this->commsocket = new Communication\None($this);
        }
    }

    public function getCommunicationSocket()
    {
        return $this->commsocket;
    }
}
