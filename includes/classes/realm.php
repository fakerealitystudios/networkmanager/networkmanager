<?php

namespace NetworkManager;

// Used for permissions and data
// Specifies what realms the data can load/work on
// i.e. a realm could be 5.minecraft
// or garrysmod.4
// or minecraft.10.world
// or minecraft.*
// or simply *
class Realm
{
    public static $delimiter = ".";
    private $realmSplit;

    public function __construct($fullrealm)
    {
        $this->realmSplit = explode(Realm::$delimiter, $fullrealm);
    }

    public static function getRealm($fullrealm)
    {
        return new Realm($fullrealm);
    }

    public function toString()
    {
        return implode(Realm::$delimiter, $this->realmSplit);
    }

    public function getCount()
    {
        return count($this->realmSplit);
    }

    public function getFirst()
    {
        return $this->realmSplit[0];
    }

    public function getParts()
    {
        return $this->realmSplit;
    }

    // Used for database inquiries, list of possible realms to collect
    public function getPossibleRealms($wildcards = false)
    {
        $realms = array("*");
        $i = 0;
        $temp = "";
        foreach($this->realmSplit as $realmPart)
        {
            if ($temp == "")
                $temp = $realmPart;
            else
                $temp = $temp.".".$realmPart;

            if ($i < 1)
                $i++;
            else {
                $realms[] = $temp;
                if ($wildcards) {
                    $realms[count($realms)-1] .= "%";
                }
                $realms[] = "*".substr($temp, 1);
            }
        }
        return $realms;
    }

    // Returns true when the realms are the SAME
    public function isRealm($otherFullrealm)
    {
        $otherRealmSplit = explode(Realm::$delimiter, $otherFullrealm);
        $max = count($otherRealmSplit);
        if ($max != count($this->realmSplit)) {
            return false;
        }

        for ($i = 0; $i < count($this->realmSplit); $i++) {
            if ($realm == $otherRealmSplit[$i]) {
                if (($i + 1) == $max) { // We hit the end of the realm, and we're inside it still, return true
                    return true;
                }
            }
        }
        return false;
    }

    // Returns true when the specified realm is within the realm of this object
    public function isRealmIncluded($otherFullrealm)
    {
        $otherRealmSplit = explode(Realm::$delimiter, $otherFullrealm);
        $max = count($otherRealmSplit);
        for ($i = 0; $i < count($this->realmSplit); $i++) {
            $realm = $this->realmSplit[$i];
            if ($realm == "*") {
                return true;
            } else if ($realm == $otherRealmSplit[$i]) {
                if (($i + 1) == $max) { // We hit the end of the realm, and we're inside it still, return true
                    return true;
                }
            }
        }
        return false;
    }

    // Used to check if the compared has a piece of this object's realm
    public function hasRealm($otherPart)
    {
        for ($i = 0; $i < count($this->realmSplit); $i++) {
            if ($this->realmSplit[$i] == $otherPart) {
                return $i + 1;
            }
        }
        return 0;
    }

    public static function compare($a, $b)
    {
        if (count($a) < count($b)) {
            return true;
        }
        if ($a->hasRealm("*") > $b->hasRealm("*")) {
            return true;
        }
        return strcmp($a->realmSplit[0], $b->realmSplit[0]);
    }
}

class RealmObject
{
    protected $realms = array();

    public function &getRealm($fullRealm)
    {
        $realmSplit = Realm::getRealm($fullRealm)->getParts();
        $i = 0;
        $count = count($realmSplit);
        $lrealm = &$this->realms;
        foreach ($realmSplit as $realmPart) {
            if (!isset($lrealm[$realmPart])) {
                $lrealm[$realmPart] = array("__data" => array(), "__perms" => array());
            }
            $i++;
            $lrealm = &$lrealm[$realmPart];
            if ($i == $count) {
                return $lrealm;
            }
        }
        return null;
    }

    // Includes all * realms(doesn't return reference)
    public function getRealmInclusive($fullRealm)
    {
        $result = array("__data" => array(), "__perms" => array());
        $realmSplit = Realm::getRealm($fullRealm)->getParts();
        $i = 0;
        $count = count($realmSplit);
        $lrealm = $this->realms;
        foreach ($realmSplit as $realmPart) {
            if (isset($lrealm["*"])) {
                foreach ($lrealm["*"] as $k => $lr) {
                    if (!isset($result[$k])) {continue;}
                    $result[$k] += $lr;
                }
            }
            if (!isset($lrealm[$realmPart])) {
                $lrealm[$realmPart] = array("__data" => array(), "__perms" => array());
            }
            $i++;
            $lrealm = $lrealm[$realmPart];
            if ($i == $count) {
                foreach ($lrealm as $k => $lr) {
                    if (!isset($result[$k])) {continue;}
                    $result[$k] += $lr;
                }
            }
        }
        return $result;
    }
}
