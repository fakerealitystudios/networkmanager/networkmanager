<?php

namespace NetworkManager\AuthMethods;

abstract class Base
{
    protected $db;

    public function __construct()
    {
        $this->db = \NetworkManager\Database::getInstance();
    }

    // Returns userid if valid, false otherwise
    // Useful if you'd like to override the username and password
    abstract public function login($username, $password);
    // Returns false if it receives invalid data
    // Useful to override the username, password, email, or displayname
    abstract public function register($username, $password, $email);

    // Compares the provided password to the database password
    abstract public function comparepassword($password, $db_password);
    // Encrypts the password for the database(Only used for Authentication->ChangePassword)
    abstract public function hashpassword($password);

    // Returns the remember me client and server tokens
    abstract public function remembertoken();
    abstract public function comparetoken($token, $db_token);
}
