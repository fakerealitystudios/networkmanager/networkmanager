<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/memberfull.php";

class MemberSemi extends MemberFull
{
    // Overrides the constructor for a simplified version
    public function __construct($uid)
    {
        $this->db = Database::getInstance();

        if ($uid == -1) {
            $this->uid = -1;
            $this->displayName = "Unknown";
            return;
        }

        $res = $this->db->get('members', [
            "username",
            "avatar",
            "displayName",
            "email",
            "created",
            "disabled",
        ], [
            "id" => $uid,
        ]);
        if (!$res) {
            return;
        }
        // If we didn't find user, he is invalid, quit
        $this->uid = $uid;

        $this->displayName = $res["displayName"];
        $this->avatar = $res["avatar"];
        $this->username = $res["username"];
        $this->email = $res["email"];
        $this->created = $res["created"];
        $this->disabled = $res["disabled"];

        $res = $this->db->select('memberRanks', ["rank", "realm"], [
            'uid' => $uid,
        ]);
        if ($res) {
            foreach ($res as $row) {
                $this->ranks[] = $row;
            }
        }
        {
            $rankrealm = "*";
            foreach ($this->ranks as $row) {
                if ("web" == $row['realm'] || ($row['realm'] == "*" && $rankrealm == "*")) {
                    $this->rank = $row['rank'];
                    $rankrealm = $row['realm'];
                }
            }
        }
    }
}
