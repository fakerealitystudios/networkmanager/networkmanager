<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/database.php";
require_once NM_ROOT . "includes/classes/member.php";
require_once NM_ROOT . "includes/classes/memberfull.php";
require_once NM_ROOT . "includes/classes/membernull.php";
require_once NM_ROOT . "includes/classes/membernull.php";
require_once NM_LIB_ROOT . "steam.php";

class MemberManager
{
    private static $instance;
    private $db;

    private $membersMap = array();
    private $fullMembersMap = array();

    private function __construct()
    {
        //$this->db = Database::getInstance();
    }

    // Grabs the singleton
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add($member) {
        // All member types are based on memberFull and then just don't implement certain functions
        if (is_a($member, '\NetworkManager\MemberFull')) {
            if (!$member->isValid()) return;
            if (get_class($member) == 'NetworkManager\MemberFull')
                $this->fullMembersMap[$member->getId()] = $member;
            $this->membersMap[$member->getId()] = $member;
        }
    }

    public function get($uid)
    {
        if (isset($this->fullMembersMap[$uid])) {
            return $this->fullMembersMap[$uid];
        }
        if (isset($this->membersMap[$uid])) {
            return $this->membersMap[$uid];
        }
        $this->membersMap[$uid] = new Member($uid);
        return $this->membersMap[$uid];
    }

    public function getFull($uid)
    {
        if (isset($this->fullMembersMap[$uid])) {
            return $this->fullMembersMap[$uid];
        }
        $this->fullMembersMap[$uid] = new MemberFull($uid);
        return $this->fullMembersMap[$uid];
    }

    public static function getNMID($type, $token)
    {
        // We may send other forms of steam ids, make sure it always results in 64-bit form
        if ($type == "steamid64")
            $token = (new \SteamID($token))->getSteamID64();
        $db = \NetworkManager\Database::getInstance();
        $nmid = $db->get("memberLinks", "uid", array("type" => $type, "token" => $token));
        if (!$nmid) {
            return -1;
        }
        return $nmid;
    }

    public static function getToken($nmid, $type)
    {
        $db = \NetworkManager\Database::getInstance();
        $token = $db->get("memberLinks", "token", array("uid" => $nmid, "type" => $type));
        if (!$token) {
            return -1;
        }
        return $token;
    }

    public static function addToken($nmid, $type, $token)
    {
        // We may send other forms of steam ids, make sure it always results in 64-bit form
        if ($type == "steamid64")
            $token = (new \SteamID($token))->getSteamID64();
        $db = \NetworkManager\Database::getInstance();
        try {
            $q = $db->insert("memberLinks", array("uid" => $nmid, "type" => $type, "token" => $token));
            return $q->rowCount() > 0;
        } catch (\PDOException $e) {
            return false;
        }
    }
}
