<?php

namespace NetworkManager;

require_once NM_ROOT . "includes/classes/common.php";

require_once NM_ROOT . "includes/classes/authenticationmethod.php";
require_once NM_ROOT . "includes/classes/authenticationlink.php";
require_once NM_ROOT . "includes/classes/database.php";

// Authentication Base
require_once NM_ROOT . "includes/classes/authmethods/hash.php";

// https://stackoverflow.com/questions/549/the-definitive-guide-to-form-based-website-authentication#477579
// For my reading pleasure
class Authentication
{
    private static $instance;
    private $db;
    private $authmethod;

    // Two Step Authentication
    public $twofa;

    private function __construct()
    {
        $this->db = Database::getInstance();

        $this->authmethod = new AuthMethods\HashAuthentication();

        // Should load all available auth links from the files and test if valid
    }

    // Grabs the singleton
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getAuthLinks()
    {
        if (!isset($this->links)) {
            $this->links = array();
            if ($handle = opendir(NM_CLASS_ROOT."authlinks")) {
                while(false !== ($file = readdir($handle))) {
                    if (substr($file, -4) != ".php")
                        continue;
                    $class = "\NetworkManager\LinkMethods\\".ucfirst(substr($file, 0, -4));
                    include(NM_CLASS_ROOT."authlinks/".$file);
                    $method = new $class();
                    if ($method->isEnabled())
                        $this->links[$method->getType()] = $method;
                    unset($method);
                }
                closedir($handle);
            }
        }
        return $this->links;
    }

    // Returns the current user's id
    // -1 if the user is invalid
    public function getUserId()
    {
        return isset($_SESSION["uid"]) ? $_SESSION["uid"] : -1;
    }

    // Logs out the current user(safe to call even when not logged in)
    public function logout()
    {
        // Safety unset
        unset($_SESSION["uid"]);
        unset($_SESSION["member"]);
        unset($_SESSION['oauth2state']); // used in external oauth processing
        unset($_SESSION["2fa"]);

        session_unset();
        session_destroy();

        // Remove cookies
        setcookie("uid", "", time() - 3600, "/", NM_URL_HOST);
    }

    public function cleanupSessions()
    {
        // Clean up unneeded items
        $uid = $_SESSION["uid"];
        $member = &$_SESSION["member"];
        $twofa = $_SESSION["2fa"];
        $page = $_SESSION["page"];
        $last_page = $_SESSION["last_page"];
        session_unset();
        $_SESSION["uid"] = $uid;
        $_SESSION["member"] = &$member;
        $_SESSION["2fa"] = $twofa;
        $_SESSION["page"] = $page;
        $_SESSION["last_page"] = $last_page;
    }

    // Checks if a user is logged in(locally)
    // True if logged in, false otherwise
    // https://stackoverflow.com/questions/17413480/session-spoofing-php
    public function isLoggedIn()
    {
        if (!isset($_SESSION['2fa']) || $_SESSION['2fa'] == false) {
            return false;
        }
        return $this->getUserId() != -1 ? true : false;
    }

    public function isValidUser($uid)
    {
        if ($uid < 0) {
            return false;
        }

        $res = $this->db->get('members', "id", [
            'id' => $uid,
            'disabled' => 0, // Only non-disabled accounts are valid
        ]);
        return $res != false;
    }

    public function getNumUsers()
    {
        return $this->db->count("members");
    }

    public function doweremember()
    {
        if (!isset($_COOKIE['uid'])) {
            return false;
        }
        $token = explode('&', $_COOKIE['uid']);
        if (!isset($token[0]) || !isset($token[1])) {
            // That's weird... Invalid token!
            setcookie("uid", "", time() - 3600, "/", NM_URL_HOST);
            return false;
        }
        $uuid = $token[0];
        unset($token[0]);
        $token = implode($token); // Just in case & pops up in the token since users can change the auth method... Makes me feel safe
        $row = $this->db->get("memberTokens", array("uid", "token", "ip", "counter"), array(
            "id" => $uuid,
        ));
        if (isset($row['token'])) {
            if ($this->authmethod->comparetoken($token, $row['token'])) {
                // NOTE: Decided against the IP comparing, since even I use my phone... Need to look into a better option? Nothing will probably matter, since this is open source so restricts my options that can't be spoofed... are limited
                /*if ($row['ip'] != Common::getIpAddr()) {
                // Whoa buddy, new IP! You can't use this cookie! This is just as a precaution
                // We won't even error out to them, better to not let people know we're culling them
                $this->db->delete("memberTokens", array("id" => $uuid));
                setcookie("uid", "", time() - 3600, "/", NM_URL_HOST);
                return false;
                }*/

                $expires = time() + (60 * 60 * 24 * 30);
                // We create a new token, if our UUID is used with the wrong token, someone may have stolen old credentials. We then will invalidate this UUID and email the user
                $token = $this->authmethod->remembertoken();
                $this->db->update("memberTokens", array(
                    "counter" => $row['counter'] + 1,
                    "token" => $token['server'],
                    "expires" => $expires,
                ), array(
                    "id" => $uuid,
                ));
                // Cookie expires in 1 month unless refreshed
                setcookie("uid", $uuid . "&" . $token['client'], $expires, "/", NM_URL_HOST);
                $this->performLogin($row['uid'], false);
                return true;
            } else {
                // Invalid token with a valid token id? Someone may have hijacked or attempted to hijack an account... Let's dump this id and message the owner
                // via notifications and email(if they have an email set)
                $this->db->delete("memberTokens", array("id" => $uuid));
                setcookie("uid", "", time() - 3600, "/", NM_URL_HOST);

                global $NM;
                $NM->getMember($row['uid'])->email("Security Alert", "Someone may have hijacked one of your remember me tokens! We have invalidated the token to event further abuse. Please review your account details to ensure they are accurate. In future, we could provide logs during that session...");
                $NM->getMember($row['uid'])->notify("Security Alert", "Someone may have hijacked one of your remember me tokens! We have invalidated the token to event further abuse. Please review your account details to ensure they are accurate. In future, we could provide logs during that session...");
            }
        }
        return false;
    }

    // Performs the user login
    // Returns false when invalid, true when valid
    public function login($username, $password, $rememberme = false, $force = false)
    {
        if (!$force && $this->isLoggedIn()) {
            return true;
        }

        $logininfo = $this->authmethod->login($username, $password);
        if (!$logininfo) {
            return false;
        }

        $res = $this->db->get("members", [
            "id",
            "password",
            "disabled",
        ], [
            "username" => $logininfo["username"],
        ]);
        if ($res && $res["disabled"] == 0) {
            if ($this->authmethod->comparepassword($logininfo["password"], $res["password"])) {
                $this->performLogin($res['id'], $rememberme);
                return true;
            }
        }
        return false;
    }

    private function performLogin($nmid, $rememberme = false)
    {
        $_SESSION["uid"] = $nmid;
        $_SESSION["2fa"] = !$this->collect2fa();
        $_SESSION["member"] = MemberManager::getInstance()->getFull($nmid);

        if ($rememberme) {
            $expires = time() + (60 * 60 * 24 * 30);
            $uuid = $this->db->query("SELECT UUID() `uuid`")->fetch()['uuid'];
            $token = $this->authmethod->remembertoken();
            $this->db->insert("memberTokens", array(
                "id" => $uuid,
                "uid" => $nmid,
                "token" => $token['server'],
                "ip" => Common::getIpAddr(),
                "expires" => $expires,
            ));
            // Cookie expires in 1 month unless refreshed
            setcookie("uid", $uuid . "&" . $token['client'], $expires, "/", NM_URL_HOST);
        }
    }

    public function collect2fa()
    {
        // set to true to show that 2fa is passed or not enabled
        $use2fa = false;
        
        $secret = $this->db->get("member2step", "secret", array("uid" => $this->getUserId()));
        if ($secret) {
            $use2fa = true;
            $this->twofa['2step'] = $secret;
        }
        return $use2fa;
    }

    public function get2faType()
    {
        global $NM;
        if (isset($this->twofa['2step'])) {
            return '2step';
        }
        return '';
    }

    public function changePassword($oldpassword, $newpassword)
    {
        if (!$this->isValidUser($this->getUserId())) {
            return false;
        }

        $db_password = $this->db->get("members", "password", ["id" => $this->getUserId()]);
        if (!is_string($db_password)) {
            return false;
        }

        if ($db_password != "") {
            if (!$this->authmethod->comparepassword($oldpassword, $db_password)) {
                return false;
            }

        } elseif ($db_password == "" && $oldpassword != "") {
            return false;
        }

        $hash = $this->authmethod->hashpassword($newpassword);
        if (!$hash) {
            return false;
        }

        $data = $this->db->update("members", [
            "password" => $hash,
        ], [
            "id" => $this->getUserId(),
        ]);

        if ($data->rowCount() > 0) {
            return true;
        }

        return false;
    }

    public function register($username, $password, $email, $displayName)
    {
        $registerinfo = $this->authmethod->register($username, $password, $email);
        if (!$registerinfo) {
            return false;
        }

        $this->db->insert("members", array(
            "username" => $registerinfo['username'],
            "displayName" => $displayName,
            "password" => $registerinfo['password'],
            "email" => $registerinfo['email'],
            "created" => time(),
        ));
        $nmid = $this->db->id();
        if ($nmid == 0) {
            return false;
        }

        return $nmid;
    }

    // Used to create an empty account.
    public function create($displayName)
    {
        $this->db->insert("members", array(
            "displayName" => $displayName,
            "ip" => Common::getIpAddr(),
            "created" => time(),
        ));

        $nmid = $this->db->id();
        if ($nmid == 0) {
            return false;
        }
        return $nmid;
    }

    // Performs a merger between the current logged in user and the provided account.
    // This is used by endusers to merge their own accounts. They however, can not merge with accounts older then a month old and will have to be merged by an admin
    public function merge($othuid)
    {
        if (!$this->isLoggedIn()) {
            return false;
        }

        if ($othuid == $this->getUserId()) {return true;}

        $ourcreated = $this->db->get("members", "created", array(
            "id" => $this->getUserId(),
        ));
        $othcreated = $this->db->get("members", "created", array(
            "id" => $othuid,
        ));

        $month = 60 * 60 * 24 * 30;
        if (time() < (max($othcreated, $ourcreated) + $month)) {
            $payload = array();
            $payload["action"] = "accountMerge";
            $this->smartMerge($this->getUserId(), $othuid);
            return true;
        }
        return false;
    }

    // This will decide whom is the eldest that will be taking over.
    // This function could be abused, BEWARE
    public function smartMerge($id, $otheruid)
    {
        if ($id == $otheruid) {
            return $id;
        }
        $ourcreated = $this->db->get("members", "created", array(
            "id" => $id,
        ));
        $othcreated = $this->db->get("members", "created", array(
            "id" => $otheruid,
        ));

        $mainid = -1;
        if ($othcreated > $ourcreated) { // Then our account is older and should be used
            $this->performMerge($id, $otheruid);
            $mainid = $id;
        } else { // Then the other account is older, and should be used
            $this->performMerge($otheruid, $id);
            $mainid = $otheruid;
        }
        return $mainid;
    }

    // Actually performs the account merge. The account to save should be given in the first parameter
    private function performMerge($acc, $mergeAcc)
    {
        // Disable the account
        $this->db->update("members", array(
            "disabled" => 1,
            "newid" => $acc, //incase something needs to have backwards compatability
        ), array(
            "id" => $mergeAcc,
        ));

        // Start with linked accounts
        $mergeLinks = $this->db->select("memberLinks", array("id", "type", "token"), [
            'uid' => $mergeAcc,
        ]);
        foreach ($mergeLinks as $value) {
            MemberManager::addToken($acc, $value["type"], $value["token"]);
        }
        unset($mergeLinks);
        $this->db->delete("memberLinks", array(
            'uid' => $mergeAcc,
        ));

        // Now onto permissions
        $mergePerms = $this->db->select("memberPermissions", array("id", "value", "realm"), [
            'uid' => $mergeAcc,
        ]);
        $perms = $this->db->select("memberPermissions", array("value", "realm"), [
            'uid' => $acc,
        ]);
        foreach ($perms as $key => $value) {
            foreach ($mergePerms as $mergeKey => $mergeValue) {
                if ($value["value"] == $mergeValue["value"] && $value["realm"] == $mergeValue["realm"]) {
                    unset($mergePerms[$mergeKey]);
                }
            }
        }
        unset($perms);
        foreach ($mergePerms as $value) {
            $this->db->update("memberPermissions", array(
                "uid" => $acc,
            ), array(
                "id" => $value['id'],
            ));
        }
        unset($mergePerms);
        $this->db->delete("memberPermissions", array(
            'uid' => $mergeAcc,
        ));

        // Now onto ranks
        $mergeRanks = $this->db->select("memberRanks", array("id", "rank", "realm"), [
            'uid' => $mergeAcc,
        ]);
        $ranks = $this->db->select("memberRanks", array("rank", "realm"), [
            'uid' => $acc,
        ]);
        foreach ($ranks as $key => $value) {
            foreach ($mergeRanks as $mergeKey => $mergeValue) {
                if ($value["rank"] == $mergeValue["rank"] && $value["realm"] == $mergeValue["realm"]) {
                    unset($mergeRanks[$mergeKey]);
                }
            }
        }
        unset($ranks);
        foreach ($mergeRanks as $value) {
            $this->db->update("memberRanks", array(
                "uid" => $acc,
            ), array(
                "id" => $value['id'],
            ));
        }
        unset($mergeRanks);
        $this->db->delete("memberRanks", array(
            'uid' => $mergeAcc,
        ));

        // Migrate all sessions
        $this->db->update("memberSessions", array(
            "uid" => $acc,
        ), array(
            "uid" => $mergeAcc,
        ));

        // Migrate all tickets
        $this->db->update("memberTickets", array(
            "uid" => $acc,
        ), array(
            "uid" => $mergeAcc,
        ));

        // Migrate all reports
        $this->db->update("reports", array(
            "uid" => $acc,
        ), array(
            "uid" => $mergeAcc,
        ));
        $this->db->update("reports", array(
            "otheruid" => $acc,
        ), array(
            "otheruid" => $mergeAcc,
        ));
        $this->db->update("reportMessages", array(
            "uid" => $acc,
        ), array(
            "uid" => $mergeAcc,
        ));

        // Migrate all geolocations
        $this->db->update("geolocations", array(
            "uid" => $acc,
        ), array(
            "uid" => $mergeAcc,
        ));

        // Migrate all logs(we do this last, there are a LOT of logs!)
        $this->db->update("logDetails", array(
            "value" => $acc,
        ), array(
            "type" => "nmid",
            "value" => $mergeAcc,
        ));

        $this->db->delete("memberAlts", array(
            "OR" => array(
                "uid" => $mergeAcc,
                "otheruid" => $mergeAcc,
            ),
        ));

        global $NM;
        $member = $NM->getMember($acc, true);
        \NetworkManager\ServerManager::getSelectiveCommunicationSockets($member->getCurrentServers())->merge($acc, $mergeAcc);
        \NetworkManager\ServerManager::getRestCommunicationSockets()->merge($acc, $mergeAcc);

        // Notify NMCenter of the merger
        //$res = \NetworkManager\API::post("/member/" . $acc . "/merge/" . $mergeAcc, array());
    }

    public function proccessAuthToken($token)
    {
        $res = $this->db->get("memberPending", array("type", "token", "time"), array("id" => $token));
        if (($res['time'] + 60) < time()) {
            global $_alerts;
            $_alerts[] = array(
                'type' => 'danger',
                'text' => 'Token has expired! Please generate a new token.',
            );
            return false;
        }

        // Will perform a merge as needed
        return $this->proccessToken($res['type'], $res['token'], false, false);
    }

    public function proccessTokens()
    {
        foreach($this->getAuthLinks() as $method) {
            if ($this->proccessToken($method->getType(), $method->getToken())) {
                return true;
            }
        }
        return false;
    }

    public function proccessToken($type, $token, $rememberme = false, $allowRegister = true)
    {
        global $config;
        if ($token == null || $token == false) {return false;}
        if ($this->isLoggedIn()) {
            // Link to account
            if ($this->tokenLink($type, $token)) {
                $_SESSION["member"]->linkedAccounts[] = array("type" => $type, "token" => $token);
                return true;
            } else {
                global $_alerts;
                $_alerts[] = array(
                    'type' => 'danger',
                    'text' => 'Token is already in use!',
                );
                // Token is already in use!
                // Send error
            }
        } elseif ($this->tokenLogin($type, $token, $rememberme)) {
            // Logged in to account
            return true;
        // TODO: Migrate registration system to web settings for dynamic changing
        } elseif ($allowRegister && $config['registration']['enabled']) {
            $this->performLogin($this->tokenRegister($type, $token), $rememberme);
            return true;
        }
        return false;
    }

    // Performs the user login
    // Returns false when invalid, true when valid
    public function tokenLogin($type, $token, $rememberme = false)
    {
        if ($this->isLoggedIn()) {
            return true;
        }
        $uid = MemberManager::getNMID($type, $token);
        if ($uid != -1) {
            $disabled = $this->db->get('members', "disabled", [
                'id' => $uid,
            ]);
            if ($disabled == 0) {
                $this->performLogin($uid, $rememberme);
                return true;
            }
        }
        return false;
    }

    // Links external account to local one
    public function tokenLink($type, $token)
    {
        $uid = MemberManager::getNMID($type, $token);
        if ($uid != -1)
            return $this->getUserId() == $uid;

        return MemberManager::addToken($this->getUserId(), $type, $token);
    }

    public function tokenRegister($type, $token, $displayName = "")
    {
        $this->db->insert("members", array(
            "created" => time(),
            "displayName" => $displayName,
        ));
        $nmid = $this->db->id();

        if ($nmid == 0)
            return -1;

        MemberManager::addToken($nmid, $type, $token);
        return $nmid;
    }
}
