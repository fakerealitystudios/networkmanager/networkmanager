<?php


namespace NetworkManager;

require_once NM_CLASS_ROOT . "medoo.php";

class NMMedoo extends \Medoo\Medoo
{
    protected $logsTTL = [];
    protected $distinct_mode = false;
    protected $sql_mode = false;

    protected function selectContext($table, &$map, $join, &$columns = null, $where = null, $column_fn = null)
	{
		$statement = parent::selectContext($table, $map, $join, $columns, $where, $column_fn);
        
        if ($this->distinct_mode) {
            $distinct = 'DISTINCT ';

            $this->distinct_mode = false;
        } else {
            $distinct = '';
        }

		return 'SELECT ' . $distinct . substr($statement, 7);
	}

    public function distinct()
    {
        $this->distinct_mode = true;

        return $this;
    }

    public function sql()
    {
        $this->sql_mode = true;

        return $this;
    }

    public function exec($query, $map = [])
	{
		if ($this->sql_mode)
		{
            $this->statement = null;

			return $this->generate($query, $map);
        }
        
        $time = microtime(true);

        $statement = parent::exec($query, $map);

        $time = (microtime(true) - $time) * 1000; // milliseconds

		if ($this->logging) {
            $this->logsTTL[] = [$query, $map, number_format($time, 2) . "ms"];
        } else {
            $this->logsTTL = [[$query, $map, number_format($time, 2) . "ms"]];
        }

		return $statement;
    }

    public function select($table, $join, $columns = null, $where = null)
	{
        if ($this->sql_mode)
		{
            $map = [];

            $query = $this->exec($this->selectContext($table, $map, $join, $columns, $where), $map);

            $this->sql_mode = false;

            return $query;
        } else {
            return parent::select($table, $join, $columns, $where);
        }
    }
    
    protected function aggregate($type, $table, $join = null, $column = null, $where = null)
	{
        if ($this->sql_mode)
		{
            $map = [];

            $query = $this->exec($this->selectContext($table, $map, $join, $column, $where, strtoupper($type)), $map);

            $this->sql_mode = false;

            return $query;
        } else {
            return parent::aggregate($type, $table, $join, $column, $where);
        }
	}

    public function logTTL()
    {
        return array_map(function ($log) {
            return array($this->generate($log[0], $log[1]), $log[2]);
        },
            $this->logsTTL
        );
    }
}