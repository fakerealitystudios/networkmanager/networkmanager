<?php
namespace Stiphle\Throttle;

class NMLeakyBucket extends LeakyBucket
{
    /**
     * Throttle
     *
     * @param string $key  - A unique key for what we're throttling
     * @param int $limit   - How many are allowed
     * @param int $milliseconds - In this many milliseconds
     * @return int
     */
    public function throttle($key, $limit, $milliseconds)
    {
        /**
         * Try and do our waiting without a lock
         */
        $wait = $this->getEstimate($key, $limit, $milliseconds);

        /**
         * Lock, record and release 
         */
        $this->storage->lock($key);
        $newRatio = $this->getNewRatio($key, $limit, $milliseconds);
        $this->setLastRatio($key, $newRatio);
        $this->setLastRequest($key, microtime(true));
        $this->storage->unlock($key);
        return $wait;
    }
}
