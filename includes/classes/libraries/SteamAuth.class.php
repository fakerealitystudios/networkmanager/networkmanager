<?php

if (session_id() == '') {
    session_start();
}

include "lightopenid.php";

class SteamAuth
{

    public $OpenID;

    public $SteamID;

    public function __construct()
    {
        $this->OpenID = new LightOpenID(NM_URL);
        $this->OpenID->identity = 'https://steamcommunity.com/openid';
    }

    public function Init()
    {
        if ($this->OpenID->mode == 'cancel') {
            return false;
        } else if ($this->OpenID->mode) {
            if ($this->OpenID->validate()) {
                return basename($this->OpenID->identity);
            }
        }
        return null;
    }

    public function GetLoginURL()
    {
        $this->OpenID->returnUrl = NM_URL.'login.php';
        return $this->OpenID->authUrl();
    }
}
