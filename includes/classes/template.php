<?php

namespace NetworkManager;

class Template
{
    private $data = array();
    private static $instance;

    private function __construct()
    {
        // We need these globals for the included scripts
        global $NM, $environment;
        /*foreach(get_defined_constants() as $k => $v) {
            if (substr($k, 0, 3) == "NM_") {
                $this->data[$k] = $v;
            }
        }*/

        $this->data['NM_VERSION'] = NM_VERSION;
        $this->data['NM_API_SITE'] = NM_API_SITE;

        $this->data['NM_CDN'] = NM_CDN;
        $this->data['NM_URL'] = NM_URL;
        $this->data['NM_URL_DIR'] = NM_URL_PATH;
        $this->data['NM_URL_HOST'] = NM_URL_PATH;
        $this->data['NM_URL_PATH'] = NM_URL_PATH;
        $this->data['NM_URL_FULL'] = NM_URL_FULL;

        $this->data['_POST'] = $_POST;
        $this->data['_GET'] = $_GET;
        $this->data['networkmanager'] = $NM;
        $this->data['environment'] = $environment;
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function prepareMainIncludes()
    {
        global $NM;
        global $auth;
        global $_alerts;
        global $errors;
        $this->data['errors'] = $errors;

        ob_start();
        include(NM_INCL_ROOT.'style.php');
        $this->data['include_style'] = ob_get_contents();
        ob_end_clean();
        ob_start();
        include(NM_INCL_ROOT.'script.php');
        $this->data['include_javascript'] = ob_get_contents();
        ob_end_clean();
        ob_start();
        include(NM_INCL_ROOT.'header.php');
        $this->data['include_header'] = ob_get_contents();
        ob_end_clean();
        ob_start();
        include(NM_INCL_ROOT.'sidebar.php');
        $this->data['include_sidebar'] = ob_get_contents();
        ob_end_clean();
        ob_start();
        include(NM_INCL_ROOT.'footer.php');
        $this->data['include_footer'] = ob_get_contents();
        ob_end_clean();
    }

    public static function evaluate($template, $data) {
        $template = self::performIncludes($template);

        preg_match_all("/{% (for|if)(?:.+?)%}/s", $template, $matches, PREG_PATTERN_ORDER);
        $count = array(
            'for' => 0,
            'if' => 0,
        );
        foreach($matches[1] as $exp) {
            $count[$exp]++;
        }
        $max = ($count['for'] + $count['if']);
        for ($i = 0; $i < $max; $i++) {
            $for = strstr($template, "{% for");
            $if = strstr($template, "{% if");
            if (($for != false && $if == false) || ($for != false && $for < $if)) {
                $template = self::performForExpression($template, $data);
            } elseif ($if != false) {
                $template = self::performIfExpression($template, $data);
            }
        }
        $template = self::evaluateVariables($template, $data);

        return $template;
    }

    public static function performIncludes($template) {
        $template = preg_replace_callback("/\{\|([^%\s,]+)\|\}/", function ($matches) {
            global $NM;
            $template2 = file_get_contents(NM_TEMPLATE_ROOT.$matches[1].".html");
            return $template2;
        }, $template);

        return $template;
    }

    public static function performForExpression($template, $data) {
        // First get nested groups.
        $template = preg_replace_callback("/\{% for (?:[^%]+) %\}(?:[^\{]++|(?R)|\{(?!\% endfor)|\%(?!\}))*\{% endfor %\}/s", function ($matches) use ($data) {
            // Then perform the loop
            $str = preg_replace_callback("/\{% for ([^%\s,]+)(?:, ([^%\s,]+))? in ([^%]+) %\}(.*)\{% endfor %\}/s", function($submatches) use ($data) {
                // Now evaluate the variable
                $var = self::evaluateVariable($submatches[3], $data, true);
                $str2 = "";
                if (!$var || !is_array($var)) {return "";}
                // Temporary store incase
                if (isset($data[$submatches[1]])) {
                    $temp1 = $data[$submatches[1]];
                }
                if ($submatches[2] != "" && isset($data[$submatches[2]])) {
                    $temp2 = $data[$submatches[2]];
                }
                // Now perform the loop
                foreach($var as $k => $v) {
                    if ($submatches[2] != "") {
                        // key, value
                        $data[$submatches[1]] = $k;
                        $data[$submatches[2]] = $v;
                    } else {
                        // value
                        $data[$submatches[1]] = $v;
                    }
                    // Now evaluate/perform any nested functions inside this for loop
                    $str2 .= self::evaluate($submatches[4], $data);
                }
                unset($data[$submatches[1]]);
                unset($data[$submatches[2]]);
                if (isset($temp1)) {
                    $data[$submatches[1]] = $temp1;
                    unset($temp1);
                }
                if (isset($temp2)) {
                    $data[$submatches[2]] = $temp2;
                    unset($temp2);
                }
                return $str2;
            }, $matches[0]);
            return $str;
        }, $template);

        return $template;
    }

    public static function performIfExpression($template, $data) {
        // Now find if statements
        $template = preg_replace_callback("/(?!.+\{% if)\{% if .+? %}(.+?)\{% endif %\}/s", function ($matches) use ($data) {
            // Get the full expression
            preg_match_all("/\{% (?:if |elseif |else)([^%\}]+)? %\}(.*?)(?=\{%)/s", $matches[0], $expressions, PREG_SET_ORDER);
            $text = "";
            // Perform the expression
            foreach($expressions as $exp) {
                if (self::evaluateExpression($exp[1], $data)) {
                    // Now evaluate/perform any nested functions inside this segment of the if statement
                    $text = self::evaluate($exp[2], $data);
                    break;
                }
            }
            return $text;
        }, $template);

        // Finally, return it to finish variable evaluation in the evaluate function
        return $template;
    }

    public static function evaluateExpression($exp, $data) {
        $exp = trim($exp);
        if ($exp == "") {
            return true;
        }
        preg_match("/(\S+) ?([=><!]=|[><]) ?(\S+)/", $exp, $matches);
        if (count($matches) == 0) {
            $var = self::evaluateVariable($exp, $data, true);
            return boolval($var);
        } else {
            $var1 = self::evaluateVariable($matches[1], $data, true);
            $var2 = self::evaluateVariable($matches[3], $data, true);
            if ($matches[2] == "==") {
                return $var1 == $var2;
            } elseif ($matches[2] == "!=") {
                return $var1 != $var2;
            } elseif ($matches[2] == ">") {
                return intval($var1) > intval($var2);
            } elseif ($matches[2] == ">=") {
                return intval($var1) >= intval($var2);
            } elseif ($matches[2] == "<=") {
                return intval($var1) <= intval($var2);
            } elseif ($matches[2] == "<") {
                return intval($var1) < intval($var2);
            }
        }
        return false;
    }

    public static function evaluateVariable($match, $data, $inExpression) {
        if (\is_bool($match)) {
            return \boolval($match);
        } elseif (\is_float($match)) {
            return \floatval($match);
        } elseif (\is_numeric($match)) {
           return \intval($match);
        } elseif ($match[0] === '"' && $match[-1] === '"') {
            return substr($match, 1, -1);
        } elseif ($match[0] === '\'' && $match[-1] === '\'') {
            return substr($match, 1, -1);
        }
        // When we get a string, we assume it needs full processing
        $default = null;
        preg_match_all("/\|(.+)?/", $match, $submatches, PREG_SET_ORDER);
        if (isset($submatches[0][1])) {
            $default = $submatches[0][1];
        }
        preg_match_all("/(.+?)(?:(\[|\()(.+?(?=\]|\)))(\]|\)))/", $match, $submatches, PREG_SET_ORDER);
        $var = null;
        foreach ($submatches as $submatch) {
            if (is_array($data) && isset($data[$submatch[1]])) {
                $var = $data[$submatch[1]];
            }
            if ($submatch[2] == "[") {
                if (is_array($var) && isset($var[$submatch[3]])) {
                    $var = $var[$submatch[3]];
                } elseif (is_object($var) && property_exists($var, $submatch[3])) {
                    $var = get_object_vars($var)[$submatch[3]];
                } else {
                    $var = null;
                    break;
                }
            } elseif ($submatch[2] == "(") {
                $params = array(); // Though we can't take parameters yet, we could support it later
                $var = call_user_func_array(array($var, $submatch[3]), $params);
            }
        }
        if ($var == null && is_array($data) && isset($data[$match])) {
            $var = $data[$match];
        }
        if ($var == null && $default != null) {
            $var = $default;
        }
        if (is_bool($var))
            return $var ? 'true' : 'false';
        /*if (!$inExpression) {
            if (is_string($var))
                return htmlspecialchars($var);
        }*/
        return $var;
    }

    public static function evaluateVariables($template, $data) {
        return preg_replace_callback("/\{(.+?)(?=\})\}/", function ($matches) use ($data) {
            $var = self::evaluateVariable($matches[1], $data, false);
            if (is_null($var) || is_object($var)) {
                $var = "";
            }
            if (is_array($var)) {
                return json_encode($var, JSON_PRETTY_PRINT);
            }
            if (is_numeric($var) || is_bool($var)) {
                return strval($var);
            }
            if (!is_string($var)) {
                $exception = new \Exception("Attempting to use non string in template valuation!");
                \Sentry\withScope(function (\Sentry\State\Scope $scope) use ($var, $exception): void {
                    $scope->setExtra('value', $var);

                    \Sentry\captureException($exception);
                });
                return strval($var);
            }
            return $var;
        }, $template);
    }

    public static function is_assoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public static function javascriptArray($arr) {
        if (self::is_assoc($arr))
            $text = "{";
        else
            $text = "[";
        foreach ($arr as $k => $v) {
            if (self::is_assoc($arr))
                $text .= "$k: ";
            if (is_array($v)) {
                $text .= self::javascriptArray($v);
            } elseif ($v === true) {
                $text .= "true";
            } elseif ($v === false) {
                $text .= "false";
            } else {
                $text .= strval($v);
            }
            $text .= ",";
        }
        if (self::is_assoc($arr))
            $text .= "}";
        else
            $text .= "]";
        return $text;
    }

    public function build($filePath, $data = array()) {
        $data = array_merge($data, $this->data);

        $template = file_get_contents($filePath);

        $output = self::evaluate($template, $data);

        // Remove newlines and tabs in production
        if ($this->data['environment'] == "production")
            $output = preg_replace('#(?ix)(?>[^\S ]\s*|\s{2,})(?=(?:(?:[^<]++|<(?!/?(?:textarea|pre)\b))*+)(?:<(?>textarea|pre)\b|\z))#', ' ', $output);

        return $output;
    }
}
