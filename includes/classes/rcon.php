<?php

namespace NetworkManager\RCon;

abstract class RCon
{
    protected $serverIP;
    protected $serverPort;
    protected $serverSecret;
    protected $timout;

    protected $options = array();

    /**
     * Class constructor
     *
     * @param string $serverIP      IP of the Arma server
     * @param integer $serverPort   Port of the Arma server
     * @param string $serverSecret  RCon password required by server
     * @param array $timeout        How many seconds before timeout declared
     * @param array $options        Options array of ARC
     *
     * @throws \Exception if wrong parameter types were passed to the function
     */
    protected function __construct($serverIP, $serverPort, $serverSecret, $timeout = 2, array $options = array())
    {
        $this->serverIP = $serverIP;
        $this->serverPort = $serverPort;
        $this->serverSecret = $serverSecret;
        $this->timeout = $timeout;
        $this->options = array_merge($this->options, $options);
    }

    public static function getClass($game)
    {
        $class = "\NetworkManager\RCon\\";
        switch($game)
        {
            case "arma3":
                return $class."Arma3";
            case "farmingsimulator":
                return $class."FarmingSimulator";
            case "garrysmod":
                return $class."Source";
            case "minecraft":
                return $class."Minecraft";
            case "rust":
                return $class."Rust";
            default:
                return $class."Source";
        }
    }

    /**
     * Class destructor
     */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * Creates a connection to the server
     *
     * @throws \Exception if creating the connection fails
     */
    abstract public function connect();

    /**
     * Closes the connection
     */
    abstract public function disconnect();

    /**
     * Closes the current connection and creates a new one
     */
    public function reconnect()
    {
        if (!$this->disconnected) {
            $this->disconnect();
        }

        $this->connect();
        return $this;
    }

    /**
     * Returns server's current status
     *
     * @return bool True when online, false otherwise
     */
    abstract public function getStatus();

    /**
     * Send a command to the connected server.
     *
     * @param string $command
     *
     * @return boolean|mixed
     */
    public function send($command)
    {
        return null;
    }

    /**
     * Receives the answer form the server
     *
     * @return string Any answer from the server, except the log-in message
     */
    public function receive()
    {
        return null;
    }

    /**
     * Receives the answer form the server
     *
     * @return array|bool|null List of players including display names and uids, can be false when not invalid, or null when not implentable
     */
    public function getUIDs()
    {
        return null;
    }

    /**
     * Receives the answer form the server
     *
     * @return array|bool List of players including display names, can be false when not available
     */
    abstract public function getPlayers();

    public function kickPlayer($player, $reason = "NetworkManager has deemed you unworthy")
    {
        return null;
    }
    public function banPlayer($player, $reason = "NetworkManager has deemed you unworthy")
    {
        return null;
    }

    abstract public function getLinkType();
}
