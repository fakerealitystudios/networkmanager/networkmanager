<?php

namespace NetworkManager;

class Mail
{
    private static $instance;

    private function __construct()
    {

    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function sendMail($email, $name, $subject, $body, $preheader = "")
    {
        global $NM;

        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
        try {
            //$mail->SMTPDebug = 2;
            $mail->Debugoutput = 'html';
            if ($NM->settings['email.method'] == "smtp") {
                $mail->isSMTP();
                $mail->Host = $NM->settings['smtp.host'];
                $mail->SMTPSecure = $NM->settings['smtp.protocol'];
                $mail->Port = $NM->settings['smtp.port'];
                $mail->SMTPAuth = true;
                $mail->Username = $NM->settings['smtp.username'];
                $mail->Password = $NM->settings['smtp.password'];
            }
            $mail->setFrom($NM->settings['email.outgoing'], 'NetworkManager');
            $mail->addAddress($email, $name); // Add a recipient

            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = $subject;

            $data = array();
            $data['title'] = $subject;
            $data['body'] = $body;
            $data['preheader'] = $preheader;
            $data['unsubscribe'] = NM_URL;
            $data['company'] = NM_URL;
            $mail->Body = $NM->template->build(NM_TEMPLATE_ROOT.'email.html', $data);

            $mail->send();
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            return false;
        }

        return true;
    }
}
