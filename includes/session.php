<?php

session_start();

if (isset($_SESSION["page"])) {
    $file = basename($_SESSION["page"]);
    // Login tends to refresh a lot, let's not use it
    if (substr($file, 0, 9) != 'login.php' && substr($file, 0, 10) != 'logout.php') {
        $_SESSION["last_page"] = $_SESSION["page"];
    }
} else {
    $_SESSION["last_page"] = null;
}

$_SESSION["page"] = $_SERVER['REQUEST_URI'];

// Set $SESSION_READONLY to any value if you need to disable session blocking(i.e. javascript streaming)
if (isset($SESSION_READONLY)) {
    session_write_close();
}

//var_dump($_SESSION);
