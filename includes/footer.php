<footer class="footer">
	<!-- Hopefully you can be nice and leave me some credits? :D I won't blame you if you remove it though... :( -->
	<span class="text-right">Copyright &#169; <a target="_blank" href="http://www.fakerealitystudios.com/">Fake Reality Studios LLC</a> | Queries: <a class="_blank" href="#" data-toggle="modal" data-target="#queriesModel"><?php echo count($NM->db->log()); ?></a> | TTL: <?php echo number_format($NM->getRuntime(), 2); ?></span>
	<span class="float-right">Design by <a target="_blank" href="https://www.pikeadmin.com"><b>Pike Admin</b></a></span>
</footer>

<?php
if (!function_exists('array2ul')) {
	function array2ul($array)
	{
		$out = "<ul>";
		foreach ($array as $key => $elem) {
			if (!is_array($elem)) {
				$out .= "<li><span>$key:[$elem]</span></li>";
			} else {
				$out .= "<li><span>$key</span>" . array2ul($elem) . "</li>";
			}

		}
		$out .= "</ul>";
		return $out;
	}
}
?>

<?php
if ($NM->getAuthedMember()->isValid() && !$NM->getAuthedMember()->hasPermission("nm.web.queries")) {echo "<!-- No permissions for queries -->";return;}
?>
<!-- Modal -->
<div class="modal fade" id="queriesModel" tabindex="-1" role="dialog" aria-labelledby="queriesModelLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 80%;" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="queriesModelLabel">Database Queries</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php echo array2ul($NM->db->info()); ?>
				<?php echo array2ul($NM->db->logTTL()); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>