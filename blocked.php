<?php
require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<!-- END CSS for this page -->
	</head>

	<body class="adminbody">

		<div id="main">

			<?php include NM_ROOT . "includes/header.php";?>
			<?php include NM_ROOT . "includes/sidebar.php";?>

			<div class="content-page">

				<!-- Start content -->
				<div class="content">

					<div class="container-fluid">

                        <h2>Sorry!</h2>
                        <h4>You don't have permission to view this page!</h4>

					</div>
					<!-- END container-fluid -->

				</div>
				<!-- END content -->

			</div>
			<!-- END content-page -->

			<?php include NM_ROOT . "includes/footer.php";?>

		</div>
		<!-- END main -->

		<?php include NM_ROOT . "includes/script.php";?>

		<!-- BEGIN Java Script for this page -->
		<!-- END Java Script for this page -->

	</body>
</html>
