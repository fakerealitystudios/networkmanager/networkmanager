<?php
require "includes/networkmanager.php";
//require("includes/classes/server.php");
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}
if (!$NM->getAuthedMember()->hasPermission("nm.web.view.server")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}
if (!isset($_GET["id"])) {
    header('Location: ' . NM_URL);
    exit();
}

$server = $NM->getServer($_GET["id"]);
if (!isset($server) || !$server->isValid()) {
    header('Location: ' . NM_URL);
    exit();
}

$data = array();

$data['title'] = $server->getName()." | NetworkManager";
$data['page_name'] = $server->getName();
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Servers"
	),
	array(
		'name' => $server->getName()
	),
);

$data['body'] = "";
$data['players'] = array();
$rows = $NM->db->select("memberSessions", array("id", "uid", "startTime"), array(
    "sid" => $server->getId(),
    "endTime" => "-1",
));
foreach ($rows as $row) {
    $user = $NM->getMember($row["uid"]);
    $data['players'][] = array(
        "nmid" => $user->getId(),
        "displayName" => $user->getDisplayName(),
        "usid" => $row['id'],
        "rank" => $user->getRank($server->getId()),
        "length" => \NetworkManager\Common::timeTillHumanReadable($row["startTime"], null, true),
    );
}
$data['disconnects'] = array();
$rows = $NM->db->select("memberSessions", array("id", "uid", "startTime", "endTime"), array(
    "sid" => $server->getId(),
    "endTime[!]" => "-1",
    "ORDER" => array(
        "endTime" => "DESC",
    ),
    "LIMIT" => 10,
));
foreach ($rows as $row) {
    $user = $NM->getMember($row["uid"]);
    $data['disconnects'][] = array(
        "nmid" => $user->getId(),
        "displayName" => $user->getDisplayName(),
        "usid" => $row['id'],
        "rank" => $user->getRank($server->getId()),
        "length" => \NetworkManager\Common::timeTillHumanReadable($row["startTime"], $row["endTime"], true),
        "time" => date('Y-m-d H:i:s', $row["endTime"]),
    );
}

$data['body'] = $NM->template->build(NM_TEMPLATE_ROOT . 'server.html', $data);

$data['javascript'] = '<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>

<script>
	$(document).ready(function() {
		// counter-up
		$(\'.counter\').counterUp({
			delay: 10,
			time: 600
		});
    } );
    
    var source = null;

    window.onload = function() {
        if (!!window.EventSource) {
            source = new EventSource("/stream.php?page=logs&sid='.$server->getId().'&limit=50");
        }
    
        source.addEventListener(\'message\', function(e) {
            console.log("SSE: " + e.data);
            var console_div = document.getElementById(\'console\');
            var autoScroll = false;
            if (!console_div.loaded) {
                console_div.loaded = true;
                autoScroll = true;
            }
            if (console_div.scrollTop == (console_div.scrollHeight - console_div.offsetHeight)) {
                autoScroll = true;
            }
            var logs = JSON.parse(e.data);
            logs.forEach(function (log) {
                console_div.innerHTML += "<div><span>"+prettyDate2(log.time)+"</span> - <span>"+log.log+"</span></div>";
            });
            if (autoScroll) {
                console_div.scrollTop = console_div.scrollHeight;
            }
        }, false);
    
        source.addEventListener(\'open\', function(e) {
            // Connection was opened.
            console.log("SSE: Opened");
        }, false);
    
        source.addEventListener(\'error\', function(e) {
            if (e.readyState == EventSource.CLOSED) {
                console.log("SSE: Closed");
            }
        }, false);
    };
    
    window.onbeforeunload = function() {
        source.close();
    };
</script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);