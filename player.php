<?php
require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}
if (!isset($_GET["id"])) {
    header('Location: ' . NM_URL . 'players.php');
    exit();
}
if (!$NM->getAuthedMember()->hasPermission("nm.web.view.players")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

$member = $NM->getMember($_GET["id"], true);
if (!$member->isValid()) {
    header('Location: ' . NM_URL . 'players.php');
    exit();
}

$data = array();
$data['title'] = $member->getDisplayName() . " | NetworkManager";
$data['page_name'] = $member->getDisplayName();
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Players"
	),
	array(
		'name' => $member->getDisplayName()
	),
);

if (isset($_GET['merge']) && $NM->getAuthedMember()->canTarget($member)) {
    if (!$NM->getAuthedMember()->hasPermission("nm.user.merge")) {
        // Not permitted to perform!
        header('Location: ' . NM_URL . 'blocked.php');
        exit();
    }
    $id = $auth->smartMerge($member->getId(), $_GET['merge']);
    header('Location: ' . NM_URL . 'player.php?id=' . $id);
    exit();
}

if (isset($_GET['action'])) {
    if ($member == $NM->getAuthedMember()) {
        // You can't perform actions on yourself...
    } elseif (($_GET['action'] == "addperm" || $_GET['action'] == "removeperm") && $NM->getAuthedMember()->hasPermission("nm.user.permissions", $_GET['realm'])) {
        if ($_GET['action'] == "addperm") {
            $member->givePermission($_GET['perm'], $_GET['realm']);
        } elseif ($_GET['action'] == "removeperm") {
            $member->takePermission($_GET['perm'], $_GET['realm']);
        }
    } elseif (($_GET['action'] == "addrank" || $_GET['action'] == "removerank") && $NM->getAuthedMember()->hasPermission("nm.user.rank", $_GET['realm'])) {
        if ($_GET['action'] == "addrank") {
            $member->addRank($_GET['rank'], $_GET['realm']);
        } elseif ($_GET['action'] == "removerank") {
            $member->removeRank($_GET['rank'], $_GET['realm']);
        }
    } else {
        // No permissions(we assume)
    }
}

$data['member'] = $member;
$data['links'] = array();
foreach ($member->getLinkedAccounts() as $token) {
	if (!isset($auth->getAuthLinks()[$token['type']]))
		continue;
	$method = $auth->getAuthLinks()[$token['type']];
	$data['links'][] = array('icon' => $method->getIcon(), 'profile' => $method->getProfile($token['token']), 'title' => $method->getTitle());
}

$select = array(
	"members.id",
	"members.displayName",
	"memberAlts.triggers",
	"memberAlts.likelihood",
);
$a = $NM->db->sql()->select("memberAlts", array(
	"[<]members" => array(
		"uid" => "id",
	),
	), $select, array(
		"otheruid" => $member->getId(),
	)
);
$b = $NM->db->sql()->select("memberAlts", array(
	"[<]members" => array(
		"otheruid" => "id",
	),
	), $select, array(
		"uid" => $member->getId(),
	)
);
$data['alts'] = $NM->db->query($a.' UNION '.$b)->fetchAll();
/*
$aalts = $NM->db->select("memberAlts", array(
	"[<]members" => array(
		"uid" => "id",
	),
	), array(
		"members.id",
		"members.displayName",
		"memberAlts.triggers",
	), array(
		"otheruid" => $member->getId(),
	)
);
if (!$aalts)
	$aalts = array();
$balts = $NM->db->select("memberAlts", array(
	"[<]members" => array(
		"otheruid" => "id",
	),
	), array(
		"members.id",
		"members.displayName",
		"memberAlts.triggers",
	), array(
		"uid" => $member->getId(),
	)
);
if (!$balts)
	$balts = array();

$data['alts'] = array_merge($aalts, $balts);
*/
$data['sessionCount'] = $NM->db->count("memberSessions", array("uid" => $member->getId()));
$rows = $NM->db->select("memberSessions", array("id", "sid", "startTime", "endTime"), array(
    "uid" => $member->getId(),
    "ORDER" => array(
        "id" => "DESC",
    ),
    "LIMIT" => 10,
));
$data['sessions'] = array();
foreach ($rows as $row) {
	$row['length'] = \NetworkManager\Common::timeTillHumanReadable($row["startTime"], ($row["endTime"] == -1) ? time() : $row["endTime"], true);
	$row['server'] = $NM->getServer($row["sid"])->getName();
	$data['sessions'][] = $row;
}

require NM_CLASS_ROOT . "statistics.php";
$data['stats'] = new \NetworkManager\Statistics\PlayerByNetwork($member->getId());

$data['body'] = "";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'player.html', $data);

$data['javascript'] = '<script>';
$data['javascript'] .= 'var permissions = [';
foreach ($NM->db->select("permissions", "perm") as $perm) {
	$data['javascript'] .= '"'.$perm.'",';
}
$data['javascript'] .= '];';
$data['javascript'] .= '
$(function () {
	$(\'[data-toggle="tooltip"]\').tooltip()
});

var source = null;

window.onload = function() {
	if (!!window.EventSource) {
		source = new EventSource("/stream.php?page=logs&uid='.$member->getId().'&limit=50");
	}

	source.addEventListener(\'message\', function(e) {
		console.log("SSE: " + e.data);
		var realtime_div = document.getElementById(\'realtime\');
		var autoScroll = false;
		if (!realtime_div.loaded) {
			realtime_div.loaded = true;
			autoScroll = true;
		}
		if (realtime_div.scrollTop == (realtime_div.scrollHeight - realtime_div.offsetHeight)) {
			autoScroll = true;
		}
		var logs = JSON.parse(e.data);
		logs.forEach(function (log) {
			realtime_div.innerHTML += "<div><span>"+prettyDate2(log.time)+"</span> - <span>"+log.log+"</span></div>";
		});
		if (autoScroll) {
			realtime_div.scrollTop = realtime_div.scrollHeight;
		}
	}, false);

	source.addEventListener(\'open\', function(e) {
		// Connection was opened.
		console.log("SSE: Opened");
	}, false);

	source.addEventListener(\'error\', function(e) {
		if (e.readyState == EventSource.CLOSED) {
			console.log("SSE: Closed");
		}
	}, false);
};

window.onbeforeunload = function() {
	source.close();
};

var inps = document.getElementsByClassName("permissions");
for (var i = 0; i < inps.length; i++) {
	autocomplete(inps[i], permissions);
};

function updateHierarchy(ul) {
	var lis = Array.from(ul.children).slice(1);
	var subgrouping = ul.id.replace("subgroup-", "");
	var ranks = {};
	for (var i = 0; i < lis.length; i++) {
		if (i > 0) {
			ranks[lis[i].id] = lis[i-1].id;
		} else {
			ranks[lis[i].id] = "";
		}
	}
	var post = {ranks: ranks, subgrouping: subgrouping};
	ajax(null, "ranks-update", post, function(status, data) {
		var notify = $.notify({message: "The ranks inheritance has been updated!"}, { type: "success", allow_dismiss: true });
	});
};

$(\'.list-group-sortable-ranks\').sortable({
	placeholderClass: \'list-group-item\',
	connectWith: \'.connected\',
	items: \':not(.disabled)\'
}).bind(\'sortupdate\', function(e, oldparent, ui) {
	if (oldparent != e.currentTarget) {
		updateHierarchy(oldparent);
	}
	updateHierarchy(e.currentTarget);
});
</script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);