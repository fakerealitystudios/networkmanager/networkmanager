<?php

// End all players' sessions
$NM->db->update("memberSessions", array(
    "endTime" => time(),
), array(
    "sid" => $server->getId(),
    "endTime" => "-1",
));

// End all of this server's sessions
$NM->db->update("serverSessions", array(
    "endTime" => time(),
), array(
    "sid" => $server->getId(),
    "endTime" => "-1",
));

// Cut communication
$NM->db->update("servers", array(
    "communication" => 0,
), array(
    "id" => $server->getId(),
));