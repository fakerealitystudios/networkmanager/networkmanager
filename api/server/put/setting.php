<?php

$name = array_shift($params);

if (!isset($name, $_PUT['value']))
    fatal_error(1020);

$id = $NM->db->get("settings", "id", array(
    "name" => $name,
    "realm" => $realm,
));

$q = null;
if ($id) {
    $q = $NM->db->update("settings", array(
        "value" => $_PUT['value'],
    ), array(
        "id" => $id,
    ));
} else {
    $q = $NM->db->insert("settings", array(
        "name" => $name,
        "value" => $_PUT['value'],
        "realm" => $realm,
    ));
}

if ($q->rowCount() > 0)
    fatal_error(1051);

unset($id, $q);