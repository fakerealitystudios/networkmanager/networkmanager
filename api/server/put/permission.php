<?php

if (!isset($_PUT['perm'], $_PUT['game']))
    fatal_error(1020);

$limit = $_throttle->throttle($_identifier, 10, 1000); // Limit to 10 per second from ALL servers

if ($limit != 0) {
    $return['rate_limit'] = $limit;
    fatal_error(1006);
}

// Don't save targetting perms
if (substr($_PUT['perm'], 0, 10) == "nm.target.") {
    send_payload();
}

// Used to insert a new permission node for networkmanager
// Creates a nice list in networkmanager used for auto completion and more

$has = $NM->db->has("permissions", array(
    "perm" => $_PUT['perm'],
    "game" => $_PUT['game'],
));

if (!$has)
    $NM->db->insert("permissions", array(
        "perm" => $_PUT['perm'],
        "game" => $_PUT['game'],
    ));

unset($has);