<?php

if (!isset($_PUT['type'], $_PUT['event']))
    fatal_error(1020);

$limit = $_throttle->throttle($_identifier, 10, 1000); // Limit to 10 per second from ALL servers

if ($limit != 0) {
    $return['rate_limit'] = $limit;
    fatal_error(1006);
}

if (isset($_PUT['subevent']) && $_PUT['subevent'] == "")
    unset($_PUT['subevent']);

$log = array();
$log["sid"] = $server->getId();
$log["ssid"] = isset($_PUT['ssid']) ? $_PUT['ssid'] : ((count($server->getCurrentSessions()) > 0) ? $server->getCurrentSessions()[0] : null);
$log["type"] = $_PUT['type'];
$log["event"] = $_PUT['event'];
if (isset($_PUT['subevent'])) {
    $log["subevent"] = $_PUT['subevent'];
}
$log["time"] = isset($_PUT['time']) ? $_PUT['time'] : time(); // use local system time as backup
$logDetails = array();
if (isset($_PUT['logDetails'])) {
    $logDetails = json_decode($_PUT['logDetails'], true);
    if (!isset($logDetails)) {
        $logDetails = array();
        \Sentry\configureScope(function (\Sentry\State\Scope $scope): void {
            $scope->setExtra('json_last_error_msg', json_last_error_msg());
        });
    }
    $log["args"] = count($logDetails);
    if ($log['args'] == 0)
        unset($log['args']);
}
$NM->db->insert("logs", $log);
$lid = $NM->db->id();
\Sentry\configureScope(function (\Sentry\State\Scope $scope) use ($lid): void {
    $scope->setExtra('log-id', $lid);
});
foreach ($logDetails as $detail) {
    $data = array(
        "lid" => $lid,
        "type" => $detail['type'],
        "argid" => $detail['argid'],
        "value" => $detail['value'],
    );
    if (isset($detail['vid'])) {
        $data['vid'] = $detail['vid'];
    }
    $NM->db->insert("logDetails", $data);
}

unset($log, $lid, $logDetails, $detail, $data);