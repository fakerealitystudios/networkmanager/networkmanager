<?php

$stat = array_shift($params);

if (!isset($stat, $_PUT['data']))
    fatal_error(1020);

// TODO: Add parameter checks, i.e. provider <=30 chars

$NM->db->insert("serverStatistics", array(
    "sid" => $server->getId(),
    "ssid" => isset($_PUT['ssid']) ? $_PUT['ssid'] : null,
    "provider" => isset($_PUT['provider']) ? $_PUT['provider'] : null,
    "stat" => $stat,
    "data" => $_PUT['data']
));