<?php

$ssid = array_shift($params);
if (!isset($ssid)) {
    // TODO: Remove this line and support multiple sessions per server
    $NM->db->update("serverSessions", array(
        "endTime" => \Medoo\Medoo::raw('<startTime>'),
        "invalidated" => 1,
    ), array(
        "sid" => $server->getId(),
        "endTime" => "-1",
    ));

    $NM->db->insert("serverSessions", array(
        "sid" => $server->getId(),
        "startTime" => time(),
        "endTime" => "-1",
        "invalidated" => 0,
    ));
    $ssid = $NM->db->id();
    if ($ssid == 0) {
        $return['ssid'] = -1;
    } else {
        $return['ssid'] = $ssid;
    }
} else {
    $NM->db->update("memberSessions", array(
        "endTime" => time(),
    ), array(
        "ssid" => $ssid,
        "endTime" => "-1",
    ));

    $q = $NM->db->update("serverSessions", array(
        "endTime" => time(),
    ), array(
        "id" => $ssid,
        "endTime" => "-1"
    ));

    if ($q->rowCount() == 0) {
        fatal_error(1024);
    }
}

unset($ssid, $q);