<?php

if (!isset($_PUT['name'], $_PUT['game']))
    fatal_error(1020);

$return['server'] = array();

if (!isset($_server)) {
    unset($server);
    // We're an API key with permission, spawn a new server
    $server = $NM->serverManager->create($_PUT['name'], $_PUT['game'], isset($_PUT['address']) ? $_PUT['address'] : "");
    if (!isset($server))
        fatal_error(1000, 500);

    $return['server']['id'] = $server->getId();
} else {
    // Update the server's name, game, gamemode, and address
    $data = array("name" => $_PUT['name'], "game" => $_PUT['game']);
    if (isset($_PUT['address']))
        $data['address'] = $_PUT['address'];
    if (isset($_PUT['gamemode']))
        $data['gamemode'] = $_PUT['gamemode'];
    $NM->db->update("servers", $data, array("id" => $server->getId()));
}
$return['server']['token'] = $server->newSecret();

$settings = $NM->db->select("settings", array("name", "value", "realm"), array(
    "realm" => array(
        "*",
        "*".\NetworkManager\Realm::$delimiter.$_PUT['game'],
        "*".\NetworkManager\Realm::$delimiter.$_PUT['game'].\NetworkManager\Realm::$delimiter.(isset($_PUT['gamemode']) ? $_PUT['gamemode'] : ""),
        $realm,
    )
));
$settings2 = array();
foreach($settings as $row) {
    $settings2[$row['realm']] = isset($settings2[$row['realm']]) ? $settings2[$row['realm']] : array();
    $settings2[$row['realm']][] = $row;
}

ksort($settings2);

$return['server']['settings'] = array();
// We can just do it in this manner, will properly list them in realms order.
foreach($settings2 as $row) {
    $return['server']['settings'] = array_merge($return['server']['settings'], $row);
}

include NM_ROOT."api/nm/get/instance.php";

unset($server, $settings, $settings2, $row);