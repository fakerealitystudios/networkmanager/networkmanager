<?php

$return['languages'] = array();
$rows = $NM->db->select("languages", array("lang", "event", "args", "format"));
foreach($rows as $row) {
    if (!isset($return['languages'][$row['lang']]))
        $return['languages'][$row['lang']] = array();
    if (!isset($return['languages'][$row['lang']][$row['event']]))
        $return['languages'][$row['lang']][$row['event']] = array(); // Some events have multiple for extra arguments
    $return['languages'][$row['lang']][$row['event']][] = array($row['args'], $row['format']);
}

unset($rows, $row);