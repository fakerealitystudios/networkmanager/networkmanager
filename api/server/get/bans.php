<?php

$return['bans'] = $NM->db->select("memberBans", array(
    "id",
    "uid",
    "banTime",
    "unbanTime",
    "reason"
), array(
    "OR #realms" => array(
        "realm" => array("*", "*.".$server->getGame(), $server->getRealm()."%"),
    ),
    "OR #time" => array(
        "unbanTime[>]" => time(),
        "length" => 0,
    )
));