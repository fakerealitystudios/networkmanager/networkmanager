<?php

$link = array_shift($params);
$tokens = array_shift($params);
if (!isset($link, $tokens))
    fatal_error(1020);

$tokens = explode(",", $tokens);

$return['users'] = array();
foreach ($tokens as $token) {
    $member = $NM->getMember(\NetworkManager\MemberManager::getNMID($link, $token), true);
    $ranks = array();
    if ($member->isValid()) {
        $ranks = $member->getRanks();
    }
    if (count($ranks) == 0) {
        $ranks[] = array(
            "rank" => "user",
            "realm" => "*",
        );
    }
    $return['users'][] = array(
        "memberToken" => $token,
        "ranks" => $ranks,
        "badges" => $member->getBadges(),
    );
}

unset($link, $tokens, $member, $ranks);