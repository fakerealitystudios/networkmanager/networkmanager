<?php

$limit = $_throttle->throttle($_identifier, 4, 1000); // Limit to 4 per second from ALL servers

if ($limit != 0) {
    $return['rate_limit'] = $limit;
    fatal_error(1006);
}

$actions = $NM->db->select("serverActions", array("id", "payload"), array(
    "sid" => $server->getId(),
    "performed" => 0,
));

$return['actions'] = array();

$ids = array();
foreach($actions as $action)
{
    $ids[] = $action['id'];
    $return['actions'][] = $action['payload'];
}

if (count($ids) > 0) {
    $NM->db->update("serverActions", array(
        "performed" => 1,
    ), array(
        "id" => $ids,
    ));
}

unset($actions, $action);