<?php

$return["ranks"] = array();
foreach ($NM->getRanks() as $rank) {
    $return["ranks"][] = array(
        "rank" => $rank->getRank(),
        "displayName" => $rank->getDisplayName(),
        "inherit" => $rank->getParent(),
        "realm" => $rank->getRankRealm(),
        "permissions" => $rank->perms,
        "subgrouping" => $rank->getSubGrouping(),
    );
}

unset($rank);