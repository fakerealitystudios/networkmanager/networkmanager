<?php

$rows = $NM->db->select("settings", array("name", "value", "realm"), array(
    "realm" => \NetworkManager\Realm::getRealm($realm)->getPossibleRealms(),
));

$len = \NetworkManager\Realm::getRealm($realm)->getCount();
$asterick = strstr($realm, '*');
$temp = array();
foreach($rows as $row) {
    $tempb = array(strlen($row['realm']), (strstr($row['realm'], "*") != false), $row);
    if ($tempb[0] > $len)
        continue;
    if (isset($temp[$row['name']]))
    {
        if ($temp[$row['name']][0] == $tempb[0] && $temp[$row['name']][1] == true)
            $temp[$row['name']] = $tempb;
        if ($temp[$row['name']][0] < $tempb[0])
            $temp[$row['name']] = $tempb;
    } else
        $temp[$row['name']] = $tempb;
}

$return['settings'] = array();
foreach($temp as $tempb) {
    $row = $tempb[2];
    $return['settings'][$row['name']] = $row['value'];
}

unset($rows, $len, $asterick, $temp, $row, $tempb);