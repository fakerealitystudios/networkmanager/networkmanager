<?php

$lang = array_shift($params);
if (!isset($lang))
    fatal_error(1020);

$return['language'] = array();
$rows = $NM->db->select("languages", array("event", "args", "format"), array("lang" => $lang));
foreach($rows as $row) {
    if (!isset($return['language'][$row['event']]))
        $return['language'][$row['event']] = array(); // Some events have multiple for extra arguments
    $return['language'][$row['event']][] = array($row['args'], $row['format']);
}

unset($rows, $row, $lang);