<?php

$return['server'] = array(
    'id' => $server->getId(),
    'name' => $server->getName(),
    'game' => $server->getGame(),
    'gamemode' => $server->getGamemode(),
    'realm' => $server->getRealm(),
    'address' => $server->getAddress(),
    'players' => $server->getPlayers(),
);

unset($sid, $server);