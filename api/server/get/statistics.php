<?php

require NM_CLASS_ROOT."statistics.php";

$stat = array_shift($params);

$return['top'] = array();

if ($stat == "playtime") {
    foreach (\NetworkManager\Statistics\Statistics::getTopPlaytime() as $row) {
        $return['top'][] = array("url" => NM_URL . "player.php?id=" . $row["uid"], "name" => $NM->getMember($row["uid"])->getDisplayName(), "value" => \NetworkManager\Common::timeTillHumanReadable(time() + $row["length"]));
    }
} elseif ($stat == "sessions") {
    foreach (\NetworkManager\Statistics\Statistics::getTopSessions() as $row) {
        $return['top'][] = array("url" => NM_URL . "player.php?id=" . $row["uid"], "name" => $NM->getMember($row["uid"])->getDisplayName(), "value" => $row["total"]);
    }
} elseif ($stat == "kills") {
    foreach (\NetworkManager\Statistics\Statistics::getTopKills() as $row) {
        $return['top'][] = array("url" => NM_URL . "player.php?id=" . $row["uid"], "name" => $NM->getMember($row["uid"])->getDisplayName(), "value" => $row["kills"]);
    }
} elseif ($stat == "deaths") {
    foreach (\NetworkManager\Statistics\Statistics::getTopDeaths() as $row) {
        $return['top'][] = array("url" => NM_URL . "player.php?id=" . $row["uid"], "name" => $NM->getMember($row["uid"])->getDisplayName(), "value" => $row["deaths"]);
    }
} else {
    fatal_error(1025);
}

unset($stat);