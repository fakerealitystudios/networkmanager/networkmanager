<?php

$nmids = array_shift($params);
if (!isset($nmids))
    fatal_error(1020);

$nmids = explode(",", $nmids);

$return['members'] = array();
foreach($nmids as $nmid)
{
    $member = $NM->getMember($nmid, true);
    include NM_ROOT."api/member/get/member.php";
    $return['members'][] = $return['member'];
    unset($return['member']);
}

unset($nmids, $nmid, $member);