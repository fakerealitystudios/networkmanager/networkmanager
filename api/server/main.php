<?php

$sid = array_shift($params);
$action = array_shift($params);

if (isset($_server) && !isset($sid))
    fatal_error(1020);
if (isset($_server) && $sid != $_server->getId())
    fatal_error(1005, 403);

$server = $NM->getServer($sid);

if (!isset($server) && (!isset($permissions) || (($action == NULL || $action == "server") && $_SERVER['REQUEST_METHOD'] == "PUT")))
    fatal_error(1022);