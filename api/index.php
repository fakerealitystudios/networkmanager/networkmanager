<?php

$SESSION_READONLY = true;
define('NM_API', 1);

ini_set('session.use_cookies', '0');

require "../includes/networkmanager.php";

header('Content-Type: application/json');

if (!function_exists('apache_request_headers')) {
    function apache_request_headers()
    {
        $https = array();
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) == "HTTP_") {
                $key = str_replace(" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5)))));
                $out[$key] = $value;
            } else {
                $out[$key] = $value;
            }
        }
        return $out;
    }
}

require NM_CLASS_ROOT."throttle.php";
$_throttle = new \Stiphle\Throttle\NMLeakyBucket();
if(class_exists('Memcached')) {
    $_throttle_storage = new \Stiphle\Storage\Memcached(new \Memcached());
    $_throttle->setStorage($_throttle_storage);
} elseif (class_exists('Memcache')) {
    require NM_CLASS_ROOT."ThrottleMemcache.php";
    $_throttle_storage = new \Stiphle\Storage\Memcache(new \Memcache());
    $_throttle->setStorage($_throttle_storage);
}

$errors = array(
    999 => "Not implemented.",
    1000 => "Unspecified error occurred.",
    1001 => "Invalid ServerId provided.",
    1002 => "Server Token has not yet been generated.",
    1003 => "Invalid Authorization Token provided.",
    1004 => "Insuffient Permissions.",
    1005 => "Can not perform actions on another server.",
    1006 => "Rate Limited.",

    // Auto-Logged Error types
    1010 => "Invalid Action.",
    1011 => "Invalid Scope.",

    // Parameter errors
    1020 => "Missing Parameters.",
    1021 => "NMID does not exist.",
    1022 => "SID does not exist.",
    1023 => "USID does not exist.",
    1024 => "SSID does not exist.",
    1025 => "Statistic does not exist.",
    1026 => "Rank does not exist.",

    //
    1050 => "Duplicate data.",
    1051 => "Database insertion failed.",
);

$return = array();
$return['debug'] = array();

$input = mb_convert_encoding(file_get_contents("php://input"), 'UTF-8');
$content_type = "application/x-www-form-urlencoded";
if (substr($_SERVER["CONTENT_TYPE"], 0, strlen($content_type)) == $content_type)
    parse_str($input, $input);


if (!is_array($input))
    $input = array();

switch($_SERVER['REQUEST_METHOD']) {
    case "GET":
    case "POST":
        // Already will be decoded
        break;
    default:
        ${'_'.$_SERVER['REQUEST_METHOD']} = $input;
}

function send_payload($status = 200) {
    global $return; // from this file
    global $environment;
    http_response_code($status);
    switch($status) {
        case 200:
            $return['status'] = "success";
            break;
        default:
            $return['status'] = "failure";
            break;
    }
    $return['environment'] = $environment;
    if ($environment == "development") {
        global $input, $config, $NM;
        $debug = $return['debug'];
        $debug['time'] = number_format($NM->getRuntime(), 3);

        $debug['method'] = $_SERVER['REQUEST_METHOD'];
        $debug['type'] = $_SERVER["CONTENT_TYPE"];
        $debug['data'] = array();
        $debug['data']['get'] = $_GET;
        $debug['data']['post'] = $_POST;
        $debug['data']['input'] = $input;

        if ($config['database']['logging']) {
            $debug['db'] = array();
            $debug['db']['queries'] = $NM->db->log(true);
            $debug['db']['count'] = count($debug['db']['queries']);
        }
        ksort($debug);
        $return['debug'] = $debug;
    } else {
        unset($return['debug']);
    }
    ksort($return);

    echo json_encode($return);
    exit();
}

function fatal_error($code = 1000, $status = 400)
{
    global $return, $errors, $environment;
    $return['errorcode'] = $code;
    $return['error'] = $errors[$code];

    // Don't need these messages in a non-production environment
    if ($environment == "production") {
        if ($code == 1000) {
            \Sentry\captureMessage("Unknown API Error occured", \Sentry\Severity::fatal());
        } elseif ($code >= 1004 && $code < 1020) {
            \Sentry\captureMessage("API issue occured", \Sentry\Severity::warning());
        }
    }

    send_payload($status);
}

$headers = apache_request_headers();

if (!isset($headers['Authorization'])) {
    fatal_error(1003, 401);
} elseif (!isset($headers['Server-Id'])) {
    $permissions = $NM->db->get("apiKeys", "permissions", array("secret" => $headers['Authorization']));
    if (!isset($permissions))
        fatal_error(1003, 401);
    $permissions = @json_decode($permissions, true);
    if ($permissions == null)
        $permissions = array();
} else {
    if (is_numeric($headers['Server-Id'])) {
        $_server = $NM->serverManager->get($headers['Server-Id']);
        if (!isset($_server))
            fatal_error(1001, 401);
    // Discord doesn't utilize the server id(There should only ever be 1 discord bot connecting, hopefully)
    }/* elseif ($headers['Server-Id'] == "discord") {
        $_servers = $NM->serverManager->getByGame("discord");
        if (count($_servers) != 1)
            fatal_error(1001, 401);
        
        $_server = $_servers[0];
    }*/

    if (isset($_server) && $_server->isValid() && $_server->getSecret() == "") {
        fatal_error(1002, 401);
    }

    // Authenticate server's authorization token
    if (!isset($headers['Authorization']) || $_server->getSecret() != $headers['Authorization']) {
        fatal_error(1003, 401);
    }
}

$limit = $_throttle->throttle($headers['Authorization'], 5, 1000); // Limit to 5 per second per server

if ($limit != 0) {
    $return['rate_limit'] = $limit;
    fatal_error(1006);
}

$realm = "*";
if (isset($headers['Realm'])) {
    $realm = $headers['Realm'];
} elseif (isset($_server)) {
    $realm = $_server->getRealm();
}
$return['debug']['realm'] = $realm;

// We use the Server-Id as an ID for authentication.

// ------------------------------------------------ \\
// We have been authenticated. Begin the processing \\
// ------------------------------------------------ \\

$params = array();
if (!isset($_SERVER['QUERY_STRING'])) {
    $params = explode("/", $_SERVER['REQUEST_URI']);
// TODO: This is certainly NOT MY FAVORITE, should find a better awnser
} else if (substr($_SERVER['REQUEST_URI'], 0, strlen($_SERVER['DOCUMENT_URI'])) != $_SERVER['DOCUMENT_URI'] && substr($_SERVER['REQUEST_URI'], 0, 3) != "/?/") {
    $params = explode("?", $_SERVER['REQUEST_URI']);
    $params = explode("/", substr($params[0], 1));
} else {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $params = explode("/", substr($params[0], 1));
}
$return['debug']['params'] = $params;

$_method = strtolower($_SERVER['REQUEST_METHOD']);
$scope = array_shift($params);
$action = null;

if (!file_exists($scope."/main.php"))
    fatal_error(1011);
include($scope."/main.php");
if ($action == null)
    $action = $scope;

$return['debug']['scope'] = $scope;
$return['debug']['action'] = $action;

$_identifier = $scope.".".$_method.".".$action;

if (isset($permissions)) {
    $has = false;
    foreach($permissions as $perm) {
        if (strtolower($perm) == strtolower($_identifier)) {
            $has = true;
            break;
        }
    }
    if (!$has) {
        $return['permission'] = strtolower($_identifier);
        fatal_error(1004, 403);
    }
}

$file = strtolower($scope."/".$_method."/".$action.".php");
$return['debug']['file'] = $file;

if (file_exists($file))
{
    include($file);
} else {
    fatal_error(1010, 404);
}

// No error thus far, assume we made it!
send_payload();