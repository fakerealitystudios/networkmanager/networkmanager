<?php

$nmid = array_shift($params);
$action = array_shift($params);

if (!isset($nmid) && !isset($action)) {
    return;
}

$member = $NM->getMember($nmid, true);

if (!$member->isValid())
    fatal_error(1021);

$usid = (isset(${"_{".strtoupper($scope)."}"}['usid']) ? ${"_{".strtoupper($scope)."}"}['usid'] : isset($_server)) ? $member->getSessionId($_server->getId()) : null;

$admin = new \NetworkManager\MemberNull();
$admin_usid = null;
if (isset(${"_{".strtoupper($scope)."}"}['admin_nmid'])) {
    $admin = $NM->getMember(${"_{".strtoupper($scope)."}"}['admin_nmid']);
    $admin_usid = (isset(${"_{".strtoupper($scope)."}"}['admin_usid']) ? ${"_{".strtoupper($scope)."}"}['admin_usid'] : isset($_server)) ? $admin->getSessionId($_server->getId()) : null;
}