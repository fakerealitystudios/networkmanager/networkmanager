<?php

$server = $_server;

$usid = array_shift($params);

if (!$usid) {
    $NM->db->update("memberSessions", array(
        "endTime" => time(),
    ), array(
        "uid" => $member->getId(),
        "sid" => $server->getId(),
        "endTime" => "-1",
    ));
} else {
    $q = $NM->db->update("memberSessions", array(
        "endTime" => time(),
    ), array(
        "id" => $usid,
        "endTime" => "-1"
    ));

    if ($q->rowCount() == 0) {
        fatal_error(1023);
    }
}

unset($server);