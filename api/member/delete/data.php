<?php

// The remaining params should be the indexes
if (count($params) == 0)
    fatal_error(1020);

$indexes = $params;

$return['data'] = $member->getData($realm, ...$indexes);

unset($indexes);