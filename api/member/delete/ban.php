<?php

$bid = array_shift($params);

if (!isset($bid))
    fatal_error(1020);

$q = $NM->db->delete("memberBans", array("id" => $bid, "uid" => $member->getId()));

if ($q->rowCount() == 0) {
    fatal_error(1027);
}