<?php

if (!isset($member)) {
    // We are searching for one!
    if (isset($_GET['type'], $_GET['token'])) {
        $member = $NM->getMember(\NetworkManager\MemberManager::getNMID($_GET['type'], $_GET['token']), true);

        if (!isset($member)) {
            fatal_error(1021);
        }
    } else {
        fatal_error(1020);
    }
}

if ($member->isValid()) {
    if (isset($_GET['displayName']) && array_search($_GET['displayName'], $member->getAliases()) === false)
        $NM->db->insert("memberAliases", array("uid" => $member->getId(), "alias" => $_GET['displayName']));

    $return['member'] = array();
    $return['member']['nmid'] = $member->getId();
    $return['member']['nmid_center'] = $member->getCenterId();
    $return['member']['rank'] = $member->getRealmRank($realm);
    $return['member']['ranks'] = $member->getRanks();
    $return['member']['data'] = $member->data;
    $return['member']['permissions'] = $member->perms;
    $return['member']['profileurl'] = NM_URL.'player.php?id='.$member->getId();
    $return['member']['badges'] = $member->getBadges();
    $return['member']['ips'] = $member->getIpAddresses();
    $return['member']['avatarurl'] = $member->getAvatar();
    $return['member']['displayName'] = $member->getDisplayName();
    $return['member']['linked'] = $member->getLinkedAccounts();
    $return['member']['servers'] = $member->getServers();
    $return['member']['sessions'] = $member->getSessionIds();
    $return['member']['status'] = (count($member->getCurrentServers()) > 0) ? "online" : "offline";
    $return['member']['disabled'] = $member->isDisabled();
    ksort($return['member']);
} else {
    fatal_error(1021);
}