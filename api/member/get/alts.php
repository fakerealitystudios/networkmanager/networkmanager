<?php

$aalts = $NM->db->select("memberAlts", array(
        "uid",
        "reason",
    ), array(
        "otheruid" => $member->getId(),
    )
);
$balts = $NM->db->select("memberAlts", array(
		"otheruid",
		"reason",
	), array(
		"uid" => $member->getId(),
	)
);

$return['alts'] = array_merge($aalts, $balts);
$return['note'] = "All alts listed here are only possible alts. Could alos be some friends playing from the same network, many people liking deadpool, or more. A percent chance will be added soon.";