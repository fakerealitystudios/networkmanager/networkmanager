<?php

$return['bans'] = $NM->db->select("memberBans", array(
    "uid",
    "usid",
    "adminuid",
    "adminusid",
    "length",
    "banTime",
    "unbanTime",
    "reason",
), array(
    "uid" => $member->getId(),
    "OR #realms" => array(
        "realm" => array("*", "*.".$server->getGame(), $server->getRealm()."%"),
    ),
));