<?php

// The remaining params should be the indexes
if (count($params) > 0) {
    $indexes = $params;

    $return['data'] = $member->getData($realm, ...$indexes);
} else {
    $return['data'] = $member->getData($realm);
}

unset($indexes);