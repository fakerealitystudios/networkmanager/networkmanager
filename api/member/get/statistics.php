<?php

require NM_CLASS_ROOT."statistics.php";

$return['stats'] = array();

$stat = \NetworkManager\Statistics\Statistics::getTopPlaytime($member->getId(), null, 1);
$return['stats']['playtime'] = \NetworkManager\Common::timeTillHumanReadable(time() + $stat[0]["length"]);
$stat = \NetworkManager\Statistics\Statistics::getTopSessions($member->getId(), null, 1);
$return['stats']['sessions'] = $stat[0]["total"];
$stat = \NetworkManager\Statistics\Statistics::getTopKills($member->getId(), null, 1);
$return['stats']['kills'] = $stat[0]["kills"];
$stat = \NetworkManager\Statistics\Statistics::getTopDeaths($member->getId(), null, 1);
$return['stats']['deaths'] = $stat[0]["deaths"];

unset($stat);