<?php

$server = $_server;

$usid = array_shift($params);

if (!$usid) {
    $NM->db->update("memberSessions", array(
        "endTime" => \Medoo\Medoo::raw('<startTime>'),
        "invalidated" => 1,
    ), array(
        "uid" => $member->getId(),
        "sid" => $server->getId(),
        "endTime" => "-1",
    ));

    $ip = isset($_PUT["ip"]) ? $_PUT["ip"] : null;

    if ($ip) {
        $res = \NetworkManager\API::post("/member/".$member->getCenterId(), array(
            "display_name" => $_PUT["displayName"],
            "ipaddress" => $ip,
            "nmid" => $member->getId()
        ));
    }

    $q = $NM->db->insert("memberSessions", array(
        "uid" => $member->getId(),
        "ip" => $ip,
        "sid" => $server->getId(),
        "ssid" => isset($_PUT['ssid']) ? $_PUT['ssid'] : ((count($server->getCurrentSessions()) > 0) ? $server->getCurrentSessions()[0] : null),
        "startTime" => time(),
        "endTime" => -1,
        "invalidated" => 0,
    ));
    if ($q->rowCount() == 0) {
        fatal_error(1000);
    }
    $usid = $NM->db->id();
    if ($usid == 0) {
        $return['usid'] = -1;
    } else {
        $return['usid'] = $usid;
    }
} else {
    if (isset($_PUT["ip"])) {
        $q = $NM->db->update("memberSessions", array(
            "ip" => $_PUT["ip"],
        ), array(
            "id" => $usid,
            "ip" => null
        ));

        $res = \NetworkManager\API::post("/member/".$member->getCenterId(), array(
            "display_name" => $_PUT["displayName"],
            "ipaddress" => $_PUT["ip"],
            "nmid" => $member->getId()
        ));
    }

    if ($q->rowCount() == 0) {
        fatal_error(1023);
    }
}

unset($server);