<?php

if (!isset($_PUT['stat'], $_PUT['data']))
    fatal_error(1020);

// TODO: Add parameter checks, i.e. provider <=30 chars

$NM->db->insert("memberStatistics", array(
    "uid" => $member->getId(),
    "usid" => isset($_PUT['usid']) ? $_PUT['usid'] : null,
    "provider" => isset($_PUT['provider']) ? $_PUT['provider'] : null,
    "stat" => $_PUT['stat'],
    "data" => $_PUT['data']
));