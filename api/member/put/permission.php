<?php

if (!isset($_PUT['perm']))
    fatal_error(1020);

$member->givePermission($_PUT['perm'], $realm);