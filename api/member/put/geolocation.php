<?php

$ip = array_shift($params);
$ipinfo = \NetworkManager\API::get('/core/iplookup/'.$ip)['data'];

$q = $NM->db->update("geolocations", array(
    "country" => $ipinfo['location']['country_code'],
    "province" => $ipinfo['location']['region'],
), array(
    "uid" => $member->getId(),
));

if ($q->rowCount() == 0)
    $q = $NM->db->insert("geolocations", array(
        "uid" => $member->getId(),
        "country" => $ipinfo['location']['country_code'],
        "province" => $ipinfo['location']['region'],
    ));

if ($q->rowCount() == 0)
    fatal_error(1051);