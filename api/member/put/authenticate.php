<?php

if (!isset($_PUT['type'], $_PUT['token']))
    fatal_error(1020);

// Use this for account linking/account authentication
$uuid = $NM->db->query("SELECT UUID() `uuid`")->fetch()['uuid'];

$NM->db->insert("memberPending", array(
    "id" => $uuid,
    "type" => $_PUT['type'],
    "token" => $_PUT['token'],
    "time" => time()
));

$return['token'] = $uuid;