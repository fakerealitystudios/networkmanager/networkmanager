<?php

if (!isset($_PUT['badge']))
    fatal_error(1020);

$NM->db->insert("memberBadges", array(
    "uid" => $member->getId(),
    "badge" => $_PUT['badge'],
));