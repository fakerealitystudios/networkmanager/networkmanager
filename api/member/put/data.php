<?php

// The remaining params should be the indexes
if (!isset($_PUT['value']) || count($params) == 0)
    fatal_error(1020);

$indexes = $params;

if (!$member->setData($realm, $_PUT['value'], ...$indexes))
    fatal_error(1050);