<?php

$punishment = array_shift($params);

if (!isset($punishment, $_PUT['reason']))
    fatal_error(1020);

$NM->db->insert("memberPunishments", array(
    "uid" => $member->getId(),
    "usid" => $usid,
    "adminuid" => $admin->getId(),
    "adminusid" => $admin_usid,
    "punishment" => $punishment,
    "punishment" => isset($_PUT['provider']) ? $_PUT['provider'] : null,
    "reason" => $_PUT['reason'],
    "time" => time(),
    "realm" => $realm
));

$res = \NetworkManager\API::post("/member/".$member->getCenterId()."/punishment/".$punishment, array(
    "provider" => $_PUT['provider'],
    "reason" => $_PUT['reason'],
    "admin_id" => $admin->getCenterId(),
));

unset($admin, $admin_usid);