<?php

// This request will either create a new user or will return an existing one

if (!isset($member)) {
    if (isset($_PUT['type']) && isset($_PUT['token']) && isset($_PUT['displayName'])) {
        // Attempt to get member
        $member = $NM->getMember(\NetworkManager\MemberManager::getNMID($_PUT['type'], $_PUT['token']), true);
        // Attempt to create the user instead
        if (!$member->isValid()) {
            $member = $NM->getMember($auth->tokenRegister($_PUT['type'], $_PUT['token'], $_PUT['displayName']));
            $return['new_member'] = true;
        }
    } else {
        fatal_error(1020);
    }
}

if ($member->isValid()) {
    // Insert current display name to aliases
    if (array_search($_PUT['displayName'], $member->getAliases()) === false)
        $NM->db->insert("memberAliases", array("uid" => $member->getId(), "alias" => $_PUT['displayName']));
    
    include NM_ROOT."api/member/get/member.php";
} else {
    fatal_error(1020);
}