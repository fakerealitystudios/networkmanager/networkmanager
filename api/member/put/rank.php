<?php

if (!isset($_PUT['rank']))
    fatal_error(1020);

$return['id'] = $member->giveRank($_PUT['rank'], $realm);