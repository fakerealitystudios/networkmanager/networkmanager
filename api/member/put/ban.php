<?php

// Ban id
$bid = array_shift($params);

if (!isset($bid)) {
    // Create ban
    if (!isset($_PUT['length'], $_PUT['reason']))
        fatal_error(1020);

    $NM->db->insert("memberBans", array(
        "uid" => $member->getId(),
        "usid" => $usid,
        "adminuid" => $admin->getid(),
        "adminusid" => $admin_usid,
        "banTime" => time(),
        "unbanTime" => time() + $_PUT['length'],
        "length" => $_PUT['length'],
        "reason" => $_PUT['reason'],
        "realm" => $realm
    ));

    // Create a punishment entry as well.
    array_unshift($params, "ban");
    include("punishment.php");
} else {
    // Adjust ban
    if (!isset($_PUT['length']))
        fatal_error(1020);

    $NM->db->update("memberBans", array("unbanTime" => time() + $_PUT['length'], "length" => $_PUT['length']), array("id" => $bid));
}

unset($admin, $admin_usid);