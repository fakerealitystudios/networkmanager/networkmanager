<?php

$servers = $NM->getServers();
$return['servers'] = array();

foreach ($servers as $server) {
    $return['servers'][] = array(
        'id' => $server->getId(),
        'name' => $server->getName(),
        'game' => $server->getGame(),
        'gamemode' => $server->getGamemode(),
        'realm' => $server->getRealm(),
        'address' => $server->getAddress(),
        'players' => $server->getPlayers(),
    );
}

unset($servers);