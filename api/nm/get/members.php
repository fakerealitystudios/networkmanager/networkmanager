<?php

$nmids = array_shift($params);

if ($nmids == "token") {
    
} else {
    $nmids = explode(",", $nmids);

    $return['members'] = array();

    foreach($nmids as $nmid)
    {
        $member = $NM->getMember($nmid, true);
        include NM_ROOT."api/member/get/member.php";
        $return['members'][] = $return['member'];
        unset($return['member']);
    }

    unset($nmids, $nmid, $member);
}