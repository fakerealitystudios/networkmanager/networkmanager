<?php

if (!isset($_PUT['uid'], $_PUT['title']))
    fatal_error(1020);

fatal_error(999, 501);

$NM->db->insert("tickets", array(
    "uid" => $_PUT['uid'],
    "usid" => isset($_PUT['usid']) ? $_PUT['usid'] : null,
    "otheruid" => isset($_PUT['otheruid']) ? $_PUT['otheruid'] : null,
    "otherusid" => isset($_PUT['otherusid']) ? $_PUT['otherusid'] : null,
    "sid" => isset($_PUT['sid']) ? $_PUT['sid'] : null,
    "ssid" => isset($_PUT['ssid']) ? $_PUT['ssid'] : null,
    "time" => isset($_PUT['time']) ? $_PUT['time'] : time(),
    "title" => $_PUT['title'],
    "status" => "PENDING"
));