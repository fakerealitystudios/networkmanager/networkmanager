<?php

require "includes/networkmanager.php";

if (!isset($_GET['return'])) {
    if (isset($_POST['referrer'])) {
        $referrer = $_POST['referrer'];
    } else if (isset($_SESSION["last_page"])) {
        $file = basename($_SESSION["last_page"]);
        if (substr($file, 0, 9) != 'login.php' && substr($file, 0, 10) != 'logout.php') {
            $referrer = $_SESSION["last_page"];
        }
    }
}

function handleLogin()
{
    global $NM, $auth, $referrer;

    if (!$auth->isLoggedIn()) {
        return;
    }

    // Cleanup sessions
    $auth->cleanupSessions();

    $return_url = isset($_GET['return']) ? $_GET['return'] : null;
    if (isset($return_url)) {
        $url = parse_url($return_url);
        parse_str($url["query"], $get);
        $get["uid"] = $auth->getUserId();
        $get["ticket"] = \NetworkManager\Common::GenerateRandomString(16, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'); // Used for OAuth type authentication
        $NM->db->insert("memberTickets", array(
            'uid' => $get['uid'],
            'ticket' => $get['ticket'],
            'time' => time() + 60, // ticket is valid for 1 minute
        ));
        header('Location: ' . (isset($url["scheme"]) ? $url["scheme"] : "http") . "://" . $url["host"] . $url["path"] . "?" . http_build_query($get));
    } else if (isset($referrer)) {
        header('Location: ' . $referrer);
    } else {
        header('Location: ' . NM_URL); // Goto the main site
    }
    exit();
}

if (isset($_POST["username"], $_POST["password"])) {
    if ($auth->login($_POST["username"], $_POST["password"], isset($_POST['rememberme']))) {
        handleLogin();
    } else {
        // Need to display an error
        $_alerts[] = array(
            'type' => 'danger',
            'text' => 'Invalid username or password',
        );
	}
} elseif (isset($_GET['authToken'])) {
	if ($auth->proccessAuthToken($_GET['authToken'])) {
		handleLogin();
	}
} elseif ($auth->proccessTokens()) {
    handleLogin();
} elseif ($auth->isLoggedIn()) {
    handleLogin();
} elseif ($auth->doweremember()) {
    handleLogin();
}

if (isset($_SESSION["2fa"])) {
    if ($auth->get2faType() == "") {
        $auth->collect2fa();
    }
    if ($auth->get2faType() == "2step") {
        if (isset($_POST['2step'])) {
            $googleAuthenticator = new \Dolondro\GoogleAuthenticator\GoogleAuthenticator();
            if ($googleAuthenticator->authenticate($auth->twofa['2step'], $_POST['2step'])) {
                $_SESSION['2fa'] = true;
                handleLogin();
            } else {
                $_alerts[] = array(
                    "type" => "danger",
                    "text" => "Failed to verify authenticator app, verify your app and time and try the new secret!",
                );
            }
        }
    } else {
        // Fuck you? I mean, this shouldn't really happen, but...
    }
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<link href="<?php echo NM_CDN; ?>assets/css/login.min.css" rel="stylesheet">
		<!-- END CSS for this page -->
	</head>

	<body>

		<div class="headerbar">
			<!-- LOGO -->
			<div class="headerbar-left">
				<a href="/" class="logo"><img alt="Logo" src="<?php echo NM_CDN; ?>assets/img/logo.png" /> <span>NetworkManager</span></a>
			</div>

			<nav class="navbar-custom">
				<ul class="list-inline float-right mb-0">
					<li class="list-inline-item dropdown notif">
						<a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<i class="fa-solid fa-circle-question"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-arrow-success dropdown-lg">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5><small>Help and Support</small></h5>
							</div>
							<!-- All-->
							<a title="Contact Us" target="_blank" href="https://www.networkmanager.io/contact/" class="dropdown-item notify-item notify-all"><i class="fa-solid fa-link"></i> Contact Us</a>
						</div>
					</li>
				</ul>
			</nav>
		</div>
		<!-- End Navigation -->

		<div class="container h-100" style="margin-top:75px;">

			<div class="row h-100 justify-content-center align-items-center">


				<div class="card">
					<div class="card-header">
						<h4>Login</h4>
					</div>

					<div class="card-body">

						<form data-toggle="validator" role="form" method="post" action="login.php<?php if (isset($_GET['return'])) {?>?<?php echo http_build_query(array("return" => $_GET['return']));} ?>">
                            <?php if (isset($referrer)) {?><input type="hidden" name="referrer" value="<?php echo $referrer; ?>"><?php }?>
                            <div class="row">
								<div class="col-md-12">
								<div class="form-group">
								<label>Username</label>
								<div class="input-group">
									<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-envelope-open" aria-hidden="true"></i></span>
									</div>
									<input type="text" class="form-control" name="username" data-error="Input valid username" required>
								</div>
								<div class="help-block with-errors text-danger"></div>
								</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
								<div class="form-group">
								<label>Password</label>
								<div class="input-group">
									<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fa-solid fa-unlock-keyhole" aria-hidden="true"></i></span>
									</div>
									<input type="password" id="inputPassword" data-minlength="6" name="password" class="form-control" data-error="Password to short" required />
								</div>
								<div class="help-block with-errors text-danger"></div>
								</div>
								</div>
							</div>

							<div class="row">
								<div class="checkbox checkbox-primary">
									<input id="checkbox_remember" name="rememberme" type="checkbox" name="remember">
									<label for="checkbox_remember"> Remember me</label>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
								<input type="hidden" name="redirect" value="" />
								<input type="submit" class="btn btn-primary btn-lg btn-block" value="Login" name="submit" />
								</div>
							</div>

						</form>

						<div class="clear"></div>

						<i class="fa-solid fa-rotate-left"></i> Forgot password? <a href="reset-password.php">Reset password</a><br/>
                        <div class="clear"></div>

					</div>

				</div>

				<div style="width:50px;"></div>

				<div class="card">
					<h4 class="card-header">Alternate Login</h4>
					<div class="card-body">
                        <?php
                        foreach($auth->getAuthLinks() as $method) {
							if ($method->isLogin()) {
                            ?>
                            <div class="pb-2">
                                <a href="<?php echo $method->generateUrl(NM_URL . 'login.php'); ?>"><?php echo $method->getButton(); ?></a>
                            </div>
                            <?php
							}
                        }
                        ?>
					</div>
				</div>

			</div>

		</div>

		<footer class="footer">
			<div class="container">
				<span class="text-right">
				Copyright <a target="_blank" href="https://www.networkmanager.io/">NetworkManager</a>
				</span>
				<span class="float-right">
				Template by <a target="_blank" href="https://www.pikeadmin.com"><b>Pike Admin</b></a>
				</span>
			</div>
		</footer>

		<?php if ($auth->get2faType() == '2step') {?>
		<div class="modal fade" id="authenticatorappModal" tabindex="-1" role="dialog" aria-labelledby="authenticatorappModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content" style="max-width: 475;">
					<div class="modal-header" style="height:175px;background: no-repeat url(https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2018/08/28/105420789-1535480940323google-titan-key.720x405.jpg?v=1535480976) -100px -180px">
						<button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<h5 class="modal-title" id="authenticatorappModalLabel">Authenticate via App</h5>
						<form class="input-group" role="form" method="post" action="<?php echo NM_URL; ?>login.php">
							<input type="text" name="2step" class="form-control" style="margin:0px;" placeholder="Code">
							<div class="input-group-append">
								<button class="btn btn-outline-secondary" type="submit">Submit</button>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<?php }?>

		<?php include NM_ROOT . "includes/script.php";?>

		<?php if ($auth->get2faType() == '2step') {?>
		<script>
			$("#authenticatorappModal").modal()
		</script>
		<?php }?>

		<!-- BEGIN Java Script for this page -->
		<!-- END Java Script for this page -->

	</body>
</html>
