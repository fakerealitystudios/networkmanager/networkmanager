<?php
require "includes/networkmanager.php";
$auth = $NM->auth;

header('Content-Type: application/json');
$return = array();

if (!$auth->isLoggedIn()) {
    $return["type"] = "danger";
    $return["message"] = "You're not logged in!";
} elseif (!isset($_POST['action'])) {
    $return["type"] = "danger";
    $return["message"] = "No action provided!";
} elseif (!$NM->getAuthedMember()->hasPermission("nm.web.view")) {
    $return["type"] = "danger";
    $return["message"] = "You have no permission!";
} elseif ($_POST['action'] == "server-concommand") {
    if (!$NM->getAuthedMember()->hasPermission("nm.server.concommand", $NM->getServer($_POST["sid"])->getRealm())) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $server = $NM->getServer($_POST["sid"]);
    if (!$server->isValid()) {
        $return["type"] = "danger";
        $return["message"] = "Server Id does not exist!";
        goto end;
    }
    $server->getCommunicationSocket()->command($_POST['concommand']);
} elseif ($_POST['action'] == "server-restart") {
    if (!$NM->getAuthedMember()->hasPermission("nm.server.restart", $NM->getServer($_POST["sid"])->getRealm())) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $server = $NM->getServer($_POST["sid"]);
    if (!$server->isValid()) {
        $return["type"] = "danger";
        $return["message"] = "Server Id does not exist!";
        goto end;
    }
    $server->getCommunicationSocket()->command("_restart");
} elseif ($_POST['action'] == "server-stop") {
    if (!$NM->getAuthedMember()->hasPermission("nm.server.stop", $NM->getServer($_POST["sid"])->getRealm())) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $server = $NM->getServer($_POST["sid"]);
    if (!$server->isValid()) {
        $return["type"] = "danger";
        $return["message"] = "Server Id does not exist!";
        goto end;
    }
    $server->getCommunicationSocket()->command("_restart");
} elseif ($_POST['action'] == "player-kick") {
    if (!$NM->getAuthedMember()->hasPermission("nm.commands.kick", $NM->getServer($_POST["sid"])->getRealm())) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $member = $NM->getMember($_POST["nmid"]);
    if (!$NM->getAuthedMember()->canTarget($member)) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    \NetworkManager\ServerManager::getSelectiveCommunicationSockets($member->getCurrentServers())->kick($member->getId(), "No reason, cause we can");
} elseif ($_POST['action'] == "player-ban") {
    if (!$NM->getAuthedMember()->hasPermission("nm.commands.ban")) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $member = $NM->getMember($_POST["nmid"]);
    if (!$NM->getAuthedMember()->canTarget($member)) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $banTime = time();
    $length = 1;
    $reason = "No reason, cause we can";

    $ban = array(
        "uid" => $_POST['nmid'],
        "adminuid" => $NM->getAuthedMember()->getId(),
        "banTime" => $banTime,
        "unbanTime" => $banTime + $length,
        "length" => $length,
        "reason" => $reason,
        "realm" => "*",
    );
    $NM->db->insert("memberBans", $ban);

    // Then tell the servers that the user has been banned
    \NetworkManager\ServerManager::getSelectiveCommunicationSockets($member->getCurrentServers())->ban($member->getId(), $banTime, $length, $reason);
} elseif ($_POST['action'] == "player-addrank") {
    if (!$NM->getAuthedMember()->hasPermission("nm.user.rank." . $_POST['rank'], $_POST['realm'])) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $member = $NM->getMember($_POST["nmid"]);
    if (!$NM->getAuthedMember()->canTarget($member)) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $member->addRank($_POST['rank'], $_POST['realm']);
} elseif ($_POST['action'] == "player-delrank") {
    if (!$NM->getAuthedMember()->hasPermission("nm.user.rank." . $_POST['rank'], $_POST['realm'])) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $member = $NM->getMember($_POST["nmid"]);
    if (!$NM->getAuthedMember()->canTarget($member)) {
        $return["type"] = "danger";
        $return["message"] = "You don't have permission!";
        goto end;
    }

    $member->removeRank($_POST['rank'], $_POST['realm']);
} elseif ($_POST['action'] == "player-addperm") {

} elseif ($_POST['action'] == "player-delperm") {

} elseif ($_POST['action'] == "rank-addperm") {

} elseif ($_POST['action'] == "rank-delperm") {

} elseif ($_POST['action'] == "ranks-update") {
    foreach($_POST['ranks'] as $rank => $inherit) {
        $NM->db->update("ranks", array(
            "inherit" => ($inherit != "") ? $inherit : null,
            "subgrouping" => $_POST['subgrouping']
        ), array(
            "rank" => $rank
        ));
    }
} else {
    $return["type"] = "danger";
    $return["message"] = "Action does not exist!";
    goto end;
}

end:
if (isset($return["message"]) && isset($return["type"]) && $return["type"] == "danger") {
    header("HTTP/1.0 400 Bad Request");
}
echo json_encode($return);
