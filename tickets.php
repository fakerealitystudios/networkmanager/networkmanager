<?php
require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (isset($_GET["page"])) {
    $page = $_GET["page"];
} else {
    $page = 1;
}
$limit = isset($_GET['limit']) ? $_GET['limit'] : 25;

$data = array();
$data['title'] = "Tickets | NetworkManager";
$data['page_name'] = "Tickets";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Tickets"
	),
);

$condition = array();
if (!$NM->getAuthedMember()->hasPermission("nm.web.view.tickets")) {
    $condition["uid"] = $NM->getAuthedMember()->getId();
}
$pages = ceil($NM->db->count("tickets", $condition) / $limit);
if ($page > $pages) {
    $page = $pages;
}

require NM_INCL_ROOT . "pagination.php";

$condition["LIMIT"] = $limit;
if ($page > 1) {
    $condition["LIMIT"] = [$limit * ($page - 1), $limit];
}
$condition["ORDER"] = array("id" => "DESC");

$rows = $NM->db->select("tickets", array("id", "uid", "usid", "otheruid", "otherusid", "time", "title", "status"), $condition);
$ids = array();
foreach ($rows as $row) {
    $ids[] = $row['id'];
}
//$messages = $NM->db->select("ticketMessages", array("tid", "uid", "time" => \Medoo\Medoo::raw("max(<time>)")), array("tid" => $ids, "GROUP" => ["tid", "uid"]));
$data['rows'] = array();
if ($rows != null) {
    foreach ($rows as $row) {
        $row['time'] = date("Y-m-d H:i:s", $row['time']);
        $msg = $NM->db->get("ticketMessages", array("tid", "uid", "time"), array("tid" => $row['id'], "ORDER" => ["time" => "DESC"]));
        if ($msg && $msg['tid'] == $row['id']) {
            $row['time'] = date("Y-m-d H:i:s", $msg['time']);
            $row['recentuid'] = $row['uid'];
            $row['recentDisplayName'] = $NM->getMember($msg['uid'])->getDisplayName();
        }
        /*foreach($messages as $msg) {
            if ($msg['tid'] == $row['id']) {
                $row['time'] = date("Y-m-d H:i:s", $msg['time']);
                $row['recentuid'] = $row['uid'];
                $row['recentDisplayName'] = $NM->getMember($row['uid'])->getDisplayName();
                break;
            }
        }*/
        $row['displayName'] = $NM->getMember($row['uid'])->getDisplayName();
        $row['otherDisplayName'] = $NM->getMember($row['otheruid'])->getDisplayName();
        $data['rows'][] = $row;
    }
}

$data['body'] = "";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'tickets.html', $data);

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);