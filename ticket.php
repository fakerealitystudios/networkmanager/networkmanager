<?php
require "includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

$tid = $_GET["id"];

$ticket = $NM->db->get("tickets", "*", array("id" => $tid));
if (!$NM->getAuthedMember()->hasPermission("nm.web.view.tickets")) {
    if ($ticket['uid'] != $NM->getAuthedMember()->getId()) {
        // Not permitted to view!
        header('Location: ' . NM_URL . 'blocked.php');
        exit();
    }
} elseif (!$ticket) {
    header('Location: ' . NM_URL . 'tickets.php');
    exit();
}

if (isset($_POST['content'])) {
    if ($ticket['status'] == 'PENDING' && $ticket['uid'] == $NM->getAuthedMember()->getId())
        $NM->db->update("tickets", array("status" => "OPEN"), array("id" => $tid));
    $NM->db->update("tickets", array("latest" => time()), array("id" => $tid));
    $NM->db->insert("ticketMessages", array(
        "tid" => $tid,
        "uid" => $NM->getAuthedMember()->getId(),
        "time" => time(),
        "msg" => $_POST['content'],
    ));
}

$data = array();
$data['title'] = $ticket['title']." | NetworkManager";

$data['page_name'] = $ticket['title'];
if ($ticket['status'] == 'PENDING')
    $data['page_name'] = '<kbd class="bg-warning">PENDING</kbd> '.$data['page_name'];
else if ($ticket['status'] == 'OPEN')
    $data['page_name'] = '<kbd class="bg-primary">OPEN</kbd> '.$data['page_name'];
else if ($ticket['status'] == 'CLOSED')
    $data['page_name'] = '<kbd class="bg-success">CLOSED</kbd> '.$data['page_name'];
else
    $data['page_name'] = '<kbd class="bg-danger">UNKNOWN</kbd> '.$data['page_name'];
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Tickets"
    ),
    array(
		'name' => $ticket['title']
	),
);

$rows = $NM->db->select("ticketMessages", "*", array("tid" => $tid));
$data['tid'] = $tid;
$data['rows'] = array();
foreach ($rows as $row) {
    $row['time'] = \NetworkManager\Common::timeElapsedString($row["time"]);
    $row['user'] = $NM->getMember($row["uid"]);
    $row['authedMember'] = $row['user'] == $NM->getAuthedMember();
    $data['rows'][] = $row;
}

$data['body'] = "";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'ticket.html', $data);

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);