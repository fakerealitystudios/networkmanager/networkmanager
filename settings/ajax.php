<?php

require "../includes/networkmanager.php";
$auth = $NM->auth;

header('Content-Type: application/json');
$return = array();

if (!$auth->isLoggedIn()) {
    $return["type"] = "danger";
    $return["message"] = "You're not logged in!";
    // TODO: Move to server's realm for permissions
} elseif (!$NM->getAuthedMember()->hasPermission("nm.settings.edit")) {
	$return["type"] = "danger";
	$return["message"] = "No permission!";
} elseif (isset($_GET['sid'])) {
	$server = $NM->getServer($_GET["sid"]);
	if (!$server->isValid()) {
		$return["type"] = "danger";
   		$return["message"] = "Server id does not exist!";
	} else {
		$rows = $NM->db->select("settings", array("name", "value"), array("realm[~]" => $server->getRealm()));
		$settings = array();
		foreach($rows as $row) {
			$settings[$row["name"]] = $row["value"];
		}
		if (isset($_POST['change'])) { // When just performing updates
			unset($_POST['change']);
			foreach($_POST as $k => $v) {
				if (!isset($settings[$k])) {
					if (is_array($v)) {
						$v = json_encode($v);
					}
					$NM->db->insert("settings", array(
						"name" => $k,
						"value" => $v,
						"realm" => $server->getRealm()
					));
				} else {
					if (is_array($v)) {
						$settings[$k] = json_decode($settings[$k], true);
						$v = array_replace_recursive($settings[$k], $v);
						$v = json_encode($v);
						
					}
					$NM->db->update("settings", array(
						"value" => $v
					), array(
						"name" => $k,
						"realm" => $server->getRealm()
					));
				}
				break;
			}
			$return["type"] = "success";
			$return["message"] = "Updated!";
		} elseif (isset($_POST['remove'])) {
			$return["type"] = "success";
			$return["message"] = "Removed entry!";

			if (!isset($settings[$_POST['name']])) {
				$return["type"] = "danger";
				$return["message"] = "Setting does not exist!";
			} else {
				$settings[$_POST['name']] = json_decode($settings[$_POST['name']], true);
				foreach($settings[$_POST['name']] as $k => $v) {
					if ($v == $_POST['remove']) {
						unset($settings[$_POST['name']][$k]);
					}
				}
				$settings[$_POST['name']] = array_values($settings[$_POST['name']]);

				$NM->db->update("settings", array(
					"value" => json_encode($settings[$_POST['name']])
				), array(
					"name" => $_POST['name'],
					"realm" => $server->getRealm()
				));
			}
		} elseif (isset($_POST['removeid'])) {
			$return["type"] = "success";
			$return["message"] = "Removed entry!";

			if (!isset($settings[$_POST['name']])) {
				$return["type"] = "danger";
				$return["message"] = "Setting does not exist!";
			} else {
				$settings[$_POST['name']] = json_decode($settings[$_POST['name']], true);
				unset($settings[$_POST['name']][$_POST['removeid']]);
				$settings[$_POST['name']] = array_values($settings[$_POST['name']]);

				$NM->db->update("settings", array(
					"value" => json_encode($settings[$_POST['name']])
				), array(
					"name" => $_POST['name'],
					"realm" => $server->getRealm()
				));
			}
		} else {
			$return["type"] = "success";
			$return["message"] = "Created new entry!";
			if (isset($_POST['advertisements'])) {
				preg_match_all("/(?:<span style=\"color: rgb\((.*?)\);\">(.*?)<\/span>|<p>|<\/p>|([^<]*))/", $_POST['advertisements'], $matches);
				$advertisement = array();
				foreach ($matches[1] as $k => $v) {
					if ($matches[2][$k] != "") {
						$advertisement[] = array(
							"color" => explode(", ", $v, 3),
							"text" => $matches[2][$k],
						);
					} elseif($matches[3][$k] != "") {
						$advertisement[] = array(
							"color" => array(0, 0, 0),
							"text" => $matches[3][$k],
						);
					}
				}
				$settings['advertisements'] = json_decode($settings['advertisements'], true);
				$settings['advertisements'][] = array(
					"parts" => $advertisement,
					"enabled" => true,
					"interval" => 5
				);
				$NM->db->update("settings", array(
					"value" => json_encode($settings['advertisements'])
				), array(
					"name" => "advertisements",
					"realm" => $server->getRealm()
				));
			} else {
				foreach($_POST as $k => $v) {
					if (!isset($settings[$k])) {
						$return["type"] = "danger";
						$return["message"] = "Setting does not exist!";
					} else {
						if (is_array($v)) {
							$settings[$k] = json_decode($settings[$k], true);
							$v = array_merge_recursive($settings[$k], $v);
							$v = json_encode($v);
						}
						$NM->db->update("settings", array(
							"value" => $v
						), array(
							"name" => $k,
							"realm" => $server->getRealm()
						));
					}
				}
			}
		}
	}
} else {
	$return["type"] = "danger";
    $return["message"] = "Action does not exist!";
}

if (isset($return["message"]) && isset($return["type"]) && $return["type"] == "danger") {
    header("HTTP/1.0 400 Bad Request");
}
echo json_encode($return);