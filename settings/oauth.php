<?php
require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == "addkey" && $NM->getAuthedMember()->hasPermission("nm.settings.oauth")) {
        $secret = bin2hex(random_bytes(32));
        $NM->db->insert("oauthClients", array(
            "client_id" => \Medoo\Medoo::raw("UUID()"),
            'client_secret' => $secret,
            'title' => $_POST['name'],
            'redirect_uri' => $_POST['redirect_uri'],
        ));
    } elseif ($_POST['action'] == "removekey" && $NM->getAuthedMember()->hasPermission("nm.settings.oauth")) {

    }
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>OAuth Keys | Settings | NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<!-- END CSS for this page -->
	</head>

	<body class="adminbody">

		<div id="main">

			<?php include NM_ROOT . "includes/header.php";?>
			<?php include NM_ROOT . "includes/sidebar.php";?>

			<div class="content-page">

				<!-- Start content -->
				<div class="content">

					<div class="container-fluid">

						<div class="row">
							<div class="col-xl-12">
								<div class="breadcrumb-holder">
									<h1 class="main-title float-left">OAuth Keys</h1>
									<ol class="breadcrumb float-right">
										<li class="breadcrumb-item">Home</li>
										<li class="breadcrumb-item active">Settings</li>
									</ol>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<!-- end row -->

                        <div class="card mb-3">
                            <div class="card-header">
                                <h3><i class="fa-solid fa-users"></i> OAuth Keys</h3>
                            </div>

                            <div class="card-body">
                                <p>OAuth Keys are used to authenticate users on external applications. Currently, it is only used for authentication and to provide data, but the OAuth API may be expanded later.</p>
								<p>You may leave the Redirect Uri blank to allow access from any application.</p>
                            </div>
                        </div>

						<div class="bg-white">
							<table id="keys" class="table table-responsive-xl table-hover display" style="margin:0;">
								<thead>
									<tr>
										<th>Title</th>
										<th>Identifier</th>
										<th>Redirect Uri</th>
										<th>Secret</th>
									</tr>
								</thead>
								<tbody>
								<?php
$condition = array();

$rows = $NM->db->select("oauthClients", array("client_id", "client_secret", "title", "redirect_uri"));
foreach ($rows as $row) {
    ?>
										<tr data-id="<?php echo $row["client_id"]; ?>">
											<td contenteditable="false" data-name="name"><?php echo $row["title"]; ?></td>
											<td contenteditable="false" data-name="client_id"><?php echo $row["client_id"]; ?></td>
											<td contenteditable="false" data-name="redirect_uri"><?php echo $row["redirect_uri"]; ?></td>
											<td contenteditable="false" data-name="secret" id="<?php echo $row["client_id"]; ?>"><a onclick="document.getElementById('<?php echo $row["client_id"]; ?>').innerHTML = '<?php echo $row["client_secret"]; ?>';return false;" href="#">Click here to Reveal</a></td>
										</tr>
										<?php
}
?>
								</tbody>
								<tr>
								<form class="input-group" style="padding:0px; position:relative;" data-toggle="validator" role="form" method="post" action="oauth.php">
									<input type="hidden" name="action" value="addkey">
                                    <td></td>
									<td style="padding:0"><input type="text" name="name" class="form-control" style="margin:0px;border-radius:0" placeholder="Name"></td>
									<td style="padding:0"><input type="text" name="redirect_uri" class="form-control" style="margin:0px;border-radius:0" placeholder="Redirect Uri"></td>
									<td style="padding:0"><button class="btn btn-info btn-block" style="border-radius:0" type="submit">Add</button></td>
								</form>
								</tr>
							</table>
						</div>

						<div style="padding-top:10px;">
							<button onclick="saveChanges()" class="btn btn-success">Save Changes</button>
						</div>

					</div>
					<!-- END container-fluid -->

				</div>
				<!-- END content -->

			</div>
			<!-- END content-page -->

			<?php include NM_ROOT . "includes/footer.php";?>

		</div>
		<!-- END main -->

		<?php include NM_ROOT . "includes/script.php";?>

		<!-- BEGIN Java Script for this page -->

		<!-- END Java Script for this page -->

	</body>
</html>