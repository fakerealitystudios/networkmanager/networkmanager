<?php
require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == "addkey" && $NM->getAuthedMember()->hasPermission("nm.settings.edit")) {
        $NM->serverManager->create($_POST['name'], $_POST['game'], $_POST['address']);
    } elseif ($_POST['action'] == "removekey" && $NM->getAuthedMember()->hasPermission("nm.settings.edit")) {

    }
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>API Keys | Settings | NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<!-- END CSS for this page -->
	</head>

	<body class="adminbody">

		<div id="main">

			<?php include NM_ROOT . "includes/header.php";?>
			<?php include NM_ROOT . "includes/sidebar.php";?>

			<div class="content-page">

				<!-- Start content -->
				<div class="content">

					<div class="container-fluid">

						<div class="row">
							<div class="col-xl-12">
								<div class="breadcrumb-holder">
									<h1 class="main-title float-left">API Keys</h1>
									<ol class="breadcrumb float-right">
										<li class="breadcrumb-item">Home</li>
										<li class="breadcrumb-item active">Settings</li>
									</ol>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<!-- end row -->

                        <div class="card mb-3">
                            <div class="card-header">
                                <h3><i class="fa-solid fa-users"></i> API Keys</h3>
                            </div>

                            <div class="card-body">
                                <p>API Keys are fully authenticated servers with access to the panel API and other servers. Protect the secret as if it were your own password.</p>
                            </div>
                        </div>

						<div class="bg-white">
							<table id="keys" class="table table-responsive-xl table-hover display" style="margin:0;">
								<thead>
									<tr>
										<th>Server ID</th>
										<th>Name</th>
										<th>Address</th>
										<th>Realm</th>
										<th>Secret</th>
									</tr>
								</thead>
								<tbody>
								<?php
$condition = array();

$rows = $NM->db->select("servers", array("id", "name", "address", "game", "secret"));
foreach ($rows as $row) {
    ?>
										<tr data-id="<?php echo $row["id"]; ?>">
											<td contenteditable="false" data-name="sid"><?php echo $row["id"]; ?></td>
											<td contenteditable="false" data-name="name"><?php echo $row["name"]; ?></td>
											<td contenteditable="false" data-name="address"><?php echo $row["address"]; ?></td>
											<td contenteditable="false" data-name="game"><?php echo $row["game"]; ?></td>
											<td contenteditable="false" data-name="secret" id="<?php echo $row['id']; ?>"><a onclick="document.getElementById('<?php echo $row["id"]; ?>').innerHTML = '<?php echo $row["secret"]; ?>';return false;" href="#">Click here to Reveal</a></td>
										</tr>
										<?php
}
?>
								</tbody>
								<tr>
								<form class="input-group" style="padding:0px; position:relative;" data-toggle="validator" role="form" method="post" action="api.php">
									<input type="hidden" name="action" value="addkey">
                                    <td></td>
									<td style="padding:0"><input type="text" name="name" class="form-control" style="margin:0px;border-radius:0" placeholder="Name"></td>
									<td style="padding:0"><input type="text" name="address" class="form-control" style="margin:0px;border-radius:0" placeholder="Address"></td>
									<td style="padding:0"><input type="text" name="game" class="form-control" style="margin:0px;border-radius:0" placeholder="Realm"></td>
									<td style="padding:0"><button class="btn btn-info btn-block" style="border-radius:0" type="submit">Add</button></td>
								</form>
								</tr>
							</table>
						</div>

					</div>
					<!-- END container-fluid -->

				</div>
				<!-- END content -->

			</div>
			<!-- END content-page -->

			<?php include NM_ROOT . "includes/footer.php";?>

		</div>
		<!-- END main -->

		<?php include NM_ROOT . "includes/script.php";?>

		<!-- BEGIN Java Script for this page -->

		<!-- END Java Script for this page -->

	</body>
</html>

