<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if ($NM->getAuthedMember()->hasPermission("nm.settings.edit")) {
    // Very temp solution
    if (count($_POST) > 0) {
        $arr = array();
        $arr2 = array();
        foreach($_POST as $k => $v) {
            $k = str_replace("_", ".", $k);
            $arr[] = $k;
            $arr2[] = array("name" => $k, "value" => $v, "realm" => "*");
            $NM->settings[$k] = $v;
        }
        $NM->db->delete("settings", array("name" => $arr));
        $NM->db->insert("settings", $arr2);
    }
}

$data = array();
$data['title'] = "Authentication | Settings | NetworkManager";
$data['page_name'] = "Authentication";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Settings"
	),
	array(
		'name' => "Authentication"
	),
);

$data['body'] = "";
$data['settings'] = array();
$data['settings'][] = array(
    "name" => "steam.apikey",
    "value" => isset($NM->settings["steam.apikey"]) ? $NM->settings['steam.apikey'] : "",
    "title" => "Steam API Key",
    "type" => "text",
    "description" => "This is your Steam API Key, you can retreive one from <a href='https://steamcommunity.com/dev/apikey'>here</a>."
);
$data['settings'][] = array(
    "name" => "discord.clientid",
    "value" => isset($NM->settings["discord.clientid"]) ? $NM->settings['discord.clientid'] : "",
    "title" => "Discord Client Id",
    "type" => "text",
    "description" => "This is your Discord Client Id, you can retreive one from <a href='https://discord.com/developers/applications'>here</a>."
);
$data['settings'][] = array(
    "name" => "discord.secret",
    "value" => isset($NM->settings["discord.secret"]) ? $NM->settings['discord.secret'] : "",
    "title" => "Discord Secret Key",
    "type" => "text",
    "description" => "This is your Discord Secret Key, you can retreive one from <a href='https://discord.com/developers/applications'>here</a>."
);
$data['settings'][] = array(
    "name" => "twitch.clientid",
    "value" => isset($NM->settings["twitch.clientid"]) ? $NM->settings['twitch.clientid'] : "",
    "title" => "Twitch Client Id",
    "type" => "text",
    "description" => "This is your Twitch Client Id, you can retreive one from <a href='https://dev.twitch.tv/console'>here</a>."
);
$data['settings'][] = array(
    "name" => "twitch.secret",
    "value" => isset($NM->settings["twitch.secret"]) ? $NM->settings['twitch.secret'] : "",
    "title" => "Twitch Secret Key",
    "type" => "text",
    "description" => "This is your Twitch Secret Key, you can retreive one from <a href='https://dev.twitch.tv/console'>here</a>."
);
$data['body'] .= "<form class='form' method='post' action='authentication.php'>";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'settings.html', $data);
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'buttons/submit.html', $data);
$data['body'] .= "</form>";

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);