<?php
require "../includes/networkmanager.php";
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if (isset($_POST['action'])) {
    if ($_POST['action'] == "add" && $NM->getAuthedMember()->hasPermission("nm.settings.edit")) {
        $NM->db->insert("languages", array("lang" => $_POST["lang"], "event" => $_POST["event"], "args" => $_POST["args"], "format" => $_POST["format"]));
    } elseif ($_POST['action'] == "remove" && $NM->getAuthedMember()->hasPermission("nm.settings.edit")) {

    }
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Languages - NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<!-- END CSS for this page -->
	</head>

	<body class="adminbody">

		<div id="main">

			<?php include NM_ROOT . "includes/header.php";?>
			<?php include NM_ROOT . "includes/sidebar.php";?>

			<div class="content-page">

				<!-- Start content -->
				<div class="content">

					<div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="breadcrumb-holder">
                                    <h1 class="main-title float-left">Languages</h1>
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item">Home</li>
                                        <li class="breadcrumb-item active">Settings</li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="bg-white">
							<table id="settings" class="table table-responsive-xl table-hover display" style="margin:0;">
								<thead>
									<tr>
										<th>Language</th>
										<th>Args</th>
										<th>Event</th>
										<th>Format</th>
									</tr>
								</thead>
								<tbody>
								<?php
$condition = array();

$rows = $NM->db->select("languages", "*", array("ORDER" => ["lang", "event"]));
foreach ($rows as $row) {
    ?>
										<tr data-id="<?php echo $row["id"]; ?>">
											<td contenteditable="false" data-name="lang"><?php echo $row["lang"]; ?></td>
											<td contenteditable="false" data-name="args"><?php echo $row["args"]; ?></td>
											<td contenteditable="false" data-name="event"><?php echo $row["event"]; ?></td>
											<td contenteditable="true" data-name="format"><?php echo $row["format"]; ?></td>
										</tr>
										<?php
}
?>
								</tbody>
								<tr>
								<form class="input-group" style="padding:0px; position:relative;" data-toggle="validator" role="form" method="post" action="languages.php">
									<input type="hidden" name="action" value="add">
									<td style="padding:0"><input type="text" name="lang" class="form-control" style="margin:0px;border-radius:0" placeholder="Language"></td>
									<td style="padding:0"><input type="text" name="args" class="form-control" style="margin:0px;border-radius:0" placeholder="Arguments"></td>
									<td style="padding:0"><input type="text" name="event" class="form-control" style="margin:0px;border-radius:0" placeholder="Event"></td>
									<td style="padding:0"><input type="text" name="format" class="form-control" style="margin:0px;border-radius:0" placeholder="Format"></td>
									<td style="padding:0"><button class="btn btn-info btn-block" style="border-radius:0" type="submit">Add</button></td>
								</form>
								</tr>
							</table>
						</div>

						<div style="padding-top:10px;">
							<button onclick="saveChanges()" class="btn btn-success">Save Changes</button>
						</div>

					</div>
					<!-- END container-fluid -->

				</div>
				<!-- END content -->

			</div>
			<!-- END content-page -->

			<?php include NM_ROOT . "includes/footer.php";?>

		</div>
		<!-- END main -->

		<?php include NM_ROOT . "includes/script.php";?>

		<!-- BEGIN Java Script for this page -->
		<!-- END Java Script for this page -->

	</body>
</html>
