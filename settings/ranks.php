<?php
require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if (isset($_POST['rank'])) {
    $rank = $NM->getRank($_POST['rank']);
    if (!$rank || !$rank->isValid()) {
        unset($rank);
    }
}

if (isset($_POST['action'])) {
	if ($_POST['action'] == "createrank" && $NM->getAuthedMember()->hasPermission("nm.rank.create", $_POST['realm'])) {
		$NM->rankManager->createRank($_POST['displayName'], strtolower($_POST['rank']), "", $_POST['realm'], $_POST['subgrouping']);
	} else {
        // TODO: Error notification
    }
	if (isset($rank)) {
		if ($NM->getAuthedMember()->hasPermission("nm.rank.permissions", $_POST['realm'])) {
			if ($_POST['action'] == "addperm") {
				$rank->givePermission($_POST['perm'], $_POST['realm']);
			} elseif ($_POST['action'] == "removeperm") {
				$rank->takePermission($_POST['perm'], $_POST['realm']);
			}
		} else {
			// No permissions(we assume)
		}
		unset($rank);
	}
}

$data = array();
$data['title'] = "Ranks | NetworkManager";
$data['page_name'] = "Ranks";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Settings"
	),
	array(
		'name' => "Ranks"
	),
);

$data['subgroupings'] = array();
foreach ($NM->getRanksLeveled() as $row) {
	if (!isset($data['subgroupings'][$row->getSubGrouping()])) {
		$data['subgroupings'][$row->getSubGrouping()] = array();
		$data['subgroupings'][$row->getSubGrouping()]['displayName'] = "subgroup ".$row->getSubGrouping();
		$data['subgroupings'][$row->getSubGrouping()]['ranks'] = array();
	}
	asort($row->perms);
	$data['subgroupings'][$row->getSubGrouping()]['ranks'][] = array(
		'displayName' => $row->getDisplayName(),
		'realm' => $NM->getReadableRealm($row->getRankRealm()),
		'rank' => $row->getRank(),
		'inherit' => ($row->getParent() != "" && $NM->getRank($row->getParent())) ? $NM->getRank($row->getParent())->getDisplayName() : "",
		'permissions' => $row->perms,
	);
}
ksort($data['subgroupings']);

$data['body'] = "";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'settings/ranks.html', $data);

$data['modals'] = "";
$data['modals'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'settings/rankModal.html', $data);

$data['javascript'] = '<script>';
$data['javascript'] .= 'var permissions = [';
foreach ($NM->db->select("permissions", "perm") as $perm) {
	$data['javascript'] .= '"'.$perm.'",';
}
$data['javascript'] .= '];';
$data['javascript'] .= '
var inps = document.getElementsByClassName("permissions");
for (var i = 0; i < inps.length; i++) {
	autocomplete(inps[i], permissions);
};

function updateHierarchy(ul) {
	var lis = Array.from(ul.children).slice(1);
	var subgrouping = ul.id.replace("subgroup-", "");
	var ranks = {};
	for (var i = 0; i < lis.length; i++) {
		if (i > 0) {
			ranks[lis[i].id] = lis[i-1].id;
		} else {
			ranks[lis[i].id] = "";
		}
	}
	var post = {ranks: ranks, subgrouping: subgrouping};
	ajax(null, "ranks-update", post, function(status, data) {
		var notify = $.notify({message: "The ranks inheritance has been updated!"}, { type: "success", allow_dismiss: true });
	});
};

$(\'.list-group-sortable-ranks\').sortable({
	placeholderClass: \'list-group-item\',
	connectWith: \'.connected\',
	items: \':not(.disabled)\'
}).bind(\'sortupdate\', function(e, oldparent, ui) {
	if (oldparent != e.currentTarget) {
		updateHierarchy(oldparent);
	}
	updateHierarchy(e.currentTarget);
});
</script>';

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);
/*
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>NetworkManager</title>
		<meta name="description" content="NetworkManager">
		<meta name="author" content="NetworkManager">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">

		<?php include NM_ROOT . "includes/style.php";?>

		<!-- BEGIN CSS for this page -->
		<!-- END CSS for this page -->
	</head>

	<body class="adminbody">

		<div id="main">

			<?php include NM_ROOT . "includes/header.php";?>
			<?php include NM_ROOT . "includes/sidebar.php";?>

			<div class="content-page">

				<!-- Start content -->
				<div class="content">

					<div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="breadcrumb-holder">
                                    <h1 class="main-title float-left">Ranks</h1>
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item">Home</li>
                                        <li class="breadcrumb-item active">Settings</li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-12 col-xl-12">
								<div class="card mb-3">
									<div class="card-header bg-dark text-white">
										<h3><i class="fa-solid fa-users"></i> Ranks</h3>
									</div>

									<div class="card-body p-0">

										<table id="Ranks" class="table table-striped table-bordered table-responsive-xl table-hover display mb-0">
											<thead>
												<tr>
													<th>Rank</th>
													<th>Display Name</th>
													<th>Inherits</th>
													<th>Realm</th>
													<th>Subgrouping</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>

											<?php
foreach ($NM->getRanks() as $row) {
    $displayName = "";
    if ($NM->getRank($row->getParent()) != null) {
        $displayName = $NM->getRank($row->getParent())->getDisplayName();
    }
    ?>
													<tr>
														<td><a href="<?php echo NM_URL; ?>settings/ranks.php?rank=<?php echo $row->getRank(); ?>"><?php echo $row->getRank(); ?></a></td>
														<td><?php echo $row->getDisplayName(); ?></td>
														<td><?php echo $displayName; ?></td>
														<td><?php echo $NM->getReadableRealm($row->getRankRealm()); ?></td>
														<td><?php echo $row->getSubGrouping(); ?></td>
														<td></td>
													</tr>
													<?php
}
?>
											<tr>
												<form class="input-group" style="padding:0px; position:relative;" data-toggle="validator" role="form" method="get" action="player.php">
													<input type="hidden" name="action" value="createrank">
													<td style="padding:0"><input type="text" name="rank" class="form-control" style="margin:0px;border-radius:0" placeholder="Rank Name"></td>
													<td style="padding:0"><input type="text" name="displayName" class="form-control" style="margin:0px;border-radius:0" placeholder="Display Name"></td>
													<td style="padding:0"><select class="custom-select" name="inherits">
														<option value="" selected></option><?php
foreach ($NM->getRanks() as $row) {
    ?>
															<option value="<?php echo $row->getRank(); ?>"><?php echo $row->getDisplayName(); ?></option>
															<?php
}
?></select></td>
													<td style="padding:0"><select class="custom-select" name="realm" required="required"><?php
foreach ($NM->getRealms() as $realm => $title) {
    ?>
															<option value="<?php echo $realm; ?>"><?php echo $title; ?></option>
															<?php
}
?></select></td>
													<td style="padding:0"><input type="number" name="subgrouping" class="form-control" style="margin:0px;border-radius:0" placeholder="Subgrouping"></td>
													<td style="padding:0"><button class="btn btn-info btn-block" style="border-radius:0" type="submit">Add</button></td>
												</form>
											</tr>
											</tbody>
										</table>

									</div>
								</div><!-- end card-->
							</div>
						</div>
                        <!-- end row -->

						<?php if (isset($rank)) {?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="card mb-3">
									<div class="card-header bg-dark text-white">
										<h3><i class="fa-solid fa-users"></i> Permissions - <?php echo $rank->getDisplayName(); ?></h3>
									</div>
									<div class="card-body p-0">
										<table class="table table-striped table-bordered table-responsive-xl table-hover table-sm display mb-0 flex-fill">
											<thead>
												<tr>
													<th>Permission</th>
													<th>Realm</th>
													<th style="padding:0;width:30px;"></th>
												</tr>
											</thead>
											<tbody id="perms">
											<?php
asort($rank->perms);
    foreach ($rank->perms as $row) {
        ?>
												<tr>
													<td><?php echo $row['perm']; ?></td>
													<td><?php echo $NM->getReadableRealm($row['realm']); ?></td>
													<td style="padding:0"><a class="btn btn-danger btn-block" style="border-radius:0" href="ranks.php?rank=<?php echo $rank->getRank(); ?>&action=removeperm&perm=<?php echo $row['perm']; ?>&realm=<?php echo $row['realm']; ?>"><span aria-hidden="true">&times;</span></a></td>
												</tr>
												<?php
}
    ?>
											<tr>
												<form class="input-group" style="padding:0px; position:relative;" data-toggle="validator" role="form" method="get" action="ranks.php">
													<input type="hidden" name="rank" value="<?php echo $rank->getRank(); ?>">
													<input type="hidden" name="action" value="addperm">
													<td style="padding:0"><input type="text" name="perm" class="form-control" style="margin:0px;border-radius:0" placeholder="Permission"></td>
                                                    <td style="padding:0"><select class="custom-select" name="realm" required="required"><?php
foreach ($NM->getRealms() as $realm => $title) {
        ?>
															<option value="<?php echo $realm; ?>"><?php echo $title; ?></option>
															<?php
}
    ?></select></td>
													<td style="padding:0"><button class="btn btn-info btn-block" style="border-radius:0" type="submit">Add</button></td>
												</form>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php }?>

					</div>
					<!-- END container-fluid -->

				</div>
				<!-- END content -->

			</div>
			<!-- END content-page -->

			<?php include NM_ROOT . "includes/footer.php";?>

		</div>
		<!-- END main -->

		<?php include NM_ROOT . "includes/script.php";?>

		<!-- BEGIN Java Script for this page -->
		<!-- END Java Script for this page -->

	</body>
</html>
*/