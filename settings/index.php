<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

$data = array();
$data['title'] = "General | Settings | NetworkManager";
$data['page_name'] = "General";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Settings"
	),
	array(
		'name' => "General"
	),
);

$data['body'] = "";
$data['settings'] = array();
$data['settings'][] = array(
    "name" => "language",
    "value" => isset($NM->settings["language"]) ? $NM->settings['language'] : "",
    "title" => "Lanaguage",
    //"type" => "text",
    "description" => "NetworkManager Language."
);
$data['settings'][] = array(
    "name" => "site",
    "value" => isset($NM->settings["site"]) ? $NM->settings['site'] : "",
    "title" => "Site",
    "type" => "text",
    "description" => "Site URL including scheme."
);
$data['settings'][] = array(
    "name" => "instance.id",
    "value" => isset($NM->settings["instance.id"]) ? $NM->settings['instance.id'] : "",
    "title" => "Instance ID",
    "type" => "text",
    "description" => "NetworkManager Center ID."
);
$data['settings'][] = array(
    "name" => "instance.apikey",
    "value" => isset($NM->settings["instance.apikey"]) ? $NM->settings['instance.apikey'] : "",
    "title" => "Instance API Key",
    "type" => "text",
    "description" => "NetworkManager Center API Key."
);
$data['body'] .= "<form class='form' method='post' action='index.php'>";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'settings.html', $data);
$data['body'] .= "</form>";

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);