<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.web.view.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if ($NM->getAuthedMember()->hasPermission("nm.settings.edit")) {
    // Very temp solution
    if (count($_POST) > 0) {
        $arr = array();
        $arr2 = array();
        foreach($_POST as $k => $v) {
            $k = str_replace("_", ".", $k);
            $arr[] = $k;
            $arr2[] = array("name" => $k, "value" => $v, "realm" => "*");
            $NM->settings[$k] = $v;
        }
        $NM->db->delete("settings", array("name" => $arr));
        $NM->db->insert("settings", $arr2);
    }
}

$data = array();
$data['title'] = "Email | Settings | NetworkManager";
$data['page_name'] = "Email";
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Settings"
	),
	array(
		'name' => "Email"
	),
);

$data['body'] = "";
$data['settings'] = array();
$data['settings'][] = array(
    "name" => "email.method",
    "value" => array(
		"smtp" => "SMTP",
		"php" => "PHP"
	),
    "title" => "Email Method",
    "type" => "select",
    "description" => "Method to use to send emails from NetworkManager. Use PHP if unsure."
);
$data['settings'][] = array(
    "name" => "email.outgoing",
    "value" => isset($NM->settings["email.outgoing"]) ? $NM->settings['email.outgoing'] : "",
    "title" => "Outgoing Email Address",
    "type" => "email",
    "description" => "Email address to send email as."
);
$data['settings'][] = array(
    "name" => "smtp.host",
    "value" => isset($NM->settings["smtp.host"]) ? $NM->settings['smtp.host'] : "",
    "title" => "SMTP Host",
    "type" => "text",
    "description" => "Host of the SMTP server."
);
$data['settings'][] = array(
    "name" => "smtp.port",
    "value" => isset($NM->settings["smtp.port"]) ? $NM->settings['smtp.port'] : "",
    "title" => "SMTP Port",
    "type" => "number",
    "description" => "Port for the SMTP Host."
);
$data['settings'][] = array(
    "name" => "smtp.protocol",
    "value" => array(
		"plain" => "Plaintext",
		"tls" => "TLS",
		"ssl" => "SSL",
	),
    "title" => "SMTP Protocol",
    "type" => "select",
    "description" => "Protocol used to connect to the SMTP host."
);
$data['settings'][] = array(
    "name" => "smtp.username",
    "value" => isset($NM->settings["smtp.username"]) ? $NM->settings['smtp.username'] : "",
    "title" => "SMTP Username",
    "type" => "text",
    "description" => "If your SMTP host requires authentication, provide the username and password."
);
$data['settings'][] = array(
    "name" => "smtp.password",
    "value" => isset($NM->settings["smtp.password"]) ? "*************" : "", // We don't expose the password
    "title" => "SMTP Password",
    "type" => "password",
    "description" => "If your SMTP host requires authentication, provide the username and password."
);
$data['body'] .= "<form class='form' method='post' action='email.php'>";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'settings.html', $data);
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'buttons/submit.html', $data);
$data['body'] .= "</form>";

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);