<?php

require "../includes/networkmanager.php";
$auth = \NetworkManager\Authentication::getInstance();
if (!$auth->isLoggedIn()) {
    header('Location: ' . NM_URL . 'login.php');
    exit();
}

if (!$NM->getAuthedMember()->hasPermission("nm.server.settings")) {
    // Not permitted to view!
    header('Location: ' . NM_URL . 'blocked.php');
    exit();
}

if (!isset($_GET["sid"])) {
    header('Location: ' . NM_URL);
    exit();
}
$server = $NM->getServer($_GET["sid"]);
if (!$server->isValid()) {
    header('Location: ' . NM_URL);
    exit();
}

if (isset($_POST['advertisements'])) {
    preg_match_all("/(?:<span style=\"color: rgb\((.*?)\);\">(.*?)<\/span>|<p>|<\/p>|([^<]*))/", $_POST['advertisements'], $matches);
    $advertisement = array();
    foreach ($matches[1] as $k => $v) {
        if ($matches[2][$k] != "") {
            $advertisement[] = array(
                "color" => explode(", ", $v, 3),
                "text" => $matches[2][$k],
            );
        } elseif($matches[3][$k] != "") {
            $advertisement[] = array(
                "color" => array(0, 0, 0),
                "text" => $matches[3][$k],
            );
        }
    }
}
/*
$advertisements = array();
$advertisements[] = array(
    "parts" => $advertisement,
    "enabled" => false,
    "interval" => 5
);
$advertisements[] = array(
    "parts" => $advertisement,
    "enabled" => false,
    "interval" => 5
);
$advertisements[] = array(
    "parts" => $advertisement,
    "enabled" => true,
    "interval" => 3
);
$advertisements[] = array(
    "parts" => $advertisement,
    "enabled" => true,
    "interval" => 5
);
$advertisements[] = array(
    "parts" => $advertisement,
    "enabled" => true,
    "interval" => 5
);
*/
$data = array();
$data['title'] = $server->getName()." | Settings | NetworkManager";
$data['page_name'] = $server->getName();
$data['bread'] = array(
	array(
		'name' => "Home"
	),
	array(
		'name' => "Settings"
	),
	array(
		'name' => $server->getName()
	),
);

$data['style'] = "<style>
.note-editable {
    background-color: #707070 !important;
}
</style>";

$rows = $NM->db->select("settings", array("name", "value"), array("realm[~]" => $server->getRealm()));
$settings = array();
foreach($rows as $row) {
	$settings[$row["name"]] = $row["value"];
}
$advertisements = json_decode(isset($settings['advertisements']) ? $settings['advertisements'] : "[]", true);

$data['body'] = "";
$data['ajax'] = "ajax.php?sid=".$server->getId();
$data['settings'] = array();
$data['settings'][] = array(
    "name" => "whitelisted",
    "value" => (isset($settings["whitelisted"]) && $settings['whitelisted'] == "true") ? true : false,
    "title" => "Server Whitelisted",
    "type" => "checkboxDynamic",
    "description" => "Enable or disable server whitelist. With a whitelist, only players with the nm.whitelist permission in the realm can join."
);
$data['settings'][] = array(
    "name" => "advertisements",
    "value" => $advertisements,
    "title" => "Advertisements",
    "type" => "advertisements",
    "description" => "Enable or disable server whitelist. With a whitelist, only players with the nm.whitelist permission in the realm can join."
);
if ($server->getGame() == "garrysmod") {
	$data['settings'][] = array(
		"name" => "http_whitelist_enabled",
		"value" => (isset($settings["http_whitelist_enabled"]) && $settings['http_whitelist_enabled'] == "true") ? true : false,
		"title" => "HTTP Whitelist",
		"type" => "checkboxDynamic",
		"description" => "Enable or disable http whitelist. Whitelist can help stop illicit addons from phoning home."
	);
	$settings["http_whitelist"] = json_decode(isset($settings['http_whitelist']) ? $settings['http_whitelist'] : "[]", true);
	$data['settings'][] = array(
		"name" => "http_whitelist",
		"value" => $settings["http_whitelist"],
		"title" => "HTTP Whitelist",
		"type" => "listDynamic",
		"description" => "List of http requests permitted from the server."
	);
}
$data['body'] .= "<form class='form ajax' method='post' action='".$data['ajax']."'>";
$data['body'] .= $NM->template->build(NM_TEMPLATE_ROOT . 'settings.html', $data);
$data['body'] .= "</form>";

$data['javascript'] = "<script type='text/javascript'>$('#summernote-advertisements').summernote({
	toolbar: [
		['color', ['forecolor']],
	],
	minHeight: 150,
});
$('#summernote-advertisements').summernote('foreColor', 'rgb(255,255,255)');
var advertid = ".count($advertisements).";
function addAdvertisement(e) {
    e.preventDefault();
    console.log($('#summernote-advertisements').summernote('code'));
}</script>";

$NM->template->prepareMainIncludes();
echo $NM->template->build(NM_TEMPLATE_ROOT . 'main.html', $data);